﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Text;
using ExCSS;
using FluentAssertions;
using HtmlAgilityPack;
using NUnit.Framework;
using SlaterAndGordon.ContentConverter.SectionMappers;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.Tests.SectionMappers
{
    [TestFixture]
    public class WysiwygMapperTests
    {
        [Test]
        public void WhenParagraphsMarkedWithH2FAQExist_ThenTheyGetMappedAsH2Element()
        {
            var section = new Section();
            section.Contents.Add(HtmlNode.CreateNode("<p>[H2FAQ]This is the heading text!</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 1</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 2</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 3</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>[H2FAQ]This is the heading text 2!</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 4</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 5</p>"));
            var emptyStyleSheet = new StylesheetParser().Parse("");

            var wysiwygMapper = new WysiwygMapper();

            var mappedWysiwyg = wysiwygMapper.MapWysiwyg(section, emptyStyleSheet);

            Assert.That((mappedWysiwyg.TopElements[0] as H2Element)?.Text, Is.EqualTo("This is the heading text!"));
            Assert.That((mappedWysiwyg.TopElements[4] as H2Element)?.Text, Is.EqualTo("This is the heading text 2!"));
        }

        [Test]
        public void WhenH2sGetMappedAsH2Elements_ThenIsFAQHeadingPropertySet()
        {
            var section = new Section();
            section.Contents.Add(HtmlNode.CreateNode("<p>[H2FAQ]This is the heading text!</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 1</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 2</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 3</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>[H2FAQ]This is the heading text 2!</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 4</p>"));
            section.Contents.Add(HtmlNode.CreateNode("<p>Paragraph 5</p>"));
            var emptyStyleSheet = new StylesheetParser().Parse("");

            var wysiwygMapper = new WysiwygMapper();

            var mappedWysiwyg = wysiwygMapper.MapWysiwyg(section, emptyStyleSheet);

            Assert.That((mappedWysiwyg.TopElements[0] as H2Element)?.IsFaqQuestion, Is.True);
            Assert.That((mappedWysiwyg.TopElements[4] as H2Element)?.IsFaqQuestion, Is.True);
        }

        [Test]
        public void WhenH3ElementsAreIncludedInSource_ThenTheyGetMappedAsH3Properties()
        {
            var section = new Section();
            section.Contents.Add(HtmlNode.CreateNode("<p>[H3]This is a h3 heading!</p>"));
            var emptyStyleSheet = new StylesheetParser().Parse("");

            var wysiwygMapper = new WysiwygMapper();

            var mappedWysiwyg = wysiwygMapper.MapWysiwyg(section, emptyStyleSheet);

            Assert.That(mappedWysiwyg.TopElements[0] is H3Element, Is.True);
        }

        [Test]
        public void WhenMultiLevelListsAreIncluded_ThenTheyGetMappedAsMultiLevelLists()
        {
            var section = new Section();
            section.Contents.Add(HtmlNode.CreateNode("<ul><li>hello<ul><li>hello, but level 2</li></ul></li></ul>"));
            var emptyStyleSheet = new StylesheetParser().Parse("");

            var wysiwygMapper = new WysiwygMapper();

            var mappedWysiwyg = wysiwygMapper.MapWysiwyg(section, emptyStyleSheet);

            var list = mappedWysiwyg.TopElements[0] as ListHtmlElement;
            var firstListEntry = list.ListElements[0][0] as PElement;
            (firstListEntry.Children[0] as TextElement).Text.Should().Be("hello");

            var secondListEntry = list.ListElements[0][1] as ListHtmlElement;

            var embeddedListChild = secondListEntry.ListElements[0][0] as PElement;
            (embeddedListChild.Children[0] as TextElement).Text.Should().Be("hello, but level 2");
        }

    }
}
