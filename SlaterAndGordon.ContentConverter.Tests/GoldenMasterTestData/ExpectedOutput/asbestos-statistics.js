module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestos-statistics`,
		slug: `asbestos-statistics`,
		parent: `asbestos-mesothelioma`,
		name: `Asbestos Statistics & Facts`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Asbestos Statistics & Facts | Slater + Gordon`,
				description: `The use and importation of asbestos has been banned in the UK for 20 years, however the substance to this day has a massive effect on the lives of its victims and their families. Here are some key facts about the material.`,
				robots: ``,
                schema: [
                  {
                    question: "What is asbestos?",
                    answer: `<p>Asbestos is a mineral that has been mined for hundreds of years and was used due to its fireproof qualities. There are three common types of asbestos which are white (chrysotile), brown (amosite) and blue (crocidolite).</p>`
                  },
                  {
                    question: "When was it banned in the UK?",
                    answer: `<p>In 1985 blue (crocidolite) and brown (amosite) asbestos were banned in the UK as it was believed these caused more harm than other forms of asbestos. In <a href=\"http://www.legislation.gov.uk/uksi/1992/3067/made/data.html\">1992 </a>the regulations were updated and placed some restrictions on the use of white (chrysotile) asbestos. Finally, in 1999 the importation and use of any asbestos was banned in the UK. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/\">Click here for more on the history of asbestos</a>.</p>`
                  },
                  {
                    question: "Is it banned world wide?",
                    answer: `<p>Unfortunately not. More than 50 countries have banned the use of asbestos including the UK, Australia, Canada and all of the member states of the European Union. Some other countries however simply have strong restrictions and regulations in place, but allow its use in certain cases or certain forms. Some countries regard white asbestos as being safer than its other forms and have less restrictions on it. </p><p>The United States of America is one country that doesn't ban the importation and use of asbestos and it can be found in many American products such as gaskets, roofing products, fireproof clothing and brake pads. However, there are strict regulations on its use in place.</p><p>In 2013 it was estimated that Russia was the world's leading exporter, having exported over 600,000 metric tons of asbestos whilst Kazakhstan and Brazil were the two largest importers.</p>`
                  },
                  {
                    question: "Deaths from asbestos related diseases",
                    answer: `<p>Data released by the Health and Safety Executive shows that asbestos related deaths have been growing steadily since the 1980's. And even though the use and importation of the substance was fully banned in 1999, mesothelioma deaths are currently peaking at an all time high.</p><p>This is because the disease can take many years to develop meaning that today's sufferers may have not been in contact with any asbestos fibres in over 40 years.</p><p>Since mesothelioma is so strongly related to asbestos, those who died as a result give a very strong indication of the number of people dying due to asbestos related diseases.</p><p>While new cases of exposure to asbestos fibres are fairly rare, it's believed we're very near the peak in asbestos related deaths per year and expect to start to see a decline in the coming decade.</p><p>The Health and Safety Executive estimates:</p><ul><li>There are over 5,000 asbestos related deaths per year currently, this includes mesothelioma, lung cancer and asbestosis.</li><li>In 2016 there were 2,595 mesothelioma deaths, the HSE suspects there were a similar number of lung cancer deaths linked to asbestos exposure.</li><li>In 2016 there were a reported 500 asbestosis deaths.</li></ul><p><a href=\"http://www.hse.gov.uk/statistics/causdis/asbestos-related-disease.pdf\">Click here to view the full report.</a></p><p>A separate report, again by the Health and Safety Executive shows that the vast majority of mesothelioma deaths are male, but the number of deaths of both sexes have been steadily increasing since the late 1960's. A loose approximation suggests that the male, female split is 80/20.</p><p><a href=\"http://www.hse.gov.uk/statistics/causdis/mesothelioma/mesoarea1981to2015.pdf\">Click here to view the full report.</a></p><p>In the UK the vast majority of cases reported are either in the North of England or along the Southern coastline of England. The NHS Trusts that have recorded the highest mesothelioma rates include Portsmouth, Plymouth, Southampton, Sheffield, East Yorkshire, London, Leicester and Northumbria.</p><p><a href=\"https://statistics.blf.org.uk/mesothelioma\">According to the British Lung Foundation</a>, the majority of people diagnosed with mesothelioma are over 70, over 20% are aged 51-60 and very few are diagnosed under the age of 50. It's estimated that over 85% of mesothelioma deaths occur to the over 65's.</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/\">Click here to find out more around who may be at risk</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestos facts and statistics`,
				copy: `The use and importation of asbestos has been banned in the UK for 20 years, however the substance to this day has a massive effect on the life of its victims and their families. Here are some key facts about the material.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/asbestos-statistics-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/asbestos-statistics`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}