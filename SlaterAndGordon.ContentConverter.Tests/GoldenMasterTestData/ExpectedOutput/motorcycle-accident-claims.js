module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `motorcycle-accident-claims`,
		slug: `motorcycle-accident-claims`,
		parent: `road-traffic-accidents`,
		name: `Motorcycle Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Motorcycle Accident Compensation Claims Solicitors | Slater + Gordon`,
				description: `If you've been injured while riding a motorcycle, make sure you get the compensation you deserve. Talk to us today about starting a No Win No Fee claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How long do I have to claim for motorcycle accident injuries?",
                    answer: `<p>The law states that you have three years in which to begin your claim, though you may have longer if you're claiming on behalf of a child, someone of diminished mental capacity. Time limits may vary if you claim through the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">Criminal Injuries Compensation Authority</a>.</p>`
                  },
                  {
                    question: "Can I claim if the accident was partly my fault?",
                    answer: `<p>Not every motorcycle accident is caused entirely through the inattention of drivers. Even if you believe that you may have been partly responsible for an accident, you should still talk to us to establish the full legal position. Even where you are partly to blame, you may still be able to receive compensation.</p>`
                  },
                  {
                    question: "How long does a motorcycle injury claim take?",
                    answer: `<p>As you might expect, this depends largely on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">how serious your injuries are</a>, and whether the responsible party's insurer accepts liability. However, if your injuries are considered minor, and your case relatively straightforward, we handle every <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee</a> motorcycle accident claim through our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/fast-track/\">fast track team</a> to help you receive the compensation you deserve as quickly as possible.</p>`
                  },
                  {
                    question: "How much compensation can I claim?",
                    answer: `<p>Obviously, the level of compensation you might expect will depend on the seriousness of your injuries. You should also consider claiming for related losses, such as general damages, travel expenses and even loss of earnings. If necessary, we can also claim on your behalf for medical expenses or any essential <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Motorcycle accident compensation claims`,
				copy: `It's all too common for motorcyclists to be injured due to inattention and carelessness of other road users, and for many of those injuries to be serious. So if you've been injured in a motorcycle accident, you may be entitled to compensation. The majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/motorcycle-accident-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee motorcycle injury compensation`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/motorcycle-accident-claims`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}