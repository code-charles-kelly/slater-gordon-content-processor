module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `restaurant-guidelines`,
		slug: `restaurant-guidelines`,
		parent: `food-poisoning`,
		name: `Allergen Guidelines for Restaurants | Slater and Gordon`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Allergen Guidelines for Restaurants | Slater and Gordon`,
				description: `If you suffer from food allergies you will be fully aware of how dangerous these can be. New rules and regulations were brought in December 2014 to make sure staff training and food labelling is up to scratch.`,
				robots: ``,
                schema: [
                  {
                    question: "What are the laws surrounding allergies when it comes to restaurants and cafes?",
                    answer: `<p>As of December 2014 restaurants new food allergen labelling and information requirements came into force. These new rules require all businesses serving food to make sure that customers are provided with information about the use of allergenic ingredients, or alternatively, to direct a customer to where that information can be found.</p><p>Before this, in many restaurants you were required to ask staff for further information. However, if they were not trained correctly, it could lead to serious life threatening or sometimes fatal consequences.</p>`
                  },
                  {
                    question: "Are all eateries, even small back street cafes obliged to abide by regulations when it comes to protecting customers who have allergy problems?",
                    answer: `<p>Yes. What you tend to see in most establishments that serve food, whether large or small, is a direction to the customer, to ask a member of staff.  It's obviously therefore essential that all staff members have full training and understanding of the items on the menu or of what is being sold and what allergenic ingredients they contain. </p>`
                  },
                  {
                    question: "What food items does the new law cover?",
                    answer: `<p>According to <a href=\"https://www.food.gov.uk/business-guidance/allergen-labelling-for-food-manufacturers\">the food standards agency</a> there is a list of 14 foods that are required to be correctly labelled, these are:</p><ul><li>Celery</li><li>Cereals containing gluten</li><li>Crustaceans – e.g. prawns, crabs and lobsters</li><li>Eggs</li><li>Fish</li><li>Lupin (a legume belonging to the same family of plants as peanuts).</li><li>Milk</li><li>Molluscs – e.g. mussels and oysters</li><li>Mustard</li><li>Tree nuts – e.g. almonds, hazelnuts, walnuts, brazil nuts, cashews, pecans, pistachios and macadamia nuts</li><li>Peanuts</li><li>Sesame seeds</li><li>Soybeans</li><li>Sulphur dioxide and sulphites (when there is more than a certain amount)</li></ul><p>There are also rules around <a href=\"https://www.food.gov.uk/business-guidance/allergen-guidance-for-food-businesses#pre-packed-and-non-prepacked-foods\">package labelling</a> and how clear this needs to be in terms of font size and there are directions on the use of clear, plain english so it is easy to understand and not misleading.</p>`
                  },
                  {
                    question: "What should I do if I'm eating out and have food allergies?",
                    answer: `<p>Make it clear to the restaurant staff that you have allergies. Also, do not be afraid to ask questions. Ask the waiting staff if they've had training on food allergies and cross-contamination. Also ask if you can speak to the chef or manager about meal options. Ask about the dishes, whether they're made in-house and if not, ask if the food labels are available to read yourself. Whatever you're unsure about, be confident enough to speak up.</p><p>If you've suffered a reaction to a meal in a restaurant and you believe they are to blame, what can you do?</p><p>If you've suffered an allergic reaction after eating out, make sure you keep the receipt for your purchase if you have one. If your allergic reaction has resolved relatively quickly and you just want to make sure the establishment knows about it, so they can do something about it or improve their training to make sure it doesn't happen again, then give them a call.</p><p>If you wish to take it further, you can. You must be told about potential allergenic ingredients in the food you are being sold. If you've explained clearly to the food establishment what your allergies are, but you've still been served contaminated food, then on the face of it, something has gone badly wrong and you may be able to claim compensation, depending on what injuries, if any, you've suffered as a result.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Allergen guidelines for restaurants and other food establishments`,
				copy: `If you suffer from food allergies you will be fully aware of how dangerous these can be. New rules and regulations were brought in December 2014 to make sure staff training and food labelling is up to scratch.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/restaurant-guidelines-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/restaurant-guidelines`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}