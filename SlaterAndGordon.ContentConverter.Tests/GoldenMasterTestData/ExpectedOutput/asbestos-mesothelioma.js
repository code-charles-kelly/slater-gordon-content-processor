module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestos-mesothelioma`,
		slug: `asbestos-mesothelioma`,
		parent: `industrial-disease`,
		name: `Asbestos & Mesothelioma`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Asbestos & Mesothelioma Compensation Claims | Slater + Gordon`,
				description: `Mesothelioma, asbestos related lung cancer, asbestosis and pleural thickening can have devastating effects on sufferers and their families lives. Slater and Gordon are one of the UK's leading asbestos and mesothelioma compensation claim specialists.`,
				robots: ``,
                schema: [
                  {
                    question: "What is asbestos?",
                    answer: `<p>Asbestos is a mineral that has been mined for hundreds of years and was used due to its fireproof qualities. There are three common types of asbestos which are white (chrysotile), brown (amosite) and blue (crocidolite).</p>`
                  },
                  {
                    question: "Where might I have come across asbestos?",
                    answer: `<p>It's often difficult to identify where someone might have been exposed to asbestos as it was once used everywhere, and was widely used in the UK during the 1930s through to the 1970s. It was used at home, in schools and in other public places. </p><p>The majority of people who go on to develop an asbestos related illness have been exposed to asbestos many years ago when they were working as engineers, plumbers, joiners, laggers, dockyard workers etc. </p><p>Exposure to asbestos dust occurs when a product containing asbestos is disturbed and asbestos fibres are released into the air. Someone exposed to asbestos may not have disturbed the asbestos dust themselves, but may have been close-by someone, or washed the clothes of someone who did.</p><p>Read our page <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/\">Am I at Risk</a> for more information about where you might have come across asbestos. </p>`
                  },
                  {
                    question: "What asbestos related diseases can you get from exposure to asbestos?",
                    answer: `<p>There are a number of illnesses which you can get from exposure to asbestos, when you inhale asbestos fibres. You can get full details of these illnesses, their symptoms and available treatment, by clicking on the links below:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/mesothelioma/\">Mesothelioma </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-lung-cancer\">Asbestos related lung cancer </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestosis/\">Asbestosis </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/pleural-thickening/\">Pleural thickening</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/scottish-pleural-plaques/\">Pleural plaques</a> </li></ul>`
                  },
                  {
                    question: "What help is available if you've been diagnosed with an asbestos related illness?",
                    answer: `<p>Being diagnosed with an asbestos related disease can be extremely distressing, for the person diagnosed and their family. However, there is help available:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/\">Charities and support organisations</a> - When first diagnosed with an asbestos related disease, it's often difficult to know what to do. There are a number of helpful support groups and charitable organisations who offer a range of advice and assistance to those who have been diagnosed. Click the link for detailed information.</li></ul><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/benefits\">Benefits and one-off payments</a> - When diagnosed with an asbestos related illness, you can often worry about how it will affect your finances. There are a number of benefits and one-off payments which are available for those suffering with an asbestos related condition. Click the link for detailed information.</li></ul><ul><li><a href=\"https://www.slatergordon.co.uk/contact-us/\">Compensation</a> - It's only right that if you have been diagnosed with an asbestos related illness that is down to someone else's negligence, that you're compensated for your pain and suffering. It also allows you to feel a sense of justice and gives you peace of mind that your family will not suffer any financial hardship. Our asbestos specialists have many years' experience dealing with asbestos related illnesses and are on hand to provide you with the best available legal representation. For your right to compensation, click the link to contact our experts now. </li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/turner-newall/\">Turner Newall</a> - This is a specific scheme that allows you to claim if you were exposed to asbestos as a result of Turner Newall, a large manufacturing company formerly based in Manchester. There may be other schemes that cover other companies, so do not hesitate to get in touch with questions.</li></ul>`
                  },
                  {
                    question: "How much does it cost to make a claim for compensation?",
                    answer: `<p>Slater and Gordon offer all our clients with an asbestos related illness funding on a No Win No Fee basis. What this means is that if you were to lose your case, you would not have to pay any legal fees so it's risk free. </p><p>In addition to offering No Win No Fee, Slater and Gordon will not charge our clients suffering from an asbestos related illness any costs if you win your case, so you will also get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestos and mesothelioma claims`,
				copy: `Being diagnosed with mesothelioma or another asbestos related disease can be both shocking and devastating for both you and your family. If you've been in contact with asbestos due to someone else's negligence, you may be entitled to compensation for your pain, suffering and financial losses.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/asbestos-mesothelioma-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Mesothelioma claims`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/mesothelioma/`,
						icon: `lung`
					},
					{
						title: `Asbestos related lung cancer`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-lung-cancer`,
						icon: `chemical`
					},
					{
						title: `Asbestosis claims`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestosis/`,
						icon: `asbestos-roof`
					},
					{
						title: `Pleural thickening claims`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/pleural-thickening/`,
						icon: `lung`
					}
				]
			},
			standardContent: `product-template/asbestos-mesothelioma`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
			imageTextA: {
				section: `Asbestos and mesothelioma claims`,
				heading: `What asbestos related illness can I claim for?`,
				copy: `Even minor exposure to asbestos can have fatal consequences. Asbestos related illnesses such as mesothelioma can have devastating effects on sufferers lives and that of their families. Slater and Gordon's expert solicitors are here to offer you the help and support you need and can offer help in claiming compensation for a variety of industrial diseases related to asbestos exposure.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/140064483.jpg`,
					medium: `/public/images/bitmap/medium/140064483.jpg`,
					small: `/public/images/bitmap/small/140064483.jpg`,
					default: `/public/images/bitmap/medium/140064483.jpg`,
					altText: `Granddaughter showing picture to her grandfather`
				},
				video: false
			},
		}
	}
}