module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `hotel-resort`,
		slug: `hotel-resort`,
		parent: `holiday-accident-claims`,
		name: `Holiday Hotel`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Holiday Hotel Claims | Slater + Gordon`,
				description: `Have you been injured in a resort, hotel or on an excursion? You could be entitled to compensation. Slater and Gordon specialises in No Win No Fee compensation claims for accidents on holidays and trips in the UK and abroad.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for an accident abroad?",
                    answer: `<p>Whether you've been staying at a hotel or in a resort, it's easy for injuries to arise due to someone else's negligence. From slippery pool-sides to badly-lit footpaths, there are many reasons why you could come to harm on holiday: including while you're out on an excursion. In fact, we have made successful claims for injuries resulting from  circumstances such as:</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">Slips and trips</a><strong>:</strong> due to wet floors, uneven surfaces, and worn carpeting within hotels</p><p><strong>Swimming pool accidents:</strong> including slipping over at the poolside and drowning</p><p><strong>Falls from balconies: </strong>resulting in serious injury or even death</p><p><strong>Defective equipment injuries:</strong> due to hotel furniture or apparatus such as broken sun loungers</p><p>However, there are almost as many causes of injuries as there are places to go on holiday. So no matter how you've been harmed on an excursion or holiday, ask us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee claim today</a>.</p>`
                  },
                  {
                    question: "Who is liable if I'm injured on holiday?",
                    answer: `<p>This very much depends on how you booked your holiday. If you prefer to book your own flights and accommodation separately, and were injured at your hotel, then any compensation claim would have to made against the hotel, under the laws of the country you were visiting. However, if you booked a package holiday from the UK, you may be able to make a claim against the tour operator or travel agent holiday under <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/\">the Package Travel and Linked Travel Arrangements Regulations 2018</a>. If you aren't sure whether or not you were actually on a package holiday, feel free to talk to one of our holiday claims experts today. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Hotel, resort and excursion compensation claim lawyers`,
				copy: `You go on holiday to have a good time, but sometimes injuries spoil the fun. Slater and Gordon is a specialist law firm with extensive experience of compensation claims for incidents in hotels, resorts and on excursions in the UK and abroad.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/hotel-resort-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/hotel-resort`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}