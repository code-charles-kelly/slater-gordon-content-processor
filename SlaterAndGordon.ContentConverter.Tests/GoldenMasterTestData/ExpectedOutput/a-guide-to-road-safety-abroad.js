module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `a-guide-to-road-safety-abroad`,
		slug: `a-guide-to-road-safety-abroad`,
		parent: `road-traffic-accidents`,
		name: `A guide to Driving Safely in the EU`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `A guide to Driving Safely in the EU | Slater + Gordon`,
				description: `The rules of the road in EU countries can make life difficult for UK drivers. Read this handy Slater and Gordon guide to make sure you stay safe when driving on the continent.`,
				robots: ``,
                schema: [
                  {
                    question: "What do I need to buy before driving in the EU?",
                    answer: `<p>If you're hiring a car at the airport when you arrive, it will normally come complete with most of the legally required equipment you need: though it's worth confirming this with the car hire firm before you fly.</p><p>However, if you're taking your own car, these are some of the things you'll need to add to your boot before you go:</p><ul><li><strong>Reflective warning triangles:</strong> most EU countries require that you carry one (or in some cases, two) of these, to be placed a little way up the road from you in the event of a breakdown, in addition to having switched on your hazard warning lights</li><li><strong>High vis/reflective vests: </strong>which should be worn by all members of your party whenever you are out of your car after a breakdown or accident</li><li><strong>Personal breathalyser: </strong>this is only legally required in France, but you must have one, and it must carry the French 'NF' mark and be within its 'use-by' date</li></ul>`
                  },
                  {
                    question: "Which EU driving laws do I need to be aware of?",
                    answer: `<p>Many EU driving laws are similar to our own, but bear in mind that they are very rigorously enforced in some countries, with substantial penalties, including hefty roadside fines.</p><ul><li><strong>Drink driving: </strong>the legal alcohol limit for drivers in the EU is generally lower than here in the UK, meaning that the best policy is to avoid alcohol completely if you are driving</li><li><strong>Headlights: </strong>many countries in northern Europe require you to have your dipped headlights on all the time, and you will be fined if you forget to do so</li><li><strong>Mobile phones: </strong>as in the UK, it is illegal to use your mobile phone while driving, and you risk a hefty roadside fine if caught doing so</li><li><strong>Children: </strong>it is illegal for children under 10 to travel in the front of cars in some EU countries, so do check the individual country's laws before you go if your under-10s are used to travelling in the front</li><li><strong>Seat belts: </strong> these are compulsory across the EU for front- and back-seat passengers, and on-the-spot fines will be issued for non-compliance</li><li><strong>Emissions: </strong>like London, some large cities in the EU ban high emission vehicles from central areas at certain times, so do look out for warning symbols, and do check online before you go if you know you will be driving into a particular city</li><li><strong>Speed limits in varying weather: </strong>in many EU countries, the speed limits are automatically lower when it is raining. For example, in France, the speed limits on motorways fall from 130 km/h to 110 km/h in wet weather. Look out for the signs that tell you the maximum speeds for different weather conditions</li></ul>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `A Guide to Driving Safely in the EU`,
				copy: `About to make a road journey in the EU? The differences go far beyond driving on the left. This brief Slater and Gordon guide to driving safely in the EU is designed to help keep your trip safe, enjoyable and above all, legal.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/a-guide-to-road-safety-abroad-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/a-guide-to-road-safety-abroad`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}