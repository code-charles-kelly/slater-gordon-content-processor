module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `accident-at-work-compensation`,
		slug: `accident-at-work-compensation`,
		parent: `personal-injury-claim`,
		name: `Accident at Work`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Accident at Work Compensation | Slater + Gordon`,
				description: `Your employer has a duty of care to keep you safe from harm. So if you've had an accident at work, you may be entitled to claim compensation for your injuries. Find out about No Win No Fee workplace injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "I've had an accident at work. Can I claim?",
                    answer: `<p>Many workplaces are dangerous to a certain extent, which is why there are now <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">strict health and safety rules to govern</a> everything from how needles are disposed of in hospitals to who is allowed to build scaffolding on construction sites. Unfortunately, safe working practices aren't always followed and safety equipment isn't always up to scratch. In these cases, accidents can and do happen, often leaving workers with serious injuries or illnesses that can lead to loss of earnings, and sometimes even loss of life. That's when you need to talk to a legal firm with the experience to help you seek the compensation you deserve.</p>`
                  },
                  {
                    question: "Will I have to pay to make a compensation claim?",
                    answer: `<p>We believe that everyone should be able to afford justice when they are injured through no fault of their own. That's why we handle almost all personal injury cases on a<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\"> No Win No Fee basis</a>. This means there'll be no cost to you if you were to lose you case and therefore, seeking justice will have no financial risk.</p>`
                  },
                  {
                    question: "Is my employer responsible for my accident at work?",
                    answer: `<p>Even if your accident was caused by a colleague's actions or negligence, your employer is ultimately responsible for keeping you from harm in the workplace. That shouldn't put you off from making a compensation claim for an accident at work. Every employer, by law, has to have <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers' liability insurance</a>, to make sure they can pay compensation for any accidents at work that result in illness or injury, without it affecting the firm financially.</p>`
                  },
                  {
                    question: "What is my accident at work claim worth?",
                    answer: `<p>Naturally, every compensation claim depends on the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a> as well as the chances of you making a full recovery. Other factors that might affect the value of your claim might be lost earnings or any unexpected medical expenses you may have had. Our goal is always to get you the highest possible amount of compensation to ensure you can move on with your life.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Accident at work compensation claims`,
				copy: `Have you had an accident at work? If so, it's possible that your employer or another worker has been negligent, and you may be able to claim against their employers' liability insurance. We offer a No Win No Fee service in 98% of personal injury cases.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/accident-at-work-compensation-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Construction accidents`,
						url: `/personal-injury-claim/accident-at-work-compensation/construction-site/`,
						icon: `spanner-screwdriver`
					},
					{
						title: `Defective equipment`,
						url: `/personal-injury-claim/accident-at-work-compensation/defective-equipment/`,
						icon: `plug-socket`
					},
					{
						title: `Factory and warehouse accidents`,
						url: `/personal-injury-claim/accident-at-work-compensation/factory-warehouse/`,
						icon: `factory`
					},
					{
						title: `Military injuries`,
						url: `/personal-injury-claim/accident-at-work-compensation/military/`,
						icon: `military`
					}
				]
			},
			standardContent: `product-template/accident-at-work-compensation`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
			imageTextA: {
				section: `Accident at work`,
				heading: `What kind of accidents can I claim for?`,
				copy: `In today's busy workplaces, there are hundreds of ways that you can sustain an injury that wasn't your fault. However, these are some of the accident at work claims that we typically handle for our clients:`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/194353299.jpg`,
					medium: `/public/images/bitmap/medium/194353299.jpg`,
					small: `/public/images/bitmap/small/194353299.jpg`,
					default: `/public/images/bitmap/medium/194353299.jpg`,
					altText: `Male supervisor discussing work with worker`
				},
				video: false
			},
		}
	}
}