module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `forklift`,
		slug: `forklift`,
		parent: `accident-at-work-compensation`,
		name: `Forklift Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Forklift Injury Compensation Claims | Slater + Gordon`,
				description: `Forklift accidents are all too common. If you've been injured due to another person's negligence, you're entitled to seek compensation. Find out about No Win No Fee forklift injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sorts of forklift truck accidents can I claim for?",
                    answer: `<p>Forklift trucks are the workhorses of many industries. But working at pace in tight or crowded spaces means that safety corners are sometimes cut, and that's when accidents happen. These are some of the most common types of forklift accident:</p><ul><li>Forklift <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/pedestrian-accident-claims/\">collisions with pedestrians</a></li><li>Loads falling from trucks</li><li>Unsafe driving leading to topples</li><li>Personnel being crushed</li><li>Collisions with other objects/vehicle</li></ul><p>If you've been injured in any of these scenarios – as a driver or a pedestrian – then safe working practices may have been breached and negligence may have been the cause of your injuries. Talk to a specialist solicitor about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee injury compensation claim</a> today.</p>`
                  },
                  {
                    question: "Does my employer have a duty of care?",
                    answer: `<p>Whether you work in a warehouse or a steel works, the site operator has a duty of care to ensure that all forklift drivers are fully trained and that all safety procedures are followed. This is a duty that is owed to drivers, fellow workers, and site visitors alike, and requires due care with regard to:</p><ul><li>Driver training</li><li>Safety equipment.</li><li>Experience in the duties required</li><li> Forklift maintenance</li><li>Awareness of working and safety procedures</li></ul><p>Where this duty of care has not been met, negligence may have occurred, and anyone injured as the result of a forklift accident may be able to claim compensation for their injuries. </p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that every employer is required to have, by law. It ensures that money is available to compensate workers for illness or injuries that were not their fault. Read more about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers liability insurance</a>.</p>`
                  },
                  {
                    question: "How much is my forklift accident claim worth?",
                    answer: `<p>Every injury claim is different, and the final figure for compensation depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and how much any rehabilitation might cost. However, we take most forklift injury cases on a No Win No Fee basis, and seek interim payments to help you avoid hardship if you're prevented from working by your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>The general rule is that a claim should be made within three years from the date of the accident at work. However, there are exceptions, such as if the person injured now has a lack of mental capacity, if the accident at work took place whilst abroad, or in the tragic circumstances when a death has occurred. It's therefore important that as soon as you're able to, you contact a solicitor who specialises in accidents at work. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Forklift accident compensation claims`,
				copy: `Have you been injured while driving or working close to a forklift truck? Slater and Gordon are experts when it comes to accident at work cases and can offer a No Win No Fee service in the vast majority of personal injury cases.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/forklift-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/forklift`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}