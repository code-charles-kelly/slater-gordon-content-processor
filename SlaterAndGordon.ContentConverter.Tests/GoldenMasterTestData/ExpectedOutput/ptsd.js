module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `ptsd`,
		slug: `ptsd`,
		parent: `military`,
		name: `PTSD`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `PTSD Compensation Claims Solicitors | Slater + Gordon`,
				description: `Post-Traumatic Stress Disorder (PTSD) is a debilitating condition affecting many serving and former military personnel. If your life is being impacted by PTSD, ask about making a No Win No Fee compensation claim through Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is Post-Traumatic Stress Disorder?",
                    answer: `<p>Post-Traumatic Stress Disorder (PTSD) is now recognised as a serious condition that can leave sufferers depressed, anxious and unable to live their lives the way they did before the trauma occurred. As it's caused by the fear of imminent death and other traumas, it is fairly common amongst military personnel and veterans. </p>`
                  },
                  {
                    question: "Can I claim for PTSD caused by military service?",
                    answer: `<p>While danger is inherent in combat operations, you may be eligible to claim compensation for PTSD under certain circumstances, such as where:</p><ul><li>Extreme danger resulted from negligence</li><li>Reasonable precautions weren't taken</li><li>Officers failed to recognise the symptoms of PTSD</li><li>Medical Officers failed to correctly diagnose PTSD</li></ul><p>So if you believe that your PTSD resulted from these or other failings by others, you owe it to yourself to talk to one of our military experts today.</p>`
                  },
                  {
                    question: "Are there any other causes of PTSD?",
                    answer: `<p>Many people are affected by PTSD: from fire crews and police officers who deal with death every day,  to those who have simply witnessed an horrific incident or had an unexpected brush with death. So if you suffer flashbacks, depression, anxiety, nightmares, or any other symptoms of PTSD, resulting from events that were not your fault, talk to one of our experienced and understanding solicitors about claiming compensation.</p>`
                  },
                  {
                    question: "The Armed Forces Compensation Scheme (AFCS)",
                    answer: `<p>There are a number of Government compensation schemes for forces personnel suffering from both physical injuries and PTSD. These are administered by Veterans UK and you can find out more about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/\">these schemes here</a><strong>.</strong></p>`
                  },
                  {
                    question: "What can PTSD compensation pay for?",
                    answer: `<p>There are two ways in which PTSD compensation can help you. Firstly, it can help to compensate you for loss of earnings, if your symptoms are preventing you from working and earning as much as you would be able to without PTSD. Just as importantly, there are a number of therapies that are proven to alleviate the symptoms of PTSD, and any compensation claim we undertake on your behalf will include a sum to help <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">pay for rehabilitation</a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\"> </a><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">treatments</a><strong></strong> including:</p><p>Eye Movement Desensitisation and Reprocessing (EMDR): helps to prevent difficult memories from causing stress by helping the brain to reprocess them</p><p>Cognitive Behavioural Therapy (CBT): helps you to come to terms with bad experiences by changing the way you see them</p><p>Group Psychotherapy: helps you to deal with difficult memories by confronting them in the company of a group that has shared similar experiences</p><p>If you're suffering from the symptoms of PTSD - as a result of military service or any traumatic events - talk to one of our experienced and understanding legal experts about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim today</a>.</p>`
                  },
                  {
                    question: "How much is my PTSD claim worth?",
                    answer: `<p>As you would expect, every claim is different, with the final figure for compensation dependent upon the severity of your symptoms as well as the length of time that your recovery may take. Yet our expertise is not just in securing compensation for our clients, it's also in understanding the need for rehabilitation and the support that you and your family will need right away. That's why we take most military injury cases on a No Win No Fee basis, and why obtaining interim payments to help you avoid hardship is often central to our approach.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Military accident claims`,
				heading: `Can I claim compensation for my PTSD?`,
				copy: `Post-Traumatic Stress Disorder affects many people who have suffered near-death or other traumatic experiences. If you or a loved one suffers from PTSD that relates to military service, talk to us today. Slater and Gordon handles most military compensation cases on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/134406019.jpg`,
					medium: `/public/images/bitmap/medium/134406019.jpg`,
					small: `/public/images/bitmap/small/134406019.jpg`,
					default: `/public/images/bitmap/medium/134406019.jpg`,
					altText: `Military father sitting at home with family`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee PTSD claims`,
				leftWysiwyg: `product-template/ptsd-two-panel-cta`,
				rightHeading: `Contact Slater and Gordon to start your PTSD compensation claim`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/ptsd`,
			sideLinkList: {
				heading: ``,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military accident FAQ`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `What is the armed forces compensation scheme?`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}