module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq2`,
		slug: `faq`,
		parent: `road-traffic-accidents`,
		name: `Road Traffic Accident Claims FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Road Traffic Accident Claims FAQs | Slater + Gordon`,
				description: `When you've suffered an injury in a road traffic accident, you often don't know where to begin in pursuing a claim for compensation. Our legal experts have answered the most commonly asked road traffic accident questions that you might have.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been involved in a road traffic accident that wasn't my fault. How do I start a claim for compensation?",
                    answer: `<p>When you've been injured in a road traffic accident, the most important thing to do is to seek medical attention. When you feel able to speak to, you should speak to a lawyer who specialises in road traffic accidents as soon as possible. Making a claim can often seem like a daunting prospect, but we have specialist legal advisers with many years' experience who can put your mind at ease. All you have to do to start your claim is call us on ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a> and we'll take it from there. </p>`
                  },
                  {
                    question: "What evidence will I need to provide to support my claim?",
                    answer: `<p>If you were able to take evidence at the scene of the accident, we will ask you to share that evidence with us. That evidence may include: </p><ul><li>Details of the person at fault / the other driver</li><li>Details of any witnesses </li><li>Photographs of the accident scene</li><li>Photographs of your injuries. </li></ul><p>You may also be asked to provide details of any medical appointments you've attended and any medication prescribed to help with the treatment of your injuries. We will also need to send you for an independent medical where a medical examiner will examine you and write a report based on your injuries. </p>`
                  },
                  {
                    question: "How do I fund a claim for a road traffic accident?",
                    answer: `<p>The vast majority of road traffic accident claims are funded under a No Win No Fee agreement. Under a No Win No Fee agreement, if your case is unsuccessful, you won't have to pay a penny which means they'll be no financial risk to you. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">Click here for more information on No Win No Fee claims</a>.</p>`
                  },
                  {
                    question: "Are you able to claim on behalf of someone who has died as a result of a road traffic accident?",
                    answer: `<p>If a loved one has passed away as a result of a road traffic accident, you may be able to claim on their behalf. If the deceased made a will, the executor of the estate is responsible for pursuing the claim on behalf of any dependents. If there is no will, only certain relations or family members are able to make a claim following the death of a loved one.</p><p>The people who are able to make a claim for compensation are prescribed by law and the list is extensive, but one of our experts in road traffic accidents will be able to advise whether or not you can make a claim. For more information on fatal injuries, please click <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/fatal/\">here</a>. </p>`
                  },
                  {
                    question: "Can I make a compensation claim if I was injured in a road traffic accident but was not wearing a seatbelt?",
                    answer: `<p>If someone else was at fault for you being injured in a road traffic accident then you can make a claim for compensation. If it's found that not wearing your seatbelt made your injuries worse, then you will be found partially to blame for the accident and your compensation payment will be deducted to represent the percentage of your blame.</p>`
                  },
                  {
                    question: "Can I claim compensation if I was injured in a road traffic accident in another country?",
                    answer: `<p>If you've been injured in a road traffic accident in another country, whether as a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">driver</a>, cyclist, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/motorcycle-accident-claims/\">motorcyclist</a>, passenger or pedestrian, you may still be able to make a claim for compensation.</p><p>Often in road traffic accidents which happen overseas, there can be conflicts in jurisdictions and law and in many European countries there are laws where fault doesn't need to be proved. This is why it's so important to ensure the law firm you choose has specialists in road traffic accidents abroad who understand the laws which apply and have a network of experts across the globe.  </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Road traffic accident FAQs`,
				copy: `The most commonly asked road traffic accident questions you might have, answered by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq2-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq2`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}