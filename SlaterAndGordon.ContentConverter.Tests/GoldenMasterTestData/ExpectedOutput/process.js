module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `process`,
		slug: `process`,
		parent: `road-traffic-accidents`,
		name: `Road Traffic Accident Claims Process`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Road Traffic Accident Claims Process | Slater + Gordon`,
				description: `If you have been involved in a road traffic accident our legal experts can help you through the claims process from start to finish. Call Slater and Gordon today to help with your road traffic accident claim.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a part 36 offer?",
                    answer: `<p>A Part 36 Offer is an offer that either party can make to settle all or part of the claim prior to a final hearing. It's called a Part 36 Offer as it is referred to in Part 36 of the CPR.</p><p>For example, the other side may make an offer to accept 70% responsibility of the accident, or the medical expert may deliver their final prognosis which values your claim at £10,000 and you may want to make that offer to the other side. Whatever the offer by whichever party, your lawyer should provide you with clear advice on the options so that you can make a fully informed decision.</p><p>You should however be aware that if negotiations don't reach a settlement, and the matter goes to court, there may be costs consequences on the party who didn't accept the offer, such that they may have to pay additional costs and compensation. This can happen if, for example, a party fails to beat an offer made by another party.</p>`
                  },
                  {
                    question: "What medical evidence will I need?",
                    answer: `<p>In order to support your personal injury claim, your lawyer will need to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/guide/\">obtain medical evidence</a>. In order to do this, they will have to obtain your medical records and arrange for you to have an examination with an independent medical expert who specialises in providing medical reports for the court. This is known as a medico legal report.</p><p>The medico legal report will provide details of your injury and the medical expert's prognosis (their opinion as to if and when they believe you are likely to make a full recovery from your injuries).</p><p>During the medical examination, the expert should ask you questions about the accident and about your injuries. They should also ask about the impact the injuries have had on your everyday life, for example, is there anything you now struggle to do or have been unable to do.</p><p>If the medico legal report is unclear or there are elements you disagree with, your lawyer can put questions to the medical expert.</p>`
                  },
                  {
                    question: "What is a litigation friend?",
                    answer: `<p>As children can't pursue their own claim, someone must be appointed to pursue the claim on their behalf. A person pursuing a claim on behalf of a child is known as a 'Litigation Friend'. </p><p>A Litigation Friend is usually a family member, such as a parent or a grandparent. The person acting as Litigation Friend on behalf of a child must act in the child's best interests. They also must not have any responsibility for the accident, i.e. if the child's father collides to the rear of another vehicle causing injury to his child who is a back seat passenger in his vehicle, he is unable to act as Litigation Friend for his child. </p>`
                  },
                  {
                    question: "Who can make a claim on behalf of someone who has died from a road traffic accident?",
                    answer: `<p>Only certain relations or family members are able to make a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">claim arising from the death of a loved one</a>. The people who can bring a claim are prescribed by law but the length is quite extensive. You should therefore contact a lawyer if you think you have a claim as a result of a loved one suffering a fatal injury due to a road traffic accident.</p><p>If the person who has passed away made a will, the Executor of the Estate may be required to pursue the claim on behalf of any dependents.</p>`
                  },
                  {
                    question: "What compensation can be claimed?",
                    answer: `<p>There are various claims which can be made in the event of someone's death. Each is dependent upon the circumstances and the person making the claim. These are:</p><ul><li><strong>Damages for the Estate: </strong>A claim for damages can be brought on behalf of the Estate of your loved one. This includes compensation for their pain and suffering and any out of pocket expenses.<strong> </strong></li></ul><ul><li><strong>Bereavement damages:</strong><strong> </strong>This is a statutory award for the loss of a loved one. The amount due depends on the date of death. Bereavement damages are only available to specific family members.</li><li><strong>Loss of dependency: </strong>A person is a dependent if they were reliant upon the person who has died for income and/or services such as assistance with household chores or in caring for a relative. Compensation awarded would be to replace the loss of income and/or to provide alternative assistance.</li><li><strong>Human Rights Act: </strong>Not all family members are entitled to bring a claim for bereavement damages and where family members are not deemed as dependents of their loved one, this can leave them effectively uncompensated for a negligent death. Therefore, in certain circumstances, a claim can currently be brought under <a href=\"https://www.citizensadvice.org.uk/law-and-courts/civil-rights/human-rights/the-human-rights-act-1998/\">the Human Rights Act 1998</a>. However, there's a special time limit for doing so of one year from the date of the incident that led to death.</li><li><strong>Psychiatric injury: </strong>If you witnessed the incident involving your loved one, and have suffered a recognised psychiatric injury, you may be categorised as a secondary victim, and may be able to claim in your own right.</li></ul>`
                  },
                  {
                    question: "What is an inquest?",
                    answer: `<p>An inquest is an independent judicial inquiry carried out by a Coroner to establish the identity of the person who has died, the place and time of their death and the cause of their death. It's a fact finding exercise and is not intended to apportion blame to any individual or organisation.</p><p>The length of time an inquest takes depends on what has happened and the issues that need to be explored. In our experience, they can be anything from 15 minutes to several days. <a href=\"https://www.slatergordon.co.uk/media-centre/blog/2018/11/what-is-an-inquest/\">Read more here</a>.</p>`
                  },
                  {
                    question: "What happens at an inquest?",
                    answer: `<p>Prior to an inquest taking place, the Coroner's staff may interview witnesses, take statements and fix a date for the inquest. The Coroner will request that the family of the person who has died attend the inquest along with those who were directly or indirectly involved with the death.</p><p>The Coroner will call witnesses to give evidence under oath and will ask them questions. Other parties such as the family or their legal representative are also permitted to ask questions.</p><p>Once all the evidence has been heard, the Coroner will record the findings, including the cause of death and in some cases, may decide to give a narrative conclusion which contains more detail about the events leading up to the death.</p>`
                  },
                  {
                    question: "What will a legal representative do at an inquest?",
                    answer: `<p>An inquest can be a daunting prospect and bereaved families are therefore entitled to legal representation. You should ensure you instruct a lawyer to represent you who specialises in inquests.</p><p>Legal representatives are instructed by bereaved families to represent their interests at the inquest. They can guide bereaved families through the process, ensuring any questions they have surrounding the death of their loved one are addressed and thoroughly investigated. Following the inquest, they can also explain the outcome of the inquest so the bereaved family have a full understanding.</p><p>In some cases, the Coroner will call a pre-inquest review meeting prior to the inquest taking place to discuss which witnesses are intended to be called and the statements to be admitted as evidence. Legal representatives are able to attend this on behalf of the family.</p><p>The majority of the road traffic accidents Slater and Gordon deal with are funded by way of a No Win No Fee agreement, also known as a Conditional Fee Agreement or CFA. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">A No Win No Fee agreement</a> means exactly what it says – If you don't win your case, you will not have to pay any legal costs.</p><p>This is a fair way for those who've been injured as a result of someone else's negligence, to be compensated for their pain and suffering, as well as their financial losses, without having any financial risk to them.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Road traffic accident claims process`,
				copy: `The process of claiming after a road traffic collision can be confusing and complex. The below guide has been written by our legal experts to help you understand the claims process from start to finish.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/process-two-panel-cta`,
				rightHeading: `Find out more about uninsured driver accident compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/process`,
			sideLinkList: {
				heading: `More questions about road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}