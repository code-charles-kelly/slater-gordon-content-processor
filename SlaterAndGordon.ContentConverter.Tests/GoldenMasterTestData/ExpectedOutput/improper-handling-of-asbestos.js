module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `improper-handling-of-asbestos`,
		slug: `improper-handling-of-asbestos`,
		parent: `asbestos-mesothelioma`,
		name: `Improper Handling of Asbestos`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Improper Handling of Asbestos | Slater + Gordon`,
				description: `Mesothelioma, asbestos related lung cancer, asbestosis and pleural thickening can have devastating effects on sufferers and their families lives. Slater and Gordon are one of the UK's leading asbestos and mesothelioma compensation claim specialists.`,
				robots: ``,
                schema: [
                  {
                    question: "Why was asbestos banned?",
                    answer: `<p>The use of asbestos was banned in the UK and no longer used from 1999. Before it was banned it was used everywhere because of its fireproof qualities and was said to be a 'wonder substance'. However, upon discovery of its carcinogenic - cancer causing - properties, which if disturbed, exposure to asbestos dust can lead to asbestos related illnesses such as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/mesothelioma/\">mesothelioma</a>, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-lung-cancer\">asbestos related lung cancer</a>, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestosis/\">asbestosis</a> and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/pleural-thickening/\">pleural thickening</a>, it was banned in the UK. However, it's still legal in some countries. </p>`
                  },
                  {
                    question: "Who can carry out the removal of asbestos?",
                    answer: `<p>Given that when disturbed, the dust from the asbestos can lead to fatalities, the removal of asbestos from a property should strictly be carried out by asbestos removal specialists. They have professional processes to ensure the safe removal of asbestos and to protect others from harm. </p>`
                  },
                  {
                    question: "I need some building work carrying out but I'm not sure whether there's asbestos. What should I do?",
                    answer: `<p>As the use of asbestos was only banned in the UK in 1999, any properties built prior to 2000 may contain asbestos. It's essential that any building work you carry out doesn't disturb any asbestos as this could result in devastating consequences. If you do need to undertake any building work to a property which may contain asbestos you should first arrange for professionals to conduct a survey and if there are any signs of asbestos, arrange for its safe removal by the asbestos removal specialists.</p>`
                  },
                  {
                    question: "I was walking through my local park recently when I noticed some building materials which had been dumped and it looked like it contained asbestos. What should I do?",
                    answer: `<p>Ignorance to the dangers of asbestos extends far beyond the crime of fly-tipping as those actions can put many people's lives at risk. </p><p>If you come across any materials which have been dumped and potentially contain asbestos materials, don't go near them. Leave the area and immediately contact your local council who will assess the situation and arrange for the safe removal of the potentially hazardous waste. </p>`
                  },
                  {
                    question: "Are fines issued to anyone who acts irresponsibly with the removal of asbestos?",
                    answer: `<p>Yes. In 2015 the HSE inspectors visited the site of an indian restaurant in Cleeve which was being refurbished, as there was a concern that asbestos had been spread when the roof insulation was disburbed. Despite the owner's defence was of \"naivety and a clear lack of understanding in regard to that area of the law\" he pleaded guilty to 'failing to reduce the exposure of asbestos to his co-workers and members of the public' and was also issued with a further charge of breaching the Notice issued by the Health Executive, and was fined £15,500. </p><p>Also in 2015, a man from Dagenham was found guilty of dumping asbestos on a busy public street and was sentenced to 16 weeks in jail, suspended for 18 months, for what the judge called \"a serious offence of fly-tipping waste.\" Where fly-tipping in itself is an offense, an investigation revealed a much more severe crime that placed the public at risk of the carcinogenic effects of asbestos. The dumped rubbish contained corrugated sheets of asbestos, with some of the blue asbestos left on a pavement near a school. </p>`
                  },
                  {
                    question: "I've been diagnosed with an asbestos related illness What do I do?",
                    answer: `<p>If you've been diagnosed with an asbestos related illness, speak to one of our specialists about making a claim for compensation. Call our asbestos experts today on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a>. We offer No Win No Fee agreements to all clients who have been diagnosed with an asbestos related disease and will not charge any additional fees meaning you'll get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Improper handling of asbestos`,
				copy: `Asbestos materials are extremely dangerous and it's therefore essential that you understand exactly what you should do when you come across asbestos materials to not only protect your own life, but also the lives of others.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/improper-handling-of-asbestos-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/improper-handling-of-asbestos`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}