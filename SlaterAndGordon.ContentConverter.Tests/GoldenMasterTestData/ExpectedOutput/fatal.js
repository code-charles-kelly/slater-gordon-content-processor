module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `fatal`,
		slug: `fatal`,
		parent: `serious`,
		name: `Fatal Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Fatal Injury Compensation | Slater + Gordon`,
				description: `If you've lost a loved one, financial compensation is probably the last thing on your mind. Yet if their death was caused by someone else's negligence, you may need to make a claim. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Am I entitled to claim fatal accident compensation?",
                    answer: `<p>If someone or something other than natural causes was to blame for the loss of a loved one, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">the law allows you to claim if you were a dependent</a>. This means anyone who was in any way dependent upon the deceased prior to their death, such as children or a spouse.Those who may be able to make a claim when a loved one dies includes:</p><ul><li>A spouse or financially dependent former spouse</li><li>Cohabitees or civil partners of at least two years' standing</li><li>Children, grandchildren and other descendants</li><li>Adopted children or those from a marriage or civil partnership</li><li>Parents, grandparents and others with parental responsibility</li><li>Close relatives including siblings, aunts, uncles, nieces and nephews</li></ul><p>If you believe that you are entitled to claim compensation for your loss, please contact us to speak to a legal expert who can advise you of the next steps. .</p>`
                  },
                  {
                    question: "Who might be responsible for the fatality?",
                    answer: `<p>Having responsibility for someone's death is a terrible thing. Unfortunately, there are any number of circumstances that can lead to causing someone's death, including:</p><ul><li><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Medical negligence</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Dangerous driving (road traffic collision)</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Industrial accidents</a></li><li>Criminal assaults</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/sports-injury/\">Sporting accidents</a></li></ul><p>In fact, whenever someone dies and natural causes, such as old age, are not clearly at fault, it's important to consider whether someone's actions or negligence may be to blame. That's when you may wish to speak to one of our legal experts to see if a compensation claim is the most appropriate course of action.</p>`
                  },
                  {
                    question: "How much will it cost to make a claim?",
                    answer: `<p>The vast majority of cases at Slater and Gordon are funded under a No Win No Fee agreement. This allows you to seek justice without having to worry about the cost, as if you were to lose your case, you would not have to pay anything, meaning there's no financial risk to you. </p>`
                  },
                  {
                    question: "Who can I talk to about bereavement?",
                    answer: `<p>We know from experience that money can seem unimportant at a time like this. So we understand that you need understanding and support more than anything else when a loved one has passed on. Please rest assured that when you talk to one of our specialist lawyers, they'll be conscious of the need to get you the emotional support you need, just as much as whatever financial compensation you are entitled to receive. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Fatal injury compensation claims`,
				copy: `Losing someone you love is the worst thing that can ever happen and financial compensation is probably the last thing on your mind. However, if someone else was to blame, you do have the right to sek compensation. Slater and Gordon is a leading law firm with extensive experience of fatal injury compensation claims.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/fatal-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/fatal`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}