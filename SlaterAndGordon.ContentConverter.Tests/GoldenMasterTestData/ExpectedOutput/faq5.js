module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq5`,
		slug: `faq`,
		parent: `food-poisoning`,
		name: `Food Poisoning  FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Food Poisoning Compensation Claims FAQs | Slater + Gordon`,
				description: `Food poisoning compensation claims are a complex matter. Our experts have answered the most commonly asked questions for you here.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for compensation if I've contracted food poisoning?",
                    answer: `<p>Food poisoning is really quite serious, especially if you have food allergies, for example:</p><ul><li>If you have been served food in a restaurant that didn't have all allergens listed on the menu</li><li>If allergens were not communicated to you by the waiting staff for you to make an informed decision</li><li>If food had been contaminated without your knowledge prior to consumption.</li></ul><p>If you've contracted food poisoning from a food outlet, you may be entitled to make a compensation claim for the illness you have suffered.</p>`
                  },
                  {
                    question: "Will it cost me anything to make a compensation claim for food poisoning?",
                    answer: `<p>Food poisoning compensation claims can be funded under a No Win No Fee Agreement. This means if your case is unsuccessful, you won't have to pay a penny and they'll be no financial risk to you. </p>`
                  },
                  {
                    question: "Does a claim need to be made within a certain time limit after contracting food poisoning?",
                    answer: `<p>Generally in England, Wales and Scotland the time limit to make a claim for a food poisoning related illness is three years of the date of the incident. However, there are some variations to these time limits, for example:</p><ul><li>If claiming on behalf of someone who has passed away as a result of food poisoning, the claim would need to be made within three years from the date of their death.</li><li>If claiming on behalf of a child, the three year time limit does not begin until the child reaches the age of 18 in England and Wales or the age of 16 in Scotland.</li><li>If claiming on behalf of someone who lacks mental capacity, there is no time limit to make a claim.</li></ul>`
                  },
                  {
                    question: "How long will my food poisoning compensation claim take to process?",
                    answer: `<p>Food poisoning compensation claims vary in circumstances and so it's difficult to pinpoint exactly how long a single case will take to complete. In part, it will depend on how complex the case is and how severe the illness from food poisoning is. Another factor which will determine how long a case will take is whether or not responsibility is accepted by the food outlet who provided the food. In certain severe cases of allergic reaction or food poisoning that resulted in a death, an inquest may be required, which will slow down the claims process.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Food poisoning compensation claims FAQs`,
				copy: `The most commonly asked food poisoning compensation claims questions answered here by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq5-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq5`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}