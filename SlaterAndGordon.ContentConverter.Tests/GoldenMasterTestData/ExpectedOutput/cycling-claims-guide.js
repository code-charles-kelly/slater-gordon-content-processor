module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `cycling-claims-guide`,
		slug: `cycling-claims-guide`,
		parent: `road-traffic-accidents`,
		name: `Cycling Accident Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Cycling Accident Claims Guide | Slater + Gordon`,
				description: `Cycling accidents often leave the cyclist worse off, in many cases it can be harder to claim as you do not have an insurance company to back you up. Slater and Gordon Lawyers are here to help with your cycling accident claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What can I claim for as a result of a cycling accident?",
                    answer: `<p>As well as claiming compensation for your injuries, you can also claim for medical bills and rehabilitation as well as damage to your bicycle or other property, loss of earnings due to your injuries and for additional travel costs if your bike is damaged and you can't use it for your usual commute.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Cycling accident claims guide`,
				copy: `Cycling accidents often leave the cyclist worse off, in many cases it can be harder to claim as you do not have an insurance company to back you up. Slater and Gordon Lawyers are here to help with your cycling accident claims.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/cycling-claims-guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/cycling-claims-guide`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}