module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `vibration-injury`,
		slug: `vibration-injury`,
		parent: `industrial-disease`,
		name: `HAVS & Vibration Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `HAVS & Vibration Injury Compensation Claims | Slater + Gordon`,
				description: `Hand-Arm Vibration Syndrome (HAVS) is a serious condition. If you have developed this or any other vibration injury at work, Slater and Gordon may be able to claim compensation for you on a No Win No Fee basis`,
				robots: ``,
                schema: [
                  {
                    question: "What is Hand-Arm Vibration Syndrome (HAVS)?",
                    answer: `<p>Hand-Arm Vibration Syndrome (HAVS) is a term used for a range of vibration-related injuries that include Vibration White Finger (VWF), Whole Body Vibration (WBV) and Dupuytrens Contracture. Like Carpal Tunnel Syndrome (CTS), these serious industrial diseases affect the hands, wrists, arms or fingers. They're caused by prolonged exposure to handheld vibration tools ranging from power drills to chain saws.</p><p>Most of this equipment is relatively safe when used for short periods, but research shows that using power tools for more than a few hours each day can cause vibration injuries. In fact, the <a href=\"http://www.hse.gov.uk/vibration/hav/yourhands.htm\">HSE says that using some hammer tools</a> for as little as 15 minutes a day can cause a vibration injury.</p>`
                  },
                  {
                    question: "What are the symptoms of HAVS and Vibration White Finger?",
                    answer: `<p>Both Hand-Arm Vibration Syndrome (HAVS) and Vibration White Finger (VWF) occur when intense vibration causes damage to the nerves, blood vessels and tendons of the hand or arm. The first symptoms most people are aware of include:</p><ul><li>Tingling and numbness of the fingers </li><li>Lost sense of touch in the fingers</li><li>Loss of strength in your hands </li><li>Fingers going white in the cold, then angry red as they heat up again</li></ul><p>The symptoms of Carpal Tunnel Syndrome are broadly similar, through this can be caused by <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/repetitive-strain-injury/\">repetitive twisting of the wrist</a>, as well as by vibration alone. All of these conditions are serious and give rise to a compensation claim in the event that they're caused by a negligent employer.</p>`
                  },
                  {
                    question: "Does my employer have a duty of care to prevent HAVS?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer has failed to limit the time you spent using vibrating plant or tools, they could be said to have failed in their duty of care. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">Read more on the health and safety regulations</a> designed to keep you safe in the workplace.</p>`
                  },
                  {
                    question: "Do I need a specialist HAVS injury lawyer?",
                    answer: `<p>Vibration injury claims are quite a specialised area of the law, and it makes sense to deal with solicitors who have extensive experience and a track record of successful claims. Slater and Gordon employs specialist lawyers in every area of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">industrial disease compensation claims</a>. Just as importantly, we understand the impact that a diagnosis of HAVS can have on victims and their families, and aim to be as considerate and supportive as possible throughout the claims process. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Vibration injury compensation claims`,
				copy: `Hand-Arm Vibration Syndrome (HAVS) is the collective term for a range of vibration-related conditions, including Vibration White Finger (VWF) Whole Body Vibration (WBV) and Dupuytren's Contracture. Slater and Gordon is one of the UK's leading vibration injury compensation specialists, with expert solicitors who could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/vibration-injury-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for vibration injury compensation`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/vibration-injury`,
			sideLinkList: {
				heading: `Do you have more industrial disease questions? Read our expert guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}