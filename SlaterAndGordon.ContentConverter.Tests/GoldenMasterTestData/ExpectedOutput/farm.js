module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `farm`,
		slug: `farm`,
		parent: `accident-at-work-compensation`,
		name: `Farm Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Farm Injury Claims | Slater + Gordon`,
				description: `Farming is an extremely dangerous industry. If you've been injured on a farm due to someone else's negligence, you're entitled to seek compensation. Find out about No Win No Fee farm injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sorts of farm injuries can I claim for?",
                    answer: `<p>While tractor accidents are those that usually make the headlines, there are a large number of other hazards on most modern farms, making farming an extremely dangerous industry in Britain. Some of the hazards that cause serious injuries to farm workers include:</p><p>·      <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/forklift/\"> </a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/forklift/\">Tractors, trailers and forklift trucks</a></p><p>·       Quad bikes</p><p>·       Cattle and other livestock</p><p>·       Falls from barns and haystacks</p><p>·       Falling objects in stores and barns</p><p>·       Dangerous chemicals</p><p>·       Falling into grain silos and slurry pits</p><p>There are many more hazards, which require proper <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">health and safety procedures to be followed</a> in order to avoid injuries. If you or a loved one have been harmed as a result of working on a farm, talk to one of our farming industry specialists about making a No Win No fee compensation claim today.</p>`
                  },
                  {
                    question: "Who is responsible for safety on the farm?",
                    answer: `<p>As you can hear in the video above, Ian was injured because the farm owner didn't have a gate in place to keep between workers and livestock. This is fairly typical of farm injuries, where corners are being cut and injuries occur as a result. In every case, your employer - usually the farm owner – has a duty of care to keep you free from harm by ensuring that health and safety procedures are in place. Where this hasn't happened, and you or a loved one have been injured as a result, you may be able to claim compensation for loss of earnings, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">medical treatment and rehabilitation</a>, as well as for the pain and distress caused by the accident.</p><p>If you've been <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/animals/\">injured by an animal</a><strong> </strong>but not whilst working you may still have a claim.</p>`
                  },
                  {
                    question: "How much is my farming injury claim worth?",
                    answer: `<p>Every injury claim is different, and the final figure for compensation <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">depends upon the seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and how much any rehabilitation might cost. However, we take most farming injury cases on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, and seek interim payments to help you avoid hardship if you are prevented from working by your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>The general rule is that a claim must be brought within three years of the date of the accident. However, there are exceptions, such as if you're claiming on behalf of someone who has diminishing mental capacity, in the event the accident happened abroad, or in the tragic circumstances where there is a death. It's therefore important that you speak to a specialist accidents at work solicitor as soon as you are able. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Farm injury compensation claims`,
				copy: `Have you been hurt while working on a farm? Slater and Gordon is one of Britain's largest consumer law firms, offering a No Win No Fee service to almost all our accidents at work clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/farm-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/farm`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `The dangers of cattle herding - Ian's story`,
				copy: `Former Marine, Ian Gardner, was left unable to work when a farm's health and safety failings led to injuries caused by a cow.`,
				image: false,
				video: `7jvilkplj1`
			}
		}
	}
}