module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `compensation-claims-scotland`,
		slug: `compensation-claims-scotland`,
		parent: `personal-injury-claim`,
		name: `Personal Injury Claims Scotland`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Personal Injury Claims Scotland | Slater + Gordon`,
				description: `For a free consultation call us today. From our Edinburgh office, our Scottish Personal Injury Solicitors are waiting for your call. Slater and Gordon is one of the UK's leading No Win No Fee personal injury solicitors with the expertise to help you get the compensation you need.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I make a personal injury claim in Scotland?",
                    answer: `<p>Whilst there are many similarities between English and Scottish law, there are some key differences and intricacies which is why <a href=\"https://www.slatergordon.co.uk/contact-us/edinburgh/\">we have specialist Scottish team based in Edinburgh</a>. Our expert lawyers are waiting to take your call; to start your claim we simply need some initial information about your or your loved one's injury, such as what happened, where it happened and who else was involved. Our expert care team can help by explaining everything we need to start your claim, so get in touch today.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Personal injury compensation claims in Scotland`,
				copy: `For a free consultation call us today. From our Edinburgh office, our Scottish Personal Injury Solicitors are waiting for your call. Slater and Gordon is one of the UK's leading No Win No Fee personal injury solicitors with the expertise to help you get the compensation you need.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/compensation-claims-scotland-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Road traffic accidents`,
						url: `/personal-injury-claim/road-traffic-accidents/`,
						icon: `car`
					},
					{
						title: `Accidents at work`,
						url: `/personal-injury-claim/accident-at-work-compensation/`,
						icon: `briefcase`
					},
					{
						title: `Accidents and injuries abroad`,
						url: `/personal-injury-claim/holiday-accident-claims/`,
						icon: `man-crutch`
					},
					{
						title: `Asbestos related illness`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/`,
						icon: `lungs`
					},
					{
						title: `Sexual and physical abuse`,
						url: `/personal-injury-claim/sexual-criminal-abuse/`,
						icon: `shield`
					}
				]
			},
			standardContent: `product-template/compensation-claims-scotland`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
			imageTextA: {
				section: `Personal injury`,
				heading: `What sort of personal injuries can I claim for?`,
				copy: `The list below covers some of the most common cause of personal injury Slater and Gordon deals with on a daily basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/233958265.jpg`,
					medium: `/public/images/bitmap/medium/233958265.jpg`,
					small: `/public/images/bitmap/small/233958265.jpg`,
					default: `/public/images/bitmap/medium/233958265.jpg`,
					altText: `Disabled father in wheelchair with his daughter, wife and dog in the park`
				},
				video: false
			},
		}
	}
}