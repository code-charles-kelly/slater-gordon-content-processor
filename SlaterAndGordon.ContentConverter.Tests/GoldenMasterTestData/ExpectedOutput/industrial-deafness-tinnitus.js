module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `industrial-deafness-tinnitus`,
		slug: `industrial-deafness-tinnitus`,
		parent: `industrial-disease`,
		name: `Industrial Deafness | NIHL`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Industrial Deafness Compensation Claims| NIHL | Slater + Gordon`,
				description: `Being exposed to excessive noise at work can lead to hearing loss, tinnitus and even deafness. Where your condition is as a result of your employer's negligence, Slater and Gordon offers the compensation claims expertise you need, and the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is Noise Induced Hearing Loss (NIHL)?",
                    answer: `<p>Noise Induced Hearing Loss (NIHL) is the term used to describe hearing loss, deafness or tinnitus that has been caused by excessive noise at work where there hasn't been proper ear protection or training. As you might expect, workers in heavy industry are often sufferers of NIHL. So if you believe your hearing problems could have been caused by negligence in the workplace, you may be able to claim compensation.</p>`
                  },
                  {
                    question: "What is tinnitus?",
                    answer: `<p>Tinnitus is most commonly described as a constant ringing in the ears, though some sufferers hear buzzing, white noise or other sounds. It occurs when part of the inner ear called the Cochlea is damaged. This carries thousands of tiny cells that detect sound, so when these are damaged, the brain seeks sound from elsewhere in the body, causing tinnitus. There are a number of medical conditions that can cause this, as the <a href=\"https://www.nhsinform.scot/illnesses-and-conditions/ears-nose-and-throat/tinnitus#about-tinnitus\">NHS website points out</a>, but excessive noise in the workplace is the most likely cause for sufferers who have worked in these and other noisy industries:</p><ul><li>Construction and engineering</li><li>Factories and mills</li><li>Processing</li><li>Shipbuilding</li><li>Stone cutting and quarrying</li><li>Textile mills</li><li>Power stations</li><li>Steelworks</li></ul>`
                  },
                  {
                    question: "What are the signs of work-related hearing loss?",
                    answer: `<p><strong></strong>While it's natural for your hearing to get slowly worse as you age, there are distinct signs that can indicate noise induced hearing loss. These include:</p><ul><li>Mishearing words when you're in conversation</li><li>Being unable to hear conversations where there is background noise</li><li>Having to turn the television up more than others find necessary or needing subtitles </li><li>Missing out on lots of things your partner tells you in passing</li></ul><p>If you recognise any of these signs of hearing loss - and believe that your job, or a previous job may be to blame - it's worth asking your GP to arrange a proper hearing test. If you're lucky, you may just have a little wax in the ears. And if not, it may be worth talking to a specialist solicitor about claiming for compensation on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>.</p>`
                  },
                  {
                    question: "Does my employer have a duty of care?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to foresee the harm that could be caused by your occupation, or to ensure that you were protected from damaging noise levels, they could be said to have failed in their duty of care. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">Read more on the health and safety regulations</a> that your employer should be adhering to in the workplace and talk to a specialist solicitor to find out if you believe your employer failed in their duty of care. </p>`
                  },
                  {
                    question: "Do I need a specialist industrial deafness compensation lawyer?",
                    answer: `<p>Industrial deafness claims are a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of success. Slater and Gordon employs specialist lawyers in every area of industrial disease compensation. Just as importantly, we understand the need to take every industrial disease claim seriously and get our clients the compensation and medical treatment they deserve.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Industrial deafness and tinnitus compensation claims (NIHL)`,
				copy: `Noise Induced Hearing Loss (NIHL) is a serious problem for sufferers and their families. If you've been affected, talk to Slater and Gordon, one of the UK's leading industrial deafness compensation specialists. We could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/industrial-deafness-tinnitus-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for Industrial Deafness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/industrial-deafness-tinnitus`,
			sideLinkList: {
				heading: `What are the effects of noise induced hearing loss?`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}