module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `causes-prevention`,
		slug: `causes-prevention`,
		parent: `food-poisoning`,
		name: `Causes of Food Poisoning and How to Prevent it`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Causes of Food Poisoning and How to Prevent it | Slater + Gordon`,
				description: `Food poisoning and food allergy reactions can have serious and lasting consequences. Find out how they occur and how to avoid them with Slater and Gordon, one of the UK's leading food poisoning compensation claims specialists.`,
				robots: ``,
                schema: [
                  {
                    question: "How can I avoid food poisoning?",
                    answer: `<p>You can't always avoid food poisoning if you're eating out or grabbing a sandwich. But viruses and bacteria that are present in the food we consume at home can also be dangerous, unless you follow a few important steps to help you avoid these common forms of food poisoning:</p><ul><li><strong>Salmonella:</strong> high risk foods for this include undercooked eggs, chicken and beef and unpasteurised milk. Cooking food well will kill the salmonella bacterium</li><li><strong>Shigella: </strong>this bowel infection lives in faeces and is usually spread by people failing to wash their hands after going to the toilet</li><li><strong>E.coli:</strong> infections can be avoided by washing hands thoroughly after going to the toilet or touching animals, particularly farm animals</li><li><strong>Giardia: </strong>this parasite is a tummy bug that causes diarrhoea, which is spread by food washed in dirty water, and people swallowing water in lakes and rivers </li><li><strong>Norovirus: </strong>also called the winter vomiting bug, this is spread by close contact with others. Stay off work and away from hospitals if you think you might have it</li><li><strong>Rotavirus: </strong>Spread by faeces, this nasty virus is common in babies and small children and is hard to avoid in nursery and daycare environments</li><li><strong>Campylobacter: </strong>The most common cause of food poisoning in the UK, it can be prevented by cooking meat and poultry thoroughly and avoiding unpasteurised milk and untreated water</li><li><strong>Cryptosporidium: </strong>Usually seen in the under-fives, this parasite is resistant to chlorine, so it can be caught in swimming pools, as well as by contact with infected lambs and calves at petting farms </li></ul>`
                  },
                  {
                    question: "What steps should you take to avoid food poisoning?",
                    answer: `<p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/food-poisoning/restaurant-guidelines/\">Restaurants should be taking a wide variety of steps to ensure their food is safe to eat</a>, however a lot of the rules and regulations they follow can easily be implemented at home to make your kitchen safer. So regardless if you are at home or if you are out and about, the below steps can help you keep safe from food poisoning.</p><ul><li><strong>Always wash your hands -</strong> It is important that you take care and do this correctly, thankfully <a href=\"https://www.nhs.uk/live-well/healthy-body/best-way-to-wash-your-hands/\">the NHS has put together this helpful guide</a>. This step alone is the most important to stop spreading a wide variety of bugs and illnesses.</li><li><strong>Thoroughly clean kitchen surfaces -</strong> Make sure you are cleaning kitchen surfaces properly, using the right chemicals for the job, especially if you have been preparing anything that is likely to be contaminated such as raw meat.</li><li><strong>Wash and change dishcloths regularly -</strong> if these are left damp and bunched up they can be a breeding ground for a wide variety of bacteria.</li><li><strong>Separate chopping boards - </strong> have a specific chopping board for raw meat and fish and do not use it to prepare anything else.</li><li><strong>Store raw meats correctly - </strong>ideally raw meats should be separated from ready-to-eat foods and should never be stored above other foods in the fridge as they can drip.</li><li><strong>Cook food thoroughly - </strong> Meat should be cooked thoroughly, regardless of whether you cooked it yourself or ordered it in a restaurant. If you aren't sure, cook it a little more to be on the safe side.</li><li><strong>Respect use by dates - </strong>they are there for a reason</li><li><strong>Make sure your fridge temperature is below 5C - </strong>The colder the temperature the slower bacteria can grow and spread</li><li><strong>Do not leave food out - </strong>If not serving food straight away, it should be cooled quickly and not left out for extended periods of time before storing.</li><li><strong>Reheat food correctly - </strong> Some foods such as rice need to be reheated thoroughly as they can sometimes contain certain bacteria that can survive the cooking process.</li></ul>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Illness and food poisoning compensation`,
				copy: `Read the below guide to know what to do if you are worried about food poisoning, what causes it and the best steps to avoid it.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			standardContent: `product-template/causes-prevention`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}