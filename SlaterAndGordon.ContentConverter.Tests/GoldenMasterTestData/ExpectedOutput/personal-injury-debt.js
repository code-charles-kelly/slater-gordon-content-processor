module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `personal-injury-debt`,
		slug: `personal-injury-debt`,
		parent: `personal-injury-claim`,
		name: `Personal Injury & Resulting Debts`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Personal Injury & Resulting Debts | Slater + Gordon`,
				description: `Unexpected injuries can lead to debt. Talk to Slater and Gordon today about starting a No Win No Fee compensation claim and avoiding debt with interim payments.`,
				robots: ``,
                schema: [
                  {
                    question: "What if a personal injury puts me in debt?",
                    answer: `<p>If you have suffered a personal injury that prevents you from working, debts can start to mount up pretty quickly. That's why, if the injury wasn't your fault, it's important to start an injury compensation claim right away, to help ensure that you aren't left out of pocket by the incident. <a href=\"https://www.citizensadvice.org.uk/benefits/sick-or-disabled-people-and-carers/employment-and-support-allowance/while-youre-getting-esa/esa-reporting-changes/\">Notifying the Department for Work and Pensions</a> as soon as possible may help you to claim essential benefits. But the best thing you can do to avoid debts due to injuries is to talk to expert solicitors who can seek rightful compensation on your behalf: including interim payments whenever possible, to help you avoid financial hardship while your claim is being processed.</p>`
                  },
                  {
                    question: "What are interim payments?",
                    answer: `<p>When the other side has admitted blame - this is known as accepting liability - it may still take time to determine your full compensation entitlement, especially when your injuries are severe. In this situation, you may be able to make a claim for payments to assist you financially. These payments are known as 'interim payments' and can assist with any immediate costs, such as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">house or living modifications required</a> or any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">medical or rehabilitation</a> expenses, whilst you wait for the full amount to be decided and settled.</p>`
                  },
                  {
                    question: "What sort of financial losses can I claim for?",
                    answer: `<p>If a personal injury wasn't your fault, you shouldn't be left out of pocket in any way. So once you suffer an injury, it's essential to start logging all of the extra expenses it causes - from medical treatment to taxis to and from medical appointments - and to write down details of any income you have lost due to the injury. When your claim comes to be settled, our objective is always to make sure that not only do you receive damages that compensate you for your pain and suffering, but you also get back <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">all of the money that the injury has cost you</a>. </p>`
                  },
                  {
                    question: "I'm in debt - what do I do?",
                    answer: `<p>If you aren't able to claim compensation and interim payments, it's very easy for debts to mount up quickly. In this situation, the most important thing is to act as quickly as possible by seeking expert advice about your debt problems. Organisations like Debt Charity <a href=\"https://www.stepchange.org/\">Step Change</a><strong> </strong>can be a big help if you are struggling to manage debts, and you can also seek help from the <a href=\"https://www.nationaldebtline.org/\">National Debtline</a> website. If you're not sure whether you're eligible to claim compensation or not, it's always worth talking to us to receive an honest, impartial opinion. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `What if a personal injury has put me in debt?`,
				copy: `Unexpected injuries can lead to debt, this page helps you understand what you can claim for and what other help you can find.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/personal-injury-debt-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/personal-injury-debt`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}