module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `occupational-asthma`,
		slug: `occupational-asthma`,
		parent: `industrial-disease`,
		name: `Occupational Asthma`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Occupational Asthma Compensation Claims | Slater + Gordon`,
				description: `Occupational asthma can be incredibly serious. If you have asthma due to airborne allergens or irritants at work, Slater and Gordon offers the claims expertise you need, as well as the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is occupational asthma, and what causes it?",
                    answer: `<p>Occupational asthma is a term used to describe a wide range of lung and breathing problems caused by airborne allergens and irritants in the workplace. Many of these conditions can be extremely prejudicial to your health, so taking precautions to protect workers from breathing in harmful particles or chemicals is a key part of your employer's duty of care. Where they've been negligent in this duty, you may be able to claim compensation for occupational asthma.</p>`
                  },
                  {
                    question: "What are the main types of occupational asthma?",
                    answer: `<p>Broadly speaking, occupational asthma can be split into two main categories:</p><ul><li><strong>Allergic occupational asthma:</strong> this is the most common type of occupational asthma, which occurs due to airborne allergens. Not only does this cause allergic asthma at the time, it can make you hypersensitive to that substance in the future</li><li><strong>Irritant-induced asthma:</strong> less commonly, but even more dangerously, victims will have breathed in airborne chemicals in the form of gas, vapour or smoke. Even a brief exposure to concentrated irritants like these can lead to occupational asthma</li></ul><p>If you are concerned about your lungs or breathing, it is also worth reading our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/silicosis/\">guide to Silicosis</a>.</p>`
                  },
                  {
                    question: "What are the symptoms of occupational asthma?",
                    answer: `<p>The symptoms of occupational asthma are broadly the same as ordinary asthma, usually including:</p><ul><li>Excessive wheezing</li><li>Tightness in the chest</li><li>Regular coughing</li><li>Shortness of breath</li></ul><p>If you suffer from these symptoms, you should seek a medical opinion as soon as possible. In the event that your doctor diagnoses occupational asthma, you might then wish to consider making a claim for compensation. </p>`
                  },
                  {
                    question: "What type of occupation can cause occupational asthma?",
                    answer: `<p>As occupational asthma is caused by airborne irritants and allergens, workers in many industries can be at risk. However, in addition to any working environment where dust and chemical use is prevalent, occupational asthma claims are most common among workers in these occupations:</p><ul><li>Construction and demolition</li><li>Baking and four processing</li><li>Vehicle spray painting</li><li>Metal making</li><li>Treatment and process operations</li></ul>`
                  },
                  {
                    question: "How is my employer responsible?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to foresee the harm that could be caused by your occupation, or provide the necessary protective gear, they could be said to have failed in their duty of care. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">Read more on the health and safety regulations</a> that your employer should be adhering to and talk to a specialist solicitor to find out if you might have a claim.</p>`
                  },
                  {
                    question: "Do I need a specialist occupational asthma lawyer?",
                    answer: `<p>Industrial asthma claims are quite a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of successful claims. Slater and Gordon employs specialist lawyers in every area of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">industrial disease compensation</a> claims. Just as importantly, we understand the impact that a diagnosis of occupational asthma can have on those diagnosed and their families, and aim to be as considerate and supportive as possible throughout the claims process.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Occupational asthma compensation claims`,
				copy: `Airborne allergens and chemical particles in the air can lead to occupational asthma. If you've been affected, talk to Slater and Gordon, one of the UK's leading industrial disease compensation specialists. Ask us about our No Win No Fee agreements.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/occupational-asthma-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for occupational asthma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/occupational-asthma`,
			sideLinkList: {
				heading: `Read more on industrial disease compensation claims in our expert guides and FAQs below`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}