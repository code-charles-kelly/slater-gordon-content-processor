module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `military`,
		slug: `military`,
		parent: `accident-at-work-compensation`,
		name: `Military Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Military Injury Compensation Claims | Slater + Gordon`,
				description: `Military life can be dangerous, even when you aren't in combat. If you have been hurt in a military accident, ask about making a No Win No Fee compensation claim through Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for a military accident?",
                    answer: `<p>Serving personnel have just as much right to claim compensation for injuries sustained in accidents - or as the result of negligence - as anyone else. We have considerable experience of claiming compensation against the MoD and other organisations for accidents suffered by military personnel, including serving members of the RAF, Army, Navy and Reserve Forces, as well as for retired military personnel and their families. </p><p>Over the years, we've helped clients bring claims for injuries caused by everything from defective equipment and military training accidents to road traffic accidents. Whether the consequences of a military accident are relatively mild or totally catastrophic, you deserve a prompt and understanding service to obtain your rightful compensation as quickly as possible. </p>`
                  },
                  {
                    question: "What sort of injuries can I claim for?",
                    answer: `<p>There are various circumstances in which military personnel can bring a claim for compensation as a result of a military accident or injury, including:</p><ul><li>Training accidents and unsafe working practices</li><li>Injury caused by <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/defective-equipment/\">unsafe or defective military equipment</a></li><li>Injury from unsafe or defective military accommodation</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/industrial-deafness-tinnitus/\">Noise induced hearing loss (deafness)</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/fatal/\">Fatal accidents</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a> during manoeuvres</li><li><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Clinical negligence</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/ptsd/\">PTSD</a></li></ul><p>We can also help with <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">claims where rehabilitation is needed</a>.</p><p>However your military accident - or that of a loved one - occurred, our military compensation claims specialists are ready and waiting to talk to you. </p>`
                  },
                  {
                    question: "The Law and Injury Claims Against the MoD",
                    answer: `<p>Since 1987, <a href=\"http://www.legislation.gov.uk/ukpga/1987/25/introduction\">the Crown Proceedings (Armed Force) Act 1987</a>, has allowed military personnel to claim for personal injuries sustained during service, where the MoD failed in its legal duty of care or health and safety duties.</p><p>So while injuries sustained during an active operation or ongoing combat can't be claimed for, as they're covered by 'combat immunity', circumstances surrounding injuries you are able to claim for can be complex. That's why you should feel free to talk to our military compensation experts whenever you feel that negligence may have played a part in any military injury.</p>`
                  },
                  {
                    question: "The Armed Forces Compensation Scheme (AFCS)",
                    answer: `<p>There are a number of Government compensation schemes for injured forces personnel, which are administered by Veterans UK. Slater and Gordon have military experts who <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/\">specialise in making AFCS claims</a> on behalf of those who have suffered injury or illness as a result of their military service. </p>`
                  },
                  {
                    question: "What are the time limits for a military claim?",
                    answer: `<p>For most claims in the UK, there is a strict time limit of three years in which to begin your claim, so you should ask for legal advice as soon as possible after an accident or after becoming aware of health problems resulting from your military service. Please note however that there are exceptions to this rule. It's therefore best practice to speak to a specialist lawyer as soon as practically possible.  </p>`
                  },
                  {
                    question: "How much is my military accident claim worth?",
                    answer: `<p>As you would expect, every injury claim is different, with the final figure for compensation dependent upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a> as well as the extent of recovery that may be possible. Yet our expertise is not just in winning significant compensation for our clients, it's also in understanding the need for rehabilitation and the support that you and your family will need right away, and perhaps for the rest of your lives. That's why we take the majority 9of military injury cases on a No Win No Fee basis, and why obtaining interim payments to help you avoid hardship is often central to our approach.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Military accident claims`,
				heading: `Military accident claims`,
				copy: `Military life can lead to accidents and injuries that have nothing to do with combat operations. If you've been injured in a military accident or suffered as a result of negligence, talk to us today. Slater and Gordon handles most military accident compensation cases on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/134406019.jpg`,
					medium: `/public/images/bitmap/medium/134406019.jpg`,
					small: `/public/images/bitmap/small/134406019.jpg`,
					default: `/public/images/bitmap/medium/134406019.jpg`,
					altText: `Military father sitting at home with family`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee military injury claims`,
				leftWysiwyg: `product-template/military-two-panel-cta`,
				rightHeading: `Choose Slater and Gordon to start your military accident claim`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `PTSD claims`,
						url: `/personal-injury-claim/accident-at-work-compensation/military/ptsd/`,
						icon: `head-and-brain`
					},
					{
						title: `Noise induced hearing loss`,
						url: `/personal-injury-claim/industrial-disease/industrial-deafness-tinnitus/`,
						icon: `nihl`
					},
					{
						title: `Armed forces compensation scheme`,
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						icon: `military`
					}
				]
			},
			standardContent: `product-template/military`,
			sideLinkList: {
				heading: `Questions about military accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military accident FAQ`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `What is the armed forces compensation scheme?`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			imageTextA: {
				section: `Text only`,
				heading: `Common military accidents we can help with`,
				copy: `We can help with claiming compensation for a wide range of military accidents and injuries. We offer a free consultation to evaluate the circumstances of your military accident injury claim, and to help you understand whether or not you have a case. Here are the most common military injuries we can help with:`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/224340648.jpg`,
					medium: `/public/images/bitmap/medium/224340648.jpg`,
					small: `/public/images/bitmap/small/224340648.jpg`,
					default: `/public/images/bitmap/medium/224340648.jpg`,
					altText: `Man with prosthesis tying his shoelace`
				},
				video: false
			},
			videoTextB: {
				section: ``,
				heading: `Watch Dean's story`,
				copy: `Dean was discharged from the army on medical grounds after suffering from noise induced hearing loss. Whilst he was offered protective equipment it was not sufficient for the level of noise he was submitted to.`,
				image: false,
				video: `cfo9es19vh`
			}
		}
	}
}