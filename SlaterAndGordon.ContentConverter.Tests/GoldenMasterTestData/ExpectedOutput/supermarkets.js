module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `supermarkets`,
		slug: `supermarkets`,
		parent: `injury-in-public`,
		name: `Supermarket Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Supermarket Injury Claims | Slater + Gordon`,
				description: `Supermarkets aren't always super safe. If you have been injured or made ill on a shopping trip, you may be entitled to claim compensation for your injuries. Talk to us today about No Win No Fee claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been injured or made ill in a supermarket. Can I claim?",
                    answer: `<p>A surprising number of injuries occur in Britain's supermarkets, thanks largely to slips on wet floors and trips on unexpected obstacles. Your natural instinct may be to simply pick yourself up and get on with your day. However, slips and trips like these can lead to long term injury problems, so it's worth getting an expert opinion on your injury, especially if it may mean you having to pay for physiotherapy or other medical treatment at some stage. So if you have been injured in a supermarket and someone else's negligence was to blame, you should talk to us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee injury compensation claim</a> today.</p>`
                  },
                  {
                    question: "What sort of accidents happen in supermarkets?",
                    answer: `<p>Due to the fact that supermarkets are full of bottles and cartons of liquid, spills are inevitable, which are a recipe for disaster when combined with shiny supermarket floors. But spills aren't the only cause of supermarket accidents; uneven floors, hidden obstacles and even unstable shelves and displays can all lead to injuries. </p>`
                  },
                  {
                    question: "When might you have a claim for a supermarket accident?",
                    answer: `<p>As with all compensation claims, the key factor lies in whether someone has been negligent. So if a liquid has just been spilled, for example, and you slip on it, it would be hard to prove that the supermarket owners have been negligent. However, if a spill had occurred some time ago, and hadn't been signposted or cleaned up, that would suggest quite a high degree of negligence. That's because any supermarket has to do the following to keep customers safe:</p><ul><li>Follow the guidelines set by the <a href=\"http://www.hse.gov.uk/retail/slips-and-trips.htm\">Health and Safety Executive</a></li><li>Clear hazards or spillages quickly</li><li>Mark hazards or spillages</li><li>Maintain structures, or any equipment that could cause you harm</li><li>Keep the overall environment safe and free from contamination</li></ul><p>Where they have failed to do so, and you have been harmed as a result, you may well be able to claim injury compensation based on the supermarket's negligence. Find out more about starting a No Win No Fee compensation claim. </p>`
                  },
                  {
                    question: "How do I make a supermarket injury claim?",
                    answer: `<p>Every successful compensation claim rests on being able to prove that the incident occurred and that someone else was negligent. So if you have been injured in a supermarket, it is essential to:</p><ul><li>Tell staff immediately and ask them to record the incident</li><li>Take photographs of the site of the accident immediately, before warning signs are put in place or the obstacle has been removed</li><li>Take photographs of your injuries</li><li>Get copies of all medical records that relate to your injury</li><li>Get details of any witnesses</li><li>Write a record of what happened as soon as possible</li><li>Note if there are any CCTV cameras that may aid your case</li></ul><p>We offer a more in depth guide of what to do in the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-injury-guide/\">event of an injury in a public place here</a>.</p><p>Doing all of this is the best way to prove negligence. That's because if you have slipped on a liquid spillage and there is no sign to warn you of its presence, the supermarket is more likely to be found negligent. You should then give all of this information to your solicitor when you begin your compensation claim.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Claims for injuries sustained in supermarkets`,
				copy: `If you have been hurt or made ill by a visit to a supermarket, and someone else's negligence was to blame, you may be entitled to claim compensation. Slater and Gordon is one of the UK's most experienced No Win No Fee injury compensation claims firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/supermarkets-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/supermarkets`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}