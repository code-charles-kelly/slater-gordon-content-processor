module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `water-contamination`,
		slug: `water-contamination`,
		parent: `illness`,
		name: `Water Contamination`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Water Contamination Compensation | Slater + Gordon`,
				description: `If you have been made ill due to contaminated water, here's all you need to know about your potential right to compensation. Find out about No Win No Fee water contamination compensation claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is water contamination?",
                    answer: `<p>The most common cause of water contamination is Cryptosporidium. This bacteria is found in faeces, and then passed on, due to poor hand washing regimes, either in drinking water or through water used to wash food. </p>`
                  },
                  {
                    question: "What are the symptoms?",
                    answer: `<p>If you have become ill with a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/food-poisoning/\">Cryptosporidium infection</a>, you will often experience a range of symptoms, including:</p><ul><li>Diarrhoea</li></ul><ul><li>Stomach cramps </li><li>Nausea</li><li>Vomiting</li><li>Fever</li><li>Weight loss</li></ul><p>If your doctor takes a stool sample during your illness, they should be able to confirm the presence of Cryptosporidium.</p>`
                  },
                  {
                    question: "Who is responsible if I become infected?",
                    answer: `<p>Obviously, it can be hard to prove that a specific glass of water you drank, or food that was washed in contaminated water, caused your illness. However, if you have the receipt for drinks or a meal at the establishment you believe was responsible for the water contamination, we may be able to prove a link, owing to the incubation period of the illness and the proof of your presence at the scene of the infection.</p>`
                  },
                  {
                    question: "When do waterborne contaminations occur?",
                    answer: `<p>These variety of illnesses are usually caused by microorganisms inside the water. The most common causes are:</p><ul><li>Untreated sewage entering the water source</li><li>Animal excrement</li><li>Poor water filtration and treatment</li><li>Contamination from old or leaky pipes</li></ul><p>In some instances even chemical poisoning from a nearby factory entering the water can also be a cause for illness. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Contaminated water compensation`,
				copy: ``,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/water-contamination-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/water-contamination`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}