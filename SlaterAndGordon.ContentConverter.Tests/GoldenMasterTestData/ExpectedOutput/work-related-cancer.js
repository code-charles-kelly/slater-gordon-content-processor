module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `work-related-cancer`,
		slug: `work-related-cancer`,
		parent: `industrial-disease`,
		name: `Occupational Cancer`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Occupational Cancer Compensation Claims | Slater + Gordon`,
				description: `Occupational cancers can have devastating effects . If you or a loved one suffers from cancer that may be work-related, Slater and Gordon offers the expertise and support you need, as well as the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is occupational cancer?",
                    answer: `<p>Occupational cancer is the term used to describe any cancer that has been caused by carcinogens in the workplace. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/\">Asbestos is one of the prime carcinogens</a> responsible for occupational cancer diagnoses, but there are many others. Naturally, your lifestyle or genetic makeup can affect your chances of developing cancer, but all of these carcinogens are also recognised to cause work-related cancers:</p><ul><li>Arsenic</li><li>Asbestos</li><li>Benzidine</li><li>Cadmium</li><li>Formaldehyde</li><li>Leather Dust</li><li>Rubber production</li><li>Trichloroethylene</li><li>UV Radiation</li><li>Vinyl Chloride</li><li>Wood Dust</li></ul><p>So if a current or previous employer has negligently exposed you to any of these carcinogens - or another that's specific to your industry - we may be able to claim compensation on your behalf.</p>`
                  },
                  {
                    question: "What are the most common occupational cancers?",
                    answer: `<p><strong></strong>It's important to understand that 'cancer' isn't a single disease. It develops when cells in the body grow in an abnormal  and uncontrolled manner. One of the reasons that the cause of an occupational cancer can be proven is that certain carcinogens lead to specific cancers. A few examples of these are:<strong></strong></p><p><strong>Lung Cancer:</strong> commonly caused by exposure to asbestos, but also by carcinogenic chemicals such as cadmium </p><p><strong>Bladder Cancer:</strong> sometimes caused by exposure to benzidine as well as other aromatic amines used in dyes and pigments</p><p><strong>Throat/Laryngeal Cancer:</strong> linked with exposure to acid mists</p><p><strong>Nasal/sinus Cancer</strong>: often associated with exposure to leather and wood dust</p><p><strong>Liver Cancer:</strong> exposure to vinyl chloride used to make PVC</p><p><strong>Skin Cancer:</strong> associated with exposure to mineral oils and coal-tar distillation</p><p>If you've been diagnosed with cancer and believe it may have been caused by exposure to carcinogens in the workplace, talk to a specialist solicitor today.<strong></strong></p>`
                  },
                  {
                    question: "How common is occupational cancer?",
                    answer: `<p>According to <a href=\"https://www.iosh.co.uk/books-and-resources/our-oh-toolkit/occupational-cancer.aspx\">figures from the Institution </a><a href=\"https://www.iosh.co.uk/books-and-resources/our-oh-toolkit/occupational-cancer.aspx\">of Occupational Safety and Health (IOSH)</a>, 5% of UK cancer deaths  - around 8,000 every year - are caused by exposure to carcinogens in the workplace. This means that 40 times more deaths are attributable to occupational cancer than to accidents in the workplace. If you believe that you or a loved one has developed cancer due to employer negligence in allowing exposure to carcinogens, you may be entitled to claim compensation to assist with medical bills, loss of earnings and other expenses.</p>`
                  },
                  {
                    question: "How could my employer be responsible?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to protect you from exposure to carcinogens, they could be said to have failed in their duty of care. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">Read more on the health and safety regulations</a> designed to keep you safe in the workplace and talk to a specialist solicitor to find out if your employer  - past or present - failed in their duty of care.</p>`
                  },
                  {
                    question: "Do I need a specialist occupational cancer lawyer?",
                    answer: `<p>Occupational cancer claims are a very specialised area of the law, particularly as it may be necessary to prove a link to carcinogenic exposure that occurred many years ago. Slater and Gordon employs specialist lawyers who understand all the complexities and difficulties of these <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">industrial disease cases</a>. Just as importantly, we understand the devastating impact that a diagnosis of occupational cancer can have on those diagnosed and their families, and strive to be as considerate and supportive as possible throughout the claims process and beyond. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Occupational cancer compensation claims`,
				copy: `Occupational cancer affects the lives of thousands of people in the UK. Slater and Gordon is one of the UK's leading occupational cancer compensation legal firms, with specialist solicitors who could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/work-related-cancer-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for occupational cancer compensation`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/work-related-cancer`,
			sideLinkList: {
				heading: `Do you have more questions on industrial disease compensation claims? Read our expert guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}