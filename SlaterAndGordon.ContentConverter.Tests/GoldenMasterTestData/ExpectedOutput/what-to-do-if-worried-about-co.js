module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `what-to-do-if-worried-about-co`,
		slug: `what-to-do-if-worried-about-co`,
		parent: `carbon-monoxide`,
		name: `What to do if you Suspect Carbon Monoxide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What to do if you Suspect Carbon Monoxide | Slater + Gordon`,
				description: `Carbon monoxide is a colourless, odourless gas that can kill.This short Slater and Gordon guide suggests the safest course of action if you suspect that you are subject to carbon monoxide poisoning in your home.`,
				robots: ``,
                schema: [
                  {
                    question: "What do I do if I suspect carbon monoxide poisoning?",
                    answer: `<p>Carbon monoxide is completely odourless and colourless, so the first suspicion that it is present may be if you feel unexpectedly drowsy or develop a sudden headache.</p><p>The full list of symptoms to look out for is as follows:</p><ul><li>Headaches</li><li>Unexpected drowsiness</li><li>Dizziness or confusion</li><li>Nausea and sickness</li><li>Tiredness and lethargy</li><li>Shortness of breath</li><li>Clumsiness and loss of coordination</li><li>Abdominal pain</li><li>Loss of consciousness</li></ul><p>As soon as you suspect there is a problem, you should open the windows and/or doors to get fresh air into the room. You should also turn off any items such as gas fires, stoves or boilers that could be creating the gas. If you feel unwell, you may also wish to <a href=\"https://www.nhs.uk/conditions/carbon-monoxide-poisoning/\">check the NHS advice online</a> to help you decide whether your exposure has been low or high.</p>`
                  },
                  {
                    question: "Are there other signs of a carbon monoxide buildup?",
                    answer: `<p>If you suspect that you may have a carbon monoxide buildup in your home, but aren't suffering clear physical symptoms, here are a few tell-tale signs that you might want to watch out for:</p><ul><li><strong>Yellow flames on gas fires, boilers or hobs: </strong>gas flames burn blue when they are mixed with oxygen and are combusting properly. A yellow or orange flame suggests that not all of the gas is being burned properly and carbon monoxide could be being generated</li><li><strong>Dark staining around hobs, ovens and boilers: </strong>gas flames that burn cleanly won't leave sooty deposits, whereas they will on poorly serviced and dangerous appliances</li><li><strong>Pilot lights that go out a lot: </strong>the pilot light on a properly serviced fire or boiler will be blue, and should rarely, if ever, blow out. If your pilot light needs frequent re-lighting, it could indicate a problem</li><li><strong>Condensation in windows: </strong>natural gas<strong> </strong>has a high water content, which will show as condensation on windows and other areas if it is not all being burned off on an appliance</li></ul><p>You should also bear in mind that coal fires and wood stoves can cause a buildup of carbon monoxide if the room they are in is not adequately ventilated.</p>`
                  },
                  {
                    question: "How do I reduce the risk of carbon monoxide poisoning?",
                    answer: `<p>All gas appliances need to be regularly serviced to keep them safe. So whether you are a homeowner or a renter, make sure that a qualified gas engineer services all of your appliances regularly. </p><p>If you are renting, your Landlord is legally obliged to have all gas appliances, pipework, chimneys and flues checked <a href=\"https://www.gassaferegister.co.uk/help-and-advice/carbon-monoxide-poisoning/\">every 12 months by a </a><a href=\"https://www.gassaferegister.co.uk/help-and-advice/carbon-monoxide-poisoning/\">Gas Safe engineer</a>. So your landlord should always be able to show you a CP12 Certificate to prove that this has been done within the last 12 months. Read about your <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">Landlord's other safety obligations </a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">here</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Urgent action to take if you suspect carbon monoxide poisoning`,
				copy: `Carbon monoxide gas is often called 'the silent killer', because it is completely colourless and odourless. The first indications you may have of its presence are feelings of drowsiness and a headache. If you suspect its presence, you must get fresh air immediately.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/what-to-do-if-worried-about-co-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/what-to-do-if-worried-about-co`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}