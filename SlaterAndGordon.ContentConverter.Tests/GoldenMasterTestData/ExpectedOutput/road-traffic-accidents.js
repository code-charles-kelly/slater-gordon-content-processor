module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `road-traffic-accidents`,
		slug: `road-traffic-accidents`,
		parent: `personal-injury-claim`,
		name: `Road Traffic Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Road Traffic Accident Compensation Claims | Slater + Gordon`,
				description: `If you have been injured in a road traffic accident, this is what you need to know about your possible right to compensation. Find out how to make a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How much will it cost me to claim?",
                    answer: `<p>The vast majority of our claims for road traffic accidents are conducted on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee</a> basis. This means that we bare all the upfront costs, meaning that there will only be a fee if we win your case, so you will never be out of pocket.</p>`
                  },
                  {
                    question: "What can I claim for?",
                    answer: `<p>As well as the costs of any medical bills, lost income and vehicle repairs <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">there are a variety of different elements you can claim for</a>. The amount of compensation tends to vary on the seriousness of the injury sustained and how much they affect your life.</p>`
                  },
                  {
                    question: "Injured by a Hit and Run or Uninsured Driver?",
                    answer: `<p>If you have been injured as a result of a hit and run accident, or a collision with an uninsured driver, we can often help, thanks to the <a href=\"https://www.mib.org.uk/reducing-uninsured-driving/what-we-do/\">Motor Insurers' Bureau</a> (MIB) scheme. Get in touch to ask us about making a No Win No Fee claim through the MIB. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Road traffic accident compensation`,
				copy: `Injured in a road traffic accident in the UK or abroad? The vast majority of personal injury claims that Slater and Gordon deal with are handled for our clients on a No Win No Fee basis, so there's no financial risk involved.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/road-traffic-accidents-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Car accident claims`,
						url: `/personal-injury-claim/road-traffic-accidents/car-accident-claims/`,
						icon: `car`
					},
					{
						title: `Cycling accident claims`,
						url: `/personal-injury-claim/road-traffic-accidents/cycling-accident-claims/`,
						icon: `bicycle`
					},
					{
						title: `Motorcycle accident claims`,
						url: `/personal-injury-claim/road-traffic-accidents/motorcycle-accident-claims/`,
						icon: `helmet`
					},
					{
						title: `Pedestrian accident claims`,
						url: `/personal-injury-claim/road-traffic-accidents/pedestrian-accident-claims/`,
						icon: `man-crossing-road`
					},
					{
						title: `Public transport claims`,
						url: `/personal-injury-claim/road-traffic-accidents/public-transport/`,
						icon: `car`
					},
					{
						title: `Whiplash injury claims`,
						url: `/personal-injury-claim/road-traffic-accidents/whiplash/`,
						icon: `whiplash`
					}
				]
			},
			standardContent: `product-template/road-traffic-accidents`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
			imageTextA: {
				section: `Road traffic accidents`,
				heading: `Who can claim for traffic accident injuries?`,
				copy: `It isn't just drivers who can claim for injuries sustained on the road. Road traffic accidents can cause injuries to all sorts of people, including innocent bystanders; so our friendly and approachable legal experts are always happy to discuss every case, without obligation.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/249409251.jpg`,
					medium: `/public/images/bitmap/medium/249409251.jpg`,
					small: `/public/images/bitmap/small/249409251.jpg`,
					default: `/public/images/bitmap/medium/249409251.jpg`,
					altText: `Mature man with headphones listening to music on public transport`
				},
				video: false
			},
			videoTextB: {
				section: ``,
				heading: `Joseph was hit by an SUV when he was crossing the road.`,
				copy: `After the driver denied being at fault and tried to blame Joseph, he was faced with a fight for justice.`,
				image: false,
				video: `mx8zy52iei`
			}
		}
	}
}