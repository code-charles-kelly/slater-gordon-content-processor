module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `legal-expense-insurance-claims`,
		slug: `legal-expense-insurance-claims`,
		parent: `road-traffic-accidents`,
		name: `Legal Expense Insurance`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Legal Expense Insurance Claims | Slater + Gordon`,
				description: `Legal expense insurance is an additional feature that can be added on to your motor insurance or may be automatically included. This insurance is there to help cover the costs of making a claim for compensation.`,
				robots: ``,
                schema: [
                  {
                    question: "What if legal fees are likely to be higher than the amount of fees the legal expense insurance cover?",
                    answer: `<p>If your LEI policy only covers a small amount of legal fees and your case is likely to cost more, you may be able to fund your claim on a No Win No Fee basis. </p>`
                  },
                  {
                    question: "What is the criteria which needs to be met to activate legal expense insurance cover?",
                    answer: `<p>In most cases there are three conditions which need to be met in order to activate LEI:</p><p>The accident must not have been your fault, or at least you must only be partially to blame. If you were to blame for the accident, the LEI policy will not be activated.</p><p>The claim must have a reasonable chance of success in order for the policy to be activated. Your insurers and/or your lawyers will review the case and determine whether or not the case has reasonable prospects of succeeding. The prospects of success need to be greater than the prospects of losing, i.e. usually more than 51%. For example, if the prospects of success are considered to be 65% in your favour, your insurance company are likely to activate your LEI policy. Whereas, if it's considered that you only have a 45% prospect of succeeding, your insurance company might refuse the claim.</p><p>In order for the insurance company to activate your LEI policy, they may require your legal costs to be proportionate to the amount of compensation you're likely to receive. For example, if the case is a rather complex one to prove, and therefore many hours of work need to be undertaken by your lawyer, yet the value of your compensation is estimated to be extremely low based on your injuries, then your insurers may refuse to activate your LEI policy.</p><p>The criteria above should be reviewed at the onset of your case from the evidence to hand at that time. As further evidence is gathered, outcomes to the criteria can change.</p>`
                  },
                  {
                    question: "What happens to the legal expense insurance if any of the outcomes to the criteria change?",
                    answer: `<p>As evidence for your case is gathered the outcomes to the criteria which initially activated your LEI policy can change. A case may seem quite straightforward upon initial review, for example, you have suffered a severe injury to your shoulder and the other driver was to blame.</p><p>However, the medical report may find that your shoulder injury is a pre-existing condition which has been slightly exacerbated by the accident or the engineer's report may find that in fact you were to blame for the accident.</p><p>This evidence may mean that the chances of winning your claim are lower than first thought and may result in your insurers withdrawing your LEI.</p>`
                  },
                  {
                    question: "What if the person to blame for the accident doesn't have insurance?",
                    answer: `<p>If the person to blame for the accident doesn't have insurance to make a payment of compensation, you may be able to make a claim with the Motor Insurers Bureau (MIB).</p><p>The MIB will consider claim for damage to your vehicle, damage to any property and also any claim for personal injury under the \"Uninsured Drivers Agreement\".</p><p>If the person to blame for the accident fails to stop at the scene or they're in a stolen vehicle, you can make an application for compensation with the MIB.</p><p>Claims made with the MIB for untraced drivers are made under the \"Untraced Drivers Agreement\". It's a condition when making a claim under this agreement that the incident must be reported to the police within five days if the claim is for property damage or within 14 days if the claim is for personal injury. Failing to do this may jeopardise the claim.</p><p>It's important to note that MIB claims can be rather complex and we would therefore suggest obtaining advice from an expert lawyer who specialises in road traffic accidents.</p>`
                  },
                  {
                    question: "What should someone with legal expense insurance do following a road traffic accident?",
                    answer: `<p>Regardless of whether or not you have LEI, of course, the most important thing to do is call for medical assistance where you are, or someone else at the scene is, injured. If you're medically able you should also:</p><p>Take details of the other driver/drivers involved in the accident</p><p>Contact the police</p><p>Take details of anyone who witnessed the accident</p><p>Take photographs of the scene of the accident</p><p>You should also contact your insurance company and let them know about the accident and provide them with any details or evidence you've managed to obtain.</p>`
                  },
                  {
                    question: "How is a lawyer chosen when you have legal expense insurance?",
                    answer: `<p>If you have LEI and contact your insurance company, they may recommend a law firm on their legal panel. If you already have a law firm in mind or have already contacted a law firm which is not on their panel, you can make a request with your insurance company to instruct the firm of your choice.</p><p>Whether you choose a firm from the panel or a firm of your choice, you should ensure that they're experts in road traffic accidents. Where your injuries are life changing, you should also ensure that the law firm you choose specialises in dealing with claims arising from <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">serious injuries</a>, for example, brain injuries or spinal cord injuries. Ensuring the firm has this specialism can have a significant impact on the outcome of your claim.</p>`
                  },
                  {
                    question: "If you have legal expense insurance what happens initially when you instruct a lawyer in a road traffic accident claim?",
                    answer: `<p>Whether your lawyer has been chosen by you independently or chosen from your insurers' panel the first thing your lawyer should do is take a full account from you as to what happened and the evidence that is currently available, such as any photographs you were able to obtain from the site of the accident or a police report if one is available.</p><p>From this, your lawyer will be able to provide your insurance company with information to enable them to review their criteria for the LEI policy.</p><p>As soon as your insurance company have confirmed that they're happy for your lawyer to proceed, further evidence, such as a medical report, can be sought.</p>`
                  },
                  {
                    question: "What compensation can be claimed if your case is funded by legal expense insurance?",
                    answer: `<p>Compensation for a road traffic accident claim is split into the following categories:</p><p><strong>General damages</strong></p><p>General damages are awarded based on the pain and suffering and you've suffered as a result of the accident and also on any effect this has had on your quality of life. This amount can vary significantly depending on the severity of your injuries. Lawyers use previous cases similar to yours and guidelines in order to value the general damages of your case.</p><p><strong>Special damages</strong></p><p>Special damages are expenses which have, or will, put you out of pocket as a result of your road traffic accident. Such expenses include:</p><ul><li>Travelling expenses</li><li>Replacement of property or clothing, where lost/damaged</li><li>Loss of earnings</li><li>Loss of pension</li><li>Treatment and rehabilitation costs</li><li>Care costs</li><li>Aids and equipment</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">Property or adaptations </a></li></ul><p>Where the injuries are such that you will continue to have losses in the future, these can also be included. For example, if you're no longer able to work, future loss of earnings and future loss of pension can be included, and where you need future treatment, care and rehabilitation, the cost of this can also be included. Read more on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">what you can claim for here</a>.</p>`
                  },
                  {
                    question: "What if you don't have legal expense insurance?",
                    answer: `<p>If you find that you don't have LEI, you can still obtain funding for a road traffic accident under a Conditional Fee Agreement (CFA), also known as a No Win No Fee agreement. This essentially means that if your case is unsuccessful, you'll not have to pay for any of your legal costs, meaning there's no financial risk to you. For more information about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee agreements, click here</a>. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `A guide to legal expense insurance`,
				copy: `Legal expense insurance is a feature of some motor insurance policies and is there to help you cover and legal costs of making a claim.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/legal-expense-insurance-claims-two-panel-cta`,
				rightHeading: `Find out more about uninsured driver accident compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/legal-expense-insurance-claims`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}