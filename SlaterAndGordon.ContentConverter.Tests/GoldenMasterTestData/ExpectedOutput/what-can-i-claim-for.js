module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `what-can-i-claim-for`,
		slug: `what-can-i-claim-for`,
		parent: `personal-injury-claim`,
		name: `What Can I Claim For? Damages Explained`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What Can I Claim For? Damages Explained | Slater + Gordon`,
				description: `If you or a loved one have been injured, you need to know what you can claim for. Talk to us today about the reasons you might start a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of damages can I claim for an injury?",
                    answer: `<p>When you've suffered an injury or illness which was caused by someone else's negligence, you may be entitled to claim compensation, which is otherwise known as 'damages'. </p><p>In the UK, there are two types of damages which are claimed - general damages and special damages. </p><p>The first part of a compensation claim is general damages. This award of compensation relates directly to injury and covers any pain and suffering which you have incurred and also any loss of amenity - meaning any affect your injury has had on your quality of life. The amount of general damages claimed will therefore depend on the extent of your pain, suffering and loss of amenity. There are judicial guidelines in place on the amount of general damages which can be claimed, and these together with previous reported cases relating to similar injuries are relied upon when making a claim for general damages. </p><p>The second part of a compensation claim is special damages. Special damages are broadly designed to help put you back in the financial position that you would have been in, had you not sustained your injury. They are widely referred to as 'out of pocket expenses' and cover not only expenses that you have already incurred as a result of your injury, but also any expense you may have in the future. These can include:</p><ul><li>Loss of earnings - this includes not only any lost earnings you have already incurred but if you're unable to return to work as a result of your injuries, future loss of earnings can also be claimed</li><li>Treatment, rehabilitation and care costs - this will cover any treatment, rehabilitation and care costs you have already incurred and also any you may need in the future, whether on a temporary basis or for the rest of your life </li><li>Aids, equipment and adaptations - this covers aids and equipment which may be necessary both now and in the future. It also covers any necessary <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">adaptations you may require to your house</a>, for example, having doors widened for wheelchair use, etc. and in some cases where adaptations are not possible in a property, it can also cover the cost of a new home </li><li>Loss of property - this covers any loss of property which has incurred as a result of an accident, for example, damaged clothing caused in a road traffic accident. </li><li>Travel expenses - this covers any additional travelling expenses which have been incurred as a direct result of the incident. For example, if you have had to get taxis to various medical appointments </li></ul>`
                  },
                  {
                    question: "When do you receive your compensation?",
                    answer: `<p>Once your general damages and special damages have been agreed between the parties involved, you will receive your final settlement payment. In some cases, where the other side has accepted responsibility for your injuries, you may be entitled to interim payments. These are payments made prior to final settlement being reached and can help with any ongoing expenses you may have. </p><p>In some cases where there are future costs, such as care costs, payments known as periodical payments may also be awarded as part of the overall settlement of damages. These are regular payments which are paid over a long period, in some cases for a lifetime, to ensure that all future needs are financial secure. </p><p>However, every injury compensation claim is different, so to fully understand what sort of damages you might be entitled to claim, talk to one of our expert claims solicitors</p>`
                  },
                  {
                    question: "How much is my serious injury claim worth?",
                    answer: `<p>Every injury claim is different, with the final figure for compensation <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">dependent upon the severity of the injury</a> and the possible extent of recovery, as this will affect both general and special damages. However, our expertise is not just in winning compensation for our clients, it is understanding the need for rehabilitation and the support that you and your family need. That's why we take most injury cases on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, and why obtaining interim payments to help you avoid hardship is often a key part of our approach.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `What can I claim for in a personal injury case?`,
				copy: `A guide explaining the difference between general and special damages; helping you claim for everything from medical bills to lost earnings.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/what-can-i-claim-for-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/what-can-i-claim-for`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}