module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `cycling-accident-claims`,
		slug: `cycling-accident-claims`,
		parent: `road-traffic-accidents`,
		name: `Cycling Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Cycling Accident Compensation Claims | Slater + Gordon`,
				description: `If you've been injured while cycling, you may be entitled to claim compensation for your injuries. Talk to Slater and Gordon today about making a No Win No Fee compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "How long does a cycling injury claim take?",
                    answer: `<p>There's no hard and fast rule here, as every case depends on the seriousness of your injuries and whether or not a third party accepts liability. </p>`
                  },
                  {
                    question: "Do I have to pay legal fees when claiming for a cycling accident?",
                    answer: `<p>Personal injury cases arising from cycling accidents are conducted on a No Win No Fee basis which means if your case isn't successful, you don't need to pay anything. This means there is no financial risk to you when making a claim for compensation.</p><p>If you're a Cycling UK member, you will have access to Slater and Gordon's Cycling UK Incident Line which offers a number of benefits to Cycling UK members. This includes a guarantee that Slater and Gordon will not charge you anything if you win your case, meaning that you'll keep 100% of your compensation. <a href=\"https://www.slatergordon.co.uk/unions-federations-and-charities/cycling-uk/cycling-uk-incident-line/\">Find out more about Cycling UK members' legal benefits here</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road Traffic Accident Claims`,
				heading: `Cycling Accident Claims Solicitors`,
				copy: `On Britain's busy roads, cyclists are incredibly vulnerable. Not only can drivers often fail to leave room or pay sufficient attention, potholes pose an ever-increasing risk of injury. So if you have been injured while cycling, you may be entitled to claim compensation for your injuries. Most of our clients claim on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/cycling-accident-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee cycling accident claims.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/cycling-accident-claims`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}