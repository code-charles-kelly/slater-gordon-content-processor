module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `sports-injury`,
		slug: `sports-injury`,
		parent: `injury-in-public`,
		name: `Sports Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Sports Injury Compensation | Slater + Gordon`,
				description: `Most sports have some element of danger, whether you are there to watch or take part. So if you have been injured and it wasn't your fault, talk to Slater and Gordon today about making a No Win No Fee compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of sports injuries can I claim for?",
                    answer: `<p>Most people play or watch sports for the excitement they bring, so it's hardly surprising that injuries occur almost everywhere that sports are enjoyed. Sometimes these are caused by simple accidents, with no one at fault. But where they are caused by recklessness or negligence, you may be able to claim compensation for your injuries. Sporting activities include:</p><ul><li><strong>Gyms and sports centres: </strong></li></ul><p>Many people can get injured at the gym, or taking part in a sporting activity at a sports centre, as a result of not listening to their doctor's advice or over exerting themselves. However, when training staff don't provide proper instructions, such as how to use equipment safely, and this causes you injury, you may be entitled to make a claim for compensation.  </p><p>Equipment and facilities should also be adequately maintained. If your injury occurs as a result of faulty equipment or slipping on a wet surface, you may be entitled to claim compensation. </p><ul><li><strong>Horse riding:</strong> </li></ul><p>Equestrian accidents are unfortunately common across the UK, with an approximate three million horse riders. The equestrian community are well aware that horse riding can be a very dangerous sport, where accidents can occur through no fault of anyone else. However, when instructors, stable owners, other riders or even <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">road users have been negligent</a> and you sustain an injury as a result, you may be able to claim compensation. </p><p>If someone else is to blame for providing faulty saddles and riding equipment or not implementing proper safety procedure, and you suffer an injury as a result, you may be able to make a claim. We can help with a wide variety of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/animals/\">animal injury compensation claims</a>.</p><ul><li><strong>Snow sports: </strong></li></ul><p><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/winter-skiing/\">Snow sports, such as skiing and snowboarding</a><strong></strong>, are popular sports, both in the UK and abroad, and can also be dangerous. If you suffer an injury as a result of faulty equipment, a fall on the slopes, inadequate training, others skiing or snowboarding recklessly, or any other reason which was not your fault, you may be able to make a claim. </p><ul><li><strong>Motorsport: </strong>There are many types of motorsports, from Formula 1 racing to go karting. Given the nature of the sport, there is obviously a dangerous element involved when taking part. There are occasions when accidents can occur due to no fault of the driver, but as a result of someone else's negligence. Where this is the case, you may be able to make a compensation claim for any injuries you've sustained as a result. </li></ul><p>The most common causes of motorsport accidents which result from someone else's negligence include: not providing adequate protective equipment for drivers (including helmets, gloves and overalls), not providing adequate supervision, not providing proper instructions of use and safety and not ensuring that the vehicles are properly maintained and are in full working order. </p><ul><li><strong>Football / rugby: </strong></li></ul><p>Accidents do happen as part of the game though most occur within the bounds of the rules and what's considered acceptable play. However, if serious incidents occur which are considered outside the bounds of acceptable play, such as dangerous tackles, reckless manoeuvres or violent, retaliatory behaviour, you may be entitled to make a claim. Negligent training such as poor or confusing coaching instructions, reckless training exercises and inadequate warm-up or preparation for matches can also lead to injuries which you may be entitled to claim for compensation. </p><p>Facilities should also be maintained to a good quality. If injuries are caused as a result of poor facilities, such as glass or chemicals on the pitch, spillages on indoor courts, badly maintained pitches which cause slips, trips and falls, you could be entitled to make a claim.</p><p>These are just a few examples however, if you believe you have a claim, speak to one of our sports injury compensation experts today. </p>`
                  },
                  {
                    question: "What if I have signed a waiver?",
                    answer: `<p>It isn't uncommon for sports venues or instructors to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/signed-a-waiver/\">have you sign a waiver that says you accept all responsibility for injuries</a> arising out of your presence at or participation in a sport. While these are all well and good, they do not provide any sort of legal defence against negligence such as poor crowd control, faulty equipment or a lack of basic safety measures being taken. So if you have been injured while taking part in even sports such as rock climbing or mountain biking, or while watching a sporting event, talk to us to find out if you may have a claim.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Sports injury compensation claims`,
				copy: `Accidents happen, especially when sports are involved. If you have been injured as a spectator or participant due to someone else's negligence, you may be able to claim compensation. Talk to Slater and Gordon: one of the UK's leading injury compensation firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/sports-injury-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/sports-injury`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}