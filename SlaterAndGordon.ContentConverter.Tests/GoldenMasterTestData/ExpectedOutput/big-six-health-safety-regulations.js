module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `big-six-health-safety-regulations`,
		slug: `big-six-health-safety-regulations`,
		parent: `accident-at-work-compensation`,
		name: `The HSE Six Pack`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `The HSE Six Pack | Slater + Gordon`,
				description: `The Six Pack comprises the most commonly quoted Health and Safety regulations. They're designed to keep you safe at work. We're here when they don't. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What are the Six Pack health and safety regulations?",
                    answer: `<p>The Six Pack is the term generally used for the six most commonly quoted health and safety regulations. These came into effect following six EU directives in 1993, and were updated in 1999. They comprise the:</p><ul><li><a href=\"http://www.legislation.gov.uk/uksi/1999/3242/contents/made\">Management of Health and Safety at Work Regulations 1999</a><strong>: </strong>the main set of regulations</li><li><a href=\"http://www.hse.gov.uk/msd/backpain/employers/mhor.htm\">Manual Handling Operations Regulations</a><strong>:</strong> covering handling of heavy or awkward loads</li><li><strong>Display Screen Equipment (DSE) Regulations:</strong> covering the safe use of computer screens and keyboards</li><li><strong>Workplace (Health, Safety and Welfare) Regulations:</strong> covering the environments people are asked to work in</li><li><strong>Provision and Use of Work Equipment Regulations:</strong> covering the suitability of equipment in the workplace</li><li><strong>Personal Protective Equipment (PPE) Regulations</strong>: covering the use of protective equipment</li></ul><p>These regulations are all essential to us as accident work specialists, because they clearly set out every employer's legal obligations when it comes to keeping people safe. In doing so they help make it possible for us to prove negligence on your behalf where these obligations haven't been met.</p>`
                  },
                  {
                    question: "Want to know more about safety regulations?",
                    answer: `<p>While compensation and rehabilitation is absolutely essential, it's better to avoid illness and injuries as much as possible in the first place. So if you would like to know more about the sort of accidents that lead to injury claims in the workplace, here are a few relevant guides to things covered by the <a href=\"http://www.hse.gov.uk/pubns/hse40.pdf\">Employers' Liability (Compulsory Insurance) Act 1969</a>, and the <a href=\"http://www.hse.gov.uk/legislation/hswa.htm\">Health and Safety at Work Act</a> 1974.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Understanding the six pack health and safety regulations`,
				copy: `This is our brief, explanatory guide to the Six Pack: the most commonly quoted Health and Safety regulations.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/big-six-health-safety-regulations-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/big-six-health-safety-regulations`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}