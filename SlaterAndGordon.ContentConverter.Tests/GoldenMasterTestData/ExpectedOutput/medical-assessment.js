module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `medical-assessment`,
		slug: `medical-assessment`,
		parent: `personal-injury-claim`,
		name: `Do I Need a Medical Assessment?`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Do I Need a Medical Assessment? | Slater + Gordon`,
				description: `Been injured in an accident? There's nothing to fear from your free medical assessment. Here's all you need to know. Slater and Gordon is one of Britain's foremost injury compensation claim legal firms, with the experience to help you receive the compensation your injuries deserve.`,
				robots: ``,
                schema: [
                  {
                    question: "Why would I need a medical assessment of my injury?",
                    answer: `<p>If you've been injured in an accident that wasn't your fault, you deserve to receive the appropriate amount of compensation. But that figure can only be calculated if we know exactly how serious that injury is, how long it's likely to take to settle down and if there will be any long lasting or permanent effects of your injury. That's why medical assessments are crucial to every injury compensation claim. </p>`
                  },
                  {
                    question: "Do you need permission to see my medical records?",
                    answer: `<p>Absolutely. We take your privacy very seriously - as does the law - so we'll always ask permission for us to access your medical records and to provide them to the health professional carrying out the assessment.</p>`
                  },
                  {
                    question: "What happens at a medical assessment?",
                    answer: `<p>Before we agree a final compensation figure on your behalf, we need to know that it will be enough to compensate you for the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">injuries you have suffered</a>, as well as for any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation or medical treatment</a> that might be necessary in the future. So we will usually arrange for you to see an independent medical expert, who will spend 20 minutes or so examining you to assess the seriousness of your injuries, how long they might take to settle, if there will be any long lasting or permanent effects of the injuries and also, whether a course of physiotherapy or another type of rehabilitation would be advisable. The medical expert will then send us their assessment and recommendations - with your permission of course - which we will rely on to help negotiate a settlement on your behalf, or present as evidence in court.</p>`
                  },
                  {
                    question: "Do I have to pay for my medical assessment?",
                    answer: `<p>We handle the vast majority of our personal injury compensation claims on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>. So when this is the case, we will pay for your medical assessment as part of our service, and reclaim the cost from the other side.</p>`
                  },
                  {
                    question: "How should I approach my medical assessment?",
                    answer: `<p>There's absolutely no need to be nervous about a medical assessment to help with your injury claim. All of the medical professionals we might send you to are hugely experienced, and will only perform the tests needed in order for them to understand your condition. The important thing is just to be honest about your injury, which will help them to make the right assessment, and us to get you the compensation you deserve.</p>`
                  },
                  {
                    question: "How do I start a personal injury claim?",
                    answer: `<p>If you've been injured through the negligence or recklessness of someone else, you may well be entitled to claim compensation for your injuries. As one of Britain's leading personal injury firms, we can handle everything on your behalf, including any necessary medical assessment to help you achieve the compensation you deserve. We also handle the majority of personal injury cases on a No Win No Fee basis, to give you as much security as possible. Start your claim today by talking to us about your injury. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Why would I need a medical assessment?`,
				copy: `Been injured in an accident? There's nothing to fear from your free medical assessment. Here's everything you need to know.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/medical-assessment-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/medical-assessment`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}