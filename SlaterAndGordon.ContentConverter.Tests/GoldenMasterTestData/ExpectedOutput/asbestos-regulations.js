module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestos-regulations`,
		slug: `asbestos-regulations`,
		parent: `asbestos-mesothelioma`,
		name: `UK Asbestos Law & Regulations`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `UK Asbestos Law & Regulations | Slater + Gordon`,
				description: `Asbestos is still a massive problem in the UK, demanding strict regulation. Here we briefly explain the UK's asbestos regulations. Slater and Gordon also offers you extensive experience of successful asbestosis compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What are the The UK Asbestos Regulations?",
                    answer: `<p>Asbestos was widely used in a variety of construction materials throughout the 20th century. So while its use has been banned since 1999, many buildings - both residential and commercial - still contain a lot of asbestos materials. These are generally safe as long as they remain undisturbed. However, where demolition or renovation projects are taking place, particularly in large commercial buildings such as offices and factories, the UK has strict regulations to control how, and by whom, asbestos is removed, called the <a href=\"https://www.aic.org.uk/asbestos-regulations-2012/\">Control of Asbestos Regulations 2012</a>. Where these regulations have not been followed, and workers have been exposed to asbestos dust and fibres, it's often possible to make a claim for asbestos related illnesses such as mesothelioma, asbestos related lung cancer, asbestosis and pleural thickening.</p>`
                  },
                  {
                    question: "What do the UK Asbestos Regulations insist on?",
                    answer: `<p><strong></strong><a href=\"http://www.legislation.gov.uk/uksi/1999/2373/made\">Since 1999</a><strong></strong>, the asbestos safety regulations have stated all of the below:</p><ul><li>If existing asbestos containing materials are in good condition and are not likely to be damaged, they may be left in place; but their condition must be monitored and managed to ensure they are not disturbed</li><li>If you're responsible for maintenance of non-domestic premises, you have a 'duty to manage' the asbestos in them</li><li>If you want to do any building or maintenance work in premises, or on a plant or equipment that might contain asbestos, you need to identify where it is, its type and its condition; assess the risks, and manage and control these risks</li><li>The requirements for licensed work remain the same: in the majority of cases, work with asbestos needs to be done by a licensed contractor. If you're carrying out non-licensed asbestos work, this still requires effective controls.</li><li>The control limit for asbestos is 0.1 asbestos fibres per cubic centimetre of air (0.1 f/cm3)</li><li>Training is mandatory for anyone liable to be exposed to asbestos fibres at work. </li></ul><p>However, the regulations were updated in 2012, and now additionally state that:</p><ul><li>From 6 April 2012, some non-licensed work needs to be notified to the relevant enforcing authority.</li><li>From 6 April 2012, brief written records should be kept of non-licensed work, which has to be notified </li><li>By April 2015, all workers/self employed doing notifiable non-licensed work with asbestos must be under health surveillance by a doctor</li></ul><p>If you want to know more about the UK's asbestos regulations, you will find them fully explained on the <a href=\"http://www.hse.gov.uk/asbestos/regulations.htm\">HSE website.</a> And if you believe that you have been made ill by an employer failing in their duty of care to follow these regulations, you should talk to a specialist asbestos claims solicitor right away.</p>`
                  },
                  {
                    question: "How dangerous is asbestos?",
                    answer: `<p>Asbestos was first associated with serious illnesses as far back as 1924, though it was not until 1999 that its use was banned in the UK. While asbestos has superb fire-retardant qualities, it also contains tiny fibres that when breathed in over a prolonged period can have extremely damaging effects on the lungs, leading to diseases including mesothelioma, asbestos related lung cancer, asbestosis and pleural thickening. If you or a loved one has developed any of these conditions, you may be able to claim asbestos related illness compensation. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestosis-mesothelioma/asbestos-timeline-history/\">Read more on the history of asbestos here</a>.</p>`
                  },
                  {
                    question: "Who do the UK Asbestos Regulations affect?",
                    answer: `<p>In short, if you have a 'duty to manage' the maintenance of any non-domestic building, you are also responsible for making sure that it's safe and undamaged, or that it is removed safely. It's also useful to understand that even though it's been illegal to use asbestos in the UK since 1999, some rogue builders and contractors are known to have used it since then. So even when managing a commercial premises built since 2000, care needs to be exercised to ensure that asbestos is not present. The Health and Safety Executive says that if you have a 'duty to manage', you must:</p><ul><li>take reasonable steps to find out if there are materials containing asbestos and if so, its amount, where it is and what condition it is in</li><li>presume materials contain asbestos unless there is strong evidence that they do not</li><li>make, and keep up-to-date, a record of the location and condition of the asbestos- containing materials - or materials that are presumed to contain asbestos</li><li>assess the risk of anyone being exposed to fibres from the materials identified</li><li>prepare a plan that sets out in detail how the risks from these materials will be managed</li><li>take the necessary steps to put the plan into action</li><li>periodically review and monitor the plan and the arrangements to act on it so that the plan remains relevant and up-to-date</li><li>provide information on the location and condition of the materials to anyone who is liable to work on or disturb them</li></ul><p>However, if you are in any doubt about the existence, extent or safety of asbestos in a building that you manage, the HSE says that you should arrange for a <a href=\"http://www.hse.gov.uk/asbestos/surveys.htm\">Management Survey.</a></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `The UK asbestos regulations explained`,
				copy: `Asbestos is still a massive problem in the UK, demanding strict regulation. Here we explain the UK's asbestos regulations in brief.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/asbestos-regulations-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/asbestos-regulations`,
			sideLinkList: {
				heading: `Do you have more questions on asbestos and mesothelioma claims? Read our expert guides below for more information`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `Asbestos - What do you need to know?`,
				copy: `Our personal injury lawyer Kim Harrison explains what asbestos is, why it can still be found and where it is usually found.`,
				image: false,
				video: `acsomcpmhe`
			}
		}
	}
}