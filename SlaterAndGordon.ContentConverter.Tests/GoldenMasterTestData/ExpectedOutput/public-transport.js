module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `public-transport`,
		slug: `public-transport`,
		parent: `road-traffic-accidents`,
		name: `Public Transport Accidents & Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Public Transport Accidents & Injury Claims | Slater + Gordon`,
				description: `Injuries on Public Transport occur far more often than you would think. If you were injured while travelling on public transport and the accident was not your fault our No Win No Fee solicitors can help you win the compensation you deserve.`,
				robots: ``,
                schema: [
                  {
                    question: "We can help if you were injured on public transport",
                    answer: `<p>Injuries on public transport might seem to be rare, however they happen quite frequently. Not only can collisions occur, but public transport such as trains and trams may also have, mechanised doors, luggage racks above head height from which items could fall and various other scenarios that could cause injuries, some of which could be life changing. </p><p>We find many people assume there is no one to blame, and simply get up and get on with their life, even after needing to go to hospital. Our expert lawyers can help advise you to whether or not you may have a claim, so make sure you get in touch today.</p>`
                  },
                  {
                    question: "What are the most common injuries on public transport?",
                    answer: `<p>Injuries on public transport vary massively, there is a wide variety of accidents and equipment that could potentially cause injury, below are the most common scenarios our clients have had to deal with:</p><ul><li>You may have <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/whiplash/\">suffered whiplash</a> after the bus or coach you where on collided with another vehicle</li><li>Injuries sustained during a train crash or a train coming to a sudden stop</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/claiming-against-uber/\">Crashes whilst in an uber or taxi as a passenger</a></li><li>Limbs getting trapped between a tram and the platform</li><li>Violence and abuse from other passengers</li></ul><p>No matter how you were hurt, if it was not your fault you may be owed compensation. Our trained team of legal experts are waiting for your call to offer a free telephone consultation.</p>`
                  },
                  {
                    question: "I was hurt at work, can I still claim?",
                    answer: `<p>In the vast majority of cases if a member of the public is injured on or by a form of transport, the transport company's <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">public liability insurance</a> can be claimed against to pay for any compensation owed, however this does not cover employees. If you work for a transport company and you were injured at work, legally your employer has to have <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers liability insurance</a> to cover them against any compensation claims. Therefore you will still be able to make a claim. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Click here for more information on claiming for an injury at work</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Public transport injury compensation claims`,
				copy: `Injuries on public transport occur far more often than you would think. If you were injured while travelling on trains, busses, taxis or any other form of public transport Slater and Gordon can help you claim the compensation you deserve.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/public-transport-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/public-transport`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}