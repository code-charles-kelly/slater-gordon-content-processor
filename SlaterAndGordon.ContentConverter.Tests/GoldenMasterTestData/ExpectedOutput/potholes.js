module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `potholes`,
		slug: `potholes`,
		parent: `council-highways-agency`,
		name: `Pothole`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Pothole Compensation Claims | Slater + Gordon`,
				description: `Injured by a pothole? Your local council may be responsible. Here's all you need to know about pothole claims. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been injured by a pothole. What are my rights?",
                    answer: `<p>If you're a cyclist it's quite likely that hitting a pothole in a badly-maintained road will at least damage your bike, and quite possibly cause you injury as a result.<strong> </strong>It's also likely when driving that if you hit a pothole, you may damage your wheel or possibly suspension. In either case, your local council may be liable to pay you compensation. Importantly, if you have sustained any personal injury as a result of hitting a pothole, you should speak to an expert claims solicitor right away. </p>`
                  },
                  {
                    question: "What is a pothole?",
                    answer: `<p>Pothole is the standard name for any opening in a road surface caused by water seeping into cracks, before freezing in cold weather causing the water to expand and open up fissures and holes. In some cases, these are so insignificant that you can drive or ride over them with ease; however, some potholes are so deep that they are highly likely to cause damage to vehicles and in some cases cause serious injury, especially if you're cycling. There is no fixed definition for the size of a pothole, but most councils define them as being at least 40mm deep and will resist compensation claims where they are shallower than this.</p>`
                  },
                  {
                    question: "What do I do if I hit a pothole?",
                    answer: `<p>Unfortunately, if you are a cyclist you are quite likely to come off your bike and suffer an injury if you hit a pothole. However, it is important to record details of the pothole as soon as possible: to ensure that it can't harm anyone else as much as to enable you to claim for injuries and damage to your bike. So if you are unable to record details of the pothole, you may have to get a friend or family member to carry out the recommended actions below as soon as possible – <strong>ensuring that no-one places themselves in harms way to do so.</strong></p><p>1. <strong>Assess the damage to your car or bike:</strong> even if you are not injured, you still need to ensure that your car or bike is safe to proceed.</p><p><strong>• </strong>Drivers should make sure that tyres are still intact, with no 'steel belt' showing and no bulges in the sidewalls. You should also grip either side of the tyre with both hands and check that there is no significant movement that could indicate wheel bearing or suspension damage</p><p><strong>• </strong>Cyclists should inspect their bikes carefully, making sure that not only are the tyres undamaged, but that forks are straight and handlebars are properly aligned</p><p>If you are satisfied that your car or bike is safe and undamaged, you should then proceed with caution, though drivers should listen for unusual noises, and watch out for other signs of damage, such as the car pulling to one side when braking. You should also note the location of the pothole so that you can inform the local council of its existence as soon as possible to help keep others safe. You can also report the pothole using this<a href=\"https://www.gov.uk/report-pothole%5D\"> </a><a href=\"https://www.gov.uk/report-pothole%5D\">handy tool</a> on the Government's website.</p><p>2. <strong>Get the contact details of any witnesses:</strong> if there were any witnesses to the accident, it is a good idea to ask them for their names and contact details, as their statements may be crucial to the success of any claim.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Pothole injury compensation claims`,
				copy: `If you have had a pothole-related accident - as a cyclist, motorist or pedestrian - here's what you need to know about claiming against your local council.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/potholes-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/potholes`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}