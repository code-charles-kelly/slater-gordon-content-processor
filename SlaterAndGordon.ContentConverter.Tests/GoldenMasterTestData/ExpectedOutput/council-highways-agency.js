module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `council-highways-agency`,
		slug: `council-highways-agency`,
		parent: `injury-in-public`,
		name: `Councils & Highways Agencies Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Councils & Highways Agencies Injury Claims | Slater + Gordon`,
				description: `Hurt by a loose paving stone or a pothole? Your local council or the Highways Agency may be responsible. Here's what you need to know about making a claim. Talk to us today about No Win No Fee claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been injured by a pavement or pothole. What are my rights?",
                    answer: `<p>The law demands that local councils and the Highways Agency keep roads and pavements in a good condition. Unfortunately, this is a huge task, and faulty sections of paving and dangerous potholes exist all over the UK, leading to a multitude of accidents and injuries every year. In cases where a pothole causes damage your car, you are best-advised to take it up with your local council directly, using <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/council-highways-agency/potholes/\">the advice we give here</a>. However, if tripping on an unsafe pavement or perhaps <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/cycling-accident-claims/\">cycling into a pothole on the road</a> has caused you an injury, we may be able to make a compensation claim on your behalf if it can be proven that the council has been negligent.</p>`
                  },
                  {
                    question: "How do I know if the council has been negligent?",
                    answer: `<p>There is no universal standard for how smooth and even roads or pavements have to be, but most councils accept that a pothole 4cm (approximately 1.6 inches) deep constitutes a hazard, as does a paving stone that is sticking up by at least one inch, or 2.54cm. This means that we are more likely to be able to prove negligence against a council or the Highways Agency on your behalf where the pothole or pavement that caused your injury meets or exceeds these measurements.</p><p>Naturally, this means you will have to have photographic evidence of the piece of road or pavement that led to your injury, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/council-highways-agency/potholes/\">as we explain in more detail here</a>. In addition to uneven surfaces, you may also have a claim if your injury was caused by roadworks or a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">slip, trip or fall</a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\"> </a><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">on council property</a><strong></strong>. </p>`
                  },
                  {
                    question: "What are my local authority responsible for?",
                    answer: `<p>If you have an accident that takes place on any public land or council property, you may be able to seek compensation from the local authority. That's because they are legally responsible for making sure that all roads, pavements and even bridle paths are properly maintained and kept clear of defects that might cause accidents. Where there has clearly been negligence by the council or local authority, you have every right to talk to our specialist 'public place' injury solicitors. </p>`
                  },
                  {
                    question: "How much could my claim be worth?",
                    answer: `<p>How much your claim might be worth depends largely on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">how serious your injuries are</a> and how long a full recovery might be expected to take. However, in addition to damages for pain, suffering and loss of amenity, most claims include a sum to cover any loss of earnings as well as the cost of any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">medical treatment, physiotherapy or other necessary rehabilitation</a>.</p>`
                  },
                  {
                    question: "Is there a time limit for compensation claims?",
                    answer: `<p>There is a time limit of three years from the date of any accident for you to start a compensation claim, though you can be allowed longer if you are<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\"> claiming on behalf of a child or someone with mental impairment</a>. You should also bear in mind that most injury compensation claims will require evidence to be presented, so you will need to make sure you have photographs, witness statements and medical evidence available to support your claim, even if the accident occurred up to three years ago. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Councils and highways agencies personal injury claims`,
				copy: `If you have been injured by faulty pavements or potholes in the road, you may be able to claim compensation for your injuries from the council or Highways Agency.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/council-highways-agency-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/council-highways-agency`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}