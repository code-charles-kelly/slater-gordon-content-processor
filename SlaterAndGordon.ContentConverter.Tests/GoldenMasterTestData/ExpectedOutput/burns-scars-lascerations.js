module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `burns-scars-lascerations`,
		slug: `burns-scars-lascerations`,
		parent: `serious`,
		name: `Burns & Lacerations Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Burns & Lacerations Injury Compensation | Slater + Gordon`,
				description: `Burns, scars and lacerations can all be very serious. Overcoming these injuries demands expert medical help, as well as legal assistance to secure the funding you may need. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I get compensation for burns, scars or lacerations?",
                    answer: `<p>While early medical intervention is crucial if you have suffered burns, scarring or a serious laceration, immediate legal help is also essential. That's because all of these injuries have the potential to affect your life and finances, thanks to the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">recovery time and rehabilitation they can require</a>. So where someone else is to blame for serious injuries like these, you owe it to yourself - or your loved one, if you're <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">acting on their behalf</a> - to talk to specialist compensation solicitors as early as possible.</p><p>Our legal experts can help with injury claims resulting in burns scars and lacerations that have occured in many different scenarios, but the most common tend to be:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Accidents at work</a></li><li><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Medical negligence</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Military</a></li></ul>`
                  },
                  {
                    question: "How much can I claim for a burn, laceration or scarring?",
                    answer: `<p>As with all serious injuries, the amount of compensation that you might be able to claim depends on a number of factors. These include the seriousness of the injury, the prognosis for recovery and the amount of rehabilitation or other long-term care you might need. However, where liability is accepted by the party at fault, our specialist injury claim solicitors <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/interim-payments/\">will usually seek interim payments</a> to help pay for urgent medical treatment and loss of earnings at the earliest possible stage.</p>`
                  },
                  {
                    question: "How will I pay for long-term care?",
                    answer: `<p>If you or a loved one has received a serious burns injury or laceration, it's natural that in addition to worrying about your recovery, you'll be concerned about how to pay for long-term care, should it be required. That's why expenses such as the cost of nursing care, medical costs and aftercare over the injured person's lifetime are generally part of the calculation when a compensation settlement is being considered. Expenses such as those for rehabilitation and<strong> </strong>even <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">modifications to accommodation</a> can also be included in your claim. </p>`
                  },
                  {
                    question: "How are burn injuries categorised?",
                    answer: `<p>When someone suffers a burn injury, the level of severity is categorised as first degree, second degree or third degree. These mean: </p><ul><li><strong>First degree burns:</strong> damage to the outer layer of skin only, with severe pain at first but a good chance that scarring won't occur</li></ul><ul><li><strong>Second degree burns: </strong>damage to two layers of skin leading to blistering. If the burn is to more than 10% of the body, patients may go into shock due to fluid loss</li></ul><ul><li><strong>Third degree burns: </strong>destruction of all<strong> </strong>three layers of skin, causing serious trauma and often permanent scarring</li></ul><p>Naturally, the degree and extent of a burn will greatly affect the prognosis for recovery, which will also impact on the amount of rehabilitation you may need and the compensation you might receive. Once you have taken the first step to contact us about making a claim, we will then arrange a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/medical-assessment/\">medical consultation to assess the extent of you injury</a> which will be done at no cost to you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Burn, scar and laceration injury compensation claims`,
				copy: `If you've suffered burns, scars or lacerations, the consequences can have a serious impact on your life. That's why you need immediate support from a specialist solicitor as well as medical specialists. Slater and Gordon is one of the UK's leading injury compensation firms for burns, scars and lacerations.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/burns-scars-lascerations-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/burns-scars-lascerations`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}