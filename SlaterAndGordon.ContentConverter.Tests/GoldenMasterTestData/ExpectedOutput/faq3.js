module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq3`,
		slug: `faq`,
		parent: `sexual-criminal-abuse`,
		name: `Sexual & Physical Abuse Claims FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Sexual & Physical Abuse Claims FAQs | Slater + Gordon`,
				description: `Sexual and physical abuse cases are sensitive and complicated matters. This guide includes some of the most commonly asked questions about cases of abuse together with answers provided by our legal experts.`,
				robots: ``,
                schema: [
                  {
                    question: "Will it cost me anything to make a compensation claim?",
                    answer: `<p>Slater and Gordon understand that the cost of legal services can be a worry, but there are several options available to fund cases of abuse. The majority of cases are funded under a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee Agreement</a> which means if the case is unsuccessful, you will not have to pay anything.</p><p>Criminal Injury Compensation claims (CICA) are funded by a Collective <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">Conditional Fee Agreement</a> which means if the case is unsuccessful, you will not have to pay anything.</p><p>Public funding from the <a href=\"https://www.gov.uk/government/organisations/legal-aid-agency/about\">Legal Aid Agency</a> is also available for a small amount of cases of abuse which depends on your financial circumstances and the individual facts of the case. We'll be able to explain the funding options available once we've reviewed your case.</p>`
                  },
                  {
                    question: "Can I make a claim for compensation if I don't live in the UK?",
                    answer: `<p>It doesn't matter where you live. If the abuser or the organisation we're pursuing for compensation lives or is based in the UK, you can still make a claim.</p>`
                  },
                  {
                    question: "Can I make a claim for compensation if the abuser doesn't live in the UK?",
                    answer: `<p>If the abuse occurred in the UK, a claim can still be made but the abuser we're pursuing for compensation must be identifiable and traceable and have the means to satisfy any judgment debt awarded to you in the case.</p><p>If the abuse occurred outside the UK and the abuser we're pursuing for the abuse lives or is based overseas, although it may be more difficult to justify bringing a claim in the UK, it's not impossible. Our team of experts will confirm whether or not it's possible to make a claim once they've reviewed the case.</p>`
                  },
                  {
                    question: "Is there a time limit on making a claim?",
                    answer: `<p>There are Acts of Parliament in place which set time limits for making a compensation claim. These vary for different circumstances and there's a discretion with cases of abuse and sexual assault to allow the case to proceed outside the time limit in certain circumstances. Even if the incident happened many years ago, a claim may still be possible. It's therefore important that, as soon as you're able, you seek advice from a specialist abuse and sexual assault lawyer who's familiar with this type of work and the specialist area of law relating to time limits.</p><p>Once our lawyers have reviewed your case, they'll be able to advise you on time limits in your case.</p>`
                  },
                  {
                    question: "Will criminal proceedings affect my claim?",
                    answer: `<p>We would always encourage anyone who's suffered from abuse or sexual assault to report the incident to the police if you haven't already done so. Criminal proceedings are completely separate from compensation claims but the outcome of criminal proceedings may be relevant to your case.</p>`
                  },
                  {
                    question: "Can I make a claim if the abuser hasn't been found guilty or convicted?",
                    answer: `<p>You may be able to make a claim if the abuser is found not guilty as there can be various reasons why they haven't been convicted. The evidential burden in a civil case is lower and the issues are often different to those in a criminal case. Our experts will be able to advise further on this once we know the circumstances of the case.</p>`
                  },
                  {
                    question: "Will I have to visit my lawyer's office?",
                    answer: `<p>We understand that visiting a lawyer to talk about what happened to you can be daunting which is why we'll meet you at a place convenient and comfortable for you.</p>`
                  },
                  {
                    question: "Will I have to go to court?",
                    answer: `<p>The vast majority of compensation claims successfully conclude before reaching the final court hearing which means there would be no need for you to attend. Of course there are occasions when cases do go to court but it is your lawyers' job to fully support and represent you through this process which is very different to a criminal trial. Our specialist abuse lawyers will be able to advise further on this once they know the circumstances of the case.</p>`
                  },
                  {
                    question: "Will I have the right to anonymity?",
                    answer: `<p>If you're a victim of a sexual offence, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/\">you have a guaranteed legal right to anonymity</a>. This means that the media can't publish your name or any other details which might identify you unless you choose to waive your anonymity.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Sexual and Physical Abuse Claims FAQs`,
				copy: `The most commonly asked sexual and physical abuse claims questions answered by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq3-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq3`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}