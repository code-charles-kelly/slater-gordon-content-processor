module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `guide-to-insurance-and-motor-claims`,
		slug: `guide-to-insurance-and-motor-claims`,
		parent: `road-traffic-accidents`,
		name: `Road Accident Insurance & Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Road Accident Insurance & Claims Guide | Slater + Gordon`,
				description: `If you've been involved in a road traffic accident, the process of dealing with insurers and instructing lawyers can be a daunting process. This is a simple guide to explain the process which will hopefully put your mind at ease.`,
				robots: ``,
                schema: [
                  {
                    question: "What is an insurance policy excess?",
                    answer: `<p>Most insurance policies have an \"excess\" which is an amount of the claim which you're responsible for. For example, if you make a claim for the repair of your vehicle, which the garage has confirmed will cost £4,000 and you have an excess of £200, you will have to make a payment for the first £200 of the claim and the insurance company will pay the additional £3,800. </p><p>In most cases, you will choose the amount of excess you pay when you initially take out your policy. In general, the higher the excess you agree to be responsible for, the lower the cost of the insurance premium.</p>`
                  },
                  {
                    question: "Is there criteria that must be met when making a claim on an insurance policy?",
                    answer: `<p>Policies usually have different criteria which must be met, for example, it may state that you must make a claim within a specific time frame. It's therefore important that you read your insurance policy carefully.</p><p>It's a condition with most policies however, that you cooperate with them at all times. Failure to cooperate with them may result in a breach of your policy.</p><p>It's important that you remain at the scene when you've had an accident and exchange details with the other parties involved in the accident. If you don't exchange details at the scene, you must ensure you attend a police station to report the accident and provide your details within 24 hours following the accident.</p>`
                  },
                  {
                    question: "What will happen if you don't exchange your details?",
                    answer: `<p>If you don't exchange details at the scene or report the accident and provide your details to the police within 24 hours, you're at risk of being charged with failing to stop after the accident or failing to report an accident, both of which are offences.</p>`
                  },
                  {
                    question: "When do you report the accident to your insurance company?",
                    answer: `<p>You must notify your insurance company as soon as possible after the accident takes place regardless of whether you're notifying them for information purposes or whether you intend to make a claim as the other driver may intend to make a claim against you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `A guide to insurance and road traffic accident claims`,
				copy: `Motor insurance can be a legal minefield so we've put together a guide to help you with all the complex terminology and options that you might encounter along the way.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/guide-to-insurance-and-motor-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury compensation claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/guide-to-insurance-and-motor-claims`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}