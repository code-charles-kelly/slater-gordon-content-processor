module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `accident-fault`,
		slug: `accident-fault`,
		parent: `road-traffic-accidents`,
		name: `Who Is At Fault In A Road Traffic Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Who Is At Fault In A Road Traffic Accident | Slater + Gordon`,
				description: `Legal expense insurance is an additional feature that can be added on to your motor insurance or may be automatically included. This insurance is there to help cover the costs of making a claim for compensation.`,
				robots: ``,
                schema: [
                  {
                    question: "What should you do if you have an accident and it's your fault?",
                    answer: `<p>If you have a road traffic accident that was your fault you must contact your insurers as soon as you're able. They will then deal with the matter on your behalf.</p>`
                  },
                  {
                    question: "What if you're accused of a road traffic offence?",
                    answer: `<p>Road traffic offences are defined under the Road Traffic Acts and other relevant legislation. Usual offences are careless driving, dangerous driving and causing death by dangerous driving. <a href=\"https://www.slatergordon.co.uk/driving-offences/dangerous-and-careless-driving/\">You can read more on defending dangerous driving charges here</a>.</p><p>The police will investigate the matter to determine who was at fault. If they find that they have sufficient evidence to convict you they will forward the case to the CPS (the Crown Prosecution Service). The CPS will then decide whether you should be charged with the offence.</p><p>Depending on the terms of your insurance policy, your insurers may arrange for a lawyer to represent you when being interviewed by the police and at any court hearing. Where the offence is causing death by dangerous driving, they may also provide representation at the inquest. The maximum penalty for the offence of death by dangerous driving is a custodial sentence, meaning you may face spending time in prison. It is therefore strongly recommended that you have a lawyer present with you during any interview or hearing. </p><p>If the terms of your insurance policy don't include representation, you can seek alternative legal advice from a specialist lawyer who deals with road traffic defence.</p><p><a href=\"https://www.slatergordon.co.uk/media-centre/blog/2018/03/car-accidents-determining-fault-by-location-of-damage/\">We have written this handy guide</a>, on determining fault by location of damage to vehicles involved in a collision.</p>`
                  },
                  {
                    question: "What if you were only partially at fault for the accident?",
                    answer: `<p>There are cases where both parties are partially to blame and neither are willing to accept full responsibility for the accident. In legal terms, liability means who is at fault for the accident and where parties can't agree, this is known as a 'liability dispute'.</p><p>The way in which liability dealt with is by both parties gathering evidence in support of their liability claim. This is why it's so important to ensure you obtain the contact details of any independent witnesses who may have seen the accident take place. Photographs and other evidence taken at the scene may also assist.</p><p>Lawyers for each party will then negotiate based on their evidence. If they come to an agreement it's likely that they'll agree a split liability. This means that blame will be apportioned to each party as a percentage. For example, if you were found 25% to blame for the accident, it would be a 75/25 split on liability in your favour.</p><p>If parties are unable to reach an agreement on liability, the case would go to a liability hearing at court where a judge would make the decision.</p>`
                  },
                  {
                    question: "How does compensation work when there's a split in liability?",
                    answer: `<p>If you were to make a compensation claim, and were held 25% liable for the accident, you would only receive 75% of the compensation you were awarded. For example, if your claim was valued and settled at £10,000, the compensation which would be awarded to you would be £7,500.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Who is at fault in a road traffic accident?`,
				copy: `Determining fault can be a complex process and it isn't always immediately apparent which party may be to blame.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/accident-fault-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/accident-fault`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}