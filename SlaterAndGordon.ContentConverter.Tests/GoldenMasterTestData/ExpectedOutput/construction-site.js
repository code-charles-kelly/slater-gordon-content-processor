module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `construction-site`,
		slug: `construction-site`,
		parent: `accident-at-work-compensation`,
		name: `Construction Site Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Construction Site Injury Claims | Slater + Gordon`,
				description: `Construction sites are dangerous places. If you have been injured due to someone else's negligence, you're entitled to seek compensation. Find out about No Win No Fee construction site injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sorts of injuries can I claim for?",
                    answer: `<p>Despite all of the advances that have been made in recent years, construction sites are still among Britain's most dangerous places to work. Accidents and injuries occur all the time, and many of them are down to other people not following all of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">the safety rules</a>. The most common construction site injuries are caused by:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/defective-equipment/\">Defective equipment</a></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/electrocution/\">Electrocution</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/fall-heights-objects/\">Falling object or falls from scaffolding/heights</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/forklift/\">Forklift truck injuries</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/health-safety-dangerous-practices/\">Health and safety violations</a><strong></strong></li></ul><p>Other injuries include:</p><ul><li>Defective scaffolds</li><li>Falls down holes and shafts</li><li>Crush Injuries</li><li>Unsafe plant and machinery</li><li>Falling objects</li><li>Electric shocks</li><li>Heavy lifting</li><li>Safety harness failure</li><li>Roofs not fenced off</li><li>Forklift/dumper truck hits</li><li>Chemical spillage</li><li>Poor layout of site</li><li>Inadequate safety briefings</li></ul><p>So if any of these failures  - or any other obvious negligence - have caused you to sustain an injury on site - talk to one of our specialist lawyers about starting a compensation claim today. </p>`
                  },
                  {
                    question: "Who is responsible for building site safety?",
                    answer: `<p>The honest answer to this question is: quite a lot of people are responsible. This includes the construction company, the architect, the constructions regulations co-ordinator and the site manager. Having said that, it's always possible that your injury may have been caused by an independent contractor working on the site, or even an equipment manufacturer, in the case of a faulty tool or piece of plant. The key fact is that if your injury has been caused by someone else's negligence, you will generally have a right to claim compensation for your injuries.</p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that every employer is required to have, by law. It ensures that money is available to compensate workers if they suffer any illness or injury due to their employer's negligence. </p>`
                  },
                  {
                    question: "How much is my construction injury claim worth?",
                    answer: `<p>Every injury claim is different, and the final figure for compensation depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and how much any rehabilitation might cost. However, we take most construction site injury cases on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, and seek interim payments to help you avoid hardship if you are prevented from working as a result of your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>The law states the time limit in which to bring a claim for construction site injury compensation is generally three years from the date the accident occurred. However, there are exceptions to this rule, such as if the person injured suffers a brain injury and as a result suffers diminished mental capacity or in the event of someone's death. Also, if working abroad, the time limits differ in different countries, so it's always important that you speak to a specialist lawyer with expertise in accidents at work as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Construction site injury compensation claims`,
				copy: `Have you been injured while working on or visiting a construction site? Slater and Gordon are leading injury compensation specialists, offering a No Win No Fee service to almost all of our personal injury clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/construction-site-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/construction-site`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}