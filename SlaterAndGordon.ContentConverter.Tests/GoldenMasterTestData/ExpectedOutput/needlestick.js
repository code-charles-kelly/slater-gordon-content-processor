module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `needlestick`,
		slug: `needlestick`,
		parent: `accident-at-work-compensation`,
		name: `Needlestick Injury  | Slater +Gordon`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Needlestick Injury Compensation Claims | Slater +Gordon`,
				description: `Needlestick injuries can be dangerous and distressing. If you've suffered one due to negligence, you have every right to seek compensation. Find out about No Win No Fee needlestick injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a needlestick injury?",
                    answer: `<p>Needlestick injuries occur in any situation where your skin has been accidentally pierced by a hypodermic needle. They most commonly occur in high-pressure medical situations, when a professional receives an injury from a hypodermic needle that's intended for a patient. However, it's also possible to receive a needlestick injury from carelessly discarded needles as well as from criminal intent. Naturally, in these cases, there is a risk of serious infection as well as what can be a nasty puncture wound. If this has happened to you, you need medical and legal help as soon as possible.</p>`
                  },
                  {
                    question: "What do I do first if I have a needlestick injury?",
                    answer: `<p>Regardless of whether the injury was caused by a sterile needle or one of unknown status, it's essential to treat every needlestick injury immediately, wherever you are, and then to seek urgent help from medical experts. If you've suffered a needlestick injury, you initially need to follow these six simple steps:</p><p>1. Put the needle down somewhere safe, if it's in your possession</p><p>2. Rinse the wound with clean water, allowing blood to flow freely</p><p>3. Place antiseptic liquid or soap on the wound, but don't rub it in</p><p>4. Dry the wound with a sterile pad or dressing</p><p>5. Cover the wound with a sterile pad, dressing or plaster</p><p>6. Seek urgent help from your GP or nearest A&E department</p><p>Importantly, if it can be done safely, you should also try to preserve the needle so that it can be analysed later, especially if it was not sterile.</p>`
                  },
                  {
                    question: "What after-effects might there be from needlestick injuries?",
                    answer: `<p>Even a needlestick injury from a sterile needle can cause a nasty wound and severe bruising, and that's assuming that you did not receive any of the solution that may have been in the syringe. Where you've received a needlestick injury from a needle of uncertain origin, or one that has been used by someone else, there are clear risks of serious infection.</p><p>In cases like these serious illnesses such as <a href=\"https://www.nhs.uk/common-health-questions/accidents-first-aid-and-treatments/what-should-i-do-if-i-injure-myself-with-a-used-needle/\">Hepatitis and HIV are all potential risks</a>. That's why it is essential to seek medical treatment, and analysis of the needle concerned, whenever you have received a puncture wound from a needle. You should also seek legal advice at the earliest opportunity, in case you need to make a claim for compensation.</p>`
                  },
                  {
                    question: "What are the most common causes of needlestick injuries?",
                    answer: `<p>Most needlestick injuries occur in a medical setting, where failure to follow correct procedures – or sometimes a fraught, stressful, situation arising – can lead to medical professionals inadvertently receiving needle wounds. However, needlestick injuries happen in all these scenarios, and more besides:</p><p>• Accidentally touching an infected needle in a public place</p><p>• Retrieving a needle without protective gloves in a medical setting</p><p>• Picking up rubbish containing needles <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">without proper personal protective equipment</a></p><p>• Careless discarding of 'sharps' containers</p><p>• Malicious needlestick injuries caused by criminals</p><p>However, if you've received a needlestick injury, you need medical attention and possibly immunisation against Tetanus and Hepatitis B immediately; and then you need to seek legal advice from an expert in needlestick injury compensation claims.</p>`
                  },
                  {
                    question: "Is my employer liable for an injury like this?",
                    answer: `<p>Every employer has a duty of care to ensure that you are protected by proper health and safety procedures and protective equipment wherever there is any possibility of a needlestick injury. Where this has not been the case, you may have a case for claiming compensation on the grounds of your employer's negligence. Most employers carry <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers' liability insurance</a> to help provide compensation and rehabilitation in cases like these. </p>`
                  },
                  {
                    question: "How much is my needlestick injury claim worth?",
                    answer: `<p>Every injury claim is different, and depends on the long-term prognosis. However, where negligence has been a factor, you may be eligible to claim for loss of earnings, medical treatment and travel expenses as well as for your pain and suffering. We handle most cases of this sort on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, and seek interim payments to help you avoid financial hardship if you are prevented from working by your injury.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Needlestick injury compensation claims`,
				copy: `Have you received a needlestick injury in the course of your job? You need immediate medical and legal help. Slater and Gordon are specialist needlestick injury compensation specialists, offering a No Win No Fee service in the vast majority of our personal injury cases.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/needlestick-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/needlestick`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}