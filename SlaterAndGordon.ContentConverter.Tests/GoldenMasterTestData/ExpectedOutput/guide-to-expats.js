module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `guide-to-expats`,
		slug: `guide-to-expats`,
		parent: `asbestos-mesothelioma`,
		name: `Expats Guide to Asbestos Illnesses`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Expats Guide to Asbestos Illnesses | Slater + Gordon`,
				description: `Even if you now live abroad, if you've developed an asbestos related disease from work in the UK, you can still make a compensation claim here. Slater and Gordon offers the expertise you need, and the security of No Win No Fee compensation claim agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "Can expats claim for asbestos related illnesses?",
                    answer: `<p>Asbestos related illnesses affect many thousands of people who worked in the UK in the years up to 1999, when asbestos was finally banned. Asbestos related illnesses can take up to 20 years to develop following exposure to asbestos fibres, so many workers are only now becoming ill. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/benefits/\">Benefits and one-off payments </a>are available as well as compensation to help those affected - whether you still live in the UK or abroad - and we specialise in helping British expats to claim the compensation and benefits that are rightfully theirs. To find out more and talk to an expert in helping expats to make asbestos illness compensation claims in the UK, contact us today.</p>`
                  },
                  {
                    question: "Why call us for an expat asbestosis claim?",
                    answer: `<p>Slater and Gordon has successfully represented thousands of people who've been diagnosed with an asbestos related illnesses over the years, giving us unsurpassed knowledge of both these distressing conditions and the claims process. In addition to this, we're pleased to offer help and advice on a number of related areas, including how to claim the relevant benefits when you're living as an expatriate.</p>`
                  },
                  {
                    question: "Which countries can I claim from as an expat?",
                    answer: `<p>Over the last 20 years or so, we have represented expats with asbestos related illnesses in all of the below countries:</p><ul><li>Australia</li><li>New Zealand</li><li>USA</li><li>Canada</li><li>South Africa</li><li>Spain</li><li>Cyprus</li><li>Malta</li></ul><p>However, we can provide help and representation to anyone who has contracted an asbestos related illness while in the UK, wherever you're now living. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/\">Speak to an expert solicitor today </a>to start your expat compensation claim and find out which benefits might be available to you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestos guide for UK expats`,
				copy: `Asbestos was widely used in the UK until 1999, meaning that thousands of people, including expats, now suffer from asbestos related diseases. We're one of the UK's leading asbestos compensation specialists for expat claims, with experienced lawyers who could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/guide-to-expats-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/guide-to-expats`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}