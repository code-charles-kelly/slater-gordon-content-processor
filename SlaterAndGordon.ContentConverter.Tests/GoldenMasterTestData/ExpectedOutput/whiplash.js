module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `whiplash`,
		slug: `whiplash`,
		parent: `road-traffic-accidents`,
		name: `Whiplash`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Whiplash Compensation Claims | Slater + Gordon`,
				description: `Whiplash injuries are no laughing matter. They can cause lasting health problems and require extensive and expensive rehabilitation. Talk to us today about starting a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a whiplash injury?",
                    answer: `<p>The word 'whiplash' is very accurate. It describes the movement of the neck, when a sudden jolt or impact stretches ligaments and muscles far beyond the limits they can handle. Think of how a whip moves suddenly forwards and backwards when it is 'lashed' and then imagine that happening to your neck.</p><p>Usually occurring when a vehicle you are travelling in is hit from behind - or comes to a very abrupt halt - whiplash injuries can also be caused by a fall or a blow to the head. While many people don't regard 'whiplash' as a serious injury, the fact is that it can have very serious, distressing and long-term effects. </p>`
                  },
                  {
                    question: "What are the symptoms of whiplash?",
                    answer: `<p>The first symptoms usually appear a few hours after a car crash or fall, and may begin with a headache, and feelings of stiffness and even a burning sensation in the rear of the neck. These symptoms may prevent you from turning your head from side to side, and may even spread to your arms and shoulders. In any event, it is worth talking to us about whiplash compensation.</p>`
                  },
                  {
                    question: "Do I need to see a doctor about whiplash?",
                    answer: `<p>Whiplash symptoms develop over the first few hours and may continue to get worse for some days after an accident. If, at any time you are worried, the whiplash page on the <a href=\"https://www.nhs.uk/conditions/whiplash/\">NHS website</a> is a very useful place to start. You should then call your GP or go to A&E if you become concerned about your own or someone else's whiplash injury.</p>`
                  },
                  {
                    question: "What is my whiplash injury claim worth?",
                    answer: `<p>There is no hard and fast rule for what a whiplash compensation claim may be worth. It depends on the severity of the injury, and also how long it is likely to take to settle down. In most cases, you will be referred to a specialist to assess your whiplash injury as part of any compensation claim. You may also wish to claim for loss of earnings and the cost of any physiotherapy you may require. So it's essential to take legal advice before accepting any compensation offer from an insurer; and since most of our whiplash claims are on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, there's no financial risk in doing so.</p>`
                  },
                  {
                    question: "How long have I got to make a whiplash claim?",
                    answer: `<p>As with most accidents, the law allows you up to three years to start your claim. This is particularly useful in the case of whiplash injuries, as symptoms such as neck stiffness and headaches may not surface for some time after the original injury. However, you may have longer if you are claiming for a child or someone with a mental impairment. The time limits can vary if you are claiming through the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">Criminal Injuries Compensation Authority</a>.</p>`
                  },
                  {
                    question: "How long does whiplash compensation take?",
                    answer: `<p>With very serious whiplash injuries, it is necessary to see a medical specialist so they can assess how long it will take for the symptoms to stop; or to decide if permanent damage has been done. However, with most compensation claims that are expected to be less than £25,000, we handle every No Win No Fee whiplash claim through our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/fast-track/\">fast track team</a> to get our clients the compensation they deserve as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Whiplash claims solicitors`,
				copy: `Despite what you may read in the newspapers, whiplash injuries can have serious and lasting effects. Take legal advice before you accept any offer of whiplash compensation. The vast majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee whiplash injury`,
				leftWysiwyg: `product-template/whiplash-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee whiplash injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/whiplash`,
			sideLinkList: {
				heading: `Read more guides on road traffic accidents from Slater and Gordon:`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}