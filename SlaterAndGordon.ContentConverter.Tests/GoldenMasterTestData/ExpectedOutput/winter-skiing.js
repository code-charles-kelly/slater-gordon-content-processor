module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `winter-skiing`,
		slug: `winter-skiing`,
		parent: `holiday-accident-claims`,
		name: `Winter & Skiing Accidents`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Winter & Skiing Accidents Compensation Claims | Slater + Gordon`,
				description: `Have you ever been injured whilst skiing or on a winter holiday abroad? If it wasn't your fault you could be entitled to compensation. Slater and Gordon specialise in No Win No Fee compensation claims for accidents abroad and in the UK.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for a skiing injury?",
                    answer: `<p>We've helped a wide variety of clients to claim compensation after a skiing or winter holiday accident that was not their fault. Our clients have successfully claimed for everything from serious head and spinal injuries to broken bones resulting from snowboarding injuries. Below are some of the most common examples of accidents that may lead to a claim:</p><ul><li>Dangerous skiing or snowboarding practices by others</li><li>Unkept or poorly maintained skiing equipment</li><li>Poor quality instructions from a ski instructor </li><li>Dangerous signposting or misplacement of equipment on piste</li><li>Faulty ski lifts</li></ul><p>Whatever caused your accident, if it was caused by the negligence of another, Slater and Gordon can help you claim the compensation you deserve.</p>`
                  },
                  {
                    question: "Skiing Injury Accident Facts",
                    answer: `<ul><li>Fatigue is one of the most common factors that leads to skiing accidents </li><li>The majority of ski injuries are the result of an isolated fall. Most of these are because of user error, loss of control, travelling too fast for the conditions, etc.</li><li>10% of ski accidents arise from a collision </li><li>5% are ski lift related</li><li>5% of ski accidents are a result of equipment failure (e.g. binding releasing inadvertently)</li><li>25% of ski accidents are caused by collision with a tree, pylon or other people. Of these 60-70% require hospital admission</li><li>Skiing off-piste is one of the most surefire ways to end up in danger. If in doubt never ski or snowboard away from marked ski runs.</li></ul>`
                  },
                  {
                    question: "Does it matter if my accident happened abroad?",
                    answer: `<p>Whilst it can sometimes be more complicated to claim for an accident that happened abroad, there are a wide variety of cross country treaties that ensure it is as easy and painless as possible to make a claim for compensation. Slater and Gordon has a wealth of expertise and experience in helping our clients claim for compensation globally.</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/europe/\">Read more on claiming in the US</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/europe/\">Read more on claiming in Europe</a></li></ul>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Winter holiday and skiing accidents compensation claims`,
				copy: `If you've been injured whilst skiing or on a winter holiday and it wasn't your fault, it's possible to make a compensation claim from here in the UK. Slater and Gordon is a specialist law firm with extensive experience of helping clients make claims for compensation when they've been injured in accidents abroad.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/winter-skiing-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/winter-skiing`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}