module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `scottish-pleural-plaques`,
		slug: `scottish-pleural-plaques`,
		parent: `asbestos-mesothelioma`,
		name: `Pleural Plaques Compensation Scotland`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Pleural Plaques Compensation Scotland | Slater + Gordon`,
				description: `People in Scotland who've developed pleural plaques as a result of exposure to asbestos whilst at work are able to make a claim for compensation. Slater and Gordon's dedicated advisers explain what you may be entitled to.`,
				robots: ``,
                schema: [
                  {
                    question: "What are Pleural Plaques?",
                    answer: `<p>Pleural plaques are scarring on the pleura, which is the protective membrane surrounding the lungs. The scarring is caused by the inhalation of asbestos fibres, over a long period of time, which lodge in the pleura. Pleural plaques are not dangerous in themselves and don't become cancerous.</p>`
                  },
                  {
                    question: "What are the symptoms of pleural plaques?",
                    answer: `<p>In the majority, pleural plaques don't cause any symptoms, although some people diagnosed with pleural plaques have described slight breathlessness, and pain or discomfort when they're breathing. </p>`
                  },
                  {
                    question: "How are pleural plaques diagnosed?",
                    answer: `<p>Pleural plaques are usually diagnosed following an X-ray or CT scan. However, where there are no symptoms, pleural plaques are often found by accident when undergoing tests for another reason. </p>`
                  },
                  {
                    question: "Who can claim for pleural plaques?",
                    answer: `<p>In 2007 the House of Lords ruled that anyone who had been exposed to asbestos during the course of their employment and had developed pleural plaques would no longer be able to claim compensation as it was not considered a compensatable injury. </p><p>In 2009 the Scottish Parliament passed an act allowing Scottish people to claim compensation for pleural plaques if they've been exposed to asbestos in Scotland. This means that in England and Wales, you're no longer able to claim compensation for pleural plaques. However, if you were exposed in Scotland you can. </p>`
                  },
                  {
                    question: "Can pleural plaques develop into a more serious asbestos related illness?",
                    answer: `<p>Although pleural plaques can't develop into a more serious asbestos related illness, if you've been diagnosed with pleural plaques, this means that you have been exposed to asbestos and inhaled asbestos fibres and therefore the risk of developing another asbestos illness increases. </p><p>Other more serious asbestos related conditions include: </p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/mesothelioma/\">Mesothelioma</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-lung-cancer\">Asbestos related lung cancer </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestosis/\">Asbestosis </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/pleural-thickening/\">Pleural thickening</a> </li></ul><p>However, once your GP knows about your condition, it's likely that they'll arrange to carry out regular tests and monitor you to check for early signs of other asbestos related conditions. </p>`
                  },
                  {
                    question: "What should I do if I've been diagnosed with pleural plaques?",
                    answer: `<p>If you've been diagnosed with pleural plaques and you live in Scotland, or you've been diagnosed with pleural plaques as a result of exposure to asbestos at work in Scotland, contact us today. Our Scottish team of lawyers will take details of your condition and your exposure to asbestos and will provide expert advice on making a claim. </p><p>Slater and Gordon offer No Win No Fee agreements for pleural plaques claims, meaning if your case is unsuccessful, you won't have to pay a penny and therefore, there'll be no financial risk to you. </p>`
                  },
                  {
                    question: "What should I do if I've been diagnosed with pleural plaques and was exposed in England or Wales?",
                    answer: `<p>Although you're no longer able to make a claim for pleural plaques in England and Wales, Slater and Gordon keep an Asbestos Register for anyone who has been exposed to asbestos. This is so, in the event that you do go on to develop a serious asbestos condition, we'll have the right information to start your claim straight away. </p><p>Therefore, if you've worked with asbestos in the past, please complete our Asbestos Register.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Scottish pleural plaques compensation claims`,
				copy: `Although pleural plaques are not compensatable in England and Wales, if you've developed pleural plaques as a result of your exposure to asbestos at work in Scotland, you still have the right to claim compensation. Our specialist advisers talk you through what you may be entitled to.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee pleural plaques compensation claims`,
				leftWysiwyg: `product-template/scottish-pleural-plaques-two-panel-cta`,
				rightHeading: `Talk to us about pleural plaque claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/scottish-pleural-plaques`,
			sideLinkList: {
				heading: `Do you have more questions on asbestos and mesothelioma claims? Read our expert guides below for more information`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}