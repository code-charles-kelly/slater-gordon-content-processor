module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `public-liability-insurance`,
		slug: `public-liability-insurance`,
		parent: `injury-in-public`,
		name: `Public Liability Insurance`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Public Liability Insurance Claims | Slater + Gordon`,
				description: `What is public liability insurance? All sorts of injuries occur in public places, from supermarkets to sports grounds. Read our guide to understand how legal costs are covered when an accident happens in a public place.`,
				robots: ``,
                schema: [
                  {
                    question: "How could Public Liability Insurance help me?",
                    answer: `<p>Public liability insurance is there to protect a business in case it or its activities cause injury or damage to a third party during the course of operation. This means that if you are injured and a business is at fault, their public liability insurance will cover the cost of any claim </p><p>In many of these cases, someone will have been negligent in some way or another: from failing to put their dog on a lead to forgetting to clean up a water spill on a shiny floor in an office block. That's why everyone from local authorities to local businesses carry public liability insurance, to make sure that people like you receive appropriate compensation for injuries on their land or premises that were not your fault.</p>`
                  },
                  {
                    question: "How can I make a Public Liability Insurance claim?",
                    answer: `<p>If you have been injured through no fault of your own in a public place, it may well be the case that the owner of the building or the land has a public liability insurance policy that you can claim against to receive compensation for your injuries, loss of earnings and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">the cost of any rehabilitation</a>. This is, of course, dependent upon the policy holder having been negligent, as opposed to you being injured by something beyond their control, such as the actions of another member of the public. In the first instance, talk to one of our experts to see if you may be eligible to make a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee claim</a>.</p>`
                  },
                  {
                    question: "Is it compulsory for businesses to carry public liability insurance?",
                    answer: `<p>Most public facing businesses will consider this kind of insurance completely essential as a single trip or slip due to negligence could put the company out of business.</p><p>Unlike employers liability insurance which is compulsory for virtually every company in the UK, public liability insurance is not compulsory; however most large suppliers and contractors require some form of public liability insurance before they will agree to work with a business, making it quite common.</p>`
                  },
                  {
                    question: "What does public liability insurance cover?",
                    answer: `<p>Public liability insurance is designed to help businesses cover any legal costs and compensation claims made against them by a third party who may have suffered an injury or property damage whilst on the business premises or when a business is conducting business at their premises or home address.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `What is public liability insurance?`,
				copy: `Accidents happen, and public liability insurance often pays for the injuries they cause.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/public-liability-insurance-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/public-liability-insurance`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}