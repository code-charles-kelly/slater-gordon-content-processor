module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `mesothelioma`,
		slug: `mesothelioma`,
		parent: `asbestos-mesothelioma`,
		name: `Mesothelioma  & Mesothelioma Symptoms`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Mesothelioma Compensation Claims & Mesothelioma Symptoms | Slater + Gordon`,
				description: `Mesothelioma is a serious cancer caused as a result of exposure to asbestos. This is a guide with details of what you need to know about mesothelioma. Slater and Gordon offers extensive experience of asbestos and mesothelioma compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What is mesothelioma?",
                    answer: `<p>Mesothelioma is a form of cancer which can occur in the mesothelium which is the membrane that lines the inner surface of the chest wall, known as the pleura and also the abdomen, known as the peritoneum and the testicles. </p><p>Mesothelioma is caused by breathing in asbestos dust. Inhalation of asbestos dust causes damage to the cells of the pleura, the outer lining of the lungs and the internal chest wall, and it's thought that over time this damage results in changes which can eventually turn them into cancerous cells.</p><p>Although mesothelioma of the pleura is the most common form, mesothelioma of the abdomen, or peritoneum can also occur. This is because the mesothelium also surrounds the heart and intestines.  </p><p>The average time for mesothelioma to develop after exposure to asbestos dust is around 40 years, but it can develop (or cause symptoms) after 10 years from the initial exposure.</p><p>There are approximately 2,500 people a year in the UK who are diagnosed with mesothelioma, the majority of which are men. </p>`
                  },
                  {
                    question: "What are the symptoms of mesothelioma?",
                    answer: `<p>The most common symptoms of mesothelioma include breathlessness, a persistent cough, chest pain, tiredness, loss of appetite and weight loss. However, as mesothelioma can also affect the stomach, heart and testes, you should also be aware of any new pains or changes to any of these parts of the body. </p><p>If you are suffering from any of these symptoms, particularly if you have been exposed to asbestos in the past, you should see your GP right away. In the event of a diagnosis mesothelioma, you should also consider making a claim for compensation. </p>`
                  },
                  {
                    question: "How is mesothelioma usually diagnosed?",
                    answer: `<p>While some of the symptoms of mesothelioma may be apparent with just a stethoscope, it's likely that your GP will send you for further tests to determine if the linings of the lungs, heart or stomach show signs of mesothelioma, including:</p><ul><li>X-Rays</li><li>CAT Scans</li><li>Ultrasound Scans</li><li>Biopsies</li><li>Analysis of pleural and peritoneal fluid</li></ul><p>These tests will either confirm a diagnosis of mesothelioma, or rule it out. In the case that mesothelioma is diagnosed, it will be graded on a scale of one to four using the <a href=\"https://imig.org/\">International Mesothelioma Interest Group (IMIG)</a> staging system, to determine how far the disease has advanced and what sort of treatment may be possible. In the event of such a diagnosis, you may wish to speak to an experienced solicitor to discuss the possibility of compensation for your illness. </p>`
                  },
                  {
                    question: "What treatment is available for mesothelioma?",
                    answer: `<p>Although there's no known cure for mesothelioma, there are treatments which may be available to help ease suffering, such as:</p><ul><li>Talc pleurodesis - This procedure is performed to relieve breathlessness by draining fluid from around the lungs and talc or a chemical powder is put into the body cavity surrounding the lungs to prevent further build-up of fluid</li></ul><ul><li>Radiotherapy - This treatment is performed to try to relieve breathlessness, pain and discomfort</li></ul><ul><li>Chemotherapy – This treatment is performed to try to reduce symptoms. It's only sometimes helpful and only recommended in certain cases</li></ul><ul><li>Extrapleural pneumonectomy – This surgery aims to remove the visible tumour, or as much as possible. It's a radical surgery and is only suitable for a small number of patients</li></ul><ul><li>Pleurectomy-decortication - This operation usually leads to better lung function. However, it's only rarely recommended. It aims to remove all or part of the pleura and lung tissue close to it to reduce the mesothelioma</li></ul><ul><li>New treatments - Clinical trials are carried out to research new treatments all the time. Participation in clinical trials is an important treatment option for many patients but you should discuss the option with their doctor first</li></ul><ul><li>Physiotherapy - Physiotherapy is often recommended to help maintain exercise tolerance, strength and functional ability as much as possible</li></ul><ul><li>Cordotomy - To relieve symptoms of pain</li></ul><ul><li>Analgesics - To control symptoms of pain</li></ul><p>It's important to note that treatments and clinical trials change often and it's therefore important that you speak to your consultant about your treatment options.</p>`
                  },
                  {
                    question: "Are there benefits available for people with mesothelioma?",
                    answer: `<p>Yes. There are a number of benefits available to people who have mesothelioma. There are also one-off payments which may be available depending on your circumstances, such as: </p><ul><li>Pneumoconiosis Etc. (Workers Compensation) Act 1979</li><li>The Diffuse Mesothelioma 2018 Scheme</li><li>The Diffuse Mesothelioma Scheme (DMPS)</li></ul><p>For more information about the benefits and payments you may be entitled to, read our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/benefits/\">Benefits and Payments guide</a>. </p><p>As well as benefits and payments from the Government, you may be entitled to compensation for your pain and suffering. </p><p>Slater and Gordon has a team of experts who have specialised in asbestos related illness for many years and understand the complexities which can be faced. We have a strong reputation when it comes finding insurers of companies which have been out of business for many years and securing the best possible outcome to your case. </p><p>We offer No Win No Fee agreements to all clients who have been diagnosed with an asbestos related disease and will not charge any additional fees meaning you'll get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Mesothelioma Compensation Claims`,
				copy: `If you've come into contact with asbestos, there's a risk that you could develop mesothelioma. This is a guide which explains what you need to know about asbestos related illness, mesothelioma.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee mesothelioma compensation claims`,
				leftWysiwyg: `product-template/mesothelioma-two-panel-cta`,
				rightHeading: `Talk to us about mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/mesothelioma`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}