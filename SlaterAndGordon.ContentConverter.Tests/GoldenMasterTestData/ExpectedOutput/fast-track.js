module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `fast-track`,
		slug: `fast-track`,
		parent: `road-traffic-accidents`,
		name: `Minor Injury Motor Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Minor Injury Motor Accident Claims | Slater + Gordon`,
				description: `When you have been injured in a road traffic accident, if the case is relatively straightforward and your injuries minor, you shouldn't have to wait forever for compensation. Talk to our fast track team today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How long does a fast track injury claim take?",
                    answer: `<p>When we believe that an injury claim is reasonably straightforward and meets the fast track criteria, we put those claims into our fast track system. This uses a secure electronic portal and streamlined case management software to make sure that every piece of 'paperwork' is handled as quickly as possible. From the outside it might make little difference, the only thing you will notice is the quick and efficient handling of your case.</p><p>So why wait longer than you have to, when your injury claim could be handled by our fast track team?</p>`
                  },
                  {
                    question: "Why are large claims treated differently?",
                    answer: `<p>Naturally, we want every injury compensation claim to be settled as quickly as possible. But where a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">very serious injury has occurred</a>, the need to assess and understand the long-term consequences for the client means that even after a final financial settlement has been agreed, we can remain involved in things like <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation and nursing</a> care for many years. </p>`
                  },
                  {
                    question: "Do I qualify for No Win No Fee?",
                    answer: `<p>The vast majority of the personal injury cases we put into the fast track system are handled under a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee agreement</a>. This means that we believe your case has merit, and also that even if it is ultimately unsuccessful, you won't owe us a penny, so there is no financial risk to you.</p>`
                  },
                  {
                    question: "How does our fast track claims process work?",
                    answer: `<p>We use a custom-designed and completely secure IT portal that was created purely for the purpose of making personal injury claims run as smoothly and efficiently as possible. Once we have all of the facts we need from you to start your claim, every piece of 'paperwork' associated with your claim passes through this digital portal to make the process as quick as possible.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Minor injuries fast track road traffic accident claims`,
				copy: `It's bad enough being injured in a road traffic accident through no fault of your own. If your injuries are relatively minor, you really shouldn't have to wait forever for compensation. That's why Slater and Gordon has a fast track team for minor injury compensation claims.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/fast-track-two-panel-cta`,
				rightHeading: `Find out more about fast tracking your injury compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/fast-track`,
			sideLinkList: {
				heading: `More questions about Road Traffic accidents?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}