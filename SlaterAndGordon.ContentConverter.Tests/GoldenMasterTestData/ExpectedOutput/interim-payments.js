module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `interim-payments`,
		slug: `interim-payments`,
		parent: `serious`,
		name: `What are Interim Payments?`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What are Interim Payments? | Slater + Gordon`,
				description: `Interim payments allow those who have suffered a serious injury to receive part of their compensation settlement before the final judgement is reached. For more information on interim payments, contact Slater and Gordon Today.`,
				robots: ``,
                schema: [
                  {
                    question: "How do interim payments work?",
                    answer: `<p>Interim payments are payment which may be paid to you before our final compensation settlement is agreed. These payments are designed to help people who've been seriously injured as a result of personal injury or medical negligence and who may need financial assistance in order to pay for medical care, lost earnings and other related costs.</p><p>You can only request interim payments if the other side, the defendant, admits that they were responsible for the incident which caused your injuries. The court can also order an interim payment to be made if certain criteria are met after the case has started, such as:</p><p>The other party admits liability or there's a reasonable chance that the other party will be found to be at fault if the case goes to trial</p><p>You've a good reason for requesting the payment (such as urgent medical bills or a significant adjustment needed to accommodation) You've requested a reasonable amount as an interim payment, one that's less than your total compensation payment</p><p>Interim payments can continue throughout the course of a serious injury compensation claim. You can request more than one if you need to, but the court will need to be satisfied on the same set of criteria for each payment.</p>`
                  },
                  {
                    question: "How much can I claim and how often?",
                    answer: `<p>The amount in interim payments you receive depends on your particular case, but it won't be more than 'a reasonable proportion' of the overall amount of compensation that's likely to be paid. There's no restriction on the number of applications for an interim payment you can make, but the total must be within the 'reasonable proportion' rule.</p><p>Our solicitors can advise you on the right amount to request from a court, as reasonable requests and a persuasive reason for requesting the money can make it more likely that the payment is awarded.</p>`
                  },
                  {
                    question: "How do interim payments affect the final amount of compensation?",
                    answer: `<p>When your final award of compensation is made, the interim payments will be deducted from the total amount of compensation paid. This will mean that the final sum you receive is smaller, but one of the main benefits of interim payments is that they allow you to spread compensation payments out. This means that you don't have to wait to access funds you need now.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Interim payments`,
				heading: `Welfare and benefits after serious injuries`,
				copy: `Interim payments allow those who have suffered a serious injury to receive part of their compensation settlement before the final judgement is reached.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/interim-payments-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/interim-payments`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}