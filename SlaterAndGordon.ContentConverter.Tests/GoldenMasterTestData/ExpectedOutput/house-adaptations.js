module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `house-adaptations`,
		slug: `house-adaptations`,
		parent: `serious`,
		name: `Common House Adaptations`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Common House Adaptations | Slater + Gordon`,
				description: `If serious injuries have caused permanent disability, you may need to adapt your home to make life easier. Talk to Slater and Gordon today about a No Win No Fee compensation claim to help pay for the house adaptations you might need.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of house adaptations are possible?",
                    answer: `<p>Everyone has the right to live in their own home and enjoy an independent life. So when you or a loved one has sustained a serious injury that wasn't your fault, if compensation is payable, it should ideally include money to fund any necessary house adaptations. Some of the common house adaptations that our solicitors seek compensation to pay for include: </p><ul><li>Wheelchair ramps</li><li>Handrails</li><li>Walk-in baths</li><li>Lever taps</li><li>Accessible doorways</li><li>Overbath showers</li></ul><p>If you or a loved one has suffered a serious injury that might require some of these common house adaptations, talk to us to see if a claim for injury compensation might be possible, due to someone else's actions or negligence.</p>`
                  },
                  {
                    question: "What other equipment might injury compensation pay for?",
                    answer: `<p>While major house adaptations are a key part of making life as independent as possible for those who've suffered serious injuries - and of course their families - we never overlook the need for other necessary aids to be covered by serious injury compensation claims. Many useful items of equipment are available, including:</p><ul><li><strong>For independent hygiene: </strong>grab rails, bath boards, electric bath lifts and wheelchair access showers </li><li><strong>For easier toileting:</strong> raised toilet seats, toilet frames, grab rails and commodes  </li><li><strong>For food preparation:</strong> adapted kitchen utensils and cookers</li><li><strong>For help with sleep:</strong> bed raisers, bed rails, machines to help you sit up and slide sheets to help you move position </li><li><strong>For more comfortable sitting:</strong> riser/recliner chairs, chair raisers  </li><li><strong>For pressure care:</strong> pressure relieving cushions  </li><li><strong>For help with transfers:</strong> mobile electric hoists </li><li><strong>For added mobility:</strong> standing/turning/walking frames </li><li><strong>For easier access:</strong> portable wheelchair ramp</li></ul><p>While this list isn't exhaustive, it hopefully gives you an idea of some of the aids to more independent living that are available when home adaptation becomes necessary. It may also help you to understand the necessity for every aspect of life after a serious injury to be taken into account when considering a claim for serious injury compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `House adaptation compensation claims`,
				copy: `If serious injuries have caused permanent disability, you may need to adapt your home to make life easier.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/house-adaptations-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/house-adaptations`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `Watch Susan's story`,
				copy: `Susan's active lifestyle was brought to a halt when a tree fell on her during a jog. Slater and Gordons specialist claims lawyers made sure that enough compensation was available to ensure her house and new hand bike could be adapted to suit her needs.`,
				image: false,
				video: `rccmtfj5vd`
			}
		}
	}
}