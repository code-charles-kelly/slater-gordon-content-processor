module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `car-accident-claims`,
		slug: `car-accident-claims`,
		parent: `road-traffic-accidents`,
		name: `Car Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Car Accident Compensation Claims | Slater + Gordon`,
				description: `If you have been hurt in a car accident, you need specialist legal help as soon as possible. Talk to us today about starting a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How long do I have to claim for car accident injuries?",
                    answer: `<p>Generally speaking, you can start a claim for compensation within three years of a car accident, though you may be allowed longer if you're <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">claiming on behalf</a> of a child or someone with diminished mental capacity. Time limits may vary if claiming through the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/criminal-injury/cica-process/\">Criminal Injuries Compensation Authority</a>.</p>`
                  },
                  {
                    question: "Can I claim if a car accident was partly my fault?",
                    answer: `<p>Many car accidents are caused by a combination of errors. Even if you believe you were at fault as a driver, that may not be the case in purely legal terms. So it's always worth talking to us about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation</a>, even if you may have been partially at fault.</p>`
                  },
                  {
                    question: "How long does a car accident claim take?",
                    answer: `<p>This all depends on how <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">serious your injuries</a> are - ranging from <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/whiplash-neck/\">whiplash injuries</a> to permanent disability - as well as whether the other party's insurers accept liability. However, where a claim for compensation is likely to be straightforward as your injuries are relatively minor, we handle every No Win No Fee car accident claim through our expedited <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/fast-track/\">fast track team</a> to help our clients get the compensation they need as quickly as possible. </p>`
                  },
                  {
                    question: "How much accident compensation can I claim?",
                    answer: `<p>Again, this depends on the severity of any injuries. It's also important to know <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">you can claim for a number of losses</a> caused by a car accident, including general damages, additional travel expenses and loss of earnings. You may also be able to claim for the cost of any physiotherapy or other essential rehabilitation.</p>`
                  },
                  {
                    question: "What if I was injured on public transport.",
                    answer: `<p>Accidents involving public transport such as collisions with taxis or Ubers and even busses can happen and aren't uncommon. If you were involved in one of these collisions even if you were a pedestrian or in another vehicle, Slater and Gordon could help you get the compensation you deserve to get you back on your feet.</p><p>Call us today freephone on ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road Traffic Accident Claims`,
				heading: `Car accident compensation claims`,
				copy: `Even minor car accidents can leave you with long-term injuries such as whiplash. Insurers may try to make you an early offer, but it's important to take impartial legal advice before you accept. The majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/car-accident-claims-two-panel-cta`,
				rightHeading: `Find out more about No WIn No Fee car accident compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/car-accident-claims`,
			sideLinkList: {
				heading: `For more information, read our essential guides.`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}