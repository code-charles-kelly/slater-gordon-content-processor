module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `abuse-criminal-injury`,
		slug: `abuse-criminal-injury`,
		parent: `sexual-criminal-abuse`,
		name: `Abuse & Criminal Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Abuse & Criminal Injury Claims | Slater + Gordon`,
				description: `If you've been the victim of any form of violence or abuse, we're here to give you the confidential support that you need. Talk to us today about starting a No Win No Fee abuse compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is CICA?",
                    answer: `<p>CICA stands for Criminal Injury Compensation Authority. It's an arm of the UK Government, designed to help innocent victims of abuse and violent crime claim for rightful compensation. For more information on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">the CICA claims process click here</a>, alternatively contact one of our legal experts today. </p>`
                  },
                  {
                    question: "How much compensation am I likely to receive?",
                    answer: `<p>The amount of compensation you are likely to receive for an abuse claim differs significantly as every case is different, so until we know the circumstances of your claim it's impossible to say. However, your claim will be dealt with by one of our specialists in abuse law who will provide advice on how much you are likely to receive once they have all the circumstances.</p>`
                  },
                  {
                    question: "How long do I have to make a claim?",
                    answer: `<p>Generally, the time limit for pursuing a claim for personal injury in the UK is three years. However, there are exceptions for cases of abuse, so even if it happened a very long time ago, it's still possible to make a claim for compensation. So, please don't worry if your case happened a long time ago. Speak to one of our specialists in confidence today about making a compensation claim.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Abuse and criminal injury compensation claims`,
				copy: `If you've suffered from any form of violence or abuse, you may have the right to compensation. You can talk confidentially to one of our legal specialists today. There is no financial risk either, as the majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/abuse-criminal-injury-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/abuse-criminal-injury`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}