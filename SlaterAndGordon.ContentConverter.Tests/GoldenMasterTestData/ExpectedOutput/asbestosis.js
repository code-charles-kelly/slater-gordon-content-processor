module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestosis`,
		slug: `asbestosis`,
		parent: `asbestos-mesothelioma`,
		name: `What is Asbestosis, Causes, Symptoms & Treatment`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What is Asbestosis, Causes, Symptoms & Treatment | Slater + Gordon`,
				description: `Asbestosis is a serious condition which is caused by exposure to large amounts of asbestos This guide details of what you need to know about asbestosis. Slater and Gordon offers extensive experience of asbestos and mesothelioma compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What is asbestosis?",
                    answer: `<p>Asbestosis is a long-term lung condition caused by inhaling asbestos dust. It's a form of scarring on the lungs, also known as fibrosis, which leads to your lungs shrinking and hardening. This makes it difficult for the lungs to work effectively.</p><p>Asbestosis can take on average approximately 20 to 30 years following exposure to asbestos to develop, and the effects are irreversible. Studies show that in order to develop asbestosis you must have been exposed to a large quantity of asbestos dust. </p>`
                  },
                  {
                    question: "What are the symptoms of mesothelioma?",
                    answer: `<p>Although it usually takes many years after your exposure to asbestos for you to develop symptoms, when they do develop, they typically include: </p><ul><li>Shortness of breath, </li><li>Tiredness </li><li>A persistent cough</li><li>Wheezing </li><li>Pain in your chest or shoulders </li><li>Swollen fingertips (in more serious cases)</li></ul>`
                  },
                  {
                    question: "How is asbestosis usually diagnosed?",
                    answer: `<p>A diagnosis of asbestosis will usually be made by a chest consultant following investigations which normally include x-rays and a CT Scan.</p><p>If an x-ray shows some shadowing, the consultant may ask for you to have a CT scan which takes pictures of the inside of the body and provides much more information.</p><p>Some conditions look similar on an x-ray to asbestosis and therefore the consultant should take a history of your asbestos exposure before diagnosing you with asbestosis.</p>`
                  },
                  {
                    question: "What treatment is available for mesothelioma?",
                    answer: `<p>Many people think that asbestosis is cancer. It's not. There's a risk that people suffering from asbestosis may go on to develop either <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-lung-cancer\">asbestos related lung cancer</a> or <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/mesothelioma/\">mesothelioma</a>.</p><p>Regular monitoring through x-rays and lung function tests will show progression, and if the disease does progress, treatment such as steroids or oxygen may be offered.</p><p>Please note that treatments change often and it's therefore important that you speak to your consultant about your treatment options.</p>`
                  },
                  {
                    question: "Are there benefits available for people with mesothelioma?",
                    answer: `<p>Yes. There are a number of benefits available to people who have asbestosis. There's also a one-off payment which you may be entitled to which is the Pneumoconiosis Etc. (Workers Compensation) Act 1979 payment.</p><p>For more information about the benefits and payments you may be entitled to, read our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/benefits\">Benefits and Payments guide</a>. </p><p>As well as benefits and payments from the Government, you may be entitled to compensation for your pain and suffering. </p><p>Slater and Gordon has a team of experts who have specialised in asbestos related illness for many years and understand the complexities which can be faced. We have a strong reputation when it comes finding insurers of companies which have been out of business for many years and securing the best possible outcome to your case. </p><p>We offer No Win No Fee agreements to all clients who have been diagnosed with an asbestos related disease and will not charge any additional fees meaning you'll get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestosis compensation claims`,
				copy: `When you're exposed to asbestos you're at risk of going on to develop an asbestos related illness. This is a guide which explains what you need to know about asbestos related illness, asbestosis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestosis compensation claims`,
				leftWysiwyg: `product-template/asbestosis-two-panel-cta`,
				rightHeading: `Talk to us about asbestosis claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/asbestosis`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation`
				}
			],
		}
	}
}