module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `pedestrian-accident-claims`,
		slug: `pedestrian-accident-claims`,
		parent: `road-traffic-accidents`,
		name: `Pedestrian Accident Claim`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Pedestrian Accident Claim Solicitors | Slater + Gordon`,
				description: `If you've been injured by a car, bike or motorcycle as a pedestrian, you need expert help to get the compensation you deserve. Talk to us today about starting a No Win No Fee claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How long do I have to claim for pedestrian injuries?",
                    answer: `<p>If you've been injured as a pedestrian, you have up to three years in which to begin your claim, though you may be given longer if you're claiming on behalf of a child or someone of diminished mental capacity. Time limits may vary if you are claiming through the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">Criminal Injuries Compensation Authority</a>.</p>`
                  },
                  {
                    question: "Can I claim if the accident was partly my fault?",
                    answer: `<p>Most accidents involving pedestrians are caused through the recklessness or inattention of drivers or riders. However, even if you feel that you may have been partly responsible, it's always worth talking to us to establish the legal position. Even if you are partly to blame, you may still be entitled to receive compensation.</p>`
                  },
                  {
                    question: "How long does a pedestrian injury claim take?",
                    answer: `<p>Naturally, this depends on the severity of your injuries, and whether an insurer is prepared to accept liability. However, where your injuries are considered minor and the claim likely to be straightforward, we process every <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee accident claim</a> through our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/fast-track/\">fast track team</a> to help you get the compensation you need as soon as possible.</p>`
                  },
                  {
                    question: "How much compensation can I claim?",
                    answer: `<p>As you might expect, the level of compensation awarded depends on the extent of your injuries and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">how serious they are</a>. At the same time, it's worth bearing in mind that you can claim for other losses caused by your accident, such as general damages, travel expenses and even loss of earnings. Where appropriate, we can also claim on your behalf for treatment such as physiotherapy and other <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Pedestrian accident compensation claims`,
				copy: `Pedestrians pretty much always come off second best in a collision with any vehicle. So if you've been struck on a pavement or pedestrian crossing, you may well be entitled to compensation. Most of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/pedestrian-accident-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee pedestrian injury compensation`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/pedestrian-accident-claims`,
			sideLinkList: {
				heading: `For more information, read our essential guide`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}