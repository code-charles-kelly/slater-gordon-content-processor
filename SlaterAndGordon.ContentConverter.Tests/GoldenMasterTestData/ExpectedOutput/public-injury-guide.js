module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `public-injury-guide`,
		slug: `public-injury-guide`,
		parent: `injury-in-public`,
		name: `Public Injury Claim Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Public Injury Claim Guide | Slater + Gordon`,
				description: `If you have been injured in a public place, you need to prove it. Follow this simple guide to public injury claims to make sure you get it right. Talk to us today about No Win No Fee claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of public injuries can I claim for?",
                    answer: `<p>As we explain on our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/\">Public Injury Compensation page</a>, injuries that are due to the negligence of other people can happen in all sorts of ways, and in all sorts of places. Importantly, if you are injured in a public place, there are a number of key steps you need to follow in order to make a successful injury compensation claim. That's because claims can only succeed if your claims solicitor has the evidence they need. </p>`
                  },
                  {
                    question: "What should I do immediately after an injury?",
                    answer: `<p>Amongst the most common forms of accident that give rise to a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">Public Liability Insurance</a> claims are:</p><ul><li>Slipping on unmarked liquid spillages</li><li>Tripping over hazards</li><li>Being hit by falling objects</li><li>Being harmed by careless cleaning</li><li>Being injured by faulty equipment</li></ul><p>Naturally, if you are badly hurt, the first thing you need to do is to seek medical attention. If you are able to, however, you or a friend should try to take photographs of your injury and whatever caused it as soon as possible, as well as asking for the contact details of any witnesses. </p><p>If you are in a building such as a supermarket or museum, for example, you should also make sure that a member of staff is aware of the incident, and willing to note it in the 'accident book' that most buildings keep to record accidents and injuries. </p><p>If you are in the open, such as in a private car park, you should try to note any address details on signs to help your solicitor when you start your claim, as well as whether there are any CCTV cameras around. You should then write down all the facts you can remember about the incident as soon as possible.</p>`
                  },
                  {
                    question: "What evidence will my solicitor need?",
                    answer: `<p>Even the most experienced claims solicitors need evidence to help you claim rightful compensation. So when you instruct your solicitor, you should try to have as much evidence like this to hand as possible:</p><ul><li>Photos of the scene of the accident</li><li>Photos of any injuries</li><li>Contact details of any witnesses</li><li>Any medical records that relate to the injury</li><li>The name of the land/building owner, if known</li><li>Details of any CCTV cameras in the vicinity</li><li>The notes you wrote immediately after the incident</li></ul><p>Once they have all this, your solicitor will be able to decide if your claim is likely to succeed based on the evidence, and to start investigating to find out who is liable, as well as whether or not they hold Public Liability Insurance.</p>`
                  },
                  {
                    question: "How do I choose a solicitor to act for me?",
                    answer: `<p>When you have been hurt due to someone else's negligence, you are fully entitled to claim compensation for loss of earnings and rehabilitation, as well as for your pain and distress. So you should always seek a legal firm that is not only committed to seeking justice for the victims of negligence, but that also offers its clients peace of mind. At Slater and Gordon, for example, we handle around 98% of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/medical-assessment/\">injury compensation claims on a No Win No Fee basis</a>, to avoid people having to worry about owing us money if their claim should be unsuccessful.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Public injury compensation claim guide`,
				copy: `If you have been injured in a public place, you need to follow these key steps in order to start a compensation claim for your injuries and loss of earnings.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/public-injury-guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/public-injury-guide`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}