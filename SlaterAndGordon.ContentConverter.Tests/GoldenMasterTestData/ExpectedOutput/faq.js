module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq`,
		slug: `faq`,
		parent: `personal-injury-claim`,
		name: `Personal Injury Claims FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Personal Injury Claims FAQs | Slater + Gordon`,
				description: `When you've suffered a personal injury, you often don't know where to begin in pursuing a claim for compensation. Our legal experts have answered the most commonly asked personal injury claims questions that you might have.`,
				robots: ``,
                schema: [
                  {
                    question: "Do I have to pay legal costs to make a claim for compensation?",
                    answer: `<p>The simple answer is no. If you have a claim for personal injury arising from a road traffic accident, your case can be funded under a No Win No Fee agreement. A No Win No Fee agreement, also known as a Conditional Fee Agreement, is an agreement signed by you and your lawyer which states that if you lose your case, you will not have to pay any legal costs. </p><p>This means that you can <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">pursue your claim free</a> from any financial risk. </p>`
                  },
                  {
                    question: "How long will the case take?",
                    answer: `<p>Every case is different so it's difficult to say how long a case is likely to take without first reviewing the circumstances. One of the factors involved in how long a case will take is whether or not the other party accepts responsibility for the accident. This is called \"establishing liability\" – working out who is liable for the accident.</p><p>Another factor is the severity of the injuries. For example, you may suffer a minor injury which you recover from after several months or a more serious injury that takes several years. However, our road traffic accident experts will ensure that your case is dealt with as quickly and efficiently as possible.</p>`
                  },
                  {
                    question: "How much compensation can you claim for a road traffic accident?",
                    answer: `<p>Compensation is split into <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">two parts</a>:</p><p><strong>General Damages:</strong> General damages is the amount of compensation you'll receive for pain, suffering and loss of amenity. It's based on the severity of the injuries you have suffered and the length of time it's taken to recover, if you have indeed recovered from the injuries sustained. As a result, every case is different. However, lawyers who specialise in road traffic accidents have years of experience in this area and will be able to provide an estimate of the amount of general damages you're likely to achieve.</p><p><strong>Special Damages:</strong> Special damages is the amount of compensation you receive to cover any losses / 'out of pocket expenses' you've incurred as a result of the accident, such as:</p><ul><li>Loss of property</li><li>Prescription costs and medication</li><li>Aids and equipment</li><li>Private treatment costs / rehabilitation</li><li>Travelling expenses</li><li>Loss of earnings</li><li>Loss of pension</li><li>Property adaptations </li></ul><p>It also takes into account costs and losses which will continue in the future, such as: </p><ul><li>Future loss of earnings</li><li>Future pension loss</li><li>Future aids and equipment </li><li>Future care costs </li></ul><p>This is to ensure that you don't suffer financial hardship as a result of your accident.</p>`
                  },
                  {
                    question: "Can I get compensation payments before the case has settled?",
                    answer: `<p>Once liability has been established (i.e. the other party has accepted full or partial blame for the accident) you can apply for early payments of your compensation. These are called interim payments.</p><p>People who've suffered a serious injury can often, as a result, suffer financial difficulties, for example if they've been unable to work or have had to pay for private treatment or rehabilitation costs. Interim payments can be applied for to cover these costs and form part of your overall claim. So, for example, if your case settles for £10,000 and you have already received two interim payments of £1,000 your final payment of compensation will be £8,000.</p>`
                  },
                  {
                    question: "Are there time limits on making a claim for compensation?",
                    answer: `<p>In England, Wales and Scotland, the general rule is that a claim should be brought within three years of the date that the incident causing an injury or illness occurred or the date you were first aware that you had suffered an injury or illness due to negligence. Additional time limits include:</p><ul><li> Where a claim is being made on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">behalf of someone</a> who has died, the claim must be brought within three years of their date of death</li><li>Where a claim is being made on behalf of a child, the three year time limit does not begin until the child reaches age 18 in England and Wales or the child reaches age 16 in Scotland</li><li>The time limit when making a claim under <a href=\"https://www.legislation.gov.uk/ukpga/1998/42/contents\">The Human Rights Act</a> is one year</li><li>There is no time limit on making a claim where the injured person lacks mental capacity</li></ul><p>Different time limits may apply if the incident causing injury occurred whilst abroad. Our experts in <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/\">accidents abroad </a>will be able to confirm this. </p><p>Although these are the general rules when it comes to time limits, there may be occasions when the court would make a discretionary decision in order to pursue a claim outside the general time limits, so it's always best to speak to a specialist lawyer who can advise you on whether or not you can pursue a claim.</p>`
                  },
                  {
                    question: "Will I have to go to court?",
                    answer: `<p>Understandably, many people are concerned about having to attend court as it can be a daunting prospect.  However, the vast majority of claims for compensation reach a successful settlement before a final hearing at court. There are a small number of cases that unfortunately don't reach a successful conclusion prior to the final hearing and in those cases, it is necessary to attend court.</p><p>However, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/\">our expert lawyers</a> will be with you every step of the way and will ensure that the process is as stress free as possible.</p>`
                  },
                  {
                    question: "Will I need a medical examination?",
                    answer: `<p>In order to make a claim for personal injury, it's important that we have evidence of your injuries and you will therefore have to attend a medical examination with an independent medical expert who specialises in preparing reports to use as evidence in a legal case. These reports are known as medico legal reports.</p><p>Depending on the type and severity of your injuries, you may need to attend more than one medical examination to ensure we have all the medico legal reports we need to support your claim for compensation.</p>`
                  },
                  {
                    question: "Can I make a personal injury claim if I was partly to blame for the accident?",
                    answer: `<p>Even if you are partly to blame for your accident, you can still make a claim for compensation. If you're found to be partially to blame for the accident, this will be reflected in your compensation payment. For example, if you are found to be 20% to blame for the accident, and your claim settled for an overall figure of £10,000, you would only receive compensation for 80% of that figure and you would therefore receive £8,000.</p>`
                  },
                  {
                    question: "My child has been injured, how do I make a claim on their behalf?",
                    answer: `<p>Children are unable to make claims on their own behalf and must therefore be made by a trusted adult, usually a parent or grandparent. </p><p>When someone makes a claim on behalf of a child, they are known as a 'Litigation Friend'. A Litigation Friend can't be anyone who has was in any way responsible for the accident. This means, for example, if the mother of the injured child was driving the vehicle, and they were either fully or partially responsible for the accident that caused the injury, they would not be able to be the Litigation Friend and instead would have to be another trusted family member or friend. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Personal injury claims FAQs`,
				copy: `The most commonly asked personal injury claims questions you might have, answered by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}