module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `head-brain`,
		slug: `head-brain`,
		parent: `serious`,
		name: `Head & Brain Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Head & Brain Injury Compensation Claims | Slater + Gordon`,
				description: `Head and brain injuries can devastate lives. You need expert legal help immediately, and the funding to help you get on with life. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "We can help you with the challenges of head and brain injuries",
                    answer: `<p>Medical professionals treat every head injury seriously, as they know how far-reaching the effects can be. While many sufferers go on to make a full recovery, some will require special care for the rest of their lives. As lawyers with extensive experience of head and brain injury cases, we know how difficult it can be and what's required for both the sufferer and their family. We understand the importance of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/interim-payments/\">seeking interim payments</a> in advance of a final settlement, make sure you have the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">funding to pay for rehabilitation</a>, ongoing expenses and perhaps even <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">specially adapted housing</a>. If you're <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">claiming on behalf of a loved one</a> because they have been left with a diminished mental capacity, we're here to listen, and help.</p>`
                  },
                  {
                    question: "How much can I claim for a head or brain injury?",
                    answer: `<p>It goes without saying that every head or brain injury is different, and the amount of compensation that may ultimately be paid depends on a variety of factors: from long-term prognosis to the cost of any care required. However, where head or brain injuries leave the sufferer with impaired mental capacity, or requiring a high level of care, compensation payments in the millions of pounds are not unheard of where someone else's negligence is to blame. </p>`
                  },
                  {
                    question: "How will we pay for long-term care?",
                    answer: `<p>It's quite natural for the families of those with serious head or brain injuries to worry about where the money for long-term care will come from. That's why the cost of long-term care, medical costs, and aftercare over the injured party's lifetime is factored into the calculation of the compensation settlement. We're also aware of the need for other expenses such as any necessary home modifications and the full cost of rehabilitation to be included in your claim.</p>`
                  },
                  {
                    question: "How do head and brain injuries happen?",
                    answer: `<p>Sadly, head and brain injuries can be very serious, and can be caused in a variety of ways, from collisions on the sports field to accidents on the road. If you would like more information, you will find links to resources on a number of types of personal injury below:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accident</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/sports-injury/\">Sports injury</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Work accident</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Accidents abroad</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/faulty-products/\">Defective products</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Illness</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Industrial disease</a></li></ul><p>Yet however, it occurred, if you or a family member has suffered a head or brain injury that was caused by someone else's negligence, it is essential for you to claim compensation as soon as possible, all of you live your lives to the greatest possible extent.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Head and Brain Injury Compensation Claims`,
				copy: `For those who suffer from a head or brain injury, there can be terrible consequences for them and their families. You therefore need legal advice from someone with a vast amount of experience in this field of law Slater and Gordon is one of the UK's leading head and brain personal injury firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/head-brain-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/head-brain`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `Watch Kile's story`,
				copy: `Kile Hughes' life was transformed when he was assaulted in March 2008, leaving him unable to walk and talk with severe brain injuries.`,
				image: false,
				video: `y4t0uet75e`
			}
		}
	}
}