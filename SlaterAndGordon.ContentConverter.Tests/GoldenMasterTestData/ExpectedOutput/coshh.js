module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `coshh`,
		slug: `coshh`,
		parent: `industrial-disease`,
		name: `What is COSHH? COSHH Regulations Explained`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What is COSHH? COSHH Regulations Explained | Slater + Gordon`,
				description: `Under COSHH employers have to reduce or prevent employee exposure to hazardous substances in the workplace. Slater and Gordon have produced a guide to COSHH regulations and the compensation process.`,
				robots: ``,
                schema: [
                  {
                    question: "What is COSHH?",
                    answer: `<p>The Control of Substances Hazardous to Health Regulations 2020 or COSHH is a law that requires employers to prevent or reduce employee exposure to such substances whilst at work in environments where substance usage is unavoidable. </p>`
                  },
                  {
                    question: "When might exposure to a substance lead to a claim for compensation?",
                    answer: `<p>Many substances can be hazardous and which is why COSHH is in place for all employers to adhere to, such as:</p><ul><li>Reading and understanding the safety sheets on all products. </li><li>Completing a hazard checklist</li><li>Carrying out a thorough risk assessment</li><li>Putting control measures in place </li><li>Checking and maintaining equipment (i.e. ventilation, personal protective equipment (PPE), to remain in control</li><li>Ensuring staff have adequate training, instruction and information </li><li>Arranging for regular health checks and monitoring exposure</li></ul><p>If your employer failed to follow the <a href=\"http://www.hse.gov.uk/coshh/basics.htm\">COSHH regulations</a> and you've suffered an industrial disease or an injury as a result, you may have the right to claim compensation. </p>`
                  },
                  {
                    question: "What sort of substances does the COSHH cover?",
                    answer: `<p>COSHH doesn't cover substances such as lead, asbestos or radioactive substances. However, other substances included in the COSHH which can cause harm to employers if the COSHH regulations are not adhered to include: </p><ul><li>Chemicals</li><li>Products containing chemicals</li><li>Fumes</li><li>Dusts</li><li>Vapours</li><li>Mists</li><li>Nanotechnology</li><li>Gases </li><li>Biological agents</li><li>Germs that cause disease </li></ul>`
                  },
                  {
                    question: "What is PPE?",
                    answer: `<p>PPE stands for <a href=\"http://www.hse.gov.uk/toolbox/ppe.htm\">Personal Protective Equipment</a>. When employees are working with hazardous substances, employers should ensure, as part of their control measures procedure, to provide employees with PPE. The COSHH states that \"PPE should be used when all other measures are inadequate to control exposure\".  </p><p>Types of PPE include: </p><ul><li>Protective clothing and footwear</li><li>Eye protection </li><li>Protective gloves </li><li>Respirators </li></ul>`
                  },
                  {
                    question: "What if I'm injured at work because my employer hasn't taken preventative safety measures?",
                    answer: `<p>If you're injured at work because your employer hasn't taken the appropriate measures to ensure your safety and prevent you from harm, you may be entitled to make a claim for your injuries. </p><p>If your case is successful, your compensation will be paid from your <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employer's liability insurance </a>and if you're able to return to work, your employer is required to make reasonable adjustments so you can carry out your work with ease and they legally aren't allowed to treat you any differently from any other employees just because you've made a claim.</p>`
                  },
                  {
                    question: "What can I claim for?",
                    answer: `<p>When you're injured at work, it can often lead to time off work to recover. There are cases where you might not be paid during your recovery period and can lead to financial worries. Compensation is designed to put you in the same financial position you would have been but for the accident. Therefore, as well as seeking compensation for pain and suffering, we also seek compensation for:</p><ul><li>Loss of income</li><li>Loss of future income if you're unable to return to work</li><li>Medical expenses</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">Rehabilitation</a></li><li>Treatment </li></ul><p><a href=\"\"></a>Depending on the severity of your injury, your lawyer may be able to seek interim payments to pay for urgent treatment or home adaptations to aid your recovery as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Industrial disease and COSHH regulations.`,
				copy: `Under COSHH employers have to reduce or prevent employee exposure to hazardous substances in the workplace. Slater and Gordon have produced a guide to COSHH regulations and the compensation process.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/coshh-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for industrial disease claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/coshh`,
			sideLinkList: {
				heading: `Want to know more about industrial disease compensation?`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}