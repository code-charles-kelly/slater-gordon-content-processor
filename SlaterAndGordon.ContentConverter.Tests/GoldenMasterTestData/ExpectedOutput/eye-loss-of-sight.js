module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `eye-loss-of-sight`,
		slug: `eye-loss-of-sight`,
		parent: `serious`,
		name: `Eye Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Eye Injury Compensation Claims | Slater + Gordon`,
				description: `Your sight is incredibly precious. So if you've suffered a serious eye injury due to someone else's negligence, it's vital that you receive the compensation you need as soon as possible. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How much can I claim for an eye injury?",
                    answer: `<p>As you might expect, the amount of compensation for an eye injury depends on the seriousness of the injury and on the prognosis for any future improvement. The calculation for financial compensation also has to take into account factors like <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">loss of earnings, loss of amenity, and the cost of ongoing treatment</a>, rehabilitation and support. However, substantial compensation to help you adapt to sight loss and live life to the fullest extent can run into millions of pounds, particularly if you or a loved one have lost their sight as a result of someone else's negligence.</p>`
                  },
                  {
                    question: "What help is available after an eye injury?",
                    answer: `<p>Financial compensation for eye injuries and sight loss isn't simply for the pain and suffering you've had to endure. It can also help to pay for medical treatment such as laser eye surgery, prosthetics, specialist medical care, assistance dogs and even <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">adaptations to a property or the purchase of a new property where required</a>. That's why it is important to talk to a legal firm that understands all of the ramifications of an eye injury or loss of sight as soon as possible.</p>`
                  },
                  {
                    question: "Who might be to blame for an eye injury?",
                    answer: `<p>Sadly, accidents happen, and eye injuries can occur where no one is to blame. However, on many occasions they happen in the workplace due to a lack of safety precautions, such as:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/health-safety-dangerous-practices/\">A lack of proper training in safe working practices</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Inadequate or missing goggles, safety glasses or face shields </a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/defective-equipment/\">Defective machinery</a></li><li>Improper storage or handling of dangerous chemicals</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Military Accidents</a></li></ul><p>So if you think that your eye injury - or that of a relative - has been caused by someone else's negligence, either in the workplace or somewhere else, it's worth talking to one of our specialist eye injury lawyers to find out whether it's possible to claim compensation.</p>`
                  },
                  {
                    question: "What sort of eye injuries can I claim for?",
                    answer: `<p>Any serious injury that's been caused by someone else's negligence has the potential to give rise to a compensation claim. Eyes are such delicate and essential organs, almost any damage is serious, whether it's caused by an object piercing the eye, scratches to the surface, or sometimes even damage caused by a severe blow to the head. However, these are the eye injuries that we most commonly seek compensation for:</p><ul><li>Full or partial blindness</li><li>Deterioration due to environmental factors</li><li>Damage to the cornea</li><li>Loss of an eye</li><li>Detached retina</li></ul><p>However an eye injury has occurred, and whatever the degree of harm that has occurred, if someone else's negligence has been involved, you should talk to a specialist claims solicitor as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Eye injury compensation claims`,
				copy: `Losing your sight can be devastating. If you've suffered an eye injury that was the result of someone else's negligence, you should talk to a specialist lawyer as soon as possible. Slater and Gordon is one of the UK's leading eye injury and loss of sight claims legal firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/eye-loss-of-sight-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/eye-loss-of-sight`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}