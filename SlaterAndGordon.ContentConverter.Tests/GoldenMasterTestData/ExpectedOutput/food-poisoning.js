module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `food-poisoning`,
		slug: `food-poisoning`,
		parent: `illness`,
		name: `Food Poisoning Claim Lawyers`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Food Poisoning Claim Lawyers | Slater + Gordon`,
				description: `Not only can food poisoning and food allergy reactions have unpleasant consequences, they can even lead to fatalities. Find out more about starting a No Win No Fee food poisoning compensation claim with Slater and Gordon here.`,
				robots: ``,
                schema: [
                  {
                    question: "What sorts of food poisoning can I claim for?",
                    answer: `<p>Food poisoning takes many forms in the UK and can have very serious consequences. So if you have suffered food poisoning that was caused by any of these viral, bacterial or parasitic conditions, you may have grounds for claiming food poisoning compensation:</p><ul><li>Salmonella</li><li>Shigella</li><li>E.coli</li><li>Giardia</li><li>Norovirus</li><li>Rotavirus</li><li>Campylobacter</li><li>Cryptosporidium</li></ul><p>Unusually, you do not have to prove negligence in order for your food poisoning claim to succeed, only that:</p><ul><li>The food was not safe</li><li>You have been ill</li><li>The unsafe food made you ill</li></ul><p>In fact, under the terms of the The Consumer Protection Act 1987, if you can establish the source of the food poisoning, infection or allergic reaction, then the law imposes a 'strict liability' rule on the offending restaurant, shop or supplier. Here's what to do <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/food-poisoning/causes-prevention/\">if you are worried about contaminated food</a>.</p>`
                  },
                  {
                    question: "How is food poisoning caused?",
                    answer: `<p>Whether food poisoning stems from a snack or meal you ate in a restaurant, or one you bought in a supermarket, the reasons behind it are usually:</p><ul><li>Food has been insufficiently cooked to kill bacteria</li><li>Food has not been refrigerated or covered</li><li>Bacteria has passed from food to food</li><li>Poor hygiene standards have affected preparation</li></ul><p>You can find out more about the causes of food poisoning on the <a href=\"https://www.food.gov.uk/\">food standards agency website</a>. You will also receive useful advice and possibly even support in gathering evidence for a compensation claim from your local <a href=\"https://www.food.gov.uk/about-us/local-authorities#.UMhsx6xm6_I\">Environmental Health Officer</a>. We also offer guides on what restaurants should be doing in an effort to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/food-poisoning/restaurant-guidelines/\">reduce the risk of food poisoning here</a>.</p>`
                  },
                  {
                    question: "Can I claim for a food allergy reaction?",
                    answer: `<p><strong></strong>Sadly, the tragic story featured in the video below is not as rare as you might hope. The law states that all food and drink sold in the UK is required to clearly declare the presence of known allergens including nuts or dairy products in signage and packaging. Where this has not been done satisfactorily and an allergic reaction has occurred - whatever its level of seriousness - a claim for rightful compensation may be in order.</p>`
                  },
                  {
                    question: "How do I claim for food poisoning?",
                    answer: `<p><strong></strong>Whether you have suffered food poisoning or an allergic reaction to food, gathering evidence as early as possible is essential. So if you believe you have been the victim of someone else's negligence, take these steps as soon as possible:</p><ul><li>Seek immediate medical assistance</li><li>Inform your local <a href=\"https://www.food.gov.uk/about-us/local-authorities#.UMhsx6xm6_I\">Environmental Health Department </a></li><li>Stay off work or school for 48 hours to avoid contagion</li><li>Write a list of everything you ate in the previous 24 hours</li><li>Keep suspect food, if you still have it, in its packaging</li><li>Preserve the food (i.e. freeze it) but seal it in polythene first</li><li>Keep all relevant receipts from restaurants or supermarkets</li><li>Collect names and contact details of witnesses</li></ul><p>You will also need to keep this evidence, and have medical evidence from a gastroenterologist in order to help us prove your illness and its seriousness. Once this is done, it's time to ask Slater and Gordon about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a>. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Illness and food poisoning compensation claims`,
				copy: `Have you suffered from food poisoning or a food allergy reaction in the UK and think that a food or drink retailer or producer was to blame? Slater and Gordon are leading food poisoning claim lawyers offering a No Win No Fee service to the majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/food-poisoning-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/food-poisoning`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/restaurant-guidelines/`,
						copy: `Restaurant guidelines`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}