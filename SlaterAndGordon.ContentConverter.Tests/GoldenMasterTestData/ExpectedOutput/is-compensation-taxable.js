module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `is-compensation-taxable`,
		slug: `is-compensation-taxable`,
		parent: `personal-injury-claim`,
		name: `Is Personal Injury Compensation Taxable?`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Is Personal Injury Compensation Taxable? | Slater + Gordon`,
				description: `Is the taxman going to take some of your personal injury compensation? Here we explain why tax won't be payable on your damages, following a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Will my personal injury compensation be taxable?",
                    answer: `<p>We know that you have enough to worry about if you've been injured in an accident that wasn't your fault. So the good news is that the taxman isn't interested in charging you tax on any compensation payments you receive for an illness or injury that was caused by someone else's negligence. That's because most tax is based on earnings, or income, and the damages you receive as compensation don't fall into that category. So if the thought of having to deal with a tax bill has been putting you off making a compensation claim, you don't have to delay any longer. Talk to us now about a tax-free compensation claim, which we may even be able to help you make on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>. </p>`
                  },
                  {
                    question: "Will I have to pay tax on interest?",
                    answer: `<p>This is an area that causes a lot of confusion, so let's break it up into two answers:</p><p>But rest assured that in the event of a successful injury compensation claim, we will be happy to advise you on how best to legally avoid paying any more tax than you need to. </p><p>Talk to one of our legal experts today for help with all your personal injury compensation claims needs.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Is your personal injury compensation taxable?`,
				copy: `Receiving a large lump sum compensation claim can have an affect on your finances but these winnings are rarely taxed.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/is-compensation-taxable-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/is-compensation-taxable`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}