module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `skin-conditions-dermatitis`,
		slug: `skin-conditions-dermatitis`,
		parent: `industrial-disease`,
		name: `Dermatitis & Skin Condition`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Dermatitis & Skin Condition Compensation Claims | Slater + Gordon`,
				description: `Occupational dermatitis is usually avoidable. So if you have developed a skin problem at work, it's likely your employer has been negligent. Slater and Gordon offers the expertise you need to claim compensation, and the security of No Win No Fee agreements.`,
				robots: ``
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Dermatitis and other skin condition compensation claims`,
				copy: `Hand-Arm Vibration Syndrome (HAVS) is the collective term for a range of vibration-related conditions, including Vibration White Finger (VWF) Whole Body Vibration (WBV) and Dupuytren's Contracture. Slater and Gordon is one of the UK's leading vibration injury compensation specialists, with expert solicitors who could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/skin-conditions-dermatitis-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for dermatitis claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/skin-conditions-dermatitis`,
			sideLinkList: {
				heading: `Do you have more questions on industrial injuries? Read our expert guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}