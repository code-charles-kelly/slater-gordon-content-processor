module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `migrant-agency-worker`,
		slug: `migrant-agency-worker`,
		parent: `accident-at-work-compensation`,
		name: `Migrant & Agency Worker`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Migrant & Agency Worker Claims | Slater + Gordon`,
				description: `All workers are equal when it comes to injuries caused by negligence. So, if you are a migrant or agency worker in the UK who has been injured at work, you have the legal right to seek compensation. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What rights do migrant workers have?",
                    answer: `<p>Migrant and agency workers in the UK are protected by all <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">UK employment laws</a>. This means that even if you're in the country on a temporary basis, whoever hired you and pays you has a duty of care to make sure that you're not harmed in the workplace. If you've been injured or made ill by <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/health-safety-dangerous-practices/\">unsafe working practices</a> or negligence, you have every right to make a claim for compensation. Speak to a legal expert about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a> today.</p>`
                  },
                  {
                    question: "How much can I claim for my injury?",
                    answer: `<p>This all depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and how much any r<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">ehabilitation such as occupational therapy or physiotherapy</a> might cost. However, we take most migrant and agency injury cases on a No Win No Fee basis, and seek interim payments to help you avoid financial hardship if you are unable to work because of your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>Generally, the rule is that you have three years from the date you were injured in which to make a claim. There are exceptions however, such as if the injured person has diminished mental capacity as a result of the accident, if the accident causing injury was abroad, or in the tragic circumstances that the person dies. You should therefore speak to an accidents at work specialist as soon as you are able. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Migrant and agency worker Compensation Claims`,
				copy: `Migrant and agency workers are an essential part of the UK's workforce, tackling some of our most difficult and essential jobs. Slater and Gordon is one of Britain's leading injury compensation legal firms, here to offer a No Win No Fee service to almost all of our personal injury clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/migrant-agency-worker-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/migrant-agency-worker`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}