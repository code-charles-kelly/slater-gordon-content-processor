module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `silicosis`,
		slug: `silicosis`,
		parent: `industrial-disease`,
		name: `Silicosis`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Silicosis Compensation Claims Solicitors | Slater + Gordon`,
				description: `Silicosis compensation claims are still being made. If you or a loved one suffers from this serious lung disease, Slater and Gordon offers all the expertise you need, as well as the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is silicosis?",
                    answer: `<p>Silicosis is a serious industrial disease, in which the lungs are affected by long-term exposure to crystalline silica dust. This occurs in industries including mining, quarrying, sandblasting, stone masonry, construction, demolition, ceramics and glass making. Very fine dust is inhaled into the lungs, leading to inflammation and scar tissue, as well as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/occupational-asthma/\">occupational asthma</a>.</p>`
                  },
                  {
                    question: "What are the symptoms of silicosis?",
                    answer: `<p>Most people with silicosis first notice that their cough won't go away and they begin to feel short of breath. This can ultimately lead to persistent feelings of weakness and tiredness. It's important to seek medical advice if you suffer any of these symptoms, as well as to make sure you begin your claim for silicosis compensation within three years of your condition being diagnosed. To find out more about the symptoms of silicosis, <a href=\"https://www.nhs.uk/conditions/silicosis/\">read more on the NHS website</a>.</p>`
                  },
                  {
                    question: "What are the long term effects of silicosis?",
                    answer: `<p>Unfortunately, silicosis has serious long term effects on the health of sufferers. In some cases, respiratory failure can occur, and it is not unusual for related conditions to develop as a result of silicosis, including:</p><ul><li>Tuberculosis</li><li>Lung cancer</li><li>Heart failure</li><li>Chronic Obstructive Pulmonary Disease (COPD)</li><li>Arthritis</li><li>Kidney disease </li></ul><p>These are just some of the reasons why it's essential to make a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No fee compensation claim</a> if you have been diagnosed with silicosis.</p>`
                  },
                  {
                    question: "How might my employer be responsible?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to foresee the harm that could be caused by your occupation, or provide the protective gear specified, they could be said to have failed in their duty of care. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">Read more on the health and safety regulations</a> that should be keeping all employees safe in the workplace.</p>`
                  },
                  {
                    question: "Do I need a specialist silicosis lawyer?",
                    answer: `<p><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Industrial disease claims</a><strong></strong> are quite a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of successful claims. Slater and Gordon employs specialist lawyers in every area of industrial disease compensation claims. Just as importantly, we understand the impact that this kind of diagnosis can have on sufferers and their families, and aim to be as considerate and supportive as possible throughout the claims process.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Silicosis compensation claims`,
				copy: `Silicosis is a serious lung condition affecting thousands of people who were exposed to crystalline silica dust at work. Slater and Gordon is one of the UK's leading silicosis compensation specialists, with dedicated solicitors who could help you on a No Win No Fee basis`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/silicosis-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for silicosis compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/silicosis`,
			sideLinkList: {
				heading: `Read expert guides and FAQs on industrial disease below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}