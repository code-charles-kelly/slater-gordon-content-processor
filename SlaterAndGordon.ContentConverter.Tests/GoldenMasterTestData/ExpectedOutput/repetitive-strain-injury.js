module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `repetitive-strain-injury`,
		slug: `repetitive-strain-injury`,
		parent: `industrial-disease`,
		name: `Repetitive Strain Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Repetitive Strain Injury Compensation Claims | Slater + Gordon`,
				description: `Repetitive strain injury is seldom your fault if it occurred at work. So if it's affecting your life, it's time to talk to Slater and Gordon. We can offer all the expertise you need, and handle most cases on a No Win No Fee basis.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a repetitive strain injury?",
                    answer: `<p>Repetitive strain injury occurs when the same tasks are repeated over and over again. They can occur in a number of workplace situations, including:</p><ul><li>Production lines</li><li>Working in restricted spaces</li><li>Badly designed desk layouts</li><li>Repeated heavy lifting</li><li>Operating machinery</li></ul><p>Use of vibrating machinery such as chainsaws and other power tools can also cause repetitive strain injuries and related injuries such as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/vibration-injury/\">Hand-Arm Vibration Syndrome (HAVS)</a>, which is the collective term for a range of conditions including<strong> </strong>Vibration White Finger (VWF).</p>`
                  },
                  {
                    question: "What are the symptoms of repetitive strain injury?",
                    answer: `<p>Common symptoms include:</p><ul><li>Tenderness or pain</li><li>Stiffness</li><li>Cramp</li><li>Numbness</li><li>Throbbing</li><li>Tingling </li><li>Weakness</li></ul><p>If you have any of these symptoms in a part of your body that is constantly used at work - such as an elbow that repeatedly bends to pull a lever, for example - you should check out the advice on the <a href=\"https://www.nhs.uk/conditions/repetitive-strain-injury-rsi/\">NHS website</a> or speak to your doctor about it. If diagnosed, you can then talk to one of our experts about claiming for <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">compensation on a No Win No Fee basis</a>.</p>`
                  },
                  {
                    question: "How might my employer be responsible?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to foresee the harm that could be caused by your occupation, they could be said to have failed in their duty of care.<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\"> Read more on the health and safety regulations</a> ensuring your workplace is safe and talk to a specialist solicitor to find out if your employer failed in their duty of care. </p>`
                  },
                  {
                    question: "Do I need a specialist repetitive strain injury lawyer?",
                    answer: `<p>Industrial disease claims are quite a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of successful claims. Slater and Gordon employs specialist lawyers in every area of industrial disease compensation claims. Just as importantly, we understand that a repetitive strain injury can be a distressing condition, and aim to be as considerate and supportive as possible throughout the claims process.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Repetitive strain injury compensation Claims`,
				copy: `Repetitive strain injuries (RSI) can seriously affect your long-term physical and emotional well-being. If your RSI developed as a result of your work, Slater and Gordon could help you to claim the compensation you deserve on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee repetitive strain injury claim`,
				leftWysiwyg: `product-template/repetitive-strain-injury-two-panel-cta`,
				rightHeading: `Talk to our No Win No Fee solicitors today`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/repetitive-strain-injury`,
			sideLinkList: {
				heading: `Do you have more questions on industrial diseases? Read our expert guides and FAQs below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}