module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `industrial-disease`,
		slug: `industrial-disease`,
		parent: `personal-injury-claim`,
		name: `Industrial Disease Compensation Lawyers`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Industrial Disease Compensation Lawyers | Slater + Gordon`,
				description: `A diagnosis of an industrial disease can be life changing. Claiming for financial compensation and rehabilitation is essential. Slater and Gordon offers the expertise and understanding you need, as well as the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is an industrial disease?",
                    answer: `<p>The term 'industrial disease' describes more than simply diseases; it's a term for almost any illness or condition caused by your employment. If your employer has failed in their duty of care to keep you safe from harm, it's usually possible to make a claim for industrial disease compensation, even when the company you worked for is no longer in business. The most common types of industrial disease giving rise to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claims</a> include:</p><ul><li><strong>Asbestos diseases:</strong> including a number of lung conditions caused by exposure to asbestos fibres, including mesothelioma, asbestos-related lung cancer, asbestosis and pleural thickening. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/\">Read more</a>.</li><li><strong>Hand-Arm Vibration Syndrome (HAVS): </strong>the collective term for conditions including: Vibration White Finger (VWF) and Carpal Tunnel Syndrome (CTS). <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/vibration-injury/\">Read more</a>.</li><li><strong>Repetitive Strain Injury (RSI)</strong>: a condition caused by repeating the same movements over a long period, causing pain in the muscles, nerves and tendons. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/repetitive-strain-injury/\">Read more</a>.</li><li><strong>Hearing loss and deafness: </strong>hearing loss and conditions such as tinnitus can be caused by exposure to loud noise in the workplace. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/industrial-deafness-tinnitus/\">Read more</a>.</li><li><strong>Skin conditions/occupational dermatitis: </strong>exposure to dangerous chemicals at work can lead to a number of skin conditions, including dermatitis. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/skin-conditions-dermatitis/\">Read more</a>.</li><li><strong>Occupational asthma, respiratory and lung disease:</strong> exposure to dust or fumes containing irritants can cause silicosis, occupational asthma, lung cancer and asbestosis. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/occupational-asthma/\">Read more</a>.</li><li><strong>Occupational cancers: </strong>caused by carcinogenic chemicals and hazardous dusts, leading primarily to lung cancer, nasal cancer, skin cancer and bladder cancer. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/work-related-cancer/\">Read more</a>.</li></ul>`
                  },
                  {
                    question: "Do I need a specialist industrial disease compensation lawyer?",
                    answer: `<p>Industrial disease claims are a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of success. Slater and Gordon employs specialist lawyers in every area of industrial disease compensation. Just as importantly, we understand the need to take every industrial disease claim seriously and make sure our clients receive the compensation and medical treatment they deserve whilst dealing with every case with understanding and compassion.</p>`
                  },
                  {
                    question: "What is the Health and Safety Act and COSHH?",
                    answer: `<p>The Health and Safety at Work etc Act 1974 is the primary piece of legislation protecting workers in the UK. You can <a href=\"http://www.legislation.gov.uk/\">find out more about it on the legislation.gov.uk website</a>. The <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/coshh/\">Control of Substances Hazardous to Health (COSHH) law</a> is designed to ensure that the health of employees is not affected by dangerous substances. You can find out more about COSHH on the <a href=\"http://www.hse.gov.uk/coshh/basics.htm\">Health and Safety Executive</a> (HSE) website. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Industrial disease compensation claims`,
				copy: `If you've been diagnosed with an industrial disease, we have the expertise to seek rightful compensation on your behalf. Slater and Gordon's industrial disease specialists are here to advise and support you and your family throughout the claims process and beyond.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/industrial-disease-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for industrial disease claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Mesothelioma and other asbestos related diseases`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/`,
						icon: `lung`
					},
					{
						title: `Other work related cancers`,
						url: `/personal-injury-claim/industrial-disease/work-related-cancer/`,
						icon: `pillpot`
					},
					{
						title: `Work related asthma`,
						url: `/personal-injury-claim/industrial-disease/occupational-asthma/`,
						icon: `lung`
					},
					{
						title: `Work related dermatitis/other skin conditions`,
						url: `/personal-injury-claim/industrial-disease/skin-conditions-dermatitis/`,
						icon: `dermatitis`
					},
					{
						title: `Chemical poisoning`,
						url: `/personal-injury-claim/industrial-disease/chemical-poisoning/`,
						icon: `chemical`
					},
					{
						title: `Hand arm vibration syndrome / vibration white finger`,
						url: `/personal-injury-claim/industrial-disease/vibration-injury/`,
						icon: `repetitive-strain`
					},
					{
						title: `Work related hearing loss and tinnitus`,
						url: `/personal-injury-claim/industrial-disease/industrial-deafness-tinnitus/`,
						icon: `nihl`
					},
					{
						title: `Repetitive strain injury`,
						url: `/personal-injury-claim/industrial-disease/repetitive-strain-injury/`,
						icon: `repetitive-strain`
					},
					{
						title: `Silicosis claims`,
						url: `/personal-injury-claim/industrial-disease/silicosis/`,
						icon: `lung`
					},
					{
						title: `Other accidents at work`,
						url: `/personal-injury-claim/accident-at-work-compensation/`,
						icon: `factory`
					}
				]
			},
			standardContent: `product-template/industrial-disease`,
			sideLinkList: {
				heading: `Want to know more about industrial disease compensation?`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			imageTextA: {
				section: `Industrial disease`,
				heading: `What can we help you with?`,
				copy: `We can help you with a wide variety of industrial disease claims, here are just some examples of the kinds of work related cases we deal with on a daily basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/194973397.jpg`,
					medium: `/public/images/bitmap/medium/194973397.jpg`,
					small: `/public/images/bitmap/small/194973397.jpg`,
					default: `/public/images/bitmap/medium/194973397.jpg`,
					altText: `Senior couple with tablet sat in a chair`
				},
				video: false
			},
		}
	}
}