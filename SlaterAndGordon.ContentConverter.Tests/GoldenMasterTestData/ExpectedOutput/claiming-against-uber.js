module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claiming-against-uber`,
		slug: `claiming-against-uber`,
		parent: `road-traffic-accidents`,
		name: `Uber Taxi Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Uber Taxi Accident Compensation Claims | Slater + Gordon`,
				description: `If you have been involved in an accident involving a taxi or uber then you may be entitled to compensation. Slater and Gordon offer No Win No Fee compensation claims and expert support. Find out how we can help you with your claim today.`,
				robots: ``,
                schema: [
                  {
                    question: "How do you claim for injuries sustained in an Uber road accident?",
                    answer: `<p>Users who download the Uber app must agree to a host of <a href=\"https://www.uber.com/legal/gbr/terms\">terms and conditions</a> which include a limitation of liability clause, in which Uber state they \"will not be liable to you in any way.\"</p><p>People wishing to get in touch with Uber after an accident in one of their taxis can be left very frustrated as the company doesn't provide any customer service phone numbers, only a 'support' email address and a contact form on their website.</p><p>If you've been injured as a passenger in an Uber, contact an experienced <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">road traffic accident lawyer</a> as soon as possible. Slater and Gordon has expert lawyers who can advise you on the necessary steps in pursuing a personal injury claim against Uber or one of their drivers.</p><p>Slater and Gordon offer a free consultation to anyone injured in an accident through no fault of their own. The majority of claims are dealt with on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee</a> basis, meaning there's no financial risk to you.</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/public-transport/\">Click here for more information on other forms of public transport injury claims</a>.</p><p>Call us 24/7 on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a> and we'll be happy to help you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Can I Claim if I'm injured in an Uber Taxi Collision?`,
				copy: `If you're a passenger injured in a taxi road collision, you can make a personal injury claim against the taxi company – but what if the accident happened in an Uber?`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/claiming-against-uber-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee road traffic accident claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claiming-against-uber`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}