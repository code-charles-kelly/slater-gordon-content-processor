module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `steps-to-take`,
		slug: `steps-to-take`,
		parent: `accident-at-work-compensation`,
		name: `What To do After an Accident at Work`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What To do After an Accident at Work| Slater + Gordon`,
				description: `If you've been injured in an accident at work, follow these seven simple steps to help you claim rightful compensation. Then ask us about making a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What to do after an injury at work",
                    answer: `<p>Accidents happen in the workplace. In the majority of cases, they're due to either employer negligence or a workmate failing to follow <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">health and safety rules</a>. If you're injured at work and it wasn't your fault, you may have to make a compensation claim to help make up for your <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">loss of earnings</a> and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">pay for rehabilitation</a>. To make that as likely as possible, always follow these seven simple steps after receiving an injury at work:</p>`
                  },
                  {
                    question: "How do I deal with my employer?",
                    answer: `<p>This is one of the questions we're most commonly asked when someone has had an injury at work. You have no need to worry about this, as your right to claim compensation for injuries caused by someone else's negligence is enshrined in law. That's why every employer is legally required to have at least<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\"> £5 million of employers' liability insurance</a> in place: to make sure they can pay any necessary compensation without damaging the company. It's also important to know that while you have to report the injury to your employer so it can go into the Accident Book, you shouldn't sign anything without first speaking to your lawyer, who will be happy to deal with your employer for you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Steps to take after an accident at work`,
				copy: `If you've been injured at work, it's important to follow the right steps to avoid prejudicing your right to claim compensation. This simple guide is intended to help you get it right after an accident at work that wasn't your fault.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/steps-to-take-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/steps-to-take`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `What to do following an accident at work`,
				copy: `Specialist accident at work lawyer, Tracey Benson, advises on what steps you should take following an accident at work.`,
				image: false,
				video: `v5g1uzeipc`
			}
		}
	}
}