module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `help-support`,
		slug: `help-support`,
		parent: `sexual-criminal-abuse`,
		name: `Abuse Support Resources`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Abuse Support Resources | Slater + Gordon`,
				description: `The effects upon someone who has suffered abuse can stay with them for a lifetime. If you or a loved one have suffered from abuse the resources featured here can help begin the healing process and help you come to terms and cope with what has happened to you.`,
				robots: ``,
                schema: [
                  {
                    question: "NSPCC",
                    answer: `<p>The NSPCC is a UK registered children's charity fighting against child abuse. They help protect children from child abuse and also help those who have been abused to rebuild their lives.</p><p><a href=\"https://www.nspcc.org.uk/\">https://www.nspcc.org.uk/</a></p><p>The Samaritans is a charity which provides confidential emotional support, 24 hours per day, 7 days per week, to anyone suffering emotional distress or struggling to cope, across the UK.</p><p><a href=\"https://www.samaritans.org/\">https://www.samaritans.org/</a></p>`
                  },
                  {
                    question: "NAPAC",
                    answer: `<p>NAPAC is a UK registered charity which was set up to offer a range of services and support to adult survivors of all types of childhood abuse including neglect and physical, sexual and emotional abuse.</p><p><a href=\"https://napac.org.uk/\">https://napac.org.uk/</a> </p><p>HAVOCA, or Help for Adult Victims of Child Abuse, is a forum run by adult survivors of child abuse, which offers support, friendship and advice to those who have been affected by childhood abuse.</p><p><a href=\"http://www.havoca.org/\">http://www.havoca.org/</a> </p><p>Male Survivor is an organisation which provides life-changing support including forums and a range of recovery therapies to male adult survivors of child abuse.</p><p><a href=\"https://malesurvivor.org/\">https://malesurvivor.org/</a></p><p>Action on Elder Abuse, or AEA, is a specialist organisation in the UK focusing on the issue of elder abuse and offering a range of guidance and support services to those who have been affected by elder abuse.</p><p><a href=\"https://www.elderabuse.org.uk/\">https://www.elderabuse.org.uk/</a></p><p>NWG is a UK based charitable organisation, whose aim is to offer support and advice to those working with children and young people under 18 who are affected by abuse through sexual exploitation.</p><p><a href=\"https://www.nwgnetwork.org/\">https://www.nwgnetwork.org/</a></p><p>SupportLine provides a confidential telephone helpline offering emotional support to anyone on any issue. The service is mainly aimed at those who are socially isolated, vulnerable at risk and also offers support to victims of any form of abuse.</p><p><a href=\"https://www.supportline.org.uk/\">https://www.supportline.org.uk/</a></p><p>Safeline is a charity designed to prevent sexual abuse as well as help anyone who has been affected by it.</p><p><a href=\"https://www.safeline.org.uk/\">https://www.safeline.org.uk/</a></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Support resources for victims of sexual, physical and child abuse`,
				copy: `Here are a variety of charities and support services for people who have suffered from abuse and would like additional help and support.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/help-support-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/help-support`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}