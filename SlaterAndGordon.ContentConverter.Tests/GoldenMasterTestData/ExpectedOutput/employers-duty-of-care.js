module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `employers-duty-of-care`,
		slug: `employers-duty-of-care`,
		parent: `asbestos-mesothelioma`,
		name: `Employers Duty of Care When Asbestos is Found`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Employers Duty of Care When Asbestos is Found | Slater + Gordon`,
				description: `Every employer has a duty of care to protect workers from harm in the workplace. This is particularly important if asbestos is discovered. Slater and Gordon offers extensive experience of making successful asbestos compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What is my employer's duty of care when asbestos is found?",
                    answer: `<p>While the dangers of asbestos fibres have been known for many years, its use wasn't banned entirely in the UK until 1999. This means that many, many buildings - both domestic and commercial - contain materials with some element of asbestos fibres. Under the <a href=\"http://www.hse.gov.uk/asbestos/regulations.htm\">Control of Asbestos Regulations 2012</a>, if any asbestos is found, it must be registered and disposed of by a specialist contractor. </p><p>Unfortunately, there have been a number of cases where asbestos has been found and work has not been stopped, leading to a number of innocent people being exposed to dangerous asbestos fibres. This is a clear breach of the employer's duty of care. If it has happened to you, contact us immediately to talk to one of our asbestos claims experts.</p>`
                  },
                  {
                    question: "What should happen if asbestos is discovered?",
                    answer: `<p>In the event that asbestos is discovered - or even suspected - work should stop immediately, as asbestos is dangerous if it's disturbed and fibres are released. The Health and Safety Executive (HSE) has created a checklist for all employers to follow if asbestos is discovered or suspected, which <a href=\"http://www.hse.gov.uk/asbestos/managing/intro.htm\">you can download here</a>. They also point out the following facts:</p><ul><li>Asbestos is only dangerous when disturbed - avoid unnecessary disturbance</li><li>If unsure, presume that material does contain asbestos</li><li>The duty to manage doesn't require asbestos removal</li><li>You don't always need a specialist. When you do, get a competent one</li></ul><p>If you believe that your employer has failed in its duty of care to stop work when asbestos has been discovered, you should talk to an experienced asbestos claims solicitor right away. </p>`
                  },
                  {
                    question: "Is it safe for me to remove asbestos if I wear protective gear?",
                    answer: `<p>Asbestos removal is a highly specialised field requiring special training and highly specialised Personal Protective Equipment (PPE). Unless you have been specially trained in asbestos removal, you should refuse to undertake any work that involves disturbing any asbestos material and ensure that your employer reports the asbestos discovery to the relevant authorities. You can find out where to <a href=\"http://www.hse.gov.uk/asbestos/faq.htm\">report asbestos to on the HSE website</a>. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `What is an employer's duty of care when asbestos is found?`,
				copy: `Every employer has a duty of care to protect workers from harm in the workplace. This is particularly important if asbestos is discovered.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/employers-duty-of-care-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/employers-duty-of-care`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}