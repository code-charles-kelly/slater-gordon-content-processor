module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestos-timeline-history`,
		slug: `asbestos-timeline-history`,
		parent: `asbestos-mesothelioma`,
		name: `The History of Asbestos Use & Its Regulations`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `The History of Asbestos Use & Its Regulations | Slater + Gordon`,
				description: `This is a brief history of asbestos, how it's been used and how it's evolved over the years. All the way from its first commercial uses all the way to it being banned in the UK.`,
				robots: ``,
                schema: [
                  {
                    question: "Asbestos use commercial timeline",
                    answer: `<ul><li>1880s. The start of commercial importation of asbestos initially used for the textile industry.</li><li>1898. Factory Inspector Report produced as the annual report of the Chief Inspecting Officer of Factories for 1898. The report is critical of dusty conditions in factories and the adverse impact that it may have on the health of workers. The statement was repeated in the annual reports for a number of years following - demonstrating that from a very early stage the potentially dangerous effects of asbestos were noted.</li><li>1899. Ferodo Ltd is established in Derbyshire. This is a leading producer of asbestos brake linings, who eventually become a subsidiary of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/turner-newall/\">Turner and Newall Ltd</a>.</li><li>1907. Report of the Departmental Committee for Industrial Disease by Dr H Montague Murray. The report was given in evidence to the Home Office Departmental Committee on compensation for industrial diseases. Dr Montague Murray performed a post mortem on an unnamed worker who had worked for 14 years in the asbestos industry. The lungs were stiff and black with fibrosis caused by inhalation of asbestos dust. The worker previously told Murray that he was the only survivor from 10 others in his work room. Dr Murray prepared his report following this post mortem.</li><li>1906. The Compensation Act first introduced in 1897 is updated to include six Industrial Diseases to the Workmen's Compensation Act, however still none of them are asbestos related.</li><li>1920. Turner and Newall Ltd are established in Rochdale, Lancashire and are to become the world's leading producer of asbestos products.</li><li>1924. Nellie Kershaw, an English textile worker, is the first published medical case as an Asbestos related death due to pulmonary asbestosis.</li><li>At the same time fibrosis of the lung due to inhalation of asbestos dust by W. E. Cooke is published in the British Medical Journal. A further 1927 report, again by W. E. Cooke published in the British Medical Journal introduced the term asbestosis to describe characteristic lung fibrosis attributable to asbestos.</li><li>1929. The annual report of Chief Inspecting Officer of Factories 1928 notes reports of cases involving pulmonary fibrosis attributed to asbestos exposure.</li><li>1930. The Government commissioned a report entitled 'Report on the Effects of Asbestos Dust on the Lungs and the Suppression in the Asbestos Industry' by Mereweather. A well considered and in depth study considering the risk of fibrosis resulting from exposure to asbestos. The findings are that high levels of asbestosis in asbestos factory workers are becoming more common and legislation is recommended.</li><li>1931. The Asbestos Industry Regulations receive Royal Assent and come into force on 1 March 1932. This is the first recognised substantial piece of legislation to be passed which is dedicated to the control of asbestos concentrating on the use of asbestos within factories.</li><li>1933. The annual report of the Chief Inspecting Officer of Factories in 1932 reports the updated number of known deaths attributed to asbestosis and silicosis.</li><li>1948. <a href=\"https://www.legislation.gov.uk/uksi/1948/1145/contents/made\">Building (Safety, Health and Welfare) Regulations 1948</a> come into force on 1 October 1948. Regulation 82 refers to the use of asbestos within the building industry.</li><li>1948. National Insurance (Industrial Injuries) Act 1946 finally comes into effect replacing the Workmen's Compensation Scheme with the Industrial Injuries Scheme.</li><li>1955. Report of R Doll entitled 'Mortality from Lung Cancer in Asbestos Workers' published in the British Medical Journal demonstrates the link between asbestos dust and cancer. The report follows the growing emergence of the link between lung cancers and asbestos exposure and follows on from a number of further annual reports from the Chief Inspecting Officer of Factories.</li><li>1960. Wagner produces his report 'Diffuse Pleural Mesothelioma and Asbestos Exposure in a North Western Cape Province'. This is published in the British Journal of Medicine. This is South African research and reports a clear association between exposure of blue asbestos (Crocidolite) and mesothelioma.</li><li>1961. <a href=\"http://www.legislation.gov.uk/uksi/1960/1932/made\">The Ship Building and Ship Repairing Regulations </a>come into force on 31 March 1961. This is additional to the Factories Act 1937 and specifically governs the use of asbestos in the shipbuilding industry.</li><li>1967. Voluntary industry ban on the import of blue (Crocidolite) asbestos.</li><li>1968. The British Occupational Hygiene Society suggests a safety standard for white (Chrysotile) asbestos of 0.2 fibres/ml. The asbestos industry conducts a single survey at Turner and Newall's Rochdale plant and comes up with the level of 2 fibres/ml to be incorporated into the 1969 Asbestos Regulations. Later work suggests that 1 in 10 workers could contract asbestos related disease at this level.</li><li>1969. Asbestos Regulations 1969 come into force on 14 May 1970 and give guidance on the first quantitative limits for asbestos dust exposure. The duties imposed under these regulations were far wider than any previous legislation and applied to all factories, building operations and works of engineering construction. They imposed stringent requirements with regard to preventing the inhalation of asbestos fibres.</li><li>1974. The Health and Safety at Work Act provides for more far reaching duties on the employer.</li><li>1983. The Asbestos (Licensing) Regulations are enacted, they cover the most hazardous jobs such as asbestos stripping or removal.</li><li>1985. The import of brown and blue asbestos is banned.</li><li>1987. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/\">The Control of Asbestos at Work Regulations</a> 1987 are introduced which means further tightening of dust limits and controls regarding the use of asbestos at work.</li><li>1995. Turner and Newall sells its last asbestos business, the company comprises of numerous subsidiaries and other groups. The firm is acquired by Federal Mogul in 1998. Both firms of which are now in insolvent administration.</li><li>2002. <a href=\"http://www.legislation.gov.uk/uksi/2002/2675/made\">The Control of Asbestos at Work Regulations are further updated</a>.</li><li>2009. The Turner and Newall Scheme of Arrangements is introduced.</li></ul>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `A historical timeline of asbestos use and its regulation`,
				copy: `The use of asbestos in production and manufacture of various goods dates back to before the 1900s and ends with the Control of Asbestos at Work Regulations 2002.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/asbestos-timeline-history-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/asbestos-timeline-history`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}