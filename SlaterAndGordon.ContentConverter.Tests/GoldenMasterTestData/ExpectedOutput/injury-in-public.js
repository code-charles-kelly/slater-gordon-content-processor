module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `injury-in-public`,
		slug: `injury-in-public`,
		parent: `personal-injury-claim`,
		name: `Public Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Public Injury Compensation Claims | Slater + Gordon`,
				description: `All sorts of injuries occur in public places, from supermarkets to sports grounds. If you have been hurt by someone else's negligence, you may be able to claim compensation. Talk to Slater and Gordon today about making a No Win No Fee compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "Who was to blame for my injury?",
                    answer: `<p>Some accidents in public places are no-one else's fault. For instance, if you are not looking where you are going and fall down a kerb. However, if that accident could have been avoided by the land or business owner ensuring the area was safe - from a shop to a car park to an airport - you may be entitled to claim compensation for your injury.</p>`
                  },
                  {
                    question: "What is Public Liability Insurance?",
                    answer: `<p>If you trip on a faulty pavement, the local council will usually be responsible, whereas if you are in a public building, the owner will generally be liable. That's why most councils, landowners and even dog owners carry <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">Public Liability Insurance</a><strong>.</strong></p><p>This is an essential insurance policy, which ensures that money is available to pay for financial compensation and any necessary rehabilitation or medical expenses that arise from slips, trips or falls caused by negligence on the policyholder's land or premises.</p>`
                  },
                  {
                    question: "How do I make a compensation claim?",
                    answer: `<p>In the first instance, it's important to make sure you can <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-injury-guide/\">prove that your injury was caused by someone else's negligence</a>. You should take photos of the location the incident took place, making sure that you capture the direct cause of your accident, such as an uneven paving stone for example. You should also try to get the contact details of any witnesses, as well as photographs of your injuries, and details of any medical treatment you received. If you are in a public building, you should also try to find out who owns it, and if you were bitten by a dog, the same applies.  Once you have gathered all of this evidence, it's time to discuss the possibility of making a No Win No Fee compensation claim with a specialist claims solicitor.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Public injury compensation`,
				copy: `If you have sustained an injury in a public place and it was due to someone else's negligence, you may have a right to claim compensation for your injuries and loss of earnings. Talk to Slater and Gordon: one of the UK's leading injury compensation firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/injury-in-public-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Supermarket injuries`,
						url: `/personal-injury-claim/injury-in-public/supermarkets/`,
						icon: `supermarket`
					},
					{
						title: `Council claims`,
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/`,
						icon: `house`
					},
					{
						title: `Slips, trips and falls`,
						url: `/personal-injury-claim/injury-in-public/slips-trips-falls/`,
						icon: `man-crutch`
					},
					{
						title: `Sports Injuries`,
						url: `/personal-injury-claim/injury-in-public/sports-injury/`,
						icon: `sport`
					},
					{
						title: `Animal injuries`,
						url: `/personal-injury-claim/injury-in-public/animals/`,
						icon: `animal`
					}
				]
			},
			standardContent: `product-template/injury-in-public`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
			imageTextA: {
				section: `Injury in public`,
				heading: `What sort of public injuries could I claim for?`,
				copy: `There are literally thousands of accidents in public places that lead to personal injury compensation claims being made every year. Some are just that: accidents that no-one is to blame for. However, where negligence has occurred, you may be able to claim compensation for injuries, rehabilitation and loss of earnings. These claims include:`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/247763215.jpg`,
					medium: `/public/images/bitmap/medium/247763215.jpg`,
					small: `/public/images/bitmap/small/247763215.jpg`,
					default: `/public/images/bitmap/medium/247763215.jpg`,
					altText: `Young woman wearing sunglasses sat on a bus`
				},
				video: false
			},
		}
	}
}