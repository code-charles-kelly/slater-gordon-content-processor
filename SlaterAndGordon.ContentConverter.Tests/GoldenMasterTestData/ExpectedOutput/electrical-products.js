module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `electrical-products`,
		slug: `electrical-products`,
		parent: `faulty-products`,
		name: `Electrical Product`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Electrical Product Compensation Claims | Slater + Gordon`,
				description: `From exploding mobile phones to incendiary washing machines, defective products can cause serious injuries. Ask out about starting a No Win No Fee defective product compensation claim with Slater and Gordon today.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of defective electrical products have caused injury?",
                    answer: `<p>Manufacturers have a duty to test electrical products to make sure they are both safe and fit for purpose. When a company has failed in this duty, serious injuries and even death can occur. Slater and Gordon has acted for clients in a number of high profile electrical product claims. These include the <a href=\"https://www.theguardian.com/uk-news/2016/aug/08/defective-beko-tumble-dryer-mishell-moloney-birmingham-coroner-fatal-fire\">tragic case of a mother who died</a> of carbon monoxide poisoning when her Beko washing machine caught fire, and another case in which a <a href=\"https://www.mirror.co.uk/news/uk-news/sparks-were-flying-out-jeans-9059259\">mobile phone burst into flames</a> inside a man's trouser pocket. Sadly, these are far from being isolated incidents; many faulty electrical products have been found to cause serious injuries, including:</p><ul><li>Mobile Phones  - notably the Galaxy Note 7 </li><li>Washing machines and tumble dryers</li><li>E-cigarettes</li><li>Hoverboards</li><li>Electric showers</li></ul><p>So if you or a loved one has been injured by a faulty electrical product, you may be able to claim for compensation.</p>`
                  },
                  {
                    question: "Why do electrical product liability claims arise?",
                    answer: `<p>Electrical product liability claims arise when someone has been injured or even killed by a faulty electrical product. Sometimes this is due to a lack of testing, a simple manufacturing error or sometimes simply bad design. Yet other common reasons for injury claims arising from electrical products include:</p><ul><li>The manufacturer failing to recall a product with a known fault</li><li>A production line error that leads to poor wiring or earthing</li><li>Failure to display a necessary safety notice on an electrical product</li></ul><p>It's also important to realise that electrical manufacturers can't escape responsibility just because a product comes with terms and conditions that discharges them of all blame for injuries. Consumer law says that every product must be of satisfactory quality, fit for purpose and as described. Where it's not, and someone has been injured, you can talk to us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a>.</p>`
                  },
                  {
                    question: "Do I claim against the manufacturer or the shop?",
                    answer: `<p><strong></strong>Where an electrical product has failed due to a design or manufacturing defect, but hasn't caused an injury, you can usually return it to the shop it came from and ask for a refund or replacement under <a href=\"https://www.which.co.uk/consumer-rights/regulation/consumer-rights-act\">the Consumer Rights Act</a>. However, if a faulty electrical product has caused an injury, you can only seek compensation from the manufacturer, under the Consumer Protection Act 1987. That's when Slater and Gordon may be able to help you on a No Win No Fee basis.<strong></strong></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Faulty products`,
				heading: `Electrical product compensation claims`,
				copy: `Have you or a loved one been harmed by a defective electrical product? If faulty electrical goods have caused an injury, you may be entitled to compensation. Slater and Gordon are leading electrical product liability lawyers, offering a No Win No Fee service to the majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/140393239.jpg`,
					medium: `/public/images/bitmap/medium/140393239.jpg`,
					small: `/public/images/bitmap/small/140393239.jpg`,
					default: `/public/images/bitmap/medium/140393239.jpg`,
					altText: `group of young people having a beverage`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/electrical-products-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee defective product claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/electrical-products`,
			sideLinkList: {
				heading: `Questions about faulty product compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faulty-products/claim-guide/`,
						copy: `Faulty product claim guide`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can i claim for?`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `Personal injury and debt support`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Five-Figure Settlement for Eye Injury Caused by Faulty Packaging`,
					link: `/resources/latest-case-studies/2017/01/five-figure-settlement-for-eye-injury-caused-by-faulty-packaging/`,
					copy: `A five-figure settlement was awarded to a man in compensation for an eye injury caused by a faulty handle.`
				},
				{
					heading: `Jogger Awarded 5 Figure Settlement for Defective Kerb Fall`,
					link: `/resources/latest-case-studies/2016/01/jogger-awarded-5-figure-settlement-for-defective-kerb-fall/`,
					copy: `A jogger has been awarded a five-figure compensation settlement for extensive injuries to their leg sustained following a fall on defective kerb.`
				},
				{
					heading: `Settlement Awarded for Trip on Defective Manhole`,
					link: `/resources/latest-case-studies/2015/09/settlement-awarded-for-trip-on-defective-manhole/`,
					copy: `Slater and Gordon Lawyers secure settlement for client who tripped on a defective manhole`
				}
			],
		}
	}
}