module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `amputation-limb-damage`,
		slug: `amputation-limb-damage`,
		parent: `serious`,
		name: `Amputation & Limb Damage`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Amputation & Limb Damage Compensation | Slater + Gordon`,
				description: `Amputations and limb damage can have devastating consequences. You need specialist legal help, rehabilitation and financial compensation. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What kind of limb injury can I claim for?",
                    answer: `<p>Serious limb injuries can be recovered from - as is usually the case with broken arms and legs - or can lead to amputations, where soft tissue and blood vessels are damaged beyond repair. In either case, overcoming the accident can be an emotional struggle as well as a financial one. That's why, when negligence has been a cause or contributing factor, we provide specialist support for those who've suffered limb damage and amputation in a variety of cases, the most common of which are:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Limb damage sustained in road traffic accidents</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Limb damage sustained in the workplace</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">Broken limbs due to slips, trips and falls</a></li><li><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Amputations due to medical negligence</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Limb damage due to military accident</a></li></ul>`
                  },
                  {
                    question: "How much can I claim for a limb injury?",
                    answer: `<p>As you would expect, compensation payments for limb injuries and amputations vary greatly, as factors such as loss of mobility, loss of earnings and trauma suffered are all taken into account. However, our specialist solicitors fully understand the complexities and difficulties of every limb injury, and so will always seek to claim <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">compensation that enables rehabilitation</a> to begin at the earliest possible stage, and for you to get on with the rest of your life to the fullest extent possible. </p><p>Where there are future needs, such as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">house adaptations and modifications</a>, rehabilitation and treatment as well as any loss of earnings, we will always factor those costs into your compensation claim.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Amputation and limb damage compensation claims`,
				copy: `Serious limb injuries, particularly those leading to amputation, can be life-changing. If you have suffered a limb injury or an amputation due to someone else's negligence, you need expert legal help. Slater and Gordon is one of the UK's most experienced limb injury compensation firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/amputation-limb-damage-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/amputation-limb-damage`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}