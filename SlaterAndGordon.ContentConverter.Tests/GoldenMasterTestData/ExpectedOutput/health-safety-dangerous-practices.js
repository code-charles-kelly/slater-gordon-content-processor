module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `health-safety-dangerous-practices`,
		slug: `health-safety-dangerous-practices`,
		parent: `accident-at-work-compensation`,
		name: `Dangerous Practice Injuries`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Dangerous Practice Injuries | Slater + Gordon`,
				description: `Every workplace is dangerous if unsafe practices are followed. If you've been injured due to someone else's negligence, you're entitled to seek compensation. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been injured by dangerous health and safety practices.",
                    answer: `<p>Health and safety laws exist to keep everyone safe in the workplace. Yet the sad truth is that many firms allow or even encourage dangerous health and safety practices where these can save them time or money. You may also find yourself working with colleagues who don't follow proper safety practices: either to save time or because they simply don't see the need for things like goggles, safety rails and scaff tags. If you've been injured at work due to either of these scenarios, you need to talk to a specialist injury claims solicitor as soon as possible about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee claim</a>.</p>`
                  },
                  {
                    question: "What are dangerous health and safety practices?",
                    answer: `<p>If you're working for - or even with - people who like to cut corners when it comes to safety, there are many different ways for them to do so. These are some of the most common dangerous health and safety practices we have come across:</p><ul><li><strong>Inadequate workplace training: </strong>where workers aren't trained in the safe use of equipment</li><li><strong>Non-adherence to health and safety measures:</strong> such as removing safety guards from machinery and failing to provide personal protection equipment </li><li><strong>Co-worker negligence:</strong> where a colleague or colleagues flout the safety rules your employer has trained you all to follow</li><li><strong>Dangerous practices in the workplace: </strong>such as poor lighting, blocked exits and unsafe floors</li><li><strong>Poor manual handling: </strong>due to a lack of training in the proper ways to lift and handle heavy or hazardous objects</li></ul><p>If you've been injured or made ill at work due to these or any other dangerous working practices, you may be able to make a No Win No Fee compensation claim against your employer's liability insurance. Talk to a specialist solicitor now to find out if you may have a claim.</p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that the law requires every employer to have. It ensures that money is available to compensate workers for illness or injuries that were not their fault. Read more about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers liability insurance</a>.</p>`
                  },
                  {
                    question: "How much is my injury claim worth?",
                    answer: `<p>Every dangerous health and safety practice injury claim is different, and the final amount of compensation you could receive depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">how much any rehabilitation might cost</a>. However, we take almost all accident at work cases on a No Win No Fee basis, and will often seek interim payments to help you avoid financial hardship if you're prevented from working by your injuries. </p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>There's a general rule that claims must be brought within three years from the date date the accident at work occurred. However, there are exceptions, such as whether or not the injured person has diminished capacity as a result of the accident, whether the accident happened abroad or whether or not the accident resulted in death. It's therefore important that you speak to a lawyer specialising in accidents at work as soon as you are able. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Dangerous health and safety practice injury compensation claims`,
				copy: `Have you been injured as a result of dangerous health and safety working practices or a workmate's carelessness? Slater and Gordon are leading injury compensation specialists, offering a No Win No Fee service to a vast majority of our personal injury clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/health-safety-dangerous-practices-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/health-safety-dangerous-practices`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `What regulations should my employer follow?`,
				copy: `Personal injury lawyer, Nicholas discusses regulations employers should be covering and what the regulations require.`,
				image: false,
				video: `uw3hm1sytb`
			}
		}
	}
}