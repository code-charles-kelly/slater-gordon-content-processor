module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `no-win-no-fee-claims`,
		slug: `no-win-no-fee-claims`,
		parent: `personal-injury-claim`,
		name: `No Win No Fee Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `No Win No Fee Injury Compensation Claims | Slater + Gordon`,
				description: `Just what does No Win No Fee really mean? Here are all the facts you need to know about a Conditional Fee Agreement - also known as a No Win No fee agreement with Slater and Gordon solicitors.`,
				robots: ``,
                schema: [
                  {
                    question: "What is No Win No Fee?",
                    answer: `<p>The legal term for this is Conditional Fee Agreement (CFA), which is also commonly known as No Win No Fee. Basically, it allows you to pursue an injury compensation claim secure in the knowledge that if the claim is unsuccessful, you won't owe us any money. So it's really just a way of helping you to seek rightful compensation, even if you don't want to take any financial risks. </p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/how-to-make-a-claim/\">Start a claim</a></p>`
                  },
                  {
                    question: "What can I use a No Win No Fee agreement for?",
                    answer: `<p>While we're happy to discuss No Win No Fee agreements for all sorts of cases, they usually involve some sort of injury. It doesn't matter whether your injury happened at work or in the street, or as the result of anything from a road accident to a food poisoning incident. The essential point is that someone else must be to blame for your illness or injury in order for you to claim compensation from them, or, more usually, their insurers.</p><p>Below are the most common types of injury we can help you claim for:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/health-safety-dangerous-practices/\">Accidents at work</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/\">Accidents and injuries abroad</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/\">Injuries in public places</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Industrial disease</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/faulty-products/\">Faulty product claims</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/\">Illness claims</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/\">Sexual and physical abuse</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/\">Asbestos related illness</a><strong></strong></li><li><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Military accidents</a><strong></strong></li></ul>`
                  },
                  {
                    question: "Will you act for me on a No Win No Fee basis?",
                    answer: `<p>We handle the vast majority of all our personal injury compensation cases on a No Win No Fee basis, because we have the experience to know when you have a strong case, and a lot of confidence in our own abilities. So if you have been injured and someone else was clearly at fault, we'll be happy to talk to you about claiming on a No Win No fee basis. </p>`
                  },
                  {
                    question: "What happens if I lose a No Win No Fee claim?",
                    answer: `<p>A No Win No Fee agreement covers you from having to pay your legal fees should you lose your case. This means in the event that you did lose your case, you would not be charged. When funding your case through a No Win No Fee agreement, we also take out an insurance policy known as After the Event insurance (ATE) on your behalf to cover any additional costs, such as court fees. Again, if you did lose your case, you wouldn't have to pay the premium of this policy, meaning you will not have to pay a penny. </p><p>An exception to this rule would be if the court decided that you had acted dishonestly or maliciously in making the claim and if you're found in breach of your agreement. In this case, you might be liable to pay costs. However, we always discuss every case thoroughly with our clients before pursuing them, and always make sure that we all have the facts straight from the very beginning.</p>`
                  },
                  {
                    question: "How much will my solicitor get paid if I win?",
                    answer: `<p>If you win your case, the other side will pay our costs. The amount of costs we would charge the other side would depend on how many hours of work had been undertaken in order to win the case. It could be a straightforward case which takes several months to come to a conclusion, or it could be a complex case with serious injuries which takes several years.</p><p>When lawyers take on cases on a No Win No Fee basis, they run the risk that if the case is lost, they won't get paid for any of the work they have done. This is why when taking on that risk, if successful, law firms can charge a fee, known as a success fee. However, we like to agree a success fee at the start of any No Win No Fee claim. This is usually a small percentage of the eventual damages that might be won. </p><p>There are certain cases where we don't charge a success fee, for example, if you have an asbestos related illness. There are also certain organisations where members of those organisations are not charged a success fee, such as Cycling UK and a number of Trade Unions. If you are a member of an organisation, please tell your solicitor so we can advise whether you're eligible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `No Win No Fee injury compensation claims`,
				copy: `If you've been injured through no fault of your own, you may be entitled to claim compensation, but put off by the thought of ending up with a big legal bill. That's why the vast majority of Slater and Gordon clients claim injury compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/no-win-no-fee-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/no-win-no-fee-claims`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}