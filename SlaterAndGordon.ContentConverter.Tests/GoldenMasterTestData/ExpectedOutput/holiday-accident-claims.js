module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `holiday-accident-claims`,
		slug: `holiday-accident-claims`,
		parent: `personal-injury-claim`,
		name: `Holiday Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Holiday Accident Claims | Slater + Gordon`,
				description: `Had an accident or an injury while abroad? In many cases you can start a claim even after you get home. Slater and Gordon specialise in No Win No Fee compensation claims for accidents abroad and in the UK.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for an accident abroad?",
                    answer: `<p>Whether you are working or holidaying abroad, accidents are just as likely to happen due to someone else's negligence overseas as they are in the UK. That can cause complications while you're abroad, especially if you have to pay for additional travel expenses or even medical care. However, once you're back in the UK, it's perfectly possible to make an injury compensation claim for all of these eventualities and more:</p><p>If you can't wait until your return to the UK before starting your claim, we also have the expertise and the resources to provide you with International legal representation, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">medical treatments and rehabilitation support</a>. </p>`
                  },
                  {
                    question: "Can I claim compensation if I was on a package holiday?",
                    answer: `<p>In some cases, accidents abroad will be the responsibility of the tour operator or UK-based travel agent you book with. So if your claim related to some element of a package holiday, it may be easier to claim for an accident abroad than if you simply slipped on a wet floor in a shopping centre. However, as long as you <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/\">gather evidence to support your claim</a><strong> </strong>and report it to a member of staff wherever the accident occurs, it's possible to make <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee claim</a> for many accidents abroad.</p>`
                  },
                  {
                    question: "What are the most common form of accidents abroad?",
                    answer: `<p>Our personal injury solicitors can help if you were injured abroad in a road traffic accident that was not your fault. Whether you were injured as a pedestrian or if your injuries occurred in car crash, on a coach trip, in a motorbike accident or whilst you were riding a bicycle, a scooter or a horse; you can claim compensation by calling our No Win No Fee solicitors and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">providing the details about the accident</a>.</p><p>You can claim compensation if you slipped, tripped and fell injuring yourself in a hotel or on the grounds of a resort; slips and trips are quite common near swimming pools and also occur when boarding or leaving an aeroplane or a train. Falls from balconies are unfortunately another common accident which can occur abroad. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/\">Click here for more on injuries in public places</a>.</p><p>Compensation claims for accidents at sea include cruise ship and boating accidents, scuba diving and water sports accidents. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/cruise-ship/\">Click here for more information see Accidents at Sea.</a></p><p>Need more advice on accidents abroad? Read our help and support FAQs below.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Compensation claims for accidents abroad`,
				copy: `If you've been injured abroad and it wasn't your fault, it's possible to make a compensation claim from here in the UK. Slater and Gordon is a specialist claims firm with extensive experience of helping clients who have been injured in accidents abroad.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/holiday-accident-claims-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Accidents whilst on an airplane`,
						url: `/personal-injury-claim/holiday-accident-claims/air/`,
						icon: `plane`
					},
					{
						title: `Accidents on cruise ships`,
						url: `/personal-injury-claim/holiday-accident-claims/cruise-ship/`,
						icon: `cruise-ship`
					},
					{
						title: `Accidents in a hotel or resort`,
						url: `/personal-injury-claim/holiday-accident-claims/hotel-resort/`,
						icon: `hotel`
					},
					{
						title: `Accidents in public`,
						url: `/personal-injury-claim/injury-in-public/`,
						icon: `accidents-in-public`
					}
				]
			},
			standardContent: `product-template/holiday-accident-claims`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
			imageTextA: {
				section: `Accidents abroad`,
				heading: `How can we help you with your accidents abroad claim?`,
				copy: `Whether travelling away on business or with the family, you are still at risk of injury or an accident abroad. If you or a loved one have been injured while abroad you may be entitled to make a travel accident claim.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/82909040.jpg`,
					medium: `/public/images/bitmap/medium/82909040.jpg`,
					small: `/public/images/bitmap/small/82909040.jpg`,
					default: `/public/images/bitmap/medium/82909040.jpg`,
					altText: `Mother and son at the zoo together`
				},
				video: false
			},
		}
	}
}