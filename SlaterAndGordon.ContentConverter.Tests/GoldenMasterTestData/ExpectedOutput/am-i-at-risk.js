module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `am-i-at-risk`,
		slug: `am-i-at-risk`,
		parent: `asbestos-mesothelioma`,
		name: `Am I at risk of developing mesothelioma?`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Am I at risk of developing mesothelioma? | Slater + Gordon`,
				description: `Exposure to asbestos carries a number of serious health risks. If you or a loved one has been exposed to asbestos, Slater and Gordon offers the expertise you need, and the security of No Win No Fee compensation claim agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "Am I at risk of asbestosis?",
                    answer: `<p>If you've been exposed to asbestos during your working life, there are significant risks that you could develop mesothelioma or another asbestos related condition such as asbestos related lung cancer, asbestosis, pleural thickening or pleural plaques. If you know or suspect that this happened to you, particularly if you worked in the building or construction industries in the 1970s to 1990s, you should seek medical advice, even if you don't have any symptoms of lung problems. If you believe you may have an asbestos related disease, you can also speak to one of our experts for advice regarding a compensation claim.</p>`
                  },
                  {
                    question: "Which professions are most at risk from asbestos?",
                    answer: `<p>Asbestos was very common as a building material in the last century, largely for its fire-retardant properties. For this reason, almost any occupation that involved work on buildings from before the year 2000 could have meant exposure to asbestos. According to the <a href=\"http://www.hse.gov.uk/asbestos/risk.htm\">Health and Safety Executive</a> (HSE), members of all these professions may have been affected:</p><ul><li>Heating and ventilation engineers</li><li>Demolition workers</li><li>Carpenters and joiners</li><li>Plumbers</li><li>Roofing contractors</li><li>Painters and decorators</li><li>Plasterers</li><li>Construction workers</li><li>Fire and burglar alarm installers</li><li>Shop fitters</li><li>Gas fitters</li><li>Computer and data installers</li><li>General maintenance staff eg caretakers</li><li>Telecommunications engineers</li><li>Architects, building surveyors, and other such professionals</li><li>Cable layers</li><li>Electricians</li></ul><p>In recent years, we've also seen an increasing number of teachers and lecturers working in schools and colleges who have gone on to develop an asbestos related condition after being exposed in those buildings which contained asbestos and where asbestos was disturbed. See the video below.</p>`
                  },
                  {
                    question: "Where was the greatest asbestos risk?",
                    answer: `<p>According to the HSE, there are a number of factors that put workers at risk from asbestos fibres. These include:</p><ul><li>working on buildings from before 2000</li><li>working on an unfamiliar site</li><li>where asbestos containing materials were not identified before a job was started</li><li>where asbestos containing materials were identified, but this information was withheld<a href=\"http://www.hse.gov.uk/asbestos/risk-assessments.htm\"></a></li><li><a href=\"http://www.hse.gov.uk/asbestos/risk-assessments.htm\"></a>where workers weren't trained to recognise and work safely with asbestos</li></ul><p>If you believe that you may have been made ill by exposure to asbestos fibres for any of these reasons, talk to us about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a> today. </p>`
                  },
                  {
                    question: "My exposure to asbestos didn't take place at work. Can I still make a claim?",
                    answer: `<p>There are various ways in which you could have been exposed to asbestos when you were not in work. However, if you've been exposed to asbestos and someone else was at fault, regardless if you were for the company who exposed you, you may still have the right to claim compensation. Examples of this would be:</p><ul><li>Wives washing their husbands' overalls which were covered with asbestos dust - there have been many examples of this over the years. In this case, the company where your husband worked would be responsible. </li><li>Residents of an area where a nearby asbestos factory who found asbestos around their property - unfortunately we've also seen cases where factories are responsible for exposure as processes were not in place to prevent asbestos fibres being airborne to nearby residents. </li><li>Professional builders carrying out building work on a property which contained asbestos - most building companies when coming across asbestos will follow the health and safety regulations and ensure risks are minimised. However, if they don't there can be catastrophic consequences. </li></ul><p>If someone else has been negligent for your exposure to asbestos, regardless of whether it happened at work, you may still have the right to compensation. If you're not sure of your rights and whether you have a claim, <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact one of our specialists</a> who will provide you with all the expert advice you need. </p>`
                  },
                  {
                    question: "What products contain asbestos fibres?",
                    answer: `<p>The Asbestos Information Centre<strong> </strong>has compiled a list of nearly 70 types of products that were manufactured with an element of asbestos fibres between the 1940s and 1999. This list is fairly exhaustive, and includes a number of surprising sources of asbestos fibres, including everything from packaging to plaster and from tiling to theatre props. It's worth <a href=\"https://www.aic.org.uk/list-typical-asbestos-containing-materials-acms/\">checking the list they give here</a>, if you believe you might have been exposed to asbestos fibres. You can also speak to one of our specialist asbestos solicitors if you believe you may have been made ill through exposure to asbestos.</p>`
                  },
                  {
                    question: "Does asbestos cause many deaths?",
                    answer: `<p>One of the worst things about asbestos related illnesses is that the symptoms can emerge many years after the original exposure occurred. That's why, even though asbestos materials have been illegal for over 20 years, a significant number of asbestos related illnesses and deaths are still occurring. According to HSE figures for 2018, there were over 500 asbestos related disease deaths, including those from mesothelioma, lung cancer and asbestosis.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Am I at risk of asbestos or mesothelioma?`,
				copy: `Asbestos has had a wide variety of uses throughout history. Here we outline all the professions that may have come into contact with the carcinogenic material..`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/am-i-at-risk-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/am-i-at-risk`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}