module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `slips-trips-falls`,
		slug: `slips-trips-falls`,
		parent: `injury-in-public`,
		name: `Slips, Trips & Falls Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Slips, Trips & Falls Injury Compensation Claims | Slater + Gordon`,
				description: `Slips, trips and falls can all cause serious injuries. Where you have been hurt by someone else's negligence, you may be able to claim compensation. Talk to Slater and Gordon today about making a No Win No Fee compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been hurt by tripping over, do I have a claim?",
                    answer: `<p>The most common reason for a slip, trip or fall claim is when you are in a public place where the owners should expect people to be walking around, yet the ground you are walking on is in an unsafe condition due to negligence. This can include:</p><p>·       Wet, slippery floors in public places like supermarkets and airports</p><p>·       Uneven flooring in public places including banks, offices and shops</p><p>·       Cracked, wobbly or uneven pavements</p><p>·       Unexpected obstacles that cause a tripping hazard</p><p>·       Inadequate lighting that makes it hard to see kerbs and other obstacles</p><p>So if you have had a slip, trip or fall in a public place that has led to you being injured, and you believe that it was caused by any of these reasons, you should ask us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee injury compensation claim</a>.</p>`
                  },
                  {
                    question: "I was hurt at work, can I still claim?",
                    answer: `<p>A large number of slip, trip and fall claims arise in the workplace; which is why every reputable employer carries <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">Public Liability insurance</a>, which will cover them in case visitors have an accident on their premises due to their negligence. In fact, you can claim wherever a slip, trip or fall has been caused by negligence: including your rented accommodation, if, for example, your accident has been caused by a tripping hazard that your landlord should have fixed. As long as you were not in your own home, or trespassing on private property, if your accident was caused by someone else's negligence, you may be able to claim compensation for your injuries. </p><p>Where an accident occurs in a public place and the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">injured person is an employee</a>, the property owner's <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">Employer's Liability insurance</a> will cover them for any compensation claim which may arise. If you have had an accident in a public place where you are an employee, our Accident at Work<strong> </strong>specialists can help. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Slip, trip and fall compensation claims`,
				copy: `Slips, trips and falls can all lead to serious injuries. If you have been hurt in this way and believe that it was caused by someone else's negligence, talk to Slater and Gordon: one of the UK's leading injury compensation firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/slips-trips-falls-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/slips-trips-falls`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}