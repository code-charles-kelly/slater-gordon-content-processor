module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `signed-a-waiver`,
		slug: `signed-a-waiver`,
		parent: `personal-injury-claim`,
		name: `What if I Signed a Waiver? Injury Claim`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `What if I Signed a Waiver? Injury Claim | Slater + Gordon`,
				description: `Have you been injured at the gym or taking part in an event, but had signed a waiver first? You may still be able to claim if someone else was at fault. Talk to Slater and Gordon today about making a No Win No Fee compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "What are waivers for?",
                    answer: `<p>A lot of people believe that signing a waiver immediately means you are not allowed to sue a venue or establishment if you're injured. However, this is simply not the case. A waiver is intended to inform the participants of an activity of the potential risk of harm. However, this doesn't mean they can be negligent or relinquish the duty of care that many of these establishments will have.</p>`
                  },
                  {
                    question: "I signed a waiver. Can I still claim for an injury?",
                    answer: `<p>Waivers are very popular with organisations such as gyms and companies who organise events, such as fun runs and charity abseiling. After all, they don't want to be sued by everyone who accidentally hurts themselves while they're on the premises or at the event. So waivers are perfectly legal and it's entirely reasonable for you to be asked to sign one. However, a waiver doesn't absolve event organisers or gym owners from having a proper duty of care towards people who have signed a waiver. So if you've been injured due to any of these things, you may still be able to claim for your injuries:</p><ul><li>Faulty or dangerous equipment</li><li>Inadequate training</li><li>Poorly organised events</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">Slips on unexpected wet surfaces</a></li><li>Trips on uneven surfaces</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/sports-injury/\">Sporting accidents</a> (as a participant or spectator)</li></ul><p>Naturally, if you were taking part in a fun run, for example, and you stumbled and fell, that would be simply bad luck. However, if you were hurt tripping over a hidden, man-made obstacle in the starting area, that might well be considered negligence by the organisers. Talk to one of our expert solicitors to see if <a href=\"https://www.legislation.gov.uk/ukpga/1977/50\">the </a><a href=\"https://www.legislation.gov.uk/ukpga/1977/50\">Unfair Contract Terms Act </a>could help you to make a claim, even if you have signed a waiver. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `What if I signed a waiver? Can I still make a claim?`,
				copy: `Even if you signed a waiver of liability at a gym or sporting event, you still have the right to be kept safe, and to claim compensation if you were injured through someone else's negligence. Talk to Slater and Gordon: one of the UK's leading injury compensation firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/signed-a-waiver-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/signed-a-waiver`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}