module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `personal-injury-claim`,
		slug: `personal-injury-claim`,
		parent: false,
		name: `Personal Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Personal Injury Compensation Claims Solicitors | Slater + Gordon`,
				description: `If you've been injured and it wasn't your fault, you may be entitled to personal injury compensation. It doesn't matter if you were in the UK or abroad, at work or in the street: wherever you have been injured we want to help. Slater and Gordon handles the majority of personal injury claims on a No Win No Fee basis.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a personal injury?",
                    answer: `<p>A personal injury isn't always visible like a bruise or a broken bone; the words 'personal injury' describe many different types of harm that can befall you in your daily life. That's why our personal injury solicitors are experienced in dealing with everything from slips and trips to industrial disease. If a personal injury has been caused by someone else, you will often be able to claim compensation for the injuries sustained. The majority of our personal injury claims are made on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee</a> basis.</p>`
                  },
                  {
                    question: "Do I have a claim?",
                    answer: `<p>Everyone is entitled to go about their everyday life in safety. Unfortunately, the fact is that every year tens of thousands of people in the UK are injured or made ill by the carelessness or negligence of others. When an incident occurs which causes you to be injured or become ill, due to someone else's carelessness or negligence, you may be able to claim compensation. Slater and Gordon handle thousands of personal injury claims, for all types injuries and illnesses.</p><p>In short, if you've sustained a personal injury that wasn't your fault - even if you were partly at fault - we're here to help. Talk to us today about making a No Win No Fee personal injury claim. </p>`
                  },
                  {
                    question: "How much is my personal injury claim worth?",
                    answer: `<p>Every personal injury claim is different, so the final figure you might receive depends on a number of factors such as the severity of your injuries and any other damages that need to be claimed for such as loss of earnings. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">Click here to learn what you can claim for</a>.</p><p>We handle a large proportion of personal injury compensation cases on a No Win No Fee<strong> </strong>basis, and will often seek interim payments where possible to help you avoid hardship caused by any personal injury or illness that wasn't your fault. </p>`
                  },
                  {
                    question: "What is a No Win No Fee personal injury claim?",
                    answer: `<p>If you've been harmed and someone else was to blame, you have the right to seek compensation without any financial risk to you. That's why we handle the vast majority of personal injury cases on a No Win No Fee basis, to give you the confidence to ask for rightful compensation but without taking any financial risks. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/faq/\">Click here to learn more about our No Win No Fee claims</a>.</p>`
                  },
                  {
                    question: "Are Slater and Gordon the right claims firm for me?",
                    answer: `<p>When you've suffered a personal injury, you need solicitors you can talk to and trust. We are proud of our 90+ years experience we have in house and the many cases we've won, don't take our word for how good we are. Instead, listen to what some of our valued clients have to say about us.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Personal injury compensation claims solicitors`,
				copy: `If you've suffered a personal injury or been made ill, you may be entitled to compensation. It doesn't matter whether you were in the UK or abroad, at work or in a public place, we're here to help. Talk to us about the possibility of making a personal injury claim under a No Win No Fee agreement.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/personal-injury-claim-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Road traffic accidents`,
						url: `/personal-injury-claim/road-traffic-accidents/`,
						icon: `car`
					},
					{
						title: `Accidents at work`,
						url: `/personal-injury-claim/accident-at-work-compensation/`,
						icon: `briefcase`
					},
					{
						title: `Accidents and injuries abroad`,
						url: `/personal-injury-claim/holiday-accident-claims/`,
						icon: `person-with-crutch`
					},
					{
						title: `Asbestos related illness`,
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/`,
						icon: `lungs`
					},
					{
						title: `Public`,
						url: `/personal-injury-claim/injury-in-public/`,
						icon: `man-crossing-road`
					},
					{
						title: `Sexual and physical abuse`,
						url: `/personal-injury-claim/sexual-criminal-abuse/`,
						icon: `shield`
					},
					{
						title: `Illness claims`,
						url: `/personal-injury-claim/illness/`,
						icon: `serious-injury`
					},
					{
						title: `Faulty products`,
						url: `/personal-injury-claim/faulty-products/`,
						icon: `plug-socket`
					},
					{
						title: `Industrial disease`,
						url: `/personal-injury-claim/industrial-disease/`,
						icon: `factory`
					}
				]
			},
			onwardJourneysB: {
				title: ``,
				ctas: [
					{
						title: `Head or brain injury`,
						url: `/personal-injury-claim/serious/head-brain/`,
						icon: `head-and-brain`
					},
					{
						title: `Amputation or limb damage`,
						url: `/personal-injury-claim/serious/amputation-limb-damage/`,
						icon: `man-crutch`
					},
					{
						title: `Burns scars or lacerations`,
						url: `/personal-injury-claim/serious/burns-scars-lascerations/`,
						icon: `burns-scars-lacer`
					},
					{
						title: `Whiplash`,
						url: `/personal-injury-claim/road-traffic-accidents/whiplash/`,
						icon: `whiplash`
					}
				]
			},
			standardContent: `product-template/personal-injury-claim`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
			imageTextA: {
				section: `Personal injury`,
				heading: `What sort of personal injuries can I claim for?`,
				copy: `The list below covers some of the most common causes of personal injury Slater and Gordon deals with on a daily basis. If you have been injured through no fault of your own contact us or click on the icons below to learn more about how we can help you based on your injury type.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/233958265.jpg`,
					medium: `/public/images/bitmap/medium/233958265.jpg`,
					small: `/public/images/bitmap/small/233958265.jpg`,
					default: `/public/images/bitmap/medium/233958265.jpg`,
					altText: `Disabled father in wheelchair with his daughter, wife and dog in the park`
				},
				video: false
			},
			imageTextB: {
				section: `Personal injury`,
				heading: `Or tell us where you have been hurt.`,
				copy: `Personal injuries can happen in many different unexpected ways. Regardless of how or where you have been injured through someone else's negligence, we're here to help you through it. Select your accident type below to learn more about how we can help you.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/222814214.jpg`,
					medium: `/public/images/bitmap/medium/222814214.jpg`,
					small: `/public/images/bitmap/small/222814214.jpg`,
					default: `/public/images/bitmap/medium/222814214.jpg`,
					altText: `Young family with child and a dog walking outdoors`
				},
				video: false
			},
			videoTextB: {
				section: ``,
				heading: `Keith's Story`,
				copy: `Keith was one of more than 50 people who survived with life-changing injuries. Hospital worker, Keith Chapman, was walking across Westminster Bridge when terrorist, Khalid Masood drove his car into a crowd, murdering six people.`,
				image: false,
				video: `wkkw2tx7mv`
			}
		}
	}
}