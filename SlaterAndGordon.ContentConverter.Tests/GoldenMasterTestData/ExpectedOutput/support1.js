module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `support1`,
		slug: `support`,
		parent: `asbestos-mesothelioma`,
		name: `Living With Mesothelioma, Support Links | Slater and Gordon`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Living With Mesothelioma, Support Links | Slater and Gordon`,
				description: `It can be hard to live with an asbestos related illness, such as mesothelioma. Fortunately, there are a number of organisations that can help you to make the most of life. Slater and Gordon also offers you considerable experience of making successful asbestos compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "How do I live with mesothelioma, asbestos related lung cancer, asbestosis or pleural thickening?",
                    answer: `<p>Given their impact on the lungs and other vital organs, asbestos-related illnesses aren't easy to live with. However, there are a number of extremely supportive organisations and charities that offer advice and support to everyone living with asbestos-related disease. They include:</p><ul><li><a href=\"https://a-a-s-c.org.uk/\">Asbestos Awareness and Support Cymru (AASC)</a>: 01495 272 2479</li><li><a href=\"http://www.asbestossupportwm.org/\">Asbestos Support West Midlands</a>: 0121 678 8853</li><li><a href=\"https://www.cavsg.com/\">Cheshire Asbestos Victims Support Group (CAVSG)</a>: 01928 576 641</li><li><a href=\"https://asbestossupport.co.uk/\">Derbyshire Asbestos Support Team (DAST)</a>: 01246 380 415</li><li><a href=\"https://disabilityfirst.org/\">Disability First</a>: 0125 347 2202</li><li><a href=\"http://www.asbestos-victims-support.org/\">Greater Manchester Asbestos Victims Support Group (GMAVSG)</a>: 0161 636 7555</li><li><a href=\"https://www.hasag.co.uk/\">Hampshire Asbestos Support Awareness Group (HASAG)</a>: 02380 010 015</li><li><a href=\"https://mavsg.org/\">Merseyside Asbestos Victims Support Group (MAVSG)</a>: 0151 236 1895</li><li><a href=\"http://www.readley.co.uk/\">Readley Asbestos Support Awareness Group</a>: 07929 391529</li><li><a href=\"http://saragasbestossupport.org/\">South Yorkshire Asbestos Victim Support Group (SARAG)</a>: 01709 360 672</li><li><a href=\"https://www.swasag.co.uk/about/\">South West Asbestos Support and Awareness Group (SWASAG)</a>: 07446 255524</li><li><a href=\"http://www.asbestosforum.org.uk/\">Asbestos Victims Support Forum UK</a> (AVSGF-UK): 0161 636 7555</li><li><a href=\"https://www.blf.org.uk/\">British Lung Foundation</a>: 03000 030 555</li><li><a href=\"https://www.cancerresearchuk.org/about-cancer/mesothelioma\">Cancer Research UK for mesothelioma</a>: 0808 800 4040</li><li><a href=\"https://www.mesothelioma.uk.com/\">Mesothelioma UK</a>: 0800 169 2409</li></ul><p>The support these organisations and charities provide is invaluable. </p>`
                  },
                  {
                    question: "How does mesothelioma affect your life?",
                    answer: `<p>Any diagnosis of cancer is distressing, for sufferers and their families. It can be a time of great fear and uncertainty. However, advice on living life to the fullest after a diagnosis is available from a number of organisations, including <a href=\"https://www.mesothelioma.uk.com/\">Mesothelioma UK</a><strong>.</strong></p><p>In addition to specialist help and support, you may need financial help to protect you and your family for the future. For that reason, you may wish to speak to a specialist solicitor to discuss the possibility of making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a>.</p>`
                  },
                  {
                    question: "How do I stop smoking after a mesothelioma diagnosis?",
                    answer: `<p>One of the most distressing aspects of mesothelioma is that sufferers feel breathless for much of the time. This situation can be made worse if you are a smoker; and being diagnosed with mesothelioma doesn't necessarily make it any easier to quit smoking. That's why the <a href=\"https://www.nhs.uk/live-well/quit-smoking/nhs-stop-smoking-services-help-you-quit/\">NHS</a> provides an enormous amount of support to help mesothelioma sufferers to give up smoking as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Living with an asbestos related illness such as mesothelioma`,
				copy: `Mesothelioma, asbestos related lung cancer, asbestosis and pleural thickening are all linked to exposure to asbestos fibres. As they affect the lungs and other organs, they can all affect the quality of your life. Slater and Gordon is here to help you claim the compensation you deserve, and there are a number of other organisations that provide advice and support.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/support1-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/support1`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `Our Asbestos Team`,
				copy: `Slater and Gordon work with a variety of asbestos support groups nationally, to help innocent people affected by related diseases.`,
				image: false,
				video: `hn0l4jdiek`
			}
		}
	}
}