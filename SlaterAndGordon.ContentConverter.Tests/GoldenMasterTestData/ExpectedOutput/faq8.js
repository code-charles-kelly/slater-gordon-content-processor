module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq8`,
		slug: `faq`,
		parent: `asbestos-mesothelioma`,
		name: `Asbestos & Mesothelioma FAQ`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Asbestos & Mesothelioma FAQ | Slater + Gordon`,
				description: `Asbestos related illnesses such as mesothelioma are very serious conditions that might leave you or your loved one with a wide variety of doubts and uncertainties. Here we try to cover a list of the most common questions our clients ask us.`,
				robots: ``,
                schema: [
                  {
                    question: "I've been diagnosed with an asbestos related condition, can I make a compensation claim?",
                    answer: `<p>If you've been diagnosed with an asbestos related condition, such as mesothelioma, asbestos related lung cancer, asbestosis or pleural thickening, you may have the right to claim compensation. </p><p>Pleural plaques is no longer compensatable in England and Wales. However, it is still compensatable in Scotland. Read our page on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/scottish-pleural-plaques\">Scottish pleural plaques claims </a>for more information. </p>`
                  },
                  {
                    question: "Where would I have been exposed to asbestos?",
                    answer: `<p>The vast majority of people who have developed an asbestos related illness, were exposed to asbestos during their employment when working in trades such as the building trade, However, there has been an increasing number of people who have suffered second hand exposure to asbestos, such as family members who washed the overalls of people who had worked with asbestos all day and those who lived near asbestos factories. There have also been a number of teachers and lecturers who have developed an asbestos related disease from asbestos in schools where the asbestos has been disturbed. Read our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/\">Am I at Risk </a>page for more information about where you may have been exposed to asbestos</p>`
                  },
                  {
                    question: "How do I start a compensation claim after being diagnosed with an asbestos related condition?",
                    answer: `<p>The process of starting a compensation claim is straightforward and won't take hours of your time. All you need to do is get in touch with us via freephone number ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a> explaining the circumstances of your diagnosis and your exposure. Your case will then be assessed and allocated to a specialist asbestos lawyer who will contact you to take some further information and to arrange to visit to you at home to discuss the process of making a claim in more detail.</p>`
                  },
                  {
                    question: "Do I need to make my asbestos compensation claim within a certain time after diagnosis?",
                    answer: `<p>The law states that an asbestos compensation claim generally needs to be made within three years from the date you were diagnosed with an asbestos related condition. In the unfortunate event that a loved one has passed away as a result of an asbestos related condition, you would generally have to make a claim within three years from the date they passed away. </p>`
                  },
                  {
                    question: "How long will my asbestos compensation claim take to process?",
                    answer: `<p>Asbestos cases vary in circumstances so it's difficult to pinpoint exactly how long a single case will take to complete. One factor which will help determine how long a case will take is whether or not responsibility is accepted by the other side. </p>`
                  },
                  {
                    question: "If the company I worked for has ceased trading, can I still make an asbestos compensation claim?",
                    answer: `<p>It doesn't matter if the company you worked for has gone out of business, even if they went out of business many years ago. At the time you were exposed to asbestos, the company would have had <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers' liability insurers</a> and it is those insurers who will be responsible for the compensation. Slater and Gordon has an excellent success rate of tracing companies and their insurers due to many years' experience dealing with asbestos related illness cases, so you needn't worry.</p>`
                  },
                  {
                    question: "Can I make an asbestos compensation claim if I no longer live in the UK?",
                    answer: `<p>If you were exposed to asbestos in the UK but live outside of the UK, you can still make a claim for compensation and may be entitled to payments from the UK Government. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/\">We cover this topic in more detail here</a>.</p>`
                  },
                  {
                    question: "Can I make a compensation claim on behalf of someone who has died from an asbestos disease?",
                    answer: `<p>You can make a claim on behalf of a loved one who has passed away from an asbestos disease. However, you must make the claim within three years of the date they passed away so you should speak to an experienced lawyer without delay. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">Click here to learn more on claiming on behalf of a loved one</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestos and mesothelioma frequently asked questions`,
				copy: `A list of the most common questions our lawyers get asked about asbestos and its effects.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/faq8-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq8`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}