<div class="inner-block">
  <h2 class="text text--heading text--large">Who can you talk to about sexual assault or abuse?</h2>
  <p class="text">This brief guide is designed to provide a reference point to help them understand the legal definitions of abuse, and the ways in which our experienced solicitors might help them to seek justice and compensation.</p>
  <p class="text">If you, a family member, or a close friend has suffered from any kind of abuse or sexual assault, we know that it can be incredibly hard to talk about. Even though you are totally blameless, it's inevitable that telling someone about it will be a highly emotional experience. As a firm that has specialised in cases of sexual abuse and assault for many years, we have a choice of lawyers available for you to talk to, all of whom have had these difficult conversations many, many times. </p>
  <p class="text">When you're ready to talk, they will be here for you: making every conversation as stress-free as possible. In the meantime, here are some of the legal definitions of sexual abuse and assault, to help you understand the legal description of the wrong that has been done, as well as the reasons why you have a right to seek both justice and compensation.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Why is it possible to claim compensation if you've been abused?</h2>
  <p class="text">If you've suffered from abuse or a sexual assault not only has a criminal offence been committed, but you've also suffered as a result. Just like with any other personal injury case, if you've been injured, either physically and/or psychologically, you should be compensated for your suffering. There can also be other costs which have been incurred when you have suffered from abuse or a sexual assault, such as loss of earnings if you've been unable to work and treatment such as counselling. It's therefore important that not only do you get the justice you deserve, but also, you're compensated for your suffering and also any financial losses. </p>
  <p class="text">If you or a loved one have suffered from abuse or a sexual assault, contact one of our experienced abuse lawyers. They're all caring people and good listeners, having supported so many people who have suffered abuse or a sexual assault for so many years. So when you're ready to talk to someone, they're here to listen. </p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">What are the most common types of abuse cases?</h2>
  <p class="text">There's never an excuse for abuse or a sexual assault, and you have a right to be compensated for your suffering, no matter what type of abuse case you have. However, some of the most common types of abuse cases include:</p>
  <p class="text">• Abuse suffered as a child in local authority or foster care</p>
  <p class="text">• A failure by Social Services to protect a child from abuse</p>
  <p class="text">• Abuse by a person in a position of trust (such as a priest or teacher)</p>
  <p class="text">• Abuse suffered within the family</p>
  <p class="text">• Abuse of the elderly in a nursing or care home</p>
  <p class="text">• Sexual or physical abuse suffered as an adult</p>
  <p class="text">• A single incident of a sexual or physical assault</p>
  <p class="text">If you, a family member or a friend have been subjected to abuse or a sexual assault, regardless of the situation, we are here to listen in confidence and advise you on whether you may be able to seek justice by claiming compensation.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Why should someone sue an abuser?</h2>
  <p class="text">It's perfectly natural for people who have suffered from abuse to want to get the justice they deserve. In most cases, it's part of the healing process as some people find that it helps them, on an emotional level, to take positive action. On a practical level, where you have suffered psychological or physical harm, it can adversely affect your education or your ability to work. It might even mean that you spend money you can't really afford on counselling. For both emotional and practical purposes, these are very good reasons to seek financial compensation for your suffering.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Can I stay anonymous if I make a claim?</h2>
  <p class="text">Many victims of abuse fear that their lives will become public if they make a claim against whoever was responsible for the abuse. You can rest assured that we always ensure complete anonymity for our clients, even in cases that arouse press interest.</p>
  <p class="text">It's important to understand that we, and the law, ensure that you have this protection. Our specialist team has dealt with many abuse cases over the years, and victim anonymity has never been compromised, even when the abuser is someone in the public eye and the case is reported in the media. Here you can find out more about how <a href="/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/" class="link link--inline"><span class="link__text">we protect your confidentiality and anonymity</span></a>.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How do I start a compensation claim?</h2>
  <p class="text">It's very simple to contact us and arrange an informal chat. You can either call us on {{{meta.phonenumber.contentLink}}} or <a href="/contact-us/" class="link link--inline"><span class="link__text">contact us online</span></a> to speak to one of our legal experts. We'll make sure you can speak to an experienced abuse solicitor who will talk to you over the phone, or arrange to speak to you at a location convenient for you for an informal chat. You can rest assured that we'll not only understand what you've been through, but also be totally honest with you about the chances of your case being successful. </p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How much does it cost to make a claim?</h2>
  <p class="text">The vast majority of cases are run under a <a href="/personal-injury-claim/no-win-no-fee-claims/" class="link link--inline"><span class="link__text">No Win No Fee agreement</span></a>, which means if you were to lose your case, you wouldn't have to pay a penny and therefore, there would be no financial risk to you. </p>
  <p class="text">It's also possible that one of your existing insurance policies offers legal expenses cover, and if a criminal claim is made <a href="/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/" class="link link--inline"><span class="link__text">under the CICA Scheme</span></a>, it can be funded under what's called a Contingency Fee Agreement.</p>
  <p class="text">However, we'll be able to discuss all of the possible funding options once we know more about your case.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How much compensation might I get?</h2>
  <p class="text">There is no standard figure for abuse compensation claims. In successful cases, <a href="/personal-injury-claim/what-can-i-claim-for/" class="link link--inline"><span class="link__text">the amount won is affected by a wide variety of factors</span></a>, from loss of earnings to the cost of medical treatment or counselling.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How long will my cases take?</h2>
  <p class="text">Again, there is no fixed amount of time that abuse cases take to reach a conclusion, particularly if it occurred some time ago and witnesses or paperwork need to be found. All we can promise is that your lawyer will do everything in their power to deal with your case as efficiently as possible.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Will I have to go to court?</h2>
  <p class="text">The vast majority of abuse compensation cases are settled out of court. However, in the rare situation that you were asked to speak in court, you would still keep your anonymity, and it's highly unlikely that the abuser would be at the hearing. In any event, we would be at your side throughout.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Will criminal proceedings affect my compensation claim?</h2>
  <p class="text">Criminal proceedings are completely separate from a civil claim for compensation, although the outcome of a criminal case is relevant. However, even if an abuser is not found guilty of abuse in a criminal court, it may still be possible to make a civil compensation claim, as the evidential burden in a civil case is lower, and the issues are often different to those in the criminal case. We'll be able to tell you more once we know the circumstances of your case.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Can I claim if I - or my abuser - don't live in the UK?</h2>
  <p class="text">It's still possible to pursue a claim for compensation if you or the abuser lives overseas. It's also possible to claim if the abuse itself occurred overseas, but the laws in other countries may come into account. We can advise you further once we have all the details.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">What if I need help with other areas of my life?</h2>
  <p class="text">We have experts in many areas of the law, such as employment law, family law, conveyancing, wills, trusts and probate. We also have a Court of Protection team who protect the finances of those who don't feel they have the mental capacity to look after their own, and an independent financial planning company who can provide investment services in respect of any compensation received to help ensure your future is financially secure. We'll be there for you for all your future's legal and financial needs. </p>
</div>
