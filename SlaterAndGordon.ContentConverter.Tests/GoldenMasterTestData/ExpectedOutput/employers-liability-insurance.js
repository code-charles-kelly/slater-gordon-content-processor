module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `employers-liability-insurance`,
		slug: `employers-liability-insurance`,
		parent: `accident-at-work-compensation`,
		name: `Employers' Liability Explained`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Employers' Liability Explained | Slater + Gordon`,
				description: `Employers' Liability Insurance is legally required by most UK employers. Here's what you need to know about it, and about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>Employers' liability insurance is a policy intended to ensure that compensation can be paid to any employees who are injured or made ill as a result of their jobs. It's legally required for most companies, with the only exceptions being:</p><ul><li>Public service employers such as the NHS, Government departments and passenger transport executives</li><li>Family firms, where all of the employees are related, except when they are Limited companies</li><li>Businesses that only employ the owner, or a single employee abroad</li></ul><p>Employers' liability insurance must offer cover of at least £5 million per claim, but limits of £10 million per claim are not uncommon. This ensures that even in the most serious compensation cases  - where employer negligence is proven and death or a life-changing injury has occurred - every employer has insurance cover to meet the costs of compensation and rehabilitation for an employee.</p>`
                  },
                  {
                    question: "What does employers' liability insurance cover?",
                    answer: `<p>Employers' liability insurance offers far more protection to employees than it does to employers. It ensures that a legal minimum of money is available to pay employees compensation for any illness or injury arising out employer negligence. With that in mind, insurers can't refuse to pay a claim simply because your employer:</p><ul><li>Hasn't provided reasonable protection to employees</li><li>Hasn't keep specified records </li><li>Has done something the insurers told them not to do - such as admitting liability</li><li>Hasn't done something they were told to do - like reporting the incident</li><li>Hasn't met legal requirements connected with protection of employees</li></ul><p>However, where an employer has been seriously negligent, many insurance companies reserve the right to sue them to reclaim the cost of the compensation paid to employees.</p>`
                  },
                  {
                    question: "What are the consequences for employers who don't have employers' liability insurance?",
                    answer: `<p>Unless they fall into one of the categories above, <a href=\"https://www.gov.uk/employers-liability-insurance\">the law</a> says that under the <a href=\"http://www.hse.gov.uk/pubns//hse40.pdf\">Employers' Liability (Compulsory Insurance) Act 1969</a>, every UK employer must have employers' liability insurance of at least £5 million, issued by an insurer that is authorised by the Financial Conduct Authority (FCA). If they fail to do so, they can be fined up to £2,500 a day, and even failing to display a valid employers' liability insurance certificate can lead to a £1,000 fine.</p>`
                  },
                  {
                    question: "Does my employer have employers' liability insurance?",
                    answer: `<p>Your employer <strong>must</strong> hold employers' liability insurance <strong>and</strong> display a certificate somewhere that employees can see it. This is your reassurance that your employer can compensate you for any injuries or illness that arise from your employment. So, if your employer hasn't put this certificate on display, it's worth checking to make sure that they have the right cover, and remind them that they could be fined for failing to have it, as well as for failing to display a certificate.</p>`
                  },
                  {
                    question: "Talk to us about claiming against your employer",
                    answer: `<p>If you've had an accident at work that was not your fault Slater and Gordon can help you claim against your employers' liability insurance. <a href=\"https://www.slatergordon.co.uk/contact-us/\">Contact us online</a> or call us freephone on ${contentShared.meta.phonenumber.number} today.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Employers' liability insurance guide`,
				copy: `Employers' liability insurance is a legal requirement for almost every UK employer. This guide covers all you need to know about how it helps to pay for injury compensation.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/employers-liability-insurance-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/employers-liability-insurance`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}