module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `armed-forces-compensation-scheme`,
		slug: `armed-forces-compensation-scheme`,
		parent: `military`,
		name: `Armed Forces Compensation Scheme (AFCS)`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Armed Forces Compensation Scheme (AFCS) | Slater + Gordon`,
				description: `If you've suffered as a result of an injury or illness, or a loved one has died whilst in military services, our military experts can help you make a claim under the Armed Forces Compensation Scheme (AFCS).`,
				robots: ``,
                schema: [
                  {
                    question: "What is the Armed Forces Compensation Scheme(AFCS)?",
                    answer: `<p>The AFCS is a Government scheme managed by Veterans UK, which provides a set amount of compensation for any injury, illness or death that has been caused by service in the military either on or after 6 April 2005. It's a 'no fault' scheme, which means that responsibility for the incident causing the injury or illness will not be admitted but the MOD will look to compensate you for your injuries suffered during service. </p>`
                  },
                  {
                    question: "Who can make an AFCS claim?",
                    answer: `<p>The scheme is available to all current and former members of the UK Armed Forces, including Reservists. In order to claim, you don't have to have left the armed forces, you can still be serving. </p><p>You can also make a claim in the event that you've lost a loved one due to an incident whilst they were in service. Guidelines state that those eligible would be \"someone with whom you are cohabiting in an exclusive and substantial relationship, with financial and wider dependence\". If you're unsure whether or not you can make a claim, contact our military experts on freephone ${contentShared.meta.phonenumber.number} who will advise you if you're eligible. </p>`
                  },
                  {
                    question: "What type of injuries can you claim AFCS for?",
                    answer: `<p>If you're taking part in military activities such as training or organised sports, and suffer any injury or illness, whether a minor condition, a more serious condition or a mental condition, you can make an AFCS claim. The seriousness of the condition will obviously depend on the amount of compensation you'll be achieve. </p>`
                  },
                  {
                    question: "How much AFCS compensation could you receive?",
                    answer: `<p>'Lump-sum payments' are awarded for pain and suffering with the amount depending on the severity of the injury. Currently lump sum payments range from £1,236 to £650,000 with £650,000 being the absolute maximum you could receive. <a href=\"https://www.gov.uk/government/publications/armed-forces-compensation/armed-forces-compensation-what-you-need-to-know\">More info on the .gov website</a>.</p><p>When a serious injury or illness causes a significant loss of earning capacity, monthly tax-free payments known as Guaranteed Income Payments (GIP) may also be awarded. There are a number of factors which will determine the level of GIP you'll receive and will constitute a percentage of your salary. These payments will also be adjusted annually as inflation and cost of living increases are considered.</p><p>Our military claim experts can advise you further once they have details of your case. Call on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a>.</p>`
                  },
                  {
                    question: "Are you able to appeal an AFCS decision?",
                    answer: `<p>If the injury or illness you sustained worsened following an AFCS decision, there are circumstances in which the decision could be reviewed. If you're unhappy with a decision it's possible to apply for reconsideration and if still unsatisfied, appeal to an independent tribunal. </p>`
                  },
                  {
                    question: "Are there any time limits when making an AFSC claim?",
                    answer: `<p>Whilst there are exceptions, the claim should be made within seven years of the date the injury occurred or, in respect of an illness (<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/ptsd/\">such as PTSD</a>), the date you first requested medical advice for your illness. </p>`
                  },
                  {
                    question: "Why choose Slater and Gordon to act on your behalf?",
                    answer: `<p>Making an AFCS claim can be a long and complex procedure. Our experts in military claims are here to help reduce the burden of dealing with this procedure at, what can be, a difficult time. Our specialist lawyers have a great deal of expertise in military claims and making AFCS claims.</p><p>As we're experts in this field, we can ensure that the correct evidence is submitted to enable you to receive the maximum amount of compensation possible, and we will challenge decisions where we don't believe this has been achieved.</p><p>We also handle all AFCS claims on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, meaning there's no financial risk to you: as if your claim is unsuccessful, there will be no cost payable by you. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Military accident claims`,
				heading: `The armed forces compensation scheme (AFCS)`,
				copy: `Slater and Gordon have military experts who specialise in making AFCS claims on behalf of those who have suffered injury or illness as a result of their military service. The majority of our personal injury claims are on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/134406019.jpg`,
					medium: `/public/images/bitmap/medium/134406019.jpg`,
					small: `/public/images/bitmap/small/134406019.jpg`,
					default: `/public/images/bitmap/medium/134406019.jpg`,
					altText: `Military father sitting at home with family`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee military injury claims`,
				leftWysiwyg: `product-template/armed-forces-compensation-scheme-two-panel-cta`,
				rightHeading: `Choose Slater and Gordon to start your military accident claim`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/armed-forces-compensation-scheme`,
			sideLinkList: {
				heading: `Need more information on military accidents? Read our FAQ and support guides below.`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military accident FAQ`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `What is the armed forces compensation scheme?`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}