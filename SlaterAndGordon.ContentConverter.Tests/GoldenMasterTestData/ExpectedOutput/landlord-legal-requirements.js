module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `landlord-legal-requirements`,
		slug: `landlord-legal-requirements`,
		parent: `carbon-monoxide`,
		name: `Carbon Monoxide & Smoke Alarm Regulations`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Carbon Monoxide & Smoke Alarm Regulations | Slater + Gordon`,
				description: `Essential information for landlords: from the 1st October 2015 regulations required smoke alarms to be installed in rented residential accommodation and carbon monoxide alarms in rooms with a solid fuel appliance.`,
				robots: ``,
                schema: [
                  {
                    question: "What is required from landlords?",
                    answer: `<p>In order to protect tenants, landlords are required to ensure that working smoke alarms are installed on each storey of rented residential accommodation, with the addition of carbon monoxide alarms in any rooms where a solid fuel appliance is present. </p>`
                  },
                  {
                    question: "Why has the law for landlords changed?",
                    answer: `<p>The regulations for Landlords with regard to smoke alarm and carbon monoxide alarm installation changed in 2015, in response to a high level of fatalities in rental property. Government figures show that up to 2015, there were around 50 deaths in the UK every year due to carbon monoxide poisoning. They also reveal that people are four times more likely to die in a house fire where working smoke alarms aren't present. To underline the seriousness of the issue, non-compliance is punishable by fines of up to £5,000.</p>`
                  },
                  {
                    question: "What do the new regulations demand of landlords?",
                    answer: `<p><a href=\"https://www.gov.uk/government/publications/smoke-and-carbon-monoxide-alarms-explanatory-booklet-for-landlords/the-smoke-and-carbon-monoxide-alarm-england-regulations-2015-qa-booklet-for-the-private-rented-sector-landlords-and-tenants\">The Smoke and Carbon Monoxide Alarm (England) Regulations 2015</a> require landlords to have a working smoke alarm fitted on every storey of a rental property, and in every room that contains a solid fuel-burning appliance, such as a coal fire or wood-burning stove. Landlords are also required to test that these appliances are working on the first day of each new tenancy, after which the tenants are responsible for regular testing.</p>`
                  },
                  {
                    question: "What do the new regulations ask of tenants?",
                    answer: `<p>If you are renting in a residential property - whether a house or a room in a House of Multiple Occupancy (HMO) - you should make sure that your landlord has installed a working smoke alarm on each storey of the property you are renting, and a working carbon monoxide alarm in every room that has an open fire or solid fuel stove.You should do this on the day you move in, and make sure you know how to conduct future tests yourself. After this, the Fire Service advises that you should check that smoke alarms and carbon monoxide alarms are working at least once a month, and change the batteries once a year. </p>`
                  },
                  {
                    question: "What do I do if my landlord won't fit alarms?",
                    answer: `<p>You should really make sure that smoke detectors and carbon monoxide alarms are present and working on the day you move in. However, if this hasn't happened - and your landlord either prevaricates or refuses to act - you should complain to your <a href=\"https://www.gov.uk/find-local-council\">local council's Environmental Health Department</a> who will issue an enforcement notice to your landlord, telling them to fit the appropriate alarms within 28 days. If they fail to do so, they can be fined up to £5,000 and your local council will take responsibility for fitting the alarms you need. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Carbon monoxide and smoke alarm regulations for UK landlords`,
				copy: `Whether you are a tenant or a landlord you should know the following guidelines to make sure your household or that of your tenant is safe.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			standardContent: `product-template/landlord-legal-requirements`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}