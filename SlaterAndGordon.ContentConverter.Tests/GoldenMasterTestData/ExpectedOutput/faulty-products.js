module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faulty-products`,
		slug: `faulty-products`,
		parent: `personal-injury-claim`,
		name: `Defective Product`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Defective Product Compensation Claims | Slater + Gordon`,
				description: `From contaminated cosmetics to unsafe stepladders, defective products can cause serious injuries. Speak to Slater and Gordon today about starting a No Win No Fee defective product compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of injuries do defective products cause?",
                    answer: `<p>Manufacturers have a duty to test the products they sell to ensure that they are both safe and fit for purpose. When a company has been negligent in design, testing or manufacture, serious injuries can occur. Slater and Gordon has acted for injured parties in a wide range of product liability cases. Examples of defective product injury cases we have dealt with in the past include:</p><ul><li>A broken arm caused by falling off a defective ladder</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/food-poisoning/\">Poisoning caused by contaminated food</a> bought in a shop</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/burns-scars-lascerations/\">Burns caused by electrical goods</a> breaking or catching fire</li><li>Scarring caused by contaminated or untested cosmetics</li><li>Hair loss and scarring caused by defective hair products used in salons</li></ul><p>So if you or a loved one has been injured due to any faulty product - from a ladder to a lipstick - you may be able to claim compensation on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No fee basis</a>.</p>`
                  },
                  {
                    question: "Why do product liability claims arise?",
                    answer: `<p>Broadly speaking, product liability claims arise when someone has been injured by a faulty product. Yet it isn't simply a lack of testing or an intrinsically dangerous product design that leads to product liability injury compensation claims. Other common reasons for injury claims arising from defective products include:</p><ul><li>A manufacturer failing to recall a product it knows is faulty</li><li>Contamination that occurs during manufacturing</li><li>Failure to display a necessary warning on a product</li></ul><p>Importantly, <a href=\"https://www.gov.uk/guidance/product-safety-advice-for-businesses\">manufacturers can't escape responsibility</a> just because a product comes with terms and conditions or a contract that exonerates them of all blame for injuries or any other losses. Every product must be of satisfactory quality, fit for purpose and as described. Whether it's you or a loved one who has been injured, you should talk to us about making a No Win No Fee compensation claim.</p>`
                  },
                  {
                    question: "Who is to blame - the manufacturer or the shop?",
                    answer: `<p><strong></strong>If you've bought something that's faulty, but hasn't caused an injury, you have every right to return it to the shop it came from and ask for a refund or replacement under the <a href=\"https://www.which.co.uk/consumer-rights/regulation/consumer-rights-act\">Consumer Rights Act</a>. Where a faulty product has caused an injury, you can seek redress and compensation from the manufacturer, under <a href=\"https://www.legislation.gov.uk/ukpga/1987/43\">the Consumer Protection Act 1987</a>. There are cases in which the shop may also be at fault for the injury. However, Slater and Gordon's defective product liability experts can advise you once they know the circumstances of the case and in the vast majority of cases, can fund your claim on a No Win No Fee basis..<strong></strong></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Faulty products`,
				heading: `Defective product liability claims`,
				copy: `Have you been injured by a defective product? If faulty goods have caused harm to you or a loved one, you may be entitled to compensation. Slater and Gordon are leading defective product liability lawyers, offering a No Win No Fee service to the majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/140393239.jpg`,
					medium: `/public/images/bitmap/medium/140393239.jpg`,
					small: `/public/images/bitmap/small/140393239.jpg`,
					default: `/public/images/bitmap/medium/140393239.jpg`,
					altText: `group of young people having a beverage`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faulty-products-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee defective product claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Drug supplement`,
						url: `/personal-injury-claim/faulty-products/drug-supplement/`,
						icon: `pillpot`
					},
					{
						title: `Electrical products`,
						url: `/personal-injury-claim/faulty-products/electrical-products/`,
						icon: `electrical`
					}
				]
			},
			standardContent: `product-template/faulty-products`,
			sideLinkList: {
				heading: `Questions about faulty product compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faulty-products/claim-guide/`,
						copy: `Faulty product claim guide`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can i claim for?`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `Personal injury and debt suppor`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Five-Figure Settlement for Eye Injury Caused by Faulty Packaging`,
					link: `/resources/latest-case-studies/2017/01/five-figure-settlement-for-eye-injury-caused-by-faulty-packaging/`,
					copy: `A five-figure settlement was awarded to a man in compensation for an eye injury caused by a faulty handle.`
				},
				{
					heading: `Jogger Awarded 5 Figure Settlement for Defective Kerb Fall`,
					link: `/resources/latest-case-studies/2016/01/jogger-awarded-5-figure-settlement-for-defective-kerb-fall/`,
					copy: `A jogger has been awarded a five-figure compensation settlement for extensive injuries to their leg sustained following a fall on defective kerb.`
				},
				{
					heading: `Settlement Awarded for Trip on Defective Manhole`,
					link: `/resources/latest-case-studies/2015/09/settlement-awarded-for-trip-on-defective-manhole/`,
					copy: `Slater and Gordon Lawyers secure settlement for client who tripped on a defective manhole`
				}
			],
			imageTextA: {
				section: `Accidents abroad`,
				heading: `How can we help you with your faulty product claim?`,
				copy: `There is such a wide variety of different products on the market coming from all over the globe, so the list of defective products can be endless. We have dealt with everything from contaminated cosmetics, to sofas covered in illegal fungicide, to electrical goods exploding and causing burns.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111791984.jpg`,
					medium: `/public/images/bitmap/medium/111791984.jpg`,
					small: `/public/images/bitmap/small/111791984.jpg`,
					default: `/public/images/bitmap/medium/111791984.jpg`,
					altText: `Student at a coffee shop working on laptop`
				},
				video: false
			},
		}
	}
}