module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `industrial-injuries-disablement-benefit`,
		slug: `industrial-injuries-disablement-benefit`,
		parent: `industrial-disease`,
		name: `Industrial Injuries Disablement Benefit`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Industrial Injuries Disablement Benefit | Slater + Gordon`,
				description: `Have you suffered as a result of an injury or illness caused at work? If so, you may be entitled to Industrial Injuries Disablement Benefit (IIDB). Find out what you can apply for then talk to Slater and Gordon about making a claim for compensation.`,
				robots: ``,
                schema: [
                  {
                    question: "What is Industrial Injuries Disablement Benefit (IIDB)?",
                    answer: `<p>IIDB is a weekly Government benefit which is paid to those who've been left unable to work due to an accident or an illness caused by work. The scheme also covers people who were not employees, but were on an approved training course when the incident took place. </p>`
                  },
                  {
                    question: "Who's eligible for Industrial Injuries Disablement Benefit (IIDB)?",
                    answer: `<p>If you've suffered an accident at work, you'll be able to claim IIDB if you were employed in the UK when the accident happened or you were on an approved training course. </p><p>If you've suffered an industrial disease you'll be able to claim IIDB if the disease you've been diagnosed with is covered in the scheme. There are over more than 70 diseases covered by the scheme. These include: </p><ul><li>Work related asthma</li><li>Silicosis </li><li>Vibration white finger </li><li>Occupational deafness </li><li>Asbestos related diseases</li></ul><p>A full list of the diseases you can claim IIDB for are listed on the <a href=\"https://www.gov.uk/government/publications/industrial-injuries-disablement-benefits-technical-guidance/industrial-injuries-disablement-benefits-technical-guidance#appendix-1-list-of-diseases-covered-by-industrial-injuries-disablement-benefit\">GOV.UK</a> website.</p>`
                  },
                  {
                    question: "How is disability assessed and quantified?",
                    answer: `<p>Under the Industrial Injuries Disablement Benefit, claimants are assessed to help understand the degree to which they have become disabled. The minimum disability covered is 14% of a fully abled person's ability to work. This would therefore enable you to receive 14% of the full monthly benefit. However, if you were assessed as being 100% disabled, you would be entitled to receive 100% of the current monthly benefit.</p>`
                  },
                  {
                    question: "How do I apply for Industrial Injuries Disablement Benefit (IIDB)?",
                    answer: `<p>To apply for IIDB after an accident, you have to submit a form <a href=\"https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/755522/bi100a_print.pdf\">BI100A to the Department for Work and Pensions</a>. Different application forms are available for specific industrial diseases, which you can obtain from your local Jobcentre Plus, or <a href=\"https://www.gov.uk/government/publications/industrial-injuries-disablement-benefit-claim-forms\">downloaded from the GOV.UK website</a>.</p>`
                  },
                  {
                    question: "Will I be entitled to other benefits?",
                    answer: `<p>Yes. There are a number of other benefits which you may be entitled to depending on the severity of your disability and your needs. Other benefits include:</p><ul><li>Carers Allowance</li><li>Constant Attendance Allowance </li><li>Exceptionally Severe Disablement Allowance </li><li>Reduced Earnings Allowance  </li><li>Attendance Allowance </li><li>Personal Independence Payment </li></ul>`
                  },
                  {
                    question: "Will I be entitled to receive a Blue Badge for parking?",
                    answer: `<p><strong></strong>If your injury or illness has left you with limited mobility, it may be possible that your local council will consider you eligible for a Blue Badge which enables you to use disabled parking spaces and to park on some yellow lines. You can find out how to apply to your local council on the <a href=\"https://www.gov.uk/apply-blue-badge\">Government's</a> website.</p>`
                  },
                  {
                    question: "I have an asbestos related disease. What other benefits am I entitled to?",
                    answer: `<p>There are a number of benefits available to those who have an asbestos related disease but there are also one-off payments in certain cases. Visit our <a href=\"https://www.slatergordon.co.uk/personal-injury/industrial-disease/asbestos-mesothelioma/benefits\">benefits and payments page </a>for asbestos related illnesses for more information about what you're entitled to.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Industrial Injuries Disablement Benefit (IIDB)`,
				copy: `If you've had an accident at work which has caused you an injury, or have developed an illness as a result of your job, you may be entitled to Industrial Injuries Disablement Benefit (IIDB) as well as compensation.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/industrial-injuries-disablement-benefit-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for industrial disease claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/industrial-injuries-disablement-benefit`,
			sideLinkList: {
				heading: `Do you have more questions on industrial disease compensation claims? Read our expert guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}