module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `spine`,
		slug: `spine`,
		parent: `serious`,
		name: `Spinal Cord Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Spinal Cord Injury Compensation | Slater + Gordon`,
				description: `Spinal cord injuries change lives so you should ensure you speak to a spinal cord injury expert to get the specialist legal help and funding you need. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How much can I claim for a spinal cord injury?",
                    answer: `<p>Naturally, every spinal cord injury is different, and the final amount of compensation awarded is based on factors including the cost of long-term care and loss of earnings as well as the loss of mobility. However, where spinal cord injuries result in paraplegia or tetraplegia, and are someone else is at fault, compensation payments in the millions of pounds are not uncommon. We can even help you claim for any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">necessary house adaptations needed</a> to get you life back on track.</p>`
                  },
                  {
                    question: "How am I going to pay for long-term care?",
                    answer: `<p>One of the biggest worries for spinal injury patients and their families is about how long-term care will be funded. So it's important to understand that the cost of long-term care, medical costs, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation and aftercare</a> over your entire lifetime is part of the overall calculation for the compensation settlement. We also strive to ensure that all your rehabilitation and other expenses such as necessary modifications to your home are taken care of as part of your claim.</p>`
                  },
                  {
                    question: "How do serious spinal cord injuries occur?",
                    answer: `<p>Unfortunately, there are many serious spinal cord injuries in the UK every year, caused by a wide variety of accidents and eventualities. These are some of the causes we can help you with:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Spinal damage due to road traffic accidents</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Spinal damage due to an accident at work</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">Spinal damage due to slips, trips and falls</a></li><li><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Spinal damage due to medical negligence</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Spinal damage due to military accident</a></li></ul><p>However it occurred, if your spinal injury was caused by someone else's negligence, you may be able to claim compensation to help you and your loved ones to live your lives to the fullest extent possible.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Spinal injury compensation claims`,
				copy: `Spinal cord injuries are extremely serious and can be devastating for the sufferer and their family. Lives can be changed forever, and you need immediate support from a specialist solicitor. Slater and Gordon is one of the UK's most experienced spinal cord injury law firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/spine-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/spine`,
			sideLinkList: {
				heading: `Our spinal injury specialist`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}