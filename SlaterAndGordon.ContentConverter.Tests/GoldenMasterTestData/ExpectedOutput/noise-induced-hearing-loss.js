module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `noise-induced-hearing-loss`,
		slug: `noise-induced-hearing-loss`,
		parent: `military`,
		name: `Military Hearing Loss Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Military Hearing Loss Accident Compensation Claims | Slater + Gordon`,
				description: `Military life can be dangerous, even when you're not in combat. If you've suffered from hearing loss in a military accident or from being surrounded by loud noises without adequate ear protection, ask about making a No Win No Fee compensation claim through Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for hearing loss suffered when in the military?",
                    answer: `<p>When working in a noisy environment, your employer should ensure that you're provided with adequate hearing protection to protect you from harm. The same applies when you're in the military, and when you're exposed to excessive noise without adequate protection, this can lead to hearing loss and also tinnitus. </p><p>If you've suffered as a result of hearing loss and/or tinnitus while serving in the military, whether the Army, RAF, Navy, special forces or the reserves, you may have the right to claim compensation. </p><p>Over the years, we've helped clients bring noise induced hearing loss claims caused by explosions and gunfire during training exercises, aircraft and vehicle noise and pyrotechnics, where adequate hearing protection was not provided. Whether the consequences of a military accident are relatively mild or totally catastrophic, you deserve a prompt and understanding service to obtain your rightful compensation as quickly as possible. </p>`
                  },
                  {
                    question: "What are the signs of noise induced hearing loss?",
                    answer: `<p>Naturally, as you get older, your hearing can worsen, so most people put the signs of hearing loss down to their age. However, if you have been exposed to excessive noise in your current job or a previous job, you may have noise induced hearing loss. </p><p>Signs you should look out for include: </p><ul><li>Mishearing words when you're in a conversation</li><li>Being unable to hear where there's background noise</li><li>Having to turn the television up more than others find necessary or needing subtitles </li><li>Missing out on a number of things your partner tells you in passing.</li></ul>`
                  },
                  {
                    question: "The Armed Forces Compensation Scheme (AFCS)",
                    answer: `<p>There are a number of Government compensation schemes for injured forces personnel, which are administered by Veterans UK. Slater and Gordon have military experts who <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/\">specialise in making AFCS claims</a> on behalf of those who have suffered hearing loss as a result of their military service. </p>`
                  },
                  {
                    question: "The Law and Injury Claims Against the MoD",
                    answer: `<p>Since 1987, <a href=\"http://www.legislation.gov.uk/ukpga/1987/25/introduction\">the Crown Proceedings (Armed Force) Act 1987</a>, has allowed military personnel to claim for personal injuries sustained during service, where the MoD failed in its legal duty of care or health and safety duties.</p>`
                  },
                  {
                    question: "How much is my military hearing claim worth?",
                    answer: `<p>As you would expect, every military hearing loss injury claim is different, with the final figure for compensation dependent upon the severity of your hearing loss as well as how it has impacted on your everyday life. As well as <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/\">claiming compensation</a> for your pain and suffering, your claim can also include any expenses incurred as a result of your hearing loss, such as loss of earnings if you've been unable to work because of your injury, hearing aids and equipment, travel to medical appointments etc.</p>`
                  },
                  {
                    question: "Can I claim for tinnitus as well as hearing loss?",
                    answer: `<p>Yes. Tinnitus is a ringing, buzzing, hissing or whistling which can be either intermittent or continuous, in one ear or both ears. Depending on how severe the tinnitus is, can cause you to suffer from sleep deprivation and can also cause stress and depression. If your tinnitus has been caused as a result of inadequate ear protection whilst in the military, you may have the right to claim compensation. </p><p>Dean was discharged from the army on medical grounds due to a hearing loss injury he sustained during infantry training. Head of the Military Injuries Team, Madelene Holdsworth discusses the impact this had on Dean and how Slater and Gordon were able to help him. <a href=\"https://www.youtube.com/watch?v=mGUxuKTbQ-8\">https://www.youtube.com/watch?v=mGUxuKTbQ-8</a></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Military accident claims`,
				heading: `Military hearing loss accident claims`,
				copy: `Military life can lead to accidents and injuries that have nothing to do with combat operations. If you've suffered from hearing loss or impairment in a military accident or suffered as a result of negligence, talk to us today. Slater and Gordon handles most military accident compensation cases on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/134406019.jpg`,
					medium: `/public/images/bitmap/medium/134406019.jpg`,
					small: `/public/images/bitmap/small/134406019.jpg`,
					default: `/public/images/bitmap/medium/134406019.jpg`,
					altText: `Military father sitting at home with family`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee military injury claims`,
				leftWysiwyg: `product-template/noise-induced-hearing-loss-two-panel-cta`,
				rightHeading: `Choose Slater and Gordon to start your military accident claim`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/noise-induced-hearing-loss`,
			sideLinkList: {
				heading: `Do you have more questions on military injury claims? Read our guide below.`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military accident FAQ`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `What is the armed forces compensation scheme?`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}