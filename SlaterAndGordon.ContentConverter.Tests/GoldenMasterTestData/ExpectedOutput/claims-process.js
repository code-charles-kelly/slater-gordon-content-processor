module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claims-process`,
		slug: `claims-process`,
		parent: `personal-injury-claim`,
		name: `Personal Injury Claims Process Explained`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Personal Injury Claims Process Explained | Slater + Gordon`,
				description: `Claiming for a personal injury can be a long and complex process. Our personal injury claims solicitors have written this guide to help explain the process from start to finish.`,
				robots: ``,
                schema: [
                  {
                    question: "How do I know if I have a claim?",
                    answer: `<p>Even this stage in the claims process can be difficult. After an accident neither party may admit fault which can slow the process down. It is key at this stage that you keep all relevant information and photographs well organised and make notes as you may struggle to remember clearly at a later date. Then get in touch with us as soon as possible so we can help you establish whether or not you have a claim.</p>`
                  },
                  {
                    question: "How does the claims process work?",
                    answer: ``
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Personal injury claims process explained`,
				copy: `Our personal injury claims solicitors have written this guide to help explain the process from start to finish.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/claims-process-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claims-process`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}