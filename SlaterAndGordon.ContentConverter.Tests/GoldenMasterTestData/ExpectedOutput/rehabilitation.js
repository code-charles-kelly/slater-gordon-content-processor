module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `rehabilitation`,
		slug: `rehabilitation`,
		parent: `serious`,
		name: `Rehabilitation`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Rehabilitation Compensation | Slater + Gordon`,
				description: `When you have suffered a serious injury, rehabilitation needs to start as soon as possible. Talk to us today about No Win No Fee compensation claims and early interim payments with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How important is rehabilitation after serious injury?",
                    answer: `<p>It's impossible to underestimate the importance of rehabilitation after a serious injury. Loss of an ability can only become worse if it's left alone in the long term. That's why we make the pursuit of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/interim-payments/\">early interim payments a key part of our strategy</a> when handling serious injury cases on behalf of our clients. Often these are necessary to avoid financial hardship to families where the main breadwinner has been rendered unable to work. But in almost every case, these payments make early rehabilitation possible: whether that means physiotherapy, specialist nursing care, a specially adapted vehicle or <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">house adaptations</a>. However serious your injury, we're here to help you start living the fullest life possible, as soon as possible. </p>`
                  },
                  {
                    question: "What sort of rehabilitation is possible?",
                    answer: `<p>Naturally, rehabilitation means different things to different people, depending on the type and seriousness of your injury. However, we're committed to making rehabilitation available to all of our injury compensation clients, by pursuing early interim payments whenever possible, and by making the cost of rehabilitation a key part of the overall compensation settlement. Here are a few examples of what rehabilitation payments might be used for:</p><ul><li>Physiotherapy</li><li>Specialist nursing</li><li>Occupational therapy</li><li>Speech therapy</li><li>Specially adapted housing</li></ul><p>You can read more about the options on our rehabilitation page, and if you would like to discuss a No Win No Fee injury compensation claim with an expert, we're here to help.</p><p>A clinical case manager is someone who typically has a background in occupational therapy, social work or nursing. Their role is separate from the case and requires them to visit the injured client and assess their needs. Their report will contain a variety of recommendations, which can be acted upon with monies from interim payments.</p>`
                  },
                  {
                    question: "What are interim payments?",
                    answer: `<p>Interim payments are payments that are made in advance while the total of compensation to be awarded is being decided. We can request these interim payments to be awarded when liability has been admitted and where you there may be an inability to work due to the injury sustained resulting in both a loss of earnings and a need for living adaptations and rehabilitation.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Injury rehabilitation compensation claims`,
				copy: `In most cases, the sooner that rehabilitation of serious injuries begins, the better the outcome will be. That's why we always strive to achieve early interim payments.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/rehabilitation-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/rehabilitation`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}