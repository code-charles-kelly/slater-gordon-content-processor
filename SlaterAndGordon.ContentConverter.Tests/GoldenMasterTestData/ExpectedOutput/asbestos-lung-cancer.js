module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `asbestos-lung-cancer`,
		slug: `asbestos-lung-cancer`,
		parent: `asbestos-mesothelioma`,
		name: `Asbestos Related Lung Cancer`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Asbestos Related Lung Cancer Compensation Claims | Slater + Gordon`,
				description: `Most people who get lung cancer put it down to smoking, as that's the most common cause. However, if you've been exposed to asbestos, your lung cancer may be asbestos related. Slater and Gordon offers extensive experience of asbestos related lung cancer compensation claims.`,
				robots: ``,
                schema: [
                  {
                    question: "What is asbestos related lung cancer?",
                    answer: `<p>With asbestos related lung cancer, asbestos fibres become lodged in the lung tissue and scar over time and develop into tumors. It's caused by breathing in asbestos dust. The asbestos fibres become lodged in the lung tissue and scar over time, causing irritation, and eventually develop into tumors. </p><p>Lung cancer will only be caused by asbestos where the amount of exposure to asbestos dust has been very heavy.</p><p>Asbestos is the most common cause of lung cancer after smoking and they often act together which increases the risk of lung cancer occurring. In many cases, asbestos and smoking will both be the cause of lung cancer rather than one or the other. </p>`
                  },
                  {
                    question: "What are the symptoms of asbestos related lung cancer?",
                    answer: `<p>The most common cause of lung cancer is smoking and there are no physical differences between lung cancer caused by smoking and lung cancer caused by exposure to asbestos.</p><p>The symptoms of lung cancer would usually include: </p><ul><li>Persistent chest infections</li><li>Persistent coughing </li><li>Breathlessness</li><li>Chest pain or discomfort</li><li>Bloodstained phlegm</li><li>Pain when taking in a deep breath</li><li>Wheezing </li><li>Loss of appetite and weight loss.</li></ul><p>If you have any of the symptoms attributable to lung cancer, you should visit your GP as soon as possible. In the event that you're diagnosed with asbestos related lung cancer, contact one of our asbestos related disease experts. </p>`
                  },
                  {
                    question: "How is asbestos related lung cancer usually diagnosed?",
                    answer: `<p>A diagnosis of lung cancer will be made by a chest consultant following investigations which normally include:</p><ul><li>X-rays</li><li>A bronchoscopy </li><li>A CT scan </li><li>A PET scan</li></ul><p>If an x-ray shows something abnormal, the consultant may ask you to have a bronchoscopy, which is when the consultant looks inside the airways by putting a narrow flexible tube down the nose or throat.</p><p>The consultant may also ask for a CT scan or a PET scan which takes pictures of the inside of the body and provides much more information. If these tests lead to a diagnosis of lung cancer, further tests may be requested to see whether the cancer has spread and help the consultant</p><p>decide on the best treatment.</p><p>It's important when you're having investigations to make sure you tell GP and any medical expert you see about any history of asbestos exposure. </p>`
                  },
                  {
                    question: "What treatment is available for asbestos related lung cancer?",
                    answer: `<p>Treatment offered will depend on how advanced the cancer is, your general health and fitness and your personal preference.</p><p>Treatments which may be available include:</p><ul><li>Surgery – Surgery may be an option, where a surgeon removes a section of the lung, the lobe or the full lung</li></ul><ul><li>Radiotherapy - This treatment is a high dose of radiation used to kill some of the cancer cells</li></ul><ul><li>Chemotherapy – This treatment is the use of drugs to destroy cancer cells. It can shrink the tumour and relieve some of the symptoms too</li></ul><ul><li>Other Treatments - There are other treatments available such as biological therapy, radio frequency ablation or photodynamic therapy. </li></ul><p>It's important to note that treatments and clinical trials change often so you should always speak to your consultant about your treatment options.</p>`
                  },
                  {
                    question: "Are there benefits available for people with asbestos related lung cancer?",
                    answer: `<p>Yes. There are a number of benefits available for people who have asbestos related lung cancer. There's also a one off payment which you may be entitled to which is the Pneumoconiosis Etc. (Workers Compensation) Act 1979 payment.</p><p>For more information about the benefits and payments you may be entitled to, read our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/benefits\">Benefits and Payments guide</a>. </p><p>As well as benefits and payments from the Government, you may be entitled to compensation for your pain and suffering. </p><p>Slater and Gordon has a team of experts who have specialised in asbestos related illness for many years and understand the complexities which can be faced. We have a strong reputation when it comes finding insurers of companies which have been out of business for many years and securing the best possible outcome to your case. </p><p>We offer No Win No Fee agreements to all clients who have been diagnosed with an asbestos related disease and will not charge any additional fees meaning you'll get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Asbestos related lung cancer compensation claims`,
				copy: `There are various causes of lung cancer. However, if you've been exposed to asbestos, your lung cancer may be asbestos related. This guide explains what you need to know about asbestos related lung cancer.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos related lung cancer compensation claims`,
				leftWysiwyg: `product-template/asbestos-lung-cancer-two-panel-cta`,
				rightHeading: `Talk to us about asbestos related lung cancer claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/asbestos-lung-cancer`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}