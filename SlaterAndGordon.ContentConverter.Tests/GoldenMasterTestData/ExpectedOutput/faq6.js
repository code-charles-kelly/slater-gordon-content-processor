module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq6`,
		slug: `faq`,
		parent: `injury-in-public`,
		name: `Injury in Public Places FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Injury in Public Places FAQs | Slater + Gordon`,
				description: `Public injuries can be a legal minefield of complex terminology. Our legal experts have answered all the most commonly asked public liability questions that you might have.`,
				robots: ``,
                schema: [
                  {
                    question: "How do I start a compensation claim after an accident in a public place?",
                    answer: `<p>The process of starting a compensation claim is straightforward and won't take hours of your time. All you need to do is make initial contact with a lawyer by calling us on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contacting us online</a>. You will need to explain the circumstances of your accident and the extent of your injuries. Your case will then be assessed and passed onto the most appropriate lawyer who will process your details and start the ball rolling with your case.</p>`
                  },
                  {
                    question: "What evidence will I need to provide to support my claim?",
                    answer: `<p>Once your compensation claim has been discussed with a lawyer, in order to support your case, you may be asked to supply photographs of the location where your injury took place. For example, if you injured yourself because of an uneven paving stone, you can measure the height of the paving stone with a ruler or tape measure to show how high it is from the ground in relation to the other paving stones. Photographs of your injuries can be helpful too.</p><p>You may also be asked to provide details of any medical appointments you've attended and any medication prescribed to help with the treatment of your injuries. There is also a possibility that you'll need to attend a medical examination with an independent medical expert who specialises in medical reporting to support legal cases, also known as medico legal reports.</p>`
                  },
                  {
                    question: "Will it cost me anything to make a compensation claim?",
                    answer: `<p>Claims for accidents in public places can be funded under a No Win No Fee Agreement. Meaning if your case is unsuccessful, you won't have to pay a penny which means they'll be no financial risk to you. Fees may apply if you do go onto win your case and receive compensation. </p>`
                  },
                  {
                    question: "Can I make a claim for compensation if I had an accident in a public place outside of the UK?",
                    answer: `<p>If you've been injured in a public place outside the UK, it may still be possible for you to claim personal injury compensation. Depending on the country where your accident took place, there can be conflicts with different jurisdictions and laws. It's very important to ensure that the law firm you choose has experts who specialise in public injury accidents overseas because they'll have an understanding of foreign laws and the ones which may apply to your case. Slater and Gordon have successfully won many compensation claims on behalf of our clients who have suffered injuries abroad.</p>`
                  },
                  {
                    question: "Does a claim need to be made within a certain time limit after an accident?",
                    answer: `<p>Generally in England, Wales and Scotland the time limit to make a claim for personal injury after an accident in a public place is three years of the date of the accident. However, there are some variations to these time limits, for example:</p><ul><li>If claiming on behalf of someone who has died, the claim would need to be made within three years from the date of their death.</li><li>If claiming on behalf of a child, the three year time limit does not begin until the child reaches the age of 18 in England and Wales or the age of 16 in Scotland.</li><li>If claiming on behalf of someone who lacks mental capacity, there is no time limit to make a claim.</li><li>If making a claim under The Human Rights Act, the time limit is one year</li></ul><p>Different time limits may apply if your accident happened overseas. For more information about claiming for personal injury in a public place whilst abroad, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/\">visit our page on accidents abroad</a>.</p>`
                  },
                  {
                    question: "How long will my injury compensation claim take to process?",
                    answer: `<p>Personal injury cases vary in circumstances and so it's difficult to pinpoint exactly how long a single case will take to complete. In part, it will depend on how complex the case is and how severe the injuries are. Another factor which will determine how long a case will take is whether or not responsibility is accepted by the other side.</p>`
                  },
                  {
                    question: "Will my compensation cover the cost of damaged clothing or personal items affected by the accident?",
                    answer: `<p>Compensation is awarded so that it allows you to live your life in a way you would have before you were injured. This means any compensation you're awarded will cover any losses and expenses incurred. This includes any lost pay from being unable to work or any costs incurred from having to replace any personal items damaged as a result of your accident. </p>`
                  },
                  {
                    question: "Can I make a claim on behalf of someone else who has been injured in an accident in a public place?",
                    answer: `<p>Children are unable to bring a claim and therefore, a responsible adult will have to make a claim on their behalf, which is usually a parent or guardian. This trusted adult will then be known as a 'Litigation Friend'.</p><p>In cases where someone has passed away due to injuries caused by an accident in a public place, you may be able to claim for compensation on their behalf. If a will was made, the Executor of the Estate would be able to make the claim. If there isn't a will, only certain relatives will be able to pursue a claim on behalf of their loved one.</p><p>In cases where the injured person lacks mental capacity and is unable to pursue their own personal injury claim, someone who has Power of Attorney or is a Deputy for that person will be able to claim on their behalf. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Injury in public place FAQs`,
				copy: `The most commonly asked public liability questions you might have, answered by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq6-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq6`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}