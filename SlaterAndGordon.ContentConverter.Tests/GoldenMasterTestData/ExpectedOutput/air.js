module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `air`,
		slug: `air`,
		parent: `holiday-accident-claims`,
		name: `Air & Flight Travel`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Air & Flight Travel Claims | Slater + Gordon`,
				description: `Have you been injured or made ill while on a flight? If the answer is yes, you may be entitled to compensation. Slater and Gordon specialises in No Win No Fee compensation claims for accidents on airlines abroad and in the UK.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for an accident on a plane?",
                    answer: `<p>Air travel is incredibly safe, with serious accidents few and far between. Having said that, we have acted for many clients who have been injured during business and holiday flights in the UK and abroad. Typically, this involves:</p><p>·   Overhead locker door injuries</p><p>·   Overhead baggage falling</p><p>·   Slips or trips – on or off the plane</p><p>·   Burns or scalds from food service</p><p>·   Trolley accidents</p><p>·   In-flight turbulence</p><p>·   Heavy landings</p><p>·   Injuries caused by other passengers</p><p>·   Allergy reaction to in-flight meals </p><p>Naturally, there are also many other ways in which injuries can arise from an aeroplane flight. So if you have been harmed on a plane and feel that someone else was to blame, ask us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee Claim</a> today. </p>`
                  },
                  {
                    question: "Can I claim for domestic flights as well as overseas flights?",
                    answer: `<p>When you're injured on a plane, it doesn't really matter whether you are flying inside the UK or abroad. That's because of something called <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/\">the Montreal Convention 1999</a>. The Montreal Convention 1999 governs international carriage by air and imposes a form of strict liability on the airline in respect of accidents causing injury.</p><p>At first glance the regime seems to be very consumer friendly but matters get a little trickier when attempting to establish that an accident has occurred. This is because the passenger needs to prove:</p><ul><li>The accident itself was unusual or unexpected (outside the ordinary course of events)</li><li>The accident was external to the passenger (it was not caused due to their internal reaction i.e. Deep Vein Thrombosis)</li><li>The accident took place upon the aircraft or during the process of embarkation or disembarkation</li></ul><p>A few examples of accidents of common accidents, for which the Convention usually allows you to claim are; being hit by luggage from the overhead locker, or being struck by the drinks trolley.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Air travel injury and illness compensation claim`,
				copy: `Air travel is incredibly safe, but if accidents and illness happen in-flight you may be entitled to compensation. Slater and Gordon is a specialist claims firm with extensive experience of helping clients who've been injured on airline flights.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/air-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/air`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}