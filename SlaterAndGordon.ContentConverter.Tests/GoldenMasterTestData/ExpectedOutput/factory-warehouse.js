module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `factory-warehouse`,
		slug: `factory-warehouse`,
		parent: `accident-at-work-compensation`,
		name: `Warehouse & Factory Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Warehouse & Factory Injury Claims | Slater + Gordon`,
				description: `Factories and warehouses are common places for accidents to happen. So if you have been injured in one of these dangerous environments, you may be able to claim compensation. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sorts of injury can I claim for?",
                    answer: `<p>Warehouses and factories are far safer than they used to be, thanks to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">greatly improved health and safety laws</a>. Having said that, the rules and regulations are not always followed as closely as they should be, leading to a wide number of injury claims involving:</p><ul><li>Heavy lifting injuries</li><li>Slips and trips</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/fall-heights-objects/\">Falling from heights or falling objects</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/defective-equipment/\">Dangerous machinery</a> </li><li>Exposure to dangerous substances</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/burns-scars-lascerations/\">Serious burns</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/electrocution/\">Electric shocks</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/forklift/\">Forklift trucks</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/industrial-deafness-tinnitus/\">Excessive noise</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/amputation-limb-damage/\">Amputations</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Asbestos</a></li></ul><p>However, the UK has an incredibly broad range of factories and warehouses, dealing with all sorts of materials. So however you've been injured or made ill at work, if negligence has been to blame, you may be able to claim compensation on a<a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\"> No Win No Fee basis</a>. Speak to an expert solicitor today.</p>`
                  },
                  {
                    question: "Who is responsible for my safety?",
                    answer: `<p>Your employer is responsible for making sure that your working environment is safe for you to use, and can be held negligent if they fail to protect you. The law says that every employer has a duty of care to their employees, meaning that in most cases, any claim for injury compensation will be made against the employers' liability insurance that your employer is legally obliged to hold.</p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that every employer is required to have, by law. It ensures that money is available to compensate factory and warehouse workers for illness or injuries caused in the workplace that were not their fault. Read more about <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">employers' liability insurance</a>.</p>`
                  },
                  {
                    question: "How much is my injury claim worth?",
                    answer: `<p>Every injury claim is different, and the final figure for compensation depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as loss of earnings, how much it might affect your ability to work in the future and the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">cost of any rehabilitation you might need</a>. We also take most injury cases on a No Win No Fee basis, and can seek interim payments to help you avoid financial hardship if you are prevented from working by your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>Generally the law states you have three years following an accident at work in which to make a claim for compensation. There are exceptions to his rule, such as if the accident happened abroad, if the person injured suffers a lack of mental capacity as a result of the accident or in the tragic event that the person goes on to die. We therefore advise that you seek advice from an accident at work specialist lawyer as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Warehouse and factory injury compensation claims`,
				copy: `Have you been injured while working in a factory or warehouse and it wasn't your fault? Slater and Gordon are leading injury compensation specialists, offering a No Win No Fee service to the majority of our personal injury clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/factory-warehouse-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/factory-warehouse`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
			videoTextB: {
				section: ``,
				heading: `Watch Karolis' story`,
				copy: `Karolis was forced to tourniquet his own leg when it was cut off by a wood chipper in a sawmill.`,
				image: false,
				video: `tlc5r75s1w`
			}
		}
	}
}