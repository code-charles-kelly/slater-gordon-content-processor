module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `benefits`,
		slug: `benefits`,
		parent: `asbestos-mesothelioma`,
		name: `Benefits & Payments for Asbestos Disease`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Benefits & Payments for Asbestos Disease Claims | Slater + Gordon`,
				description: `There are a number of benefits and one-off payments available for people diagnosed with mesothelioma and other asbestos related disease. Slater and Gordon's dedicated advisers explain what you may be entitled to.`,
				robots: ``,
                schema: [
                  {
                    question: "The Diffuse Mesothelioma 2018 Scheme",
                    answer: `<p>This Scheme pays a lump sum of compensation to mesothelioma sufferers who are not eligible to receive a payment under the <a href=\"https://www.gov.uk/industrial-injuries-disablement-benefit/further-information\">1979 Act Scheme</a> above.</p><p>To receive this payment the exposure to asbestos dust needs to have happened in the UK and the sufferer must not have received compensation from elsewhere. The scheme works in a similar way to the 1979 Act Scheme.</p>`
                  },
                  {
                    question: "The Diffuse Mesothelioma Scheme (DMPS)",
                    answer: `<p>This scheme pays a lump sum payment if you were diagnosed with mesothelioma on or after 25 July 2012 and <a href=\"https://www.gov.uk/diffuse-mesothelioma-payment\">can't find the employer responsible</a> for your contact with asbestos, or their insurer. </p><p>Again to receive this payment, the exposure to asbestos dust needs to have happened in the UK and the sufferer must not have received compensation from elsewhere.</p>`
                  },
                  {
                    question: "Constant Attendance Allowance (CAA)",
                    answer: `<p>This is a weekly benefit you can claim if the effects of your asbestos related illness means that <a href=\"https://www.gov.uk/constant-attendance-allowance\">you need daily care and attention</a> for most of the time due to your disability.</p>`
                  },
                  {
                    question: "Blue Badge for parking",
                    answer: `<p><strong></strong>If your mesothelioma, asbestos related lung cancer, asbestosis or pleural thickening has left you with limited mobility, it may be possible that your local council will consider you eligible for a Blue Badge which enables you to use disabled parking spaces and to park on some yellow lines. You can find out how to apply to your local council on the <a href=\"https://www.gov.uk/apply-blue-badge\">Government's</a> website.</p>`
                  },
                  {
                    question: "What should I do if I think I have an asbestos related illness?",
                    answer: `<p>Contact us today. We have a team of experts who have specialised in asbestos related illness for many years and understand the complexities which can be faced. We have a strong reputation when it comes finding insurers of companies which have been out of business for many years and securing the best possible outcome to your case. </p><p>We offer No Win No Fee agreements to all clients who have been diagnosed with an asbestos related disease and will not charge any additional fees meaning you'll get to keep 100% of your compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Asbestos and mesothelioma claims`,
				heading: `Benefits and payments for mesothelioma and other asbestos disease`,
				copy: `There are various benefits and payments available to those who suffer from mesothelioma and other asbestos related diseases. Our specialist advisers talk you through what you may be entitled to.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/207649491.jpg`,
					medium: `/public/images/bitmap/medium/207649491.jpg`,
					small: `/public/images/bitmap/small/207649491.jpg`,
					default: `/public/images/bitmap/medium/207649491.jpg`,
					altText: `Senior man with blue shirt and glasses standing outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee asbestos compensation claims`,
				leftWysiwyg: `product-template/benefits-two-panel-cta`,
				rightHeading: `Talk to us about asbestos and mesothelioma claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/benefits`,
			sideLinkList: {
				heading: `Do you have more questions about asbestos and mesothelioma compensation claims? Read our guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/faq/`,
						copy: `Asbestos and mesothelioma FAQ`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/support/`,
						copy: `Living with mesothelioma`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/am-i-at-risk/`,
						copy: `Who is at risk of asbestos diseases?`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/guide-to-expats/`,
						copy: `Mesothelioma guide for expats`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-register/`,
						copy: `Asbestos register`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-statistics/`,
						copy: `Asbestos statistics`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-regulations/`,
						copy: `Asbestos regulations`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/employers-duty-of-care/`,
						copy: `Asbestos: Employers duty of care`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/improper-handling-of-asbestos/`,
						copy: `Improper handling of asbestos`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-medical-definitions/`,
						copy: `Asbestos diseases: Medical definitions`
					},
					{
						url: `/personal-injury-claim/industrial-disease/asbestos-mesothelioma/asbestos-timeline-history/`,
						copy: `History of asbestos`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Former machine operator awarded £175,000 for mesothelioma diagnosis`,
					link: `/resources/latest-case-studies/2017/09/former-machine-operator-awarded-gbp175000-for-mesothelioma-diagnosis/`,
					copy: `A former machine operator exposed to asbestos while working with laggers in the 1960s-80s was awarded six-figure compensation.`
				},
				{
					heading: `Navy serviceman wins claim for asbestos disease`,
					link: `/resources/latest-case-studies/2017/08/navy-serviceman-wins-claim-for-asbestos-disease/`,
					copy: `An enlisted Navy serviceman exposed to asbestos on duty successfully pursued legal action following a diagnosis of an asbestos-related lung disease.`
				},
				{
					heading: `Former draughtsman awarded £385,000 for mesothelioma claim`,
					link: `/resources/latest-case-studies/2017/06/former-draughtsman-awarded-gbp385000-for-mesothelioma-claim/`,
					copy: `An ex-draughtsman exposed to asbestos at work in the 1960s was awarded six-figure in compensation.`
				}
			],
		}
	}
}