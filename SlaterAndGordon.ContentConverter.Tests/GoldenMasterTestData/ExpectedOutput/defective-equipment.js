module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `defective-equipment`,
		slug: `defective-equipment`,
		parent: `accident-at-work-compensation`,
		name: `Defective Equipment`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Defective Equipment Claims | Slater + Gordon`,
				description: `Have you been injured by defective equipment at work? If it happened due to someone else's negligence, you're entitled to seek compensation. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What counts as defective equipment?",
                    answer: `<p>Whether you're <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/construction-site/\">working in the construction</a> or <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">serving in the military</a>, engineering or manufacturing, you have a right to expect the equipment you use at work is safe. Unfortunately, poor maintenance and inadequate safety procedures mean that some equipment is defective, and therefore likely to cause injuries. Other causes include:</p><ul><li>Inadequate maintenance</li><li>Irregular inspections</li><li>Missing safety guards</li><li>A lack of personal protective equipment </li><li>A lack of health and safety assessments</li></ul><p>So, if you've been injured by defective equipment in the workplace and believe that negligence like this may have been the cause, contact us today to speak to an injury claims expert. </p>`
                  },
                  {
                    question: "What type of injuries are caused?",
                    answer: `<p>Due to the wide variety of equipment used in industry and the military, there are many, many <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">types of serious injury</a> that can occur due to defective equipment. Some of the most common include:</p><ul><li>Broken bones</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/eye-loss-of-sight/\">Blindness</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/amputation-limb-damage/\">Amputations</a></li><li>Loss of digits</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/electrocution/\">Electric shocks / electrocution</a></li><li>Paralysis</li></ul><p>Whatever the injury that you or a loved one sustained due to defective equipment, if someone else's negligence was to blame - or even partly to blame – contact us to discuss the possibility of making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a> with an expert lawyer.</p>`
                  },
                  {
                    question: "Who is responsible for maintaining equipment?",
                    answer: `<p>If your employer owns and provides the equipment that you use, then they're responsible for making sure that it's properly maintained and safe to use. Even if they contract maintenance out to a third party, every employer has a duty of care to their employees, meaning that in most cases, any claim for injury compensation will be made against the employers' liability insurance that every employer is legally obliged to hold.</p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that every employer is required to have, by law. It ensures that should their employees suffer an injury or illness at work, money is available to compensate those workers.</p>`
                  },
                  {
                    question: "How much is my injury claim worth?",
                    answer: `<p>Every injury claim is different, and the amount of compensation you're likely to receive will be dependent upon the seriousness of the injury, as well as how much it might affect your ability to work in the future, and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">how much any rehabilitation might cost</a>. However, we take most defective equipment injury cases on a No Win No Fee basis, and seek interim payments to help you avoid hardship if you are prevented from working by your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>With personal injury cases involving defective equipment, the time limit in which to make a compensation claim is three years. However, there are exceptions to this rule such as if the injured party passes away as a result of their injuries or has diminished mental capacity. Also, if working abroad, the time limits differ in different countries, so it's always important that you speak to a specialist lawyer with expertise in accidents at work as soon as possible.. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Defective equipment injury claims`,
				copy: `Have you been injured due to defective equipment at your place of work. Slater and Gordon is a leading injury compensation specialist, offering a No Win No Fee service to the vast majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/defective-equipment-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/defective-equipment`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}