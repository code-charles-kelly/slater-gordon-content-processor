module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `expert-guide`,
		slug: `expert-guide`,
		parent: `carbon-monoxide`,
		name: `Carbon Monoxide Claim Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Carbon Monoxide Claim Guide | Slater + Gordon`,
				description: `Proving the cause of an illness can be a complex matter, that's why we have put together this guide to aid you in spotting the signs of carbon monoxide poisoning to aid you with a successful case. Slater and Gordon is one of the UK's leading poisoning claim compensation specialists.`,
				robots: ``,
                schema: [
                  {
                    question: "What is carbon monoxide?",
                    answer: `<p>Carbon monoxide is an odourless, colourless gas that is produced in a number of ways. While it is present in car exhaust fumes, cigarette smoke and even paint fumes, the greatest danger for most people comes from badly serviced gas heating equipment and poorly ventilated areas where open fires and wood burning stoves are present.</p><p>Carbon monoxide poisoning can be fatal in some cases, and cause lasting illness in others. If you suspect that you are being exposed to carbon monoxide, you should seek medical advice immediately. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/\">Read our guide on what to do if you suspect CO poisoning</a>.</p>`
                  },
                  {
                    question: "What are the symptoms of carbon monoxide poisoning?",
                    answer: `<p>The most dangerous thing about carbon monoxide is that you can't see it or smell it. In fact, in many of the fatal cases of carbon monoxide poisoning, victims who are overcome by the fumes simply fall asleep and never wake up. That's why it is essential to recognise the symptoms that could point to a build-up of carbon monoxide:</p><ul><li>Headaches </li><li>Unexpected drowsiness</li><li>Dizziness or confusion </li><li>Nausea and sickness </li><li>Tiredness and lethargy </li><li>Shortness of breath </li><li>Clumsiness and loss of coordination</li><li>Abdominal pain</li><li>Loss of consciousness</li></ul><p>If you recognise any of these symptoms in yourself or someone you know, it is essential to seek medical advice and have the premises checked for carbon monoxide immediately. From a legal standpoint, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">your l</a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">andlord is responsible </a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">for keeping you safe from carbon monoxide</a>, so they should also be informed of your suspicions as early as possible.</p><p>In the event that you have been exposed to carbon monoxide, you should also seek legal advice immediately.</p>`
                  },
                  {
                    question: "Who might be to blame for my carbon monoxide poisoning?",
                    answer: `<p>Carbon monoxide poisoning is almost always the result of somebody else's negligence. If you have been exposed to it in a rental property, the person with legal responsibility to you is your landlord. They are required by law to have a working carbon monoxide detector in every room where solid fuel is burned, and also to ensure that gas appliances are annually serviced and properly ventilated.</p>`
                  },
                  {
                    question: "How do I make a claim for carbon monoxide poisoning?",
                    answer: `<p>In the first instance contact Slater and Gordon, as we are highly experienced in dealing with the complexities of carbon monoxide poisoning compensation claims. We will listen to what you tell us, than advise you on the likelihood of making a successful claim. In the majority of cases like this, we could then agree to pursue your claim on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, to ensure that there is no financial risk to you. Call us now on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">tell us about your illness</a> and we will call you.</p>`
                  },
                  {
                    question: "How much is a carbon monoxide poisoning claim worth?",
                    answer: `<p>As you might expect, the level of compensation you might receive depends on the extent of the poisoning and seriousness of the effects. There is no set figure for carbon monoxide compensation claims, though where lasting after-effects such as brain damage or death have occurred, judgements in the millions of pounds have been made.</p>`
                  },
                  {
                    question: "How long will my carbon monoxide claim take?",
                    answer: `<p>Again, there is no hard and fast rule for how long a carbon monoxide poisoning compensation claim will take. Where a negligent party accepts liability and has valid insurance, the claim may proceed reasonably quickly. However, where there are serious effects that require toxicology reports or even an inquest, a claim may take some time to settle. However, in these cases, we will always seek interim funding to ensure that victims suffer as little financial hardship as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Carbon monoxide poisoning claims guide`,
				copy: `There are a variety of factors that can lead to a successful compensation case for carbon monoxide poisoning. The below advice will help you make sure you have everything you need.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/expert-guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/expert-guide`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}