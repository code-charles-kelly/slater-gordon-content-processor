module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `welfare-benefits`,
		slug: `welfare-benefits`,
		parent: `serious`,
		name: `Benefits After Injuries`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Benefits After Injuries | Slater + Gordon`,
				description: `Make sure you receive the compensation you deserve and the benefits you're entitled to if you have suffered a serious injury. Ask for benefits advice and No Win No Fee compensation claims at Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What benefits could I claim after an injury?",
                    answer: `<p>State benefits are sometimes called allowances, pensions, tax credits or entitlements. They're intended to alleviate poverty by providing basic income or 'topping up'  a low income and can be divided into three broad categories: </p><ul><li>Those intended to replace earnings if you're unable to work due to sickness, disability, unemployment, pregnancy or caring responsibilities</li><li>Those that compensate for costs - contribute towards the additional costs of disability</li><li>Those that help alleviate poverty - provide a basic income or top up a low income</li></ul><p>There are many benefits which you can claim for when you've suffered an injury, but what you're entitled to depends on your individual circumstances. </p><p>The most common benefits which are claimed when someone has suffered a serious injury are: </p><p><strong>Industrial Injuries Disablement Benefit (IIDB) </strong></p><p>This is a benefit available for people who've had an accident at work or suffered an illness as a result of their working environment. </p><p><strong>Employment and Support Allowance (ESA)</strong></p><p>ESA is an income replacement benefit to assist people with their living costs if they're too unwell or disabled to work full time. </p><p>. </p><p><strong>Disability Living Allowance (DLA)</strong></p><p>This is a benefit available for children aged 15 years or younger who need assistance with their care or their mobility.</p><p><strong>Personal Independence Payment (PIP)</strong></p><p>Like DLA, this is a benefit for people who need assistance with their care or mobility but this is for people who are aged 16 to 64. </p><p><strong>Attendance Allowance (AA)</strong></p><p>Attendance Allowance is a tax free benefit for people aged 65 and older who need help with personal care or require supervision to remain safe, either due to physical or mental disability or illness. </p><p><strong>Universal Credit (UC)</strong></p><p>This is a benefit for people of working age who are on a low income or who are looking for work, are a lone parent, if you care for someone or if you're unable to work due to sickness or a disability.</p><p>These are just a few benefits that you may be entitled to. There are many types of benefits, some of which can be claimed at the same time as others and it's important to know exactly what you're entitled to. That's why it's essential to deal with a legal firm that understands not just how to claim rightful compensation, but also how to advise you on your rights and responsibilities with regard to your benefits entitlements, and how to manage compensation funds sensibly. </p>`
                  },
                  {
                    question: "What's the difference between means-tested and non means-tested benefits?",
                    answer: `<p>Means-tested benefits are payments which are available for people who can demonstrate that their income and capital (i.e. their means) are below specified limits. Non means-tested benefits are payments that don't take into account a person's income and capital. </p><p>Most of the previous means-tested benefits have now been replaced by Universal Credit. Non means-tested benefits include: </p><p>Non means-tested benefits include:</p><ul><li>Disability Living Allowance (DLA)</li><li>Attendance Allowance </li><li>Carer's Allowance </li><li>Personal Independence Payment </li><li>State Pension </li><li>Industrial Injuries Disablement Benefit (IIDB)</li><li>Statutory Sick Pay</li><li>Contributory Employment and Support Allowance </li><li>Severe Disablement Allowance </li><li>Incapacity Benefit </li><li>Contribution Based Job Seekers Allowance </li><li>Child Benefit</li><li>Guardian's Allowance </li><li>Maternity Allowance/Statutory Maternity / Paternity/ Shared Parental / Adoption Pay</li><li>Bereavement Allowance </li><li>Widowed Parent's Allowance </li></ul><p>However, in addition to speaking to the DWP, it is always worth asking your claims solicitor for advice about which benefits you might be entitled to, and how any existing or future benefits may be affected by a successful injury compensation claim.</p><p>If you've suffered a serious injury through no fault of your own, Slater and Gordon is one of Britain's leading personal injury legal firms, dedicated to helping people access the financial assistance they need for life after serious injury. Call us now on freephone ${contentShared.meta.phonenumber.number} or<a href=\"https://www.slatergordon.co.uk/contact-us/\"> </a><a href=\"https://www.slatergordon.co.uk/contact-us/\">tell us about your injury</a> and we will call you.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Welfare and benefits after serious injuries`,
				copy: `Welfare and benefits after a serious injury can be a complicated matter. Our guide can help you understand the law and what help is available to you and your family.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/welfare-benefits-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/welfare-benefits`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}