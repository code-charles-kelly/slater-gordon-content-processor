module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `chronic`,
		slug: `chronic`,
		parent: `serious`,
		name: `Chronic Pain`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Chronic Pain Compensation | Slater + Gordon`,
				description: `Chronic pain can be a terrible burden. Whether it was caused by an accident, assault or medical negligence, you may be entitled to financial compensation. Talk to us today about a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Will a solicitor understand my chronic pain?",
                    answer: `<p>One of the most difficult things for people with chronic pain to deal with is that there may be no outward signs of the distressing pain that they are in, and even the best GPs can struggle to understand something they can't see. Even so, our specialist legal team has encountered enough people suffering with chronic pain to know that an injury doesn't have to be visible to be real. Some of the injuries that have been linked to chronic pain include:</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a></p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Accidents at work</a></p><p><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">Medical negligence</a></p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/\">Military accidents</a></p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/slips-trips-falls/\">Broken limbs due to slips, trips and falls</a></p><p>So if you suffer from chronic pain that it resulted from someone else's actions or negligence, you can <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us</a>, secure in the knowledge that we understand what you're going through and will do our best to seek the compensation you need whenever possible. </p>`
                  },
                  {
                    question: "What conditions cause chronic pain?",
                    answer: `<p>It is now well understood that while previous injuries can lead to chronic pain, there are a number of other medical conditions that may be responsible. These include:</p><ul><li>Chronic pain syndrome</li><li><a href=\"https://www.nhs.uk/conditions/complex-regional-pain-syndrome/\">Complex regional pain syndrome</a></li><li>Chronic fatigue syndrome (CFS)</li><li>Somatoform disorder</li><li>Fibromyalgia</li></ul><p>Even if you have been told that the pain you are feeling is 'all in the mind' - perhaps even by a respected doctor or specialist - medical science now fully accepts that chronic pain has many causes. As specialist lawyers, we also believe that where your chronic pain has been caused by someone else's actions or negligence, it's often appropriate to seek financial compensation to help you with any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation that may help to ease the pain</a>, and to live your life to the fullest again.</p>`
                  },
                  {
                    question: "How much can I claim for my chronic pain?",
                    answer: `<p>The claim value depends on a wide variety of factors such as the severity of an injury and what <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">other expenses need to be claimed for, such as loss of earnings</a>, any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">house adaptations or modifications</a> that may be necessary and any medical costs.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Chronic pain compensation claims`,
				copy: `Living with chronic pain can make life seem a struggle. Difficult to diagnose and even harder to cure, there are many causes. If you suffer from chronic pain caused by someone else's actions or negligence, Slater and Gordon's specialist chronic pain compensation solicitors are here to help you.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/chronic-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/chronic`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}