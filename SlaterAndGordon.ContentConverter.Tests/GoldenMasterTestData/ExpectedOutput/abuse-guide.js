module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `abuse-guide`,
		slug: `abuse-guide`,
		parent: `sexual-criminal-abuse`,
		name: `Sexual Abuse Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Sexual Abuse Guide | Slater + Gordon`,
				description: `Sexual abuse and sexual assault can cause lasting trauma to sufferers. This guide is intended to provide useful information to sufferers and their families. Slater and Gordon is a leading legal firm with broad experience of supporting sufferers as they seek justice and reparation.`,
				robots: ``,
                schema: [
                  {
                    question: "Who can you talk to about sexual assault or abuse?",
                    answer: `<p>This brief guide is designed to provide a reference point to help them understand the legal definitions of abuse, and the ways in which our experienced solicitors might help them to seek justice and compensation.</p><p>If you, a family member, or a close friend has suffered from any kind of abuse or sexual assault, we know that it can be incredibly hard to talk about. Even though you are totally blameless, it's inevitable that telling someone about it will be a highly emotional experience. As a firm that has specialised in cases of sexual abuse and assault for many years, we have a choice of lawyers available for you to talk to, all of whom have had these difficult conversations many, many times. </p><p>When you're ready to talk, they will be here for you: making every conversation as stress-free as possible. In the meantime, here are some of the legal definitions of sexual abuse and assault, to help you understand the legal description of the wrong that has been done, as well as the reasons why you have a right to seek both justice and compensation.</p>`
                  },
                  {
                    question: "Why is it possible to claim compensation if you've been abused?",
                    answer: `<p>If you've suffered from abuse or a sexual assault not only has a criminal offence been committed, but you've also suffered as a result. Just like with any other personal injury case, if you've been injured, either physically and/or psychologically, you should be compensated for your suffering. There can also be other costs which have been incurred when you have suffered from abuse or a sexual assault, such as loss of earnings if you've been unable to work and treatment such as counselling. It's therefore important that not only do you get the justice you deserve, but also, you're compensated for your suffering and also any financial losses. </p><p>If you or a loved one have suffered from abuse or a sexual assault, contact one of our experienced abuse lawyers. They're all caring people and good listeners, having supported so many people who have suffered abuse or a sexual assault for so many years. So when you're ready to talk to someone, they're here to listen. </p>`
                  },
                  {
                    question: "What are the most common types of abuse cases?",
                    answer: `<p>There's never an excuse for abuse or a sexual assault, and you have a right to be compensated for your suffering, no matter what type of abuse case you have. However, some of the most common types of abuse cases include:</p><p>• Abuse suffered as a child in local authority or foster care</p><p>• A failure by Social Services to protect a child from abuse</p><p>• Abuse by a person in a position of trust (such as a priest or teacher)</p><p>• Abuse suffered within the family</p><p>• Abuse of the elderly in a nursing or care home</p><p>• Sexual or physical abuse suffered as an adult</p><p>• A single incident of a sexual or physical assault</p><p>If you, a family member or a friend have been subjected to abuse or a sexual assault, regardless of the situation, we are here to listen in confidence and advise you on whether you may be able to seek justice by claiming compensation.</p>`
                  },
                  {
                    question: "Why should someone sue an abuser?",
                    answer: `<p>It's perfectly natural for people who have suffered from abuse to want to get the justice they deserve. In most cases, it's part of the healing process as some people find that it helps them, on an emotional level, to take positive action. On a practical level, where you have suffered psychological or physical harm, it can adversely affect your education or your ability to work. It might even mean that you spend money you can't really afford on counselling. For both emotional and practical purposes, these are very good reasons to seek financial compensation for your suffering.</p>`
                  },
                  {
                    question: "Can I stay anonymous if I make a claim?",
                    answer: `<p>Many victims of abuse fear that their lives will become public if they make a claim against whoever was responsible for the abuse. You can rest assured that we always ensure complete anonymity for our clients, even in cases that arouse press interest.</p><p>It's important to understand that we, and the law, ensure that you have this protection. Our specialist team has dealt with many abuse cases over the years, and victim anonymity has never been compromised, even when the abuser is someone in the public eye and the case is reported in the media. Here you can find out more about how <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/\">we protect your confidentiality and anonymity</a>.</p>`
                  },
                  {
                    question: "How do I start a compensation claim?",
                    answer: `<p>It's very simple to contact us and arrange an informal chat. You can either call us on ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a> to speak to one of our legal experts. We'll make sure you can speak to an experienced abuse solicitor who will talk to you over the phone, or arrange to speak to you at a location convenient for you for an informal chat. You can rest assured that we'll not only understand what you've been through, but also be totally honest with you about the chances of your case being successful. </p>`
                  },
                  {
                    question: "How much does it cost to make a claim?",
                    answer: `<p>The vast majority of cases are run under a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee agreement</a>, which means if you were to lose your case, you wouldn't have to pay a penny and therefore, there would be no financial risk to you. </p><p>It's also possible that one of your existing insurance policies offers legal expenses cover, and if a criminal claim is made <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">under the CICA Scheme</a>, it can be funded under what's called a Contingency Fee Agreement.</p><p>However, we'll be able to discuss all of the possible funding options once we know more about your case.</p>`
                  },
                  {
                    question: "How long will my cases take?",
                    answer: `<p>Again, there is no fixed amount of time that abuse cases take to reach a conclusion, particularly if it occurred some time ago and witnesses or paperwork need to be found. All we can promise is that your lawyer will do everything in their power to deal with your case as efficiently as possible.</p>`
                  },
                  {
                    question: "Will criminal proceedings affect my compensation claim?",
                    answer: `<p>Criminal proceedings are completely separate from a civil claim for compensation, although the outcome of a criminal case is relevant. However, even if an abuser is not found guilty of abuse in a criminal court, it may still be possible to make a civil compensation claim, as the evidential burden in a civil case is lower, and the issues are often different to those in the criminal case. We'll be able to tell you more once we know the circumstances of your case.</p>`
                  },
                  {
                    question: "Can I claim if I - or my abuser - don't live in the UK?",
                    answer: `<p>It's still possible to pursue a claim for compensation if you or the abuser lives overseas. It's also possible to claim if the abuse itself occurred overseas, but the laws in other countries may come into account. We can advise you further once we have all the details.</p>`
                  },
                  {
                    question: "What if I need help with other areas of my life?",
                    answer: `<p>We have experts in many areas of the law, such as employment law, family law, conveyancing, wills, trusts and probate. We also have a Court of Protection team who protect the finances of those who don't feel they have the mental capacity to look after their own, and an independent financial planning company who can provide investment services in respect of any compensation received to help ensure your future is financially secure. We'll be there for you for all your future's legal and financial needs. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Sexual abuse and sexual assault legal guide`,
				copy: `Sexual abuse and sexual assault can cause lasting trauma to sufferers. This guide to abuse and assault is intended to provide useful information to sufferers and their families.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/abuse-guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/abuse-guide`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}