module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `usa`,
		slug: `usa`,
		parent: `holiday-accident-claims`,
		name: `USA Travel Accident`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `USA Travel Accident Claims | Slater + Gordon`,
				description: `Have you been injured while travelling in the USA? You may be entitled to compensation, and we have the expertise to help you claim. Slater and Gordon specialises in No Win No Fee compensation claims for accidents in the USA.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for injuries in the USA from the UK?",
                    answer: `<p>While the USA is a wonderful country, we have acted for a great many clients who have been injured while on business or holiday there. So we have the experience and the contacts to pursue compensation claims in the United States, for all of these eventualities and more:</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a><strong>:</strong> where another driver's negligence or a poor road surface was to blame</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/hotel-resort/\">Holiday accidents</a>: including poolside slips, hotel trips and even theme park ride failures caused by poor maintenance</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Accidents at work</a><strong>:</strong> if you work in the US, your employer has a duty of care to protect you from injury</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/winter-skiing/\">Skiing and other sports injuries</a><strong>:</strong> where the venue or another user's negligence has caused an injury</p><p><strong>Assaults and terrorism:</strong> where you have been assaulted or caught up in a terrorist incident</p><p><strong>Legionnaires</strong>' <strong>disease</strong>: or indeed<strong> </strong>any other illness that has been caused by negligence</p><p>However, the USA is a big country, and there are many more circumstances than we've listed here that could cause injury whilst in the USA. So talk to us about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a> for any injury that you or a family member has suffered while travelling in the USA.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>While UK law allows you up to three years in which to start a compensation claim for an injury that arose from negligence or a deliberate act, the time limits vary greatly from State to State in the USA. So if you've been injured in the USA and wish to make a No Win No Fee compensation claim from the UK, it's advisable to begin your claim as soon as possible. </p>`
                  },
                  {
                    question: "How much will my claim be worth?",
                    answer: `<p>It's very difficult to estimate how much compensation you're likely to receive for your injury until we know the circumstances. The amount of compensation you are likely to receive depends on the extent of your injuries as well as the applicable laws in whichever US State the incident occurred. However, in addition to damages for pain and suffering, you may also be able to claim for any loss of earnings as well as for the cost of any <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">medical treatment or rehabilitation</a><strong> </strong>that may be required.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `USA Travel compensation claim lawyers`,
				copy: `Travelling to the USA can be an amazing experience. But if you're injured while you're there, life can become difficult and expensive. Slater and Gordon is a specialist law firm with extensive experience of helping clients claim compensation when they've been injured in the USA.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/usa-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/usa`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}