module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `serious`,
		slug: `serious`,
		parent: `personal-injury-claim`,
		name: `Serious Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Serious Injury Compensation | Slater + Gordon`,
				description: ``,
				robots: ``,
                schema: [
                  {
                    question: "How much is my serious injury claim worth?",
                    answer: `<p>As you might expect, every serious injury claim is different, with the final figure for compensation dependent upon the severity of the injury and the extent of recovery that may be possible. Our expertise is not just in securing much needed compensation for our clients, it's also in understanding the need for rehabilitation and the support that you and your family need right away, and perhaps for the rest of your lives. That's why we take most serious injury cases on a No Win No Fee basis, and why <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/interim-payments/\">obtaining interim payments</a> to help you avoid hardship is often central to our approach. </p>`
                  },
                  {
                    question: "Is there a time limit on my claim?",
                    answer: `<p>The general rule is that a claim must be brought within three years. However, there are exceptions to that rule, such as if you're claiming on behalf of a child or someone with diminished mental capacity, or if the accident occurred while abroad. As there are exceptions to the three year rule, you should speak to a serious injury specialist as soon as you are able to ensure you're not out of time. </p>`
                  },
                  {
                    question: "Can I make a No Win No Fee claim?",
                    answer: `<p>When you or a family member has suffered a serious injury, it can often lead to financial worries too. That's why we undertake the vast majority of serious injury compensation claims on a No Win No Fee basis, so you have one less thing to worry about and don't have to take on any financial risk. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious Injuries`,
				heading: `Serious Injury Compensation Claims`,
				copy: `Slater and Gordon provides immediate support and unsurpassed experience in compensation, rehabilitation and aftercare for catastrophic injuries. Our experts are here to advise and support you and your family throughout this difficult time.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/serious-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Head and brain`,
						url: `/personal-injury-claim/serious/head-brain/`,
						icon: `head-and-brain`
					},
					{
						title: `Spinal Injuries`,
						url: `/personal-injury-claim/serious/spine/`,
						icon: `spine`
					},
					{
						title: `Amputation and limb damage`,
						url: `/personal-injury-claim/serious/amputation-limb-damage/`,
						icon: `man-crutch`
					},
					{
						title: `Fatal accidents`,
						url: `/personal-injury-claim/serious/fatal/`,
						icon: `fatal-accident`
					},
					{
						title: `Burns, scars and lacerations`,
						url: `/personal-injury-claim/serious/burns-scars-lascerations/`,
						icon: `burns-scars-lacer`
					}
				]
			},
			standardContent: `product-template/serious`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
			imageTextA: {
				section: `Serious Injuries`,
				heading: `What sort of serious injuries could I claim for?`,
				copy: `A serious or catastrophic injury is generally defined as an injury that results in permanent or life changing disability.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/246586028.jpg`,
					medium: `/public/images/bitmap/medium/246586028.jpg`,
					small: `/public/images/bitmap/small/246586028.jpg`,
					default: `/public/images/bitmap/medium/246586028.jpg`,
					altText: `Smiling woman looking out of train window`
				},
				video: false
			},
		}
	}
}