module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `chemical-poisoning`,
		slug: `chemical-poisoning`,
		parent: `industrial-disease`,
		name: `Chemical Poisoning`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Chemical Poisoning Compensation Claims | Slater + Gordon`,
				description: `Chemical poisoning in the workplace can lead to serious illness. If you've been exposed to hazardous substances at work, Slater and Gordon offers the claims expertise you need, as well as the security of No Win No Fee agreements.`,
				robots: ``,
                schema: [
                  {
                    question: "What is chemical poisoning at work?",
                    answer: `<p>Chemical poisoning is the term used to describe the effects of exposure to a broad range of substances, including:</p><ul><li>Dangerous chemicals and products containing chemicals</li><li>Vapour, fume or dust from hazardous materials</li><li>A wide variety of industrial gases</li><li>Bacteria, viruses and other biological agents</li></ul><p>Exposure to any of these chemicals or by-products can lead to a variety of serious illnesses including:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/asbestos-mesothelioma/\">Mesothelioma and other asbestos related diseases</a></li><li>Nasal cancer</li><li>Bladder Cancer</li><li>Skin Cancer</li><li>Lung Cancer (non asbestos related)</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/skin-conditions-dermatitis/\">Occupational dermatitis</a> (or any other skin condition)</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/occupational-asthma/\">Occupational asthma</a></li><li>Allergies</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/burns-scars-lascerations/\">Burns</a></li><li>Internal organ damage</li><li>Respiratory problems</li><li>Illnesses such as Leptospirosis or Legionnaires Disease</li></ul><p>If you believe you've been made ill by any form of chemical poisoning at work, or have suffered from symptoms including breathing difficulties, wheezing, coughing, sickness, or dizziness, talk to a specialist solicitor right away. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/work-related-cancer/\">Read more on work related cancers here</a>.</p>`
                  },
                  {
                    question: "Does my employer have a duty of care?",
                    answer: `<p>The law says that every employer has a duty of care to protect employees from suffering harm in the workplace. Where your employer failed to foresee the harm that could be caused by your occupation, or provide the protective gear specified, they could be said to have failed in their duty of care. Read more on the health and safety regulations designed to protect you in the workplace.</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/\">The six key health and safety regulations</a></li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/coshh/\">What is COSHH</a></li></ul>`
                  },
                  {
                    question: "Do I need a specialist chemical poisoning lawyer?",
                    answer: `<p><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/industrial-disease/\">Industrial disease claims</a><strong></strong> are quite a specialised area of the law, so it makes sense to deal with solicitors who have extensive experience and a track record of successful claims. Slater and Gordon employs specialist lawyers in every area of industrial disease compensation claims. Just as importantly, we understand the impact that a diagnosis of industrial disease can have on victims and their families, and aim to be as considerate and supportive as possible throughout the claims process.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Chemical poisoning compensation claims`,
				copy: `Chemicals and other hazardous substances in the workplace can lead to serious health problems. If you've been affected, talk to Slater and Gordon, one of the UK's leading chemical poisoning compensation specialists. We could help you on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/chemical-poisoning-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for chemical poisoning claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/chemical-poisoning`,
			sideLinkList: {
				heading: `Questions about personal road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}