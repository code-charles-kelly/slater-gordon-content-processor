module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `fall-heights-objects`,
		slug: `fall-heights-objects`,
		parent: `accident-at-work-compensation`,
		name: `Falls from Heights & Falling Objects`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Falls from Heights & Falling Objects | Slater + Gordon`,
				description: `Slips, trips and falls can happen in any workplace. But if you have been injured due to someone else's negligence, you are entitled to claim compensation. Find out about No Win No Fee injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What's the difference between a slip, a trip and a fall?",
                    answer: `<p>The law is very specific about the difference between a slip, a trip and a fall: as the degree of negligence that caused an injury depends very much on how it occurred. So while it might seem like splitting hairs over language, it's important to understand the legal definitions of slips, trips and falls:</p><ul><li><strong>Slips at work: </strong>these occur when there is too little grip between footwear and the surface you are walking on, due to water, oil, dust or another 'slippery' material on the floor</li></ul><ul><li><strong>Trips at work</strong>: these occur when a worker's foot is brought to an abrupt halt by contact with an object in its path, such as trailing cables, loose carpets, or unexpected objects in walkways</li></ul><ul><li><strong>Falls at work:</strong> there are two types of falls at work: those that occur on the same level, such as by falling into a pothole or other depression in the surface; and falls from heights, such as from rooftops, ladders, gantries and scaffolding</li></ul><ul><li><strong>Falling objects: </strong>injuries caused by falling objects are different to those when people fall, but they will also generally lead to compensation claims being made</li></ul><p>While these definitions are important in law, the most important factor that affects whether you can make a claim for a slip, trip or fall injury at work, is whether someone has been negligent, or your employer has failed in their duty of care towards you. To find out if your slip, trip or fall injury may entitle you to receive compensation, talk to one of our specialist claims solicitors today. </p>`
                  },
                  {
                    question: "Who is responsible for my injury?",
                    answer: `<p>While there are many causes for slip, trip and fall injuries in the workplace, the ultimate responsibility usually lies with your employer. They have a duty of care to ensure that your workplace is safe: from the floor coverings all the way up to any rooftop gantries. So if you've been injured and believe that your slip, trip or fall was caused by an unsafe workplace, find out if you might be entitled to receive compensation for your injuries. </p>`
                  },
                  {
                    question: "What is employers' liability insurance?",
                    answer: `<p>This is an insurance policy that the law requires every employer to have. It ensures that money is available to compensate workers for illness or injuries that were not their fault. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/\">Read more on employers liability insurance</a>.</p>`
                  },
                  {
                    question: "How much is my injury claim worth?",
                    answer: `<p>Every injury claim is different, and the final figure for compensation depends upon the <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">seriousness of the injury</a>, as well as how much it might affect your ability to work in the future, and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">how much any rehabilitation might cost</a>. However, we take most slip, trip and fall injury cases on a No Win No Fee basis, and will often seek interim payments to help you avoid financial hardship if you're prevented from working by your injuries.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>There's a general rule of law that states that claims must be brought within three years from the date of the trip, slip or fall. There are exceptions however, such as if you were working abroad when the accident took place, whether you have diminished mental capacity as a result of the accident or in the tragic event that the accident resulted in death. It's therefore important to speak to a specialist accidents at work lawyer as soon as you're able to. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Slips, trips and falls at work compensation claims`,
				copy: `Whether you have slipped, tripped or fallen at work, if someone else was to blame, you may be able to claim compensation. Slater and Gordon is a leading injury compensation firm, offering a No Win No Fee service to the vast majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/fall-heights-objects-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/fall-heights-objects`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}