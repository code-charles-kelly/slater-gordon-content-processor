module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claim-against-uninsured-driver`,
		slug: `claim-against-uninsured-driver`,
		parent: `road-traffic-accidents`,
		name: `Claims Against Uninsured Drivers`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Claims Against Uninsured Drivers | Slater + Gordon`,
				description: `If you've been hurt in a car accident with an uninsured driver, or in a hit and run, the right legal advice is crucial. Talk to us today about a No Win No Fee uninsured driver claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How can I claim if the other driver isn't insured?",
                    answer: `<p>Driving without insurance is a serious offence, and rightly so. If you've been injured by an uninsured driver, a stolen car, or even by a driver that hit and run, you probably think that you're on your own. Fortunately, at Slater and Gordon we have extensive experience of successfully pursuing claims like this for our clients through the Motor Insurers' Bureau (MIB). </p>`
                  },
                  {
                    question: "What is the Motor Insurers' Bureau?",
                    answer: `<p>The <a href=\"https://www.mib.org.uk/making-a-claim/what-we-do/\">Motor Insurers Bureau</a> runs a fund that every motor insurer pays into, providing around £30 million a year with which to pay compensation for the recklessness of uninsured drivers. As one of the UK's leading injury claim legal firms, we have extensive experience of dealing with the MIB: proving that accidents and injuries have actually occurred, and helping to make sure that the victims of uninsured drivers receive the compensation they need and deserve.</p>`
                  },
                  {
                    question: "What is my Motor Insurers' Bureau claim worth?",
                    answer: `<p>Claiming against an uninsured driver through the Motor Insurers' Bureau should make no difference compared to claiming through your own insurance with regards to the value of your compensation. This is mainly dependent on the severity of any injuries as well as any damage to your property and loss of earnings you may incur.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Road traffic accident claims`,
				heading: `Road traffic claims against uninsured drivers`,
				copy: `Being injured in a collision with an uninsured driver or a stolen vehicle can be a nightmare. Fortunately, in many cases, Slater and Gordon can still claim rightful compensation on your behalf and on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/116089288.jpg`,
					medium: `/public/images/bitmap/medium/116089288.jpg`,
					small: `/public/images/bitmap/small/116089288.jpg`,
					default: `/public/images/bitmap/medium/116089288.jpg`,
					altText: `Happy teenagers driving into the sunset`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/claim-against-uninsured-driver-two-panel-cta`,
				rightHeading: `Find out more about uninsured driver accident compensation.`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claim-against-uninsured-driver`,
			sideLinkList: {
				heading: `More questions about road traffic accident compensation?`,
				links: [
					{
						url: `/personal-injury-claim/road-traffic-accidents/faq/`,
						copy: `Road traffic accident FAQs`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/accident-fault/`,
						copy: `Who was at fault?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/guide-to-insurance-and-motor-claims/`,
						copy: `Comprehensive guide to insurance claims`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `Guide to road safety abroad`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/process/`,
						copy: `Road traffic accident claims process`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/`,
						copy: `What to do in a road traffic accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/legal-expense-insurance-claims/`,
						copy: `What is legal expense insurance?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/claiming-against-uber/`,
						copy: `I was in an Uber accident`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/professional-drivers-public-safety/`,
						copy: `Road Safety Guidelines for Public Transport Workers`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/insurance-offering/`,
						copy: `Disagree with your insurance company?`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/choosing-car-insurance-law-firm/`,
						copy: `Choosing your own motor insurance law firm`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/fast-track/`,
						copy: `Fast track team`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Teenager paralysed in road traffic accident wins £13 million payout`,
					link: `/resources/latest-case-studies/2017/12/teenager-paralysed-in-road-traffic-accident-wins-gbp13m-pay-out/`,
					copy: `A teenager who was left paralysed when her friend's car crashed has been awarded an eight-figure settlement.`
				},
				{
					heading: `Man Struck Down at Pedestrian Crossing Awarded £650,000`,
					link: `/resources/latest-case-studies/2017/07/man-struck-down-at-pedestrian-crossing-awarded-gbp650000/`,
					copy: `A man hit by a vehicle at a pedestrian crossing received a six-figure settlement when he was unable to return to his work as a skilled tradesman.`
				},
				{
					heading: `Serious Road Accident Victim Receives Six-Figure Settlement`,
					link: `/resources/latest-case-studies/2016/12/serious-road-accident-victim-receives-six-figure-settlement/`,
					copy: `A woman was awarded a six-figure settlement following a head on road traffic collision with a motorist driving on the wrong side of the road.`
				}
			],
		}
	}
}