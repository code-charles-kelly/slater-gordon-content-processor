module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `children-court-approval`,
		slug: `children-court-approval`,
		parent: `claiming-behalf`,
		name: `Is Court Approval Needed to Claim on Behalf of a Child?`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Is Court Approval Needed to Claim on Behalf of a Child? | Slater + Gordon`,
				description: `Generally, making a personal injury claim can seem daunting, and more so when claiming on behalf of another person, especially a child. Our experts are here to guide you every step of the way and this guide explains when court approval is needed to claim on behalf of a child.`,
				robots: ``,
                schema: [
                  {
                    question: "Acting as a Litigation Friend",
                    answer: `<p>When a child has been injured and has the right to compensation, they obviously are unable to act on behalf of themselves. Someone who will act in the child's best interests needs to be appointed to bring the claim on their behalf. This person is known as their 'litigation friend'. </p><p>A litigation friend is usually a parent or guardian, however, a professional person can also be appointed to act on a child's behalf. </p>`
                  },
                  {
                    question: "Court Funds Office",
                    answer: `<p>The Court Funds Office is an office in England and Wales that looks after money held on behalf of clients who are unable to look after it themselves, such as those who don't have mental capacity to look after their own financial affairs or for children, and it provides a banking and investment service. <a href=\"https://www.gov.uk/court-money-children\">The litigation friend will be responsible</a> for liaising with the Court Funds office until the child turns 18. </p><p>As any settlement money belongs to the child, the judge will often arrange for the money to be invested with the Court Funds Office until the child reaches 18 years old, when the money will be released to them. </p><p>For a free consultation about making a personal injury claim on behalf of a child call Slater and Gordon 24/7 on freephone ${contentShared.meta.phonenumber.number} or <a href=\"https://www.slatergordon.co.uk/contact-us/\">contact us online</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: ``,
				copy: `Parents often take the position that they can decide what's in their child's best interests without approval from anyone else. This is probably true in most of life's events, but not always when it comes to personal injury claims in the English legal system.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/children-court-approval-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/children-court-approval`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}