module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `cruise-ship`,
		slug: `cruise-ship`,
		parent: `holiday-accident-claims`,
		name: `Cruise & Sea Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Cruise & Sea Injury Compensation Claims | Slater + Gordon`,
				description: `Injured on a cruise ship or ferry? If you've been injured whilst travelling by sea, you may be entitled to compensation. Slater and Gordon specialises in No Win No Fee compensation claims for accidents on ships abroad and in the UK.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for an accident on a cruise ship or ferry?",
                    answer: `<p>Sea travel is quite safe and serious accidents are few and far between, but the sea can sometimes be unpredictable. There are a variety of injuries that can happen, below are some examples of what we have helped our clients claim compensation for:</p><ul><li>Due to concerns that the ship's doctor was negligent - for example, misdiagnosing a health problem or failing to spot a serious injury or condition that later worsened</li><li>Slips, trips and falls on deck or suffered an injury while embarking or disembarking - for example, tripped due to a slippery surface or missing handrail, or had a collision with luggage or other obstacles that should have been removed</li><li>Fell overboard due to a lapse in the proper safety measures</li><li>Burns or scalds from food service</li><li>Accidents during wave turbulence</li><li>Injuries caused by other passengers</li><li>Allergic reaction from on board meals</li></ul><p>Naturally, there are also many other ways in which injuries can arise from a cruise ship or ferry. So if you've been injured on a ship or boat and feel that someone else was to blame, ask us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee Claim</a> today. </p>`
                  },
                  {
                    question: "Can I claim for accidents in the UK as well as overseas accidents?",
                    answer: `<p>When it comes to a ferry or cruise ship injury, it doesn't matter where the accident happened. That is because of <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/\">the Athens Convention</a>. This was introduced in 1974 and updated in 2002 and ensures that cruise liners and ferries have enough insurance in place to cover any accidents. Smaller UK companies that use boats will most likely have <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">public liability insurance</a> that we will be able to help you claim against. So if you've been injured on a ship and it wasn't your fault, ask us about making a No Win No Fee claim today. </p>`
                  },
                  {
                    question: "Is there a time limit to claim?",
                    answer: `<p>The Athens Convention has a time limit for which you can bring a claim. The time limit is normally two years from the date of disembarkation (getting off the ship). It is important to note that the discretion to extend time under the Limitation Act 1980 does not apply to the Athens Convention limit and so it is vital that you seek legal advice as soon as possible. </p>`
                  },
                  {
                    question: "What's required to make an accident at sea claim?",
                    answer: `<p>If you suffer an accident at sea, we advise if possible that you take the following steps to help us help you:</p><ul><li>Seek medical attention - your health and safety is obviously paramount </li><li>Report the accident to a member of the ship's crew and obtain a copy of the report made</li><li>Take photographs of the accident location and any defect which you consider led to your accident</li><li>Take the names and addresses of any witnesses to your accident and ask them to confirm that they would be happy for our personal injury lawyers to contact them if necessary</li></ul>`
                  },
                  {
                    question: "What happens if I was not a passenger, but a crew member?",
                    answer: `<p>If you weren't a passenger on board a vessel but were working at the time, you may still be able to make an accident at sea claim. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Please take a look at the accidents at work section of our website</a> for guidance and useful information.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Cruise and sea injury compensation claims`,
				copy: `Cruising on the sea or ferrying to your next holiday destination should be safe and relaxing, but when accidents happen you may be entitled to claim compensation. Slater and Gordon is a specialist claims firm with extensive experience of helping clients who have been injured at sea.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/cruise-ship-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/cruise-ship`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}