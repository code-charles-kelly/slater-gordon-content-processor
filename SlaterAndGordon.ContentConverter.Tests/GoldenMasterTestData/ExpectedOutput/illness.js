module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `illness`,
		slug: `illness`,
		parent: `personal-injury-claim`,
		name: `Illness  Lawyers`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Illness Compensation Claims Lawyers | Slater + Gordon`,
				description: `If you have been struck down with an illness due to someone else's negligence, this is what you need to know about your potential right to compensation. Find out about No Win No Fee illness compensation claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for work-related illnesses?",
                    answer: `<p>Whether your illness was work related or caused in a public place, if it was due to someone else's negligence, you can claim compensation.</p><p>Work related illnesses often start with minor symptoms and the full extent of the illness may only be diagnosed after you've already retired. However, you can still claim compensation from your former employer. You have up to three years from the point your illness was diagnosed to start a compensation claim. We have successfully helped clients with the following illnesses:</p><ul><li>Repetitive strain injuries from repetitive movements or continuous vibration</li><li>Silicosis from working in factories or on construction sites</li><li>Noise-induced hearing loss from working in noisy areas such as factories or construction sites</li><li>Dermatitis from working with cleaning agents or detergents</li><li>Musculoskeletal injuries from continuous heavy lifting</li></ul>`
                  },
                  {
                    question: "How much can I claim for?",
                    answer: `<p>Your compensation depends on your illness and the consequences it has on your life. Some illnesses can cause long term effects such as chronic fatigue, rheumatologigal conditions or psychological problems. These are factored into the calculation of your final compensation settlement. Your claim for pain and suffering could be worth several thousand pounds. Contact us to establish your full legal position.</p>`
                  },
                  {
                    question: "Can I claim on behalf of a loved one?",
                    answer: `<p>Losing a loved one due to someone else's negligence is one of the worst things that can happen. Slater and Gordon have the understanding and experience to handle any claim with the utmost sensitivity. We understand that seeking rightful compensation may be a necessity if the loss of your loved one has left you in a difficult financial position.</p><p>Take your time to grieve and feel free to contact us when you are ready to do so.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Illness compensation claims`,
				copy: `Have you fallen ill and believe that someone else's negligence was to blame? Slater and Gordon are leading illness compensation specialists offering a No Win No Fee service to the majority of clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/illness-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Carbon monoxide`,
						url: `/personal-injury-claim/illness/carbon-monoxide/`,
						icon: `carbon-monox`
					},
					{
						title: `Food`,
						url: `/personal-injury-claim/illness/food-poisoning/`,
						icon: `burger-cup`
					},
					{
						title: `Water contamination`,
						url: `/personal-injury-claim/illness/water-contamination/`,
						icon: `chemical`
					}
				]
			},
			standardContent: `product-template/illness`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/restaurant-guidelines/`,
						copy: `Restaurant guidelines`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			imageTextA: {
				section: `Illness Claims`,
				heading: `What illnesses could I claim for?`,
				copy: `The occasional illness is a fact of life for all of us, with food-related stomach upsets among the most common. At the same time, it has to be recognised that some cases of food poisoning or respiratory problems are caused by other people's negligence. Some of these cases mean just a few days of sickness.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/244930688.jpg`,
					medium: `/public/images/bitmap/medium/244930688.jpg`,
					small: `/public/images/bitmap/small/244930688.jpg`,
					default: `/public/images/bitmap/medium/244930688.jpg`,
					altText: `Close-up of mature woman stood on the beach`
				},
				video: false
			},
		}
	}
}