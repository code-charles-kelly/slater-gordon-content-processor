module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `guide`,
		slug: `guide`,
		parent: `illness`,
		name: `Illness Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Illness Claims Guide | Slater + Gordon`,
				description: `Have you been made ill by food or drink in the UK? Proving your case may be easier than you think. Ask us out about starting a No Win No Fee food poisoning compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How do I claim for an illness caused by food or drink?",
                    answer: `<p>If you have been made ill by food poisoning, water contamination or a serious reaction to undeclared allergens in the UK, it's quite possible that you may be able to claim compensation. Whether you have eaten or drank in a hotel, restaurant, pub, or café; or taken food or drink away from a street food establishment, sandwich shop, supermarket or any other retailer, that food retailer is responsible for making sure that the food and drink is fit for human consumption and free of undeclared allergens. </p><p>Naturally, making a compensation claim for an illness like this is made more difficult by the fact that you may already have eaten all of whatever made you ill. However, following a few simple rules can make it possible to prove the liability of the food retailer.</p>`
                  },
                  {
                    question: "What evidence do I need to make an illness claim?",
                    answer: `<p>In the first instance, make sure that you keep as much evidence as possible of the food or drink that made you ill. If it was a sandwich, keep the wrapper and the receipt. If it was a meal, make sure to keep a copy of your bill. Where you still have some of the food or drink, keep it in the fridge until it can be analysed. And if you had to see a doctor as a result of the illness or allergic reaction, make sure that you have copies of any notes they made or test results from stool or urine samples that they requested.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `A Guide to claiming compensation for Illness`,
				copy: `If you have been made ill by food or drink in the UK, you may be entitled to claim compensation if you follow this simple guide. Slater and Gordon is a leading food poisoning and allergy claims firm, offering a No Win No Fee service to the majority of our clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/guide`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}