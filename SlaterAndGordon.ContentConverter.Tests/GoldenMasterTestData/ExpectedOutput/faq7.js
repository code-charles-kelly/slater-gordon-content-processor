module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq7`,
		slug: `faq`,
		parent: `military`,
		name: `Military Injury Compensation FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Military Injury Compensation FAQs | Slater + Gordon`,
				description: `Claiming for an injury sustained whilst serving in the armed forces can be a complex matter, so our legal experts have answered some of the most frequently asked questions below.`,
				robots: ``,
                schema: [
                  {
                    question: "Will it cost me anything to make a compensation claim?",
                    answer: `<p>Slater and Gordon understand that the cost of legal services can be a worry, but there are <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/funding-your-case/\">options available to fund personal injury cases</a> involving military injuries. You may have legal expense insurance under an existing insurance policy which will entitle you to free representation. We can check whether your policies have this cover included.</p><p>If you don't have any legal expense insurance under an existing policy, we may be able to conduct your case under a No Win No Fee agreement. This means, if your case is unsuccessful, they'll be no financial risk to you. Fees may apply if you do go on to win your case and receive compensation. For more information, visit our <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee page</a>. We'll be able to explain the funding options available once we've reviewed your case.</p>`
                  },
                  {
                    question: "What if the incident happened overseas?",
                    answer: `<p>Whilst you can't claim for incidents that occurred in combat, you can claim for incidents that happened during training exercises abroad. A number of our clients have claimed for injuries that occurred in Cyprus and Estonia, for example.</p><p>An exception to this is mistreatment following an incident in combat. For example, if there was a failure to diagnose and treat <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/military/ptsd/\">PTSD </a>then you may have a claim if the delay adversely affected your recovery. If this has happened to you, please call one of our experts for a confidential chat.</p>`
                  },
                  {
                    question: "Is there a time limit on making a claim?",
                    answer: `<p>There are laws in place which set out time limit rules for when compensation claims should be made. It's therefore important that you seek advice from a lawyer who specialises in military injury claims as soon as possible if you believe you have a claim.</p><p>Generally, claims should be made within three years from the date of the incident or the date you were first aware you had suffered an injury or illness due to negligence. There are exceptions to this rule however, so it's always best to speak to a specialist military claims lawyer as soon as possible. </p>`
                  },
                  {
                    question: "Will I have to go to court?",
                    answer: `<p>It's very rare that military injury claims will actually go to trial, a large majority settle before a trial takes place. However, if your case doesn't settle before trial, attendance at court may be necessary. We will however be by your side at all times.</p>`
                  },
                  {
                    question: "Can I claim on behalf of someone who has died as a result of a military injury?",
                    answer: `<p>In the event that a loved one has passed away due to a military injury, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">a claim can be made</a> by the person named as executor in the will if there is one or by a spouse/family member.</p>`
                  },
                  {
                    question: "Will my lawyer help me source the right treatment or rehabilitation should I need it?",
                    answer: `<p>Yes. Slater and Gordon have many years' experience working on behalf of people who have suffered serious military injuries. We see it as an important part of our role to ensure our clients get the best quality support to maximise the chances of recovery and future independence.</p><p>We work closely with case managers, medical experts and approved <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/rehabilitation/\">rehabilitation providers</a> to make sure a tailored programme is put in place and an immediate needs assessment is undertaken as soon as possible so that treatment can begin without delay.</p>`
                  },
                  {
                    question: "What if I need to pay for treatment, adaptations or other things to aid my recovery before the case has settled?",
                    answer: `<p>There's no telling how long a case will take to settle but Slater and Gordon understand that our clients may need to pay for treatment or <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">adaptations to a property</a> to aid recovery and future independence as soon as possible.</p><p>Where we've proved that the other side was partly or fully to blame for your injuries, we can obtain financial assistance in the form of 'interim payments'. These are payments which are made to you before your case reaches settlement.</p>`
                  },
                  {
                    question: "Do the MOD have immunity from certain claims being brought against them?",
                    answer: `<p>There are certain cases where the MOD would be immune from having a claim brought against them:</p><p><strong>Crown Immunity:</strong> The MOD is immune from any claim by service personnel where an injury/incident, caused by their negligence, took place before 15 May 1987. This means even if the incident occurred before 1987 but the injury was not apparent until after that date, e.g. an asbestos related illness, you can't make a claim.</p><p><strong>Combat Immunity:</strong> The MOD is immune from any claim by service personnel in respect of an injury/incident which has occurred during the course of combat. However, the MOD have sometimes been known to make ex-gratia payments in certain situations, so you should always seek legal advice.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Military accident claims`,
				heading: `Military injury compensation FAQs`,
				copy: `The most commonly asked military injury questions answered by our legal experts.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/134406019.jpg`,
					medium: `/public/images/bitmap/medium/134406019.jpg`,
					small: `/public/images/bitmap/small/134406019.jpg`,
					default: `/public/images/bitmap/medium/134406019.jpg`,
					altText: `Military father sitting at home with family`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee military injury claims`,
				leftWysiwyg: `product-template/faq7-two-panel-cta`,
				rightHeading: `Choose Slater and Gordon to start your military accident claim`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq7`,
			sideLinkList: {
				heading: `Do you have more questions on military injury claims? Read our guide below.`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military accident FAQ`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `What is the armed forces compensation scheme?`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}