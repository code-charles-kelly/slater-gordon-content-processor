module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `sexual-criminal-abuse`,
		slug: `sexual-criminal-abuse`,
		parent: `personal-injury-claim`,
		name: `Sexual & Physical Abuse`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Sexual & Physical Abuse Claims Solicitors | Slater + Gordon`,
				description: `If you've been the victim of sexual or physical abuse, you deserve support, confidentiality and compensation. Talk to us today about starting a No Win No Fee abuse compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Why it is possible to claim compensation if you've been abused?",
                    answer: `<p>If you've been sexually or physically abused, not only has a criminal offence been committed, but you've also suffered as a result. Just like with any other personal injury case, if you've been injured, either physically and/or psychologically, you should be compensated for your suffering. There can also be other costs which have been incurred when you have suffered from abuse, such as loss of earnings if you've been unable to work and treatment such as counselling. It's therefore important that not only do you get the justice you deserve, but also, you're compensated for your suffering and also any financial losses. </p><p>If you or a loved one have suffered from abuse, no matter how long ago it was, contact one of our experienced abuse lawyers. They're all caring people and good listeners, having supported so many people who have suffered abuse for so many years. So when you're ready to talk to someone, they're here to listen. </p>`
                  },
                  {
                    question: "What are the most common types of abuse cases?",
                    answer: `<p>There's never an excuse for physical, sexual or emotional abuse, and you have a right to be compensated for your suffering, no matter what type of abuse case you have. However, some of the most common types of abuse cases include:</p><p>• Abuse suffered as a child in local authority or foster care</p><p>• A failure by Social Services to protect a child from abuse</p><p>• Abuse by a person in a position of trust (such as a priest or teacher)</p><p>• Abuse suffered within the family</p><p>• Abuse of the elderly in a nursing or care home</p><p>• Sexual or physical abuse suffered as an adult</p><p>• A single incident of a sexual or physical assault</p><p>If you, a family member or a friend have been subjected to abuse, regardless of the situation, we are here to listen in confidence and advise you on whether you may be able to seek justice by claiming compensation.</p>`
                  },
                  {
                    question: "What is historic child abuse?",
                    answer: `<p>When someone has suffered abuse as a child, understandably, it can sometimes take them many years before they feel able to talk to someone about what happened to them, often when they're adults. When abuse happened many years ago, it's known as 'historic abuse'. It's also why the usual time limits for making a compensation claim doesn't always apply to abuse cases. However, it's best to speak to one of our specialist lawyers as soon as you feel able to talk.</p>`
                  },
                  {
                    question: "Will everyone find out if I claim compensation?",
                    answer: `<p>The law is very clear on this subject: victims of abuse are entitled to seek criminal and civil justice with total anonymity. So you can rest assured that everyone involved in helping you with your claim will keep everything totally confidential. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/\">Click here to find out more</a>. </p>`
                  },
                  {
                    question: "What if the abuser is a high profile / public figure and may attract the media?",
                    answer: `<p>Slater and Gordon's experts in abuse law have dealt with many high profile abuse cases over the years. We're very experienced when it comes to dealing with the press regarding abuse cases and ensuring anonymity for our clients. We're often asked by the press to comment on high profile cases given our expertise in this area of law. </p><p>We've acted for people who have suffered from abuse in many high profile cases, such as: </p><ul><li>Jimmy Savile</li><li>Rolf Harris</li><li>Max Clifford</li><li>Stuart Hall</li><li>Cyril Smith </li><li>Rochdale Exploitation Scandal </li><li>Football Coach Scandal </li><li>The Catholic Church</li><li>The Church of England </li><li>The Methodist Church </li><li>Johovah's Witnesses </li><li>Chethams School of Music </li><li>Durham Chorister</li><li>The Scout Association </li><li>St John's Ambulance Service </li><li>Ashdown House </li><li>St Pauls / Colet Court</li><li>Hillside First</li><li>St Paul's Cathedral Choir School</li><li>St George's School</li><li>Barnardos</li><li>North Wales Child Abuse Scandal</li></ul>`
                  },
                  {
                    question: "What is the Criminal Injuries Compensation Scheme?",
                    answer: `<p>Also <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/\">known as CICA</a>, this is a Government scheme to compensate victims of criminal behaviour for their injuries, suffering and loss of earnings. If your case is suitable for a CICA claim, we may be able to fund it under what's known as a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">Contingency Fee Agreement </a>which works in a similar way to a No Win No Fee Agreement. So you can rest assured that there is no financial risk to making a claim for compensation.</p>`
                  },
                  {
                    question: "Who would I have to talk to first?",
                    answer: `<p>Every sexual abuse or assault claim begins when you feel ready to talk to one of our specialist abuse claims solicitors. Each of them has helped many abuse survivors, and respects the fact that it may be hard for you to talk about your experience. There's no rush and no pressure to start a case until you're absolutely sure it's what you want to do. If you'd like to see what sort of person you might be talking to, why not watch the video below. It doesn't just talk about what we could do for you, it also tells you why it matters to us to help abuse survivors.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Confidential sexual and physical abuse solicitors`,
				copy: `If you've suffered from sexual or physical abuse, we understand how incredibly hard it is to talk about it. But when you're ready to talk, our specialist legal team is ready to listen and advise you honestly and confidentially. There's no financial risk either, as the majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/sexual-criminal-abuse-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			onwardJourneys: {
				title: ``,
				ctas: [
					{
						title: `Child abuse`,
						url: `/personal-injury-claim/sexual-criminal-abuse/child-abuse/`,
						icon: `child-abuse`
					},
					{
						title: `Criminal injury`,
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/`,
						icon: `criminal-injury`
					}
				]
			},
			standardContent: `product-template/sexual-criminal-abuse`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
			imageTextA: {
				section: `Sexual and criminal abuse`,
				heading: `How can we help you?`,
				copy: `Abuse of any kind is a traumatic and often a life changing experience. If you have experienced abuse you deserve, justice, compensation and support throughout.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/107602301.jpg`,
					medium: `/public/images/bitmap/medium/107602301.jpg`,
					small: `/public/images/bitmap/small/107602301.jpg`,
					default: `/public/images/bitmap/medium/107602301.jpg`,
					altText: `Woman sitting on the seashore watching the setting sun`
				},
				video: false
			},
		}
	}
}