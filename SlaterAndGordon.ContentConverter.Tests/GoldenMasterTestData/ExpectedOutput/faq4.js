module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq4`,
		slug: `faq`,
		parent: `serious`,
		name: `Serious Injury FAQ's`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Serious Injury FAQ's | Slater + Gordon`,
				description: `Serious injuries can have a massive effect on your life and that of your loved ones. At Slater and Gordon, our compassionate lawyers are here to help you every step of the way.`,
				robots: ``,
                schema: [
                  {
                    question: "What is a serious injury?",
                    answer: `<p>A serious injury can mean different things to different people and a lot of injuries can be considered serious. In many personal injury cases the effects of an injury can be short term and compensation can cover any pain and suffering. Usually in the personal injury industry we describe an injury as serious if it has life altering consequences that require interim payments and large sums of compensation required to cover the potentially lifelong future effects of the injury you have sustained.</p>`
                  },
                  {
                    question: "What are common forms of serious injuries?",
                    answer: `<p>Serious injuries come in many forms, some more visible than others. Some of the most common forms of serious injuries Slater and Gordon specialise in are:</p><ul><li>Loss of limbs </li><li>Spinal cord injuries </li><li>Head/brain injuries </li><li>Fatal injuries </li></ul>`
                  },
                  {
                    question: "What are interim payments?",
                    answer: `<p>In some circumstances you may require immediate financial support after an injury. This could be for medical expenses, treatment and rehabilitation, support workers or even house modifications. In a case where fault has been admitted, but the final amount of compensation has not been agreed, our lawyers will attempt to gain interim payments in an effort to ensure you get the financial help you need as quickly as possible. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/interim-payments/\">Click here for more on interim payments</a>.</p>`
                  },
                  {
                    question: "What is a trust?",
                    answer: `<p>In some cases where a large sum of money is awarded as compensation or in cases where the sufferer of the injury has been left with a diminished mental capacity, a trust can be set up to look after the money. There are a variety of reasons why we may recommend this.</p>`
                  },
                  {
                    question: "My loved one has been left with diminished mental capacity after an accident, can I claim on his behalf?",
                    answer: `<p>When someone has been left with diminished mental capacity after an accident that wasn't their fault, someone is able to make a claim on their behalf. Certain relations would be allowed to do this, such as a parent or guardian. <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">Click here for more information on claiming on behalf of another</a>.</p>`
                  },
                  {
                    question: "What is the impact of a serious injury?",
                    answer: `<p>In the vast majority of cases when someone has suffered a serious injury, the sufferer may struggle to return to their life as it was before the injury. This may mean limited mobility or diminished mental capacity. It may be difficult to return to work and <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/house-adaptations/\">modifications to one's home or car</a> may be required for example. No matter the severity of your injury Slater and Gordon's expert solicitors are there to get you the compensation you deserve.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Serious injury`,
				heading: `Serious Injury FAQ`,
				copy: `Serious injuries such as head, spinal or even amputation injuries can have a pronounced effect on your life and that of your loved ones. Such catastrophic injuries require a specialist solicitor that can help you secure the compensation to look after your future needs and any rehabilitation required.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/111080052.jpg`,
					medium: `/public/images/bitmap/medium/111080052.jpg`,
					small: `/public/images/bitmap/small/111080052.jpg`,
					default: `/public/images/bitmap/medium/111080052.jpg`,
					altText: `Happy nurse talking to elderly lady`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq4-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee serious injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq4`,
			sideLinkList: {
				heading: `Questions about serious injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/serious/faq/`,
						copy: `Serious Injury FAQ`
					},
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Machinist Awarded £175,000 Damages for Serious Arm Injury`,
					link: `/resources/latest-case-studies/2016/03/machinist-awarded-gbp175000-damages-for-serious-arm-injury/`,
					copy: `A personal injury compensation claim award was agreed after a machinist suffered significant injuries to his left arm in an accident at work.`
				},
				{
					heading: `Serious Eye Injury Claim Settled for over £20,000`,
					link: `/resources/latest-case-studies/2015/03/over-gbp20000-compensation-for-serious-eye-injury/`,
					copy: `Slater and Gordon Personal Injury Lawyer Michael Hardacre settled over £20,000 compensation for his client who sustained a serious eye injury when doing household chores.`
				},
				{
					heading: `£7 Million Settlement For Traumatic Spinal Injury`,
					link: `/resources/latest-case-studies/2017/05/gbp7-million-settlement-for-traumatic-spinal-injury/`,
					copy: `A settlement of £7 million was awarded to a woman who suffered a life-changing spinal cord injury following a road traffic accident.`
				}
			],
		}
	}
}