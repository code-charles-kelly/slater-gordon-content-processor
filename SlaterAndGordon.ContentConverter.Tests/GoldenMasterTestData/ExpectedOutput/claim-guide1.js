module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claim-guide1`,
		slug: `claim-guide`,
		parent: `faulty-products`,
		name: `Defective & Faulty Product Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Defective & Faulty Product Claims Guide | Slater + Gordon`,
				description: `Have you been injured by a faulty product? Compensation may be available from the manufacturer if you have suffered injury as a result of a faulty product. Ask us out about starting a No Win No Fee defective product compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "How do I claim for injuries caused by faulty products?",
                    answer: `<p>If you or a family member has been injured by a faulty product - from an inflammable mobile phone to a contaminated jar of face cream - you may be able to claim injury compensation from the manufacturers. Naturally, the faulty product itself is the best proof of your claim, but there are a number of things that can assist in ensuring your compensation claim will be successful.</p>`
                  },
                  {
                    question: "What proof do I need for a product liability claim?",
                    answer: `<ul><li><strong>Keeping receipts always helps</strong></li></ul><p>It's good practice to always keep receipts for everything you buy, whether it's a major purchase or not, as well as returning the guarantee card to the manufacturer on occasions when you've bought an electrical product. </p><p>In the event that an injury claim has to be made against the manufacturer, your receipt will help your solicitor to prove exactly when and where you bought the product. This is crucial, because if you can't prove that you bought the product, proving the right to injury compensation may be more complex.</p><ul><li><strong>Keep as much of the product - and the packaging - as possible</strong></li></ul><p>Making a defective product liability claim successfully relies on proving that the manufacturer is to blame for your injury. So even if you are tempted to throw the product away - as it's unlikely that you'd want to keep an unstable stepladder or contaminated cosmetics - it's essential to keep it somewhere safe, in the best condition possible, so that it can be assessed by our experts, the manufacturer's own experts, and perhaps even by the court in serious cases.</p><p>It's also helpful if all packaging, including any instructions or guarantees can accompany your evidence.</p><ul><li><strong>Keep an accurate record of everything that happened</strong></li></ul><p>Whether a defective product liability case is settled between legal firms or in court, clear, reliable evidence is an essential part of making a claim successful. So as soon as practically possible after an injury has been caused by a defective product, you should write down every relevant fact, and take photos to back up the facts whenever possible. Your notes should include:</p><ul><li>Where and when you bought the product</li><li>How often you've used it</li><li>What you were doing when the injury occurred</li><li>What injury you sustained </li><li>What medical help you sought</li><li>Any medical records to accompany your own notes</li></ul>`
                  },
                  {
                    question: "Get all the evidence to your solicitor",
                    answer: `<p>While claiming a refund for faulty goods from a retailer is usually a simple matter, where an injury has been caused, we recommend that you talk to a solicitor specialising in defective product injury claims about seeking compensation, rather than the manufacturer of the product. They have their reputation to protect, and will seldom be pleased to hear about a potentially damaging product liability claim, That's why, in the event that you've been injured by a defective product, it's generally better to speak to an experienced claims solicitor with a track record of success. In the majority of cases, Slater and Gordon will be happy to act for you on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Faulty products`,
				heading: `A Guide to claiming compensation for defective products`,
				copy: `If you or a loved one has been injured by a faulty product, you may be entitled to claim compensation by following this simple guide.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/140393239.jpg`,
					medium: `/public/images/bitmap/medium/140393239.jpg`,
					small: `/public/images/bitmap/small/140393239.jpg`,
					default: `/public/images/bitmap/medium/140393239.jpg`,
					altText: `group of young people having a beverage`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/claim-guide1-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee defective product claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claim-guide1`,
			sideLinkList: {
				heading: `Questions about faulty product compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faulty-products/claim-guide/`,
						copy: `Faulty product claim guide`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can i claim for?`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `Personal injury and debt support`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Five-Figure Settlement for Eye Injury Caused by Faulty Packaging`,
					link: `/resources/latest-case-studies/2017/01/five-figure-settlement-for-eye-injury-caused-by-faulty-packaging/`,
					copy: `A five-figure settlement was awarded to a man in compensation for an eye injury caused by a faulty handle.`
				},
				{
					heading: `Jogger Awarded 5 Figure Settlement for Defective Kerb Fall`,
					link: `/resources/latest-case-studies/2016/01/jogger-awarded-5-figure-settlement-for-defective-kerb-fall/`,
					copy: `A jogger has been awarded a five-figure compensation settlement for extensive injuries to their leg sustained following a fall on defective kerb.`
				},
				{
					heading: `Settlement Awarded for Trip on Defective Manhole`,
					link: `/resources/latest-case-studies/2015/09/settlement-awarded-for-trip-on-defective-manhole/`,
					copy: `Slater and Gordon Lawyers secure settlement for client who tripped on a defective manhole`
				}
			],
		}
	}
}