module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claim-guide`,
		slug: `claim-guide`,
		parent: `industrial-disease`,
		name: `Industrial Disease Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Industrial Disease Claims Guide | Slater + Gordon`,
				description: `Industrial diseases can be life-changing for those suffering and their families. This guide is intended to provide useful information about the claims process. Slater and Gordon is a leading law firm with broad experience of supporting sufferers as they seek justice.`,
				robots: ``,
                schema: [
                  {
                    question: "Industrial disease claims process",
                    answer: `<p>We fully understand the difficulties that those suffering with an industrial disease can face and that the claims process can seem daunting. This brief guide is designed to provide a reference point to help you understand the process involved and the ways in which our experienced solicitors will help you seek justice and compensation.</p>`
                  },
                  {
                    question: "Registering your claim with the court",
                    answer: `<p>Although it's not necessary on certain cases, in the majority, once we have the initial evidence, we'll register your claim with the court. This is because once the claim has been registered with the court, they'll put strict time limits in place in which both parties must submit evidence, reach a settlement, etc. This protects you from your case dragging on unnecessarily. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Industrial disease claims`,
				heading: `Industrial disease claims guide`,
				copy: `Industrial diseases can have devastating effects on sufferers and their families. This guide to the claims process is intended to provide much needed and useful information.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/205895667.jpg`,
					medium: `/public/images/bitmap/medium/205895667.jpg`,
					small: `/public/images/bitmap/small/205895667.jpg`,
					default: `/public/images/bitmap/medium/205895667.jpg`,
					altText: `Mature man with short beard looking out of a window`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee industrial disease claim`,
				leftWysiwyg: `product-template/claim-guide-two-panel-cta`,
				rightHeading: `Talk to us about No Win No Fee agreements for industrial disease claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claim-guide`,
			sideLinkList: {
				heading: `Do you have more questions on industrial disease compensation claims? Read our expert guides below.`,
				links: [
					{
						url: `/personal-injury-claim/industrial-disease/claim-guide/`,
						copy: `Industrial disease claims guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/coshh/`,
						copy: `COSHH guide`
					},
					{
						url: `/personal-injury-claim/industrial-disease/industrial-injuries-disablement-benefit/`,
						copy: `Industrial disease disablement benefit`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}