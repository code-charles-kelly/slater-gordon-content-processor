module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `animals`,
		slug: `animals`,
		parent: `injury-in-public`,
		name: `Animal Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Animal Injury Claims | Slater + Gordon`,
				description: `Every animal can be unpredictable. That's why there are so many injuries to humans caused by animals every year. So if you have been hurt by an aggressive or out of control animal, talk to us today about No Win No Fee claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What sort of injuries are caused by animals?",
                    answer: `<p>As you might expect, most animal injuries in the UK are caused by dogs, either by attacking people, or by running into the road and causing a vehicle to swerve. Royal Mail says that more than <a href=\"https://www.royalmail.com/personal/dog-awareness\">2,000 postal workers are bitten by dogs every year</a>, and that is very much the thin end of the wedge. However, it isn't just dogs that cause injuries to humans. Livestock, horses, zoo animals and even cats also cause a large number of injuries every year. Some of the compensation claims we deal with typically involve:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents caused by runaway animals</a></li><li>Horse riding accidents</li><li>Injuries caused by livestock</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/farm/\">Accidents on farms and in stables</a></li><li>Accidents in zoos and wildlife parks</li><li>Children injured at petting zoos and farms</li></ul><p>To find out if your animal injury might entitle you to seek compensation, <a href=\"https://www.slatergordon.co.uk/contact-us/\">talk to an expert today.</a></p>`
                  },
                  {
                    question: "I've been injured by an animal. Who do I claim against?",
                    answer: `<p>Every animal owner has a duty of care to keep it under control and prevent it from causing injury or damage. If they do not keep their animal under control and it causes injury, you can make a claim against the animal owner. In certain cases, especially if the animal concerned is a dangerous one, the <a href=\"http://www.legislation.gov.uk/ukpga/1971/22\">Animal Act 1971</a> may come into play. This law applies particularly to dangerous species, particularly where there has been negligence or the owner has provoked the animal. In either case, whether you have been injured in your workplace or anywhere else, the owner is liable to pay compensation for injuries or damage caused by any animal they own or keep.</p>`
                  },
                  {
                    question: "I've been injured by an animal at work. Should I still claim?",
                    answer: `<p>Many people are frightened to make a claim against their employer when they have been injured by an animal in the workplace, for fear that it will be held against them. However, it's worth bearing two facts in mind if you find yourself in this position:</p><ul><li>Your employer has a Duty of Care to protect you, not to mention <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/injury-in-public/public-liability-insurance/\">employer's liability insurance</a> to pay for claims like this. They should also understand that animals are unpredictable and that some events, such as a horse kicking a groom, for example, are only to be expected</li><li>Employment laws strictly prohibit you from being dismissed as the result of claiming compensation for an injury</li></ul><p>So if you have been injured by an animal in the workplace, you should talk to us about making an injury compensation claim against your employer's liability insurance. </p>`
                  },
                  {
                    question: "How long does an animal injury claim take?",
                    answer: `<p>This usually depends on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">how serious your injuries are</a> as well as whether the negligent party's insurers accept liability. However, we always aim to pursue justice for our clients as efficiently as possible, and will also seek to claim interim payments where your injury might cause you financial hardship, or where you need to begin rehabilitation as soon as possible.  </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Injury in public`,
				heading: `Animal injury compensation claims`,
				copy: `Most animals have the capacity to bite or bolt when frightened or surprised. So if you have been injured by an animal, you may be able to claim compensation for your injuries. Slater and Gordon is one of the UK's most experienced No Win No Fee injury claims firms.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/253265215.jpg`,
					medium: `/public/images/bitmap/medium/253265215.jpg`,
					small: `/public/images/bitmap/small/253265215.jpg`,
					default: `/public/images/bitmap/medium/253265215.jpg`,
					altText: `Senior couple buying fresh fruit at the market`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/animals-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee public injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/animals`,
			sideLinkList: {
				heading: `Questions about public injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/injury-in-public/faq/`,
						copy: `Public injury claims FAQs`
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-injury-guide/`,
						copy: `Guide to public injury `
					},
					{
						url: `/personal-injury-claim/injury-in-public/public-liability-insurance/`,
						copy: `Public Liability Insurance`
					},
					{
						url: `/personal-injury-claim/injury-in-public/council-highways-agency/potholes/`,
						copy: `Claiming compensation for potholes`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Woman Receives £65,000 Damages After Staircase Fall`,
					link: `/resources/latest-case-studies/2016/03/woman-receives-gbp65000-damages-after-staircase-fall/`,
					copy: `Following a fall from a flight staircases, our client received a considerable settlement after assessment.`
				},
				{
					heading: `Swimmer recovers £27,000 for swimming pool slip`,
					link: `/resources/latest-case-studies/2015/08/swimmer-recovers-gbp27000-for-swimming-pool-slip/`,
					copy: `A swimmer received substantial compensation after she broke her ankle after slipping on a pool of water.`
				},
				{
					heading: `Damages awarded for victim of 5-a-side slip and fall`,
					link: `/resources/latest-case-studies/2015/11/damages-awarded-for-victim-of-5-a-side-slip-and-fall/`,
					copy: `A personal injury compensation settlement was agreed when a slip and fall at a leisure centre ended in leg injuries.`
				}
			],
		}
	}
}