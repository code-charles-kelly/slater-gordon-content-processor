module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `carbon-monoxide`,
		slug: `carbon-monoxide`,
		parent: `illness`,
		name: `Carbon Monoxide Poisoning`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Carbon Monoxide Poisoning Compensation Claims | Slater + Gordon`,
				description: `If you or a family member have been affected by carbon monoxide poisoning, you may be entitled to claim compensation. Talk to us today about a No Win No Fee carbon monoxide poisoning claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What is carbon monoxide poisoning?",
                    answer: `<p>Carbon monoxide is an odourless, colourless gas that is usually produced by badly serviced gas boilers, car exhaust pipes and blocked flues. Also known as 'the silent killer', it is responsible for thousands of cases of carbon monoxide poisoning every year, a few of which result in permanent brain damage or even death. In most of these cases, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/\">the cause is a failure of landlords or letting agencies</a> to get boilers serviced and flues checked, though some cases are also caused by faulty workmanship by plumbers and gas engineers. </p><p>If you are <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/\">worried about carbon monoxide poisoning</a> act immediately! </p>`
                  },
                  {
                    question: "How do I prove I have carbon monoxide poisoning?",
                    answer: `<p>One of the difficulties of carbon monoxide poisoning is that symptoms can be missed even by experienced medical professionals. It isn't unknown for doctors to assume patients are suffering from ME when in fact they are being slowly poisoned by a faulty gas boiler. It is also common for insurance companies to deny all liability for carbon monoxide poisoning. That's why our specialist claims solicitors work with toxicologists, neurologists and liability engineers to prove carbon monoxide poisoning and its causes.</p><p>Read our expert guide on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/illness/carbon-monoxide/expert-guide/\">claiming for carbon monoxide poisoning here</a>.</p>`
                  },
                  {
                    question: "What will carbon monoxide poisoning compensation cover?",
                    answer: `<p>We take on most carbon monoxide poisoning cases on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>, and seek to claim compensation for our clients' pain and suffering, as well as expenses to cover things like <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/what-can-i-claim-for/\">medical treatment, prescriptions, loss of earnings and travel expenses</a>. The amount you might eventually receive depends on the severity of your condition and the chances of a full recovery; and while there are rarely quick and easy resolutions to these cases, we often seek interim payments to help our clients while a final compensation figure is being agreed. [Button]<a href=\"https://www.slatergordon.co.uk/contact-us/\">Talk to an expert</a><strong> </strong></p>`
                  }
                ]
			},
			heroHeader: {
				section: `Illness Claims`,
				heading: `Carbon monoxide poisoning compensation claim`,
				copy: `Thousands of Britons are affected by carbon monoxide poisoning every year, often caused by the negligence of a landlord, letting agent or gas engineer. The majority of Slater and Gordon clients claim compensation on a No Win No Fee basis.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/250351248.jpg`,
					medium: `/public/images/bitmap/medium/250351248.jpg`,
					small: `/public/images/bitmap/small/250351248.jpg`,
					default: `/public/images/bitmap/medium/250351248.jpg`,
					altText: `Female barista making coffee in a cafe`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/carbon-monoxide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee illness claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/carbon-monoxide`,
			sideLinkList: {
				heading: `Questions about illness compensation?`,
				links: [
					{
						url: `/personal-injury-claim/illness/guide/`,
						copy: `Guide to illness compensation`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/faq/`,
						copy: `Food poisoning FAQs`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/expert-guide/`,
						copy: `Expert guide to carbon monoxide poisoning`
					},
					{
						url: `/personal-injury-claim/illness/food-poisoning/causes-prevention/`,
						copy: `Food poisoning prevention guide`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/landlord-legal-requirements/`,
						copy: `Landlord legal requirements`
					},
					{
						url: `/personal-injury-claim/illness/carbon-monoxide/what-to-do-if-worried-about-co/`,
						copy: `What to do if your worried about a gas leak`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
		}
	}
}