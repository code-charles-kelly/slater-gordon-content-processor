module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `electrocution`,
		slug: `electrocution`,
		parent: `accident-at-work-compensation`,
		name: `Electric Shock`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Electric Shock Claims | Slater + Gordon`,
				description: `Have you received an electric shock at work? If it happened due to someone else's negligence, you may be able to seek compensation. Find out about No Win No Fee construction site injury claims with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "What causes electric shocks in the workplace?",
                    answer: `<p>Electric shocks can be dangerous, even from something as small as a 240v hairdryer. So when they occur from much higher voltage equipment, serious injury or even death can occur. The main causes of electric shocks in the workplace are:</p><ul><li>The presence of untested electrical equipment</li><li>Unqualified staff being asked to work on electrical equipment</li><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/defective-equipment/\">Faulty or malfunctioning equipment</a></li></ul><p>In both cases, where this has happened, your employer has failed in their duty of care to protect you from harm in the workplace, and you may have a case for seeking <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">injury compensation on a No Win No Fee basis</a>.</p>`
                  },
                  {
                    question: "What sort of injuries do electric shocks cause?",
                    answer: `<p>In severe cases, a substantial electric shock can prove instantly fatal, which is why the regulations for the use of electrical items in the workplace are so strict. Even where an electric shock is not fatal, any or all of these <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">serious</a><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\"> </a><strong></strong><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">symptoms</a><strong></strong> and effects may occur:</p><ul><li><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/burns-scars-lascerations/\">Burns: both minor and life-threatening</a></li><li>Muscle spasms</li><li>Breathing difficulties</li><li>Irregular heartbeats</li><li>Cardiac arrest</li><li>Broken bones due to muscle spasm</li><li>Damage to the nervous system</li></ul><p>So, if you or a loved one have suffered an electric shock in the workplace, it's essential that you first seek medical treatment, and then talk to an expert lawyer about compensation for your injuries.</p>`
                  },
                  {
                    question: "Who can electric shocks affect?",
                    answer: `<p>While you might imagine that it's mainly electricians that suffer electric shocks, they are generally too well trained to make a mistake where significant currents are present. In fact, just about any worker can be the victim of an electric shock due to poor maintenance or training, in all of these occupations, and more besides:</p><ul><li>Hairdressers</li><li>Mechanics</li><li>Engineers</li><li>Cleaners</li><li>Construction workers</li><li>Office staff</li><li>Care workers</li><li>Nurses</li><li>Theatre staff </li></ul><p>If you've had a shock from faulty or untested electrical equipment – or have been hurt while doing a task you were asked to do but unqualified for – talk to us today about making a No Win No Fee compensation claim. </p>`
                  },
                  {
                    question: "What do I do if a colleague suffers an electric shock?",
                    answer: `<p>The most important thing is NOT TO TOUCH someone while they are suffering an electric shock. That's because your body will conduct the electricity, and you will also receive a shock, and be unable to help your colleague. Instead, follow these six simple steps:</p>`
                  },
                  {
                    question: "What are the regulations for electricity in the workplace?",
                    answer: `<p>While there are many regulations for the safe use of electricity and electrical items in the workplace, the two most important concern <strong>who</strong> is allowed to carry out electrical work, and <strong>how often</strong> all electrical equipment needs to be tested:</p><p>1. You should only be asked to work on electrical installations and equipment if you are<a href=\"http://www.hse.gov.uk/electricity/withequip.htm\"> </a><a href=\"http://www.hse.gov.uk/electricity/withequip.htm\">suitably qualified</a></p><p>2. Every piece of electrical equipment in any workplace - from a kettle to an arc welding unit - must be maintained in good condition. Heavy items on building sites and in factories will need more regular testing than a kettle in a shop kitchen, but they must still be safe. It's therefore good practice for employers to make sure that all electrical equipment is regularly<a href=\"http://www.hse.gov.uk/electricity/faq-portable-appliance-testing.htm\"> </a><a href=\"http://www.hse.gov.uk/electricity/faq-portable-appliance-testing.htm\">PAT tested</a> for safety.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Electric shock compensation claims`,
				copy: `Have you had an electric shock due to faulty equipment or a lack of training at work? Slater and Gordon is a leading injury compensation specialist, offering a No Win No Fee service to almost all of our personal injury clients.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/electrocution-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/electrocution`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}