module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `faq1`,
		slug: `faq`,
		parent: `accident-at-work-compensation`,
		name: `Accident at Work Compensation FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Accident at Work Compensation FAQs | Slater + Gordon`,
				description: `Our specialist team have pulled together the most commonly asked questions regarding accident claims compensation. Find out how Slater and Gordon can help you claim for a work accident.`,
				robots: ``,
                schema: [
                  {
                    question: "What steps do I need to take after sustaining an injury due to an accident at work that wasn't my fault?",
                    answer: `<p>If you sustain an injury due to an accident at work, you should follow these steps:</p><ul><li>Seek medical advice for the best way to treat your injuries</li><li>Report the accident to your employer</li><li>Make sure the details are reported in the accident book</li><li>Report the accident to your Health and Safety Representative if there is one</li><li>Take down the names and contact details of any witnesses (they may need to provide a witness statement as evidence in your case)</li><li>Contact a lawyer who specialises in accident at work claims if you wish to make a compensation claim and believe your employer or someone else is fully or partly to blame for your injury</li></ul>`
                  },
                  {
                    question: "Will it cost me anything to make a claim for an accident at work injury?",
                    answer: `<p>Personal Injury compensation claims following an accident at work can be funded under a No Win No Fee agreement. Meaning, if your case is unsuccessful, you won't have to pay penny and they'll be no financial risk to you. Fees may apply if you do go on to win your case and receive compensation. </p>`
                  },
                  {
                    question: "Can I make a compensation claim if my work accident happened overseas?",
                    answer: `<p>If you've been injured in an accident outside the UK whilst at work, it may still be possible for you to claim personal injury compensation. Depending on the country where your accident took place, there can be conflicts with different jurisdictions and laws. It's very important to ensure that the law firm you choose has experts who specialise in injuries arising from <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/\">accidents overseas </a> as they'll have an understanding of foreign laws and the ones which may apply to your case. </p>`
                  },
                  {
                    question: "Does a claim need to be made within a certain time limit after an accident?",
                    answer: `<p>Generally in England, Wales and Scotland the time limit to make a claim for personal injury after an accident at work is three years from the date of the accident. However, there are some variations to these time limits, for example:</p><ul><li>If claiming on behalf of someone who has passed away from an injury at work, the claim would need to be made within three years from the date of their death</li></ul><ul><li>If claiming on behalf of someone who lacks mental capacity, there's no time limit to make a claim</li></ul><ul><li>Different time limits may also apply if your accident happened overseas. For more information about claiming for personal injury whilst working abroad, visit our page on <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/\">accidents abroad</a>. </li></ul>`
                  },
                  {
                    question: "How long will my injury compensation claim take to process?",
                    answer: `<p>Personal injury cases following accidents at work vary in circumstances and so it's difficult to pinpoint exactly how long a single case will take to complete. In part, it will depend on how complex the case is and how severe the injuries are. Another factor which will determine how long a case will take is whether or not responsibility is accepted by the other side. </p>`
                  },
                  {
                    question: "Can I make a claim on behalf of someone else who has been injured in an accident?",
                    answer: `<p>In cases where someone has passed away due to injuries caused by an accident at work, you may be able to claim for compensation on their behalf. If a will was made, the Executor of the Estate would be able to make the claim. If there isn't a will, only certain relatives will be able to pursue a claim on behalf of their loved one.</p><p>In cases where the injured person lacks mental capacity and is unable to pursue their own personal injury claim, someone who has Power of Attorney or is a Deputy for that person will be able to <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/\">claim on their behalf</a>.</p>`
                  },
                  {
                    question: "Will I have to go to court?",
                    answer: `<p>It's very rare that a personal injury claim following an accident at work will actually go to trial, a large majority settle before a trial takes place. However, if your case doesn't settle before trial, attendance at court may be necessary. We however, will be by your side throughout any necessary court attendance. </p>`
                  },
                  {
                    question: "Will my lawyer help me source the right treatment or rehabilitation should I need it?",
                    answer: `<p>Slater and Gordon have many years' experience working on behalf of people who have suffered life changing personal injuries. We see it as an important part of our role to ensure our clients get the best quality support to maximise the chances of recovery and future independence.</p><p>We work closely with case managers, medical experts and approved rehabilitation providers to make sure a tailored programme is put in place and an immediate needs assessment is undertaken as soon as possible so that treatment can begin without delay.</p>`
                  },
                  {
                    question: "What if I need to pay for treatments, adaptations or other things to aid my recovery before the case has settled?",
                    answer: `<p>There's no telling how long a case will take to settle but Slater and Gordon understand that our clients may need to pay for treatments or adaptations to a property to aid recovery and future independence as soon as possible.</p><p>Where we've proved that the other side was partly or fully to blame for your injuries, pain and suffering, we can obtain financial assistance in the form of interim payments. These are payments which are made to you before your case reaches settlement.</p>`
                  },
                  {
                    question: "Will any compensation awarded cover the cost of prosthetics if my accident resulted in limb loss?",
                    answer: `<p>Today, there are constant developments in prosthetics, for example, a prosthetic leg can cost more than £50,000.</p><p>If your <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">claim is successful</a>, compensation can include the cost of prosthetic limbs on a privately funded basis for your lifetime. It can also include the cost of maintenance, repairs, cosmetic covers and spare prosthetics for showering/bathing, swimming and other sports. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Accident at Work FAQs`,
				copy: `Injury in the workplace can be stressful and often life changing and the process of claiming compensation following it can seem daunting. We aim to ensure that you are well prepared and informed before you start your claim.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/255674878.jpg`,
					medium: `/public/images/bitmap/medium/255674878.jpg`,
					small: `/public/images/bitmap/small/255674878.jpg`,
					default: `/public/images/bitmap/medium/255674878.jpg`,
					altText: `Family happily sitting outside, reading book`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/faq1-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee accident at work claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/faq1`,
			sideLinkList: {
				heading: `Questions about accident at work compensation?`,
				links: [
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/faq/`,
						copy: `Military FAQs`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/steps-to-take/`,
						copy: `Steps to take following an accident`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/employers-liability-insurance/`,
						copy: `Employers liability insurance`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/big-six-health-safety-regulations/`,
						copy: `Health and safety regulations`
					},
					{
						url: `/personal-injury-claim/accident-at-work-compensation/military/armed-forces-compensation-scheme/`,
						copy: `Armed forces compensation scheme `
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£1 million Compensation for Labourer Paralysed in Work Accident`,
					link: `/resources/latest-case-studies/2015/03/gbp1-million-compensation-for-labourer-paralysed-in-work-accident/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith secured £1 million compensation for a labourer who suffered a paralysed arm in an accident at work.`
				},
				{
					heading: `£120,000 Compensation Secured for Nurse Injured in Work Accident`,
					link: `/resources/latest-case-studies/2014/04/nurse-who-suffered-injury-at-work-secures-gbp120000-compensation/`,
					copy: `In this case Personal Injury Lawyer Tracey Benson successfully settled a work accident claim for a member of The Royal College of Nursing (RCN).`
				},
				{
					heading: `£18,500 Compensation for Work Injury`,
					link: `/resources/latest-case-studies/2015/06/gbp18500-compensation-for-work-injury/`,
					copy: `Slater and Gordon Personal Injury Solicitor Nigel Smith was instructed to represent an employee who was injured at work.`
				}
			],
		}
	}
}