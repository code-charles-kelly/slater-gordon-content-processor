module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `travel-conventions-treaties`,
		slug: `travel-conventions-treaties`,
		parent: `holiday-accident-claims`,
		name: `Conventions & Regulations for Travel`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Conventions & Regulations for Travel Claims | Slater + Gordon`,
				description: `Do you understand the conventions and regulations governing travel claims? This brief guide explains how they affect UK travellers who have been injured. Slater and Gordon specialises in No Win No Fee compensation claims for injuries in the UK and abroad.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for an injury that happened abroad?",
                    answer: `<p>If you've been injured abroad, you might think that your chances of claiming compensation for your injuries are quite slim. In actual fact, there are a number of conventions under international law that are specifically designed to make life easier for people to claim compensation for injuries and other losses sustained while travelling. This short guide is designed to tell you what they are intended to do. Our experienced legal team is here to tell you how these conventions can help you if you've been injured abroad, and often to act for you in claiming injury compensation.</p>`
                  },
                  {
                    question: "What is the Montreal Convention?",
                    answer: `<p>The Montreal Convention 1999 governs international carriage by air and sets out the liability of the airline in respect of passengers and/or their baggage. The Convention imposes a form of strict liability on the airline in respect of accidents causing injury.</p><p>At first glance the regime seems to be very consumer friendly but matters get a little trickier when attempting to establish that an accident has occurred. This is because the passenger needs to prove the following:</p><ul><li>The accident itself was unusual or unexpected (outside the ordinary course of events)</li><li>The accident was external to the passenger (it was not caused due to their internal reaction i.e. Deep Vein Thrombosis)</li><li>The accident took place upon the aircraft or during the process of embarkation or disembarkation</li></ul><p>A few examples of common accidents for which the Convention usually allows you to claim are; being hit by luggage from the overhead locker, or being struck by the drinks trolley.</p><p>The Convention has a strict time limit for which you can bring a claim which is two years from the date of the accident. It is advisable, however, that if you have been injured upon an aircraft, or during embarkation, or disembarkation, that you seek advice on making a claim without delay.</p><p>Ask us about making a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/air/\">claim for an injury sustained on an aeroplane</a> or at the airport.</p>`
                  },
                  {
                    question: "What is the Athens Convention?",
                    answer: `<p>The Athens Convention 1974 applies to international carriage by sea. The provisions of the Convention can also be applied to cruises which visit international ports but start and end in the UK. The Athens Convention will apply whether you were travelling on a ship, ferry or any other form of seafarer.</p><p>Under the Athens Convention, passengers can <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/cruise-ship/\">claim compensation for injuries and losses that occurred while they were on the ship</a>. The carrier is responsible for any damages and loss suffered as a result of personal injury to passengers, where the incident occurred during the course of carriage or embarkation/disembarkation and was due to the fault or neglect of the carrier or that of the carrier's servants or agents. There is no 'strict liability' rule imposed by the Athens convention, which means that passengers have to prove that there has been negligence on the carrier's part in order to make a successful claim for compensation.</p><p>The Athens Convention will only apply during the course of carriage i.e. it will not apply for periods during which a passenger is, for example, in a marine terminal or on any other port installation.</p><p>The Athens Convention has a time limit for which you can bring a claim. The time limit is normally two years from the date of disembarkation (getting off the ship). It's important to note that the discretion to extend time under the Limitation Act 1980 doesn't apply to the Athens Convention limit and so it's vital that you seek legal advice as soon as possible.</p><p>We have extensive experience in accident at sea claims and would be happy to discuss whether we can assist you with your claim.  </p>`
                  },
                  {
                    question: "What are The Package Travel and Linked Travel Arrangements Regulations 2018?",
                    answer: `<p>This relatively new law upholds the principle that you have a right to expect the holiday package you buy - online, from a brochure or through a travel agent - is as described. This includes ensuring your safety. So if your holiday falls short of what you were led to expect and this results in an injury you have the right to make a claim under these regulations. It's worth noting that these regulations replace The Package Travel, Package Holidays and Package Tours Regulations 1992, though these regulations will still apply to holidays booked before 2018. Our experts can explain your rights however, if you sustain an injury whilst on your holiday as a result of someone else's negligence.</p>`
                  },
                  {
                    question: "What is the Rome II Regulation?",
                    answer: `<p>The Rome II Regulation is a European Union (EU) regulation that creates a harmonised set of rules within the European Union to govern the choice of law that applies in civil and commercial matters. At the time of writing, the UK is still a member of the EU, which means our expert international lawyers often rely on the Rome II Regulation, particularly when making injury compensation claims for UK clients who have been <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">hurt in a Road Traffic Accident</a> in other EU countries. However, this may change if and when the UK exits from the legal jurisdiction of the EU. To find out more about personal injury compensation claims in EU countries, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/europe/\">talk to one of our EU law specialists</a>. </p>`
                  },
                  {
                    question: "What is COTIF?",
                    answer: `<p>The convention concerning International Carriage by Rail (COTIF) applies in Europe, the Maghreb and in the Middle East. While it covers all aspects of harmonisation across international rail frontiers, the most important thing it does - if you've been injured on a train journey - is to establish a uniform legal regime for all contracts concerning the carriage of passengers and goods. This means that if you need to claim compensation for an <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/public-transport/\">injury sustained as a result of rail travel</a> in these regions, our lawyers can act for you under a clearly understood set of legal responsibilities. If you've been injured as a result of a train journey abroad, talk to an expert to find out if you may be able to claim compensation. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `A Guide to travel injury claim conventions and regulations`,
				copy: `If you've been injured while travelling abroad, you may still be able to claim compensation once you're home. This brief guide from compensation specialists Slater and Gordon tells you what you need to know about the conventions and regulations governing holiday and travel claims.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/travel-conventions-treaties-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/travel-conventions-treaties`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}