module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `child-abuse`,
		slug: `child-abuse`,
		parent: `sexual-criminal-abuse`,
		name: `Child Abuse Claims Lawyers`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Child Abuse Claims Lawyers | Slater + Gordon`,
				description: `Those who have suffered child abuse deserve confidential, compassionate legal help and support. Slater and Gordon is a leading legal firm with extensive experience of supporting those who have suffered abuse as they seek justice and compensation.`,
				robots: ``,
                schema: [
                  {
                    question: "Who can I talk to about child abuse?",
                    answer: `<p>As experienced abuse solicitors, we understand the deep hurt that sufferers of child abuse feel, however long ago the events took place. We know that many people don't feel able to talk about things until they're adults, and even then it's far from easy. That's why our promise to you is that when you seek compensation through Slater and Gordon, you will always be treated with absolute discretion and understanding, by solicitors who have helped many, many people in similar situations to you over the years. When you're ready to talk, we're here to listen.</p>`
                  },
                  {
                    question: "What is historic child abuse?",
                    answer: `<p>This is a term that describes abuse that took place in the past when you were a child. Also referred to as 'non-recent abuse', it's commonly used when describing child abuse, as many people who have suffered child abuse are unable to talk about it until they are much older. This abuse can have taken many different forms, though we most commonly help people who have suffered from:</p><p>• Physical abuse</p><p>• Sexual abuse</p><p>• Childhood neglect</p><p>If you would like to talk to someone about your experience, in absolute confidence, please contact us on our dedicated abuse line ${contentShared.meta.phonenumber.number}<strong>,</strong> or you can also use <a href=\"https://www.slatergordon.co.uk/contact-us/\">the contact form</a> and an experienced abuse solicitor will get in touch with you.</p>`
                  },
                  {
                    question: "What are the most common areas of child abuse?",
                    answer: `<p>Sadly, abuse can take place almost anywhere by anyone. These are the most common areas of child abuse:</p><ul><li>Abuse in sports clubs </li><li>Abuse in religious organisations</li><li>Abuse in schools and colleges</li><li>Neglect by Social Services</li><li>Abuse in children's care homes</li><li>Abuse in scouts clubs and other youth groups</li><li>Abuse by Individuals, including celebrities and public figures</li></ul><p>We know this because Slater and Gordon's dedicated Abuse Team specialise in fighting for those who've suffered all forms of abuse. <a href=\"https://www.slatergordon.co.uk/our-experts/richard-scorer/\">Richard Scorer, Head of the Abuse Team at Slater and Gordon</a>, is one of Britain's leading specialists in child abuse litigation, and is supported by a team with a wealth of experience in this area. Richard has written and lectured extensively on the subject of child abuse. His book 'Betrayed: The English Catholic Church and the Sex Abuse Crisis' was published in 2014 and he also co-authored the APIL Guide to Child Abuse Compensation Claims published in 2011.</p>`
                  },
                  {
                    question: "Will my case stay confidential?",
                    answer: `<p>We are committed to protecting your privacy as well as helping you to obtain justice; treating every single case as 100% confidential. Your right to confidentiality is also protected in law, and we can testify from experience that victims of abuse are never named in public or the media, even in the most high profile cases involving extremely well known predators.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Confidential and compassionate legal help for child abuse claims`,
				copy: `We understand that while child abuse is often described as 'historic', it doesn't feel that way to those who have suffered it. Slater and Gordon's specialist solicitors have a genuine understanding of the need for sensitivity and confidentiality when helping the sufferers of child abuse.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/child-abuse-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/child-abuse`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}