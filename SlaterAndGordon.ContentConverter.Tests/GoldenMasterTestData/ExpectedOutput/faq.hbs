<div class="inner-block">
  <h2 class="text text--heading text--large">Do I have to pay legal costs to make a claim for compensation?</h2>
  <p class="text">The simple answer is no. If you have a claim for personal injury arising from a road traffic accident, your case can be funded under a No Win No Fee agreement. A No Win No Fee agreement, also known as a Conditional Fee Agreement, is an agreement signed by you and your lawyer which states that if you lose your case, you will not have to pay any legal costs. </p>
  <p class="text">This means that you can <a href="/personal-injury-claim/no-win-no-fee-claims/" class="link link--inline"><span class="link__text">pursue your claim free</span></a> from any financial risk. </p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How long will the case take?</h2>
  <p class="text">Every case is different so it's difficult to say how long a case is likely to take without first reviewing the circumstances. One of the factors involved in how long a case will take is whether or not the other party accepts responsibility for the accident. This is called "establishing liability" – working out who is liable for the accident.</p>
  <p class="text">Another factor is the severity of the injuries. For example, you may suffer a minor injury which you recover from after several months or a more serious injury that takes several years. However, our road traffic accident experts will ensure that your case is dealt with as quickly and efficiently as possible.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">How much compensation can you claim for a road traffic accident?</h2>
  <p class="text">Compensation is split into <a href="/personal-injury-claim/road-traffic-accidents/" class="link link--inline"><span class="link__text">two parts</span></a>:</p>
  <p class="text"><span class="text--bold">General Damages:</span> General damages is the amount of compensation you'll receive for pain, suffering and loss of amenity. It's based on the severity of the injuries you have suffered and the length of time it's taken to recover, if you have indeed recovered from the injuries sustained. As a result, every case is different. However, lawyers who specialise in road traffic accidents have years of experience in this area and will be able to provide an estimate of the amount of general damages you're likely to achieve.</p>
  <p class="text"><span class="text--bold">Special Damages:</span> Special damages is the amount of compensation you receive to cover any losses / 'out of pocket expenses' you've incurred as a result of the accident, such as:</p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      Loss of property
    </li>
    <li class="bullet-list__item">
      Prescription costs and medication
    </li>
    <li class="bullet-list__item">
      Aids and equipment
    </li>
    <li class="bullet-list__item">
      Private treatment costs / rehabilitation
    </li>
    <li class="bullet-list__item">
      Travelling expenses
    </li>
    <li class="bullet-list__item">
      Loss of earnings
    </li>
    <li class="bullet-list__item">
      Loss of pension
    </li>
    <li class="bullet-list__item">
      Property adaptations 
    </li>
  </ul>
  <p class="text">It also takes into account costs and losses which will continue in the future, such as: </p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      Future loss of earnings
    </li>
    <li class="bullet-list__item">
      Future pension loss
    </li>
    <li class="bullet-list__item">
      Future aids and equipment 
    </li>
    <li class="bullet-list__item">
      Future care costs 
    </li>
  </ul>
  <p class="text">This is to ensure that you don't suffer financial hardship as a result of your accident.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Can I get compensation payments before the case has settled?</h2>
  <p class="text">Once liability has been established (i.e. the other party has accepted full or partial blame for the accident) you can apply for early payments of your compensation. These are called interim payments.</p>
  <p class="text">People who've suffered a serious injury can often, as a result, suffer financial difficulties, for example if they've been unable to work or have had to pay for private treatment or rehabilitation costs. Interim payments can be applied for to cover these costs and form part of your overall claim. So, for example, if your case settles for £10,000 and you have already received two interim payments of £1,000 your final payment of compensation will be £8,000.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Are there time limits on making a claim for compensation?</h2>
  <p class="text">In England, Wales and Scotland, the general rule is that a claim should be brought within three years of the date that the incident causing an injury or illness occurred or the date you were first aware that you had suffered an injury or illness due to negligence. Additional time limits include:</p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
       Where a claim is being made on <a href="/personal-injury-claim/claiming-behalf/" class="link link--inline"><span class="link__text">behalf of someone</span></a> who has died, the claim must be brought within three years of their date of death
    </li>
    <li class="bullet-list__item">
      Where a claim is being made on behalf of a child, the three year time limit does not begin until the child reaches age 18 in England and Wales or the child reaches age 16 in Scotland
    </li>
    <li class="bullet-list__item">
      The time limit when making a claim under <a href="https://www.legislation.gov.uk/ukpga/1998/42/contents" class="link link--inline" target="_blank"><span class="link__text">The Human Rights Act</span></a> is one year
    </li>
    <li class="bullet-list__item">
      There is no time limit on making a claim where the injured person lacks mental capacity
    </li>
  </ul>
  <p class="text">Different time limits may apply if the incident causing injury occurred whilst abroad. Our experts in <a href="/personal-injury-claim/holiday-accident-claims/" class="link link--inline"><span class="link__text">accidents abroad </span></a>will be able to confirm this. </p>
  <p class="text">Although these are the general rules when it comes to time limits, there may be occasions when the court would make a discretionary decision in order to pursue a claim outside the general time limits, so it's always best to speak to a specialist lawyer who can advise you on whether or not you can pursue a claim.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Will I have to go to court?</h2>
  <p class="text">Understandably, many people are concerned about having to attend court as it can be a daunting prospect.  However, the vast majority of claims for compensation reach a successful settlement before a final hearing at court. There are a small number of cases that unfortunately don't reach a successful conclusion prior to the final hearing and in those cases, it is necessary to attend court.</p>
  <p class="text">However, <a href="/personal-injury-claim/" class="link link--inline"><span class="link__text">our expert lawyers</span></a> will be with you every step of the way and will ensure that the process is as stress free as possible.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Will I need a medical examination?</h2>
  <p class="text">In order to make a claim for personal injury, it's important that we have evidence of your injuries and you will therefore have to attend a medical examination with an independent medical expert who specialises in preparing reports to use as evidence in a legal case. These reports are known as medico legal reports.</p>
  <p class="text">Depending on the type and severity of your injuries, you may need to attend more than one medical examination to ensure we have all the medico legal reports we need to support your claim for compensation.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Can I make a personal injury claim if I was partly to blame for the accident?</h2>
  <p class="text">Even if you are partly to blame for your accident, you can still make a claim for compensation. If you're found to be partially to blame for the accident, this will be reflected in your compensation payment. For example, if you are found to be 20% to blame for the accident, and your claim settled for an overall figure of £10,000, you would only receive compensation for 80% of that figure and you would therefore receive £8,000.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">My child has been injured, how do I make a claim on their behalf?</h2>
  <p class="text">Children are unable to make claims on their own behalf and must therefore be made by a trusted adult, usually a parent or grandparent. </p>
  <p class="text">When someone makes a claim on behalf of a child, they are known as a 'Litigation Friend'. A Litigation Friend can't be anyone who has was in any way responsible for the accident. This means, for example, if the mother of the injured child was driving the vehicle, and they were either fully or partially responsible for the accident that caused the injury, they would not be able to be the Litigation Friend and instead would have to be another trusted family member or friend. </p>
</div>
