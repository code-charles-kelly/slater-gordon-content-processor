module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `holiday-claim-guide`,
		slug: `holiday-claim-guide`,
		parent: `holiday-accident-claims`,
		name: `Accidents Abroad Claims Guide`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Accidents Abroad Claims Guide | Slater + Gordon`,
				description: `Accidents abroad can come in all shapes and sizes, just like accidents at home, however international laws and conventions can make it tricky to figure out where to start. Our handy guide on accident claims abroad is here to help you.`,
				robots: ``,
                schema: [
                  {
                    question: "What do I do immediately after an accident?",
                    answer: `<p>Wherever you are in the world, and whatever has happened, having evidence of where and how an injury occurred is crucial if you're to successfully claim compensation. So as soon as any injury has occurred – from a poolside slip to a minor road traffic accident – it's essential to:</p><p>• <strong>Seek medical attention:</strong> especially if you don't know how bad an injury might be, and make sure you get a copy of any medical records and x-rays</p><p>• <strong>Take photographs: </strong>of the location of the accident, making sure that if you have slipped on a wet floor, for example, the picture demonstrates an absence of warning signs, or any other relevant facts</p><p>• <strong>Get photographs:</strong> of any injury or bruising as it develops, as well as any damage to your belongings</p><p>• <strong>Alert staff or officials:</strong> and make sure that they make a record of the incident</p><p>• <strong>Get the contact details of any witnesses:</strong> it doesn't matter if they're locals or fellow Britons</p><p>• <strong>Put a report of what happened in writing:</strong> this is a good aid for your memory, and it will also be useful to email this to the venue where the incident occurred, to help create a trail of information for your claims solicitor</p><p>• <strong>Keep any receipts:</strong> these can be for medical bills, taxis, additional accommodation or any expenses that are a direct result of the accident</p><p>• <strong>Speak to a lawyer as soon as possible:</strong> you can call Slater and Gordon from abroad on  +44 20 7657 1555, or <a href=\"https://www.slatergordon.co.uk/contact-us/\">tell us about your accident online</a> and we will call you.</p>`
                  },
                  {
                    question: "What if I was injured on a flight or a cruise?",
                    answer: `<p>Surprisingly, many compensation claims arise from incidents and injuries occurring on aeroplanes and cruise ships. In these cases, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/\">international legal conventions</a> can be very helpful. Where you are injured on an aeroplane, the Montreal Convention 1999, applies, which allows you to pursue a claim from your home country and makes airlines 'strictly liable' for injuries you sustain in an accident. If you were injured on a cruise ship, the Athens Convention 1974 applies, though this does require you to prove negligence when making a claim for compensation.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `Your guide to claims for accidents abroad`,
				copy: `When you've been injured abroad and it wasn't your fault, read this handy guide then ask us about making a compensation claim from right here in the UK. Slater and Gordon is a specialist legal firm with experience of claims all over the world, particularly in the EU and USA.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/holiday-claim-guide-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/holiday-claim-guide`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}