module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `claiming-behalf`,
		slug: `claiming-behalf`,
		parent: `personal-injury-claim`,
		name: `Claiming Compensation on Behalf of Others`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Claiming Compensation on Behalf of Others | Slater + Gordon`,
				description: `If a friend or family member deserves compensation, you may be able to claim on their behalf. Ask about acting on behalf of someone in a No Win No Fee compensation claim with Slater and Gordon.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim injury compensation for someone else?",
                    answer: `<p>The law recognises that sometimes people who are entitled to claim injury compensation are unable to do so for themselves. That's why it is possible for close family or even friends to act on their behalf in order to help them to claim the injury compensation they deserve. There are a number of circumstances that might lead to this, as we explain below. And, if you want to discuss the possibility of making a claim on someone else's behalf, our expert solicitors will be happy to discuss the situation with you.</p>`
                  },
                  {
                    question: "Can I claim on behalf of a child?",
                    answer: `<p>Some of the most tragic cases we deal with involve children. In these cases, it's right and proper for the parents or guardians to pursue injury compensation on the child's behalf acting as their 'Litigation Friend'. The law also prohibits compensation claims by anyone below the age of 18, so you can often help younger friends and family members to claim rightful compensation. Sometimes <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/claiming-behalf/children-court-approval/\">court approval may be needed in order to claim on behalf of a child</a>.</p><p>However, it's worth knowing that those injured when they are below the age of 18 have the right to claim injury compensation in their own right for three years from the date of their 18th birthday, which means until the day before their 21st birthday. </p>`
                  },
                  {
                    question: "Can I claim on behalf of someone with diminished mental capacity?",
                    answer: `<p>There are all sorts of reasons why someone might not be able to claim injury compensation for themself, and diminished mental capacity, whether it was caused by the injury or not, is one of them. That's why <a href=\"https://www.legislation.gov.uk/ukpga/2005/9/introduction\">the Mental Capacity Act 2005</a> enables you to make a compensation claim on behalf of a friend or family member who lacks mental capacity as a result of:</p><ul><li>A brain injury</li><li>An illness such as Alzheimer's or Dementia </li><li>A mental illness or learning difficulties </li></ul><p>If you want to know more about claiming on someone else's behalf under the Mental Capacity Act 2005, talk to one of our claims experts today. </p>`
                  },
                  {
                    question: "Can I claim on behalf of someone with life-changing injuries?",
                    answer: `<p>When someone suffers such <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/serious/\">serious injuries</a><strong> </strong>that they would be unable to pursue an injury compensation claim on their own behalf, it's perfectly normal for a close friend or family member to act for them on their behalf. Our experienced and understanding legal experts can tell you more.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Personal injury`,
				heading: `Personal injury claims on behalf of others`,
				copy: `Do you have a friend or family member who has suffered a personal injury but can't claim compensation themselves? You may be able to claim on their behalf with Slater and Gordon. We are leading claims lawyers, offering a No Win No Fee service in most personal injury cases.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/claiming-behalf-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee personal injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/claiming-behalf`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}