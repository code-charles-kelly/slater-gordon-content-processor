module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `confidentiality-anonymity`,
		slug: `confidentiality-anonymity`,
		parent: `sexual-criminal-abuse`,
		name: `Confidentiality & Anonymity in Abuse Cases`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Confidentiality & Anonymity in Abuse Cases | Slater + Gordon`,
				description: `Confidentiality and anonymity are always of paramount importance in abuse cases. Slater and Gordon is a leading legal firm with extensive experience of supporting those who have suffered from abuse in complete confidence as they seek justice and compensation.`,
				robots: ``,
                schema: [
                  {
                    question: "How do you guarantee my privacy?",
                    answer: `<p>Where any element of sexual abuse has taken place, every case is automatically covered by the Sexual Offences Act 2003, which means that a case can't be reported by newspapers or discussed anywhere in public, such as on internet forums. In cases where abuse has been as a result of an assault or neglect, we can apply to the court for an Anonymity Order, which gives exactly the same lifetime protection.</p>`
                  },
                  {
                    question: "Can criminal cases be held anonymously?",
                    answer: `<p>Where abuse has taken place, we're able to apply to the Crown Court for cases to be held 'in camera' which is a legal term meaning 'in private'. This means that the court will be sealed, with no press or public allowed in, and only the court officers, lawyers and those who are party to the case will be allowed in. </p>`
                  },
                  {
                    question: "Can you keep all of my data private?",
                    answer: `<p>As one of Britain's leading consumer legal firms, we take client privacy and confidentiality very seriously. All of our email servers are totally secure, and we follow all of the relevant data <a href=\"https://www.slatergordon.co.uk/media-centre/blog/2017/08/gdpr-and-the-new-data-protection-bill-what-you-need-to-know/\">privacy laws under the GDPR</a>. The only time when client data is ever shared with anyone else, is if it is for legal reasons and with your permission, such as when we are asked by the police to cooperate with their enquiries or if we need to consult with a medical expert for evidence to support your case. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `Confidential and anonymous help in abuse cases`,
				copy: `Confidentiality and anonymity are always of paramount importance in abuse cases. Below we discuss the steps we undertake to ensure this is looked after.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/confidentiality-anonymity-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/confidentiality-anonymity`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-criminal-injury/cica-faq/`,
						copy: `CICA FAQs`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}