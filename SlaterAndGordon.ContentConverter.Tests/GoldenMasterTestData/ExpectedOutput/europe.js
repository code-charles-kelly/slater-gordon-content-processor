module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `europe`,
		slug: `europe`,
		parent: `holiday-accident-claims`,
		name: `EU Accident Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `EU Accident Injury Claims | Slater + Gordon`,
				description: `Have you suffered an injury while travelling within the EU? You may be entitled to compensation, and we have the experience to help you claim. Slater and Gordon specialises in No Win No Fee compensation claims for accidents in the EU.`,
				robots: ``,
                schema: [
                  {
                    question: "Can I claim for injuries in Europe from the UK?",
                    answer: `<p>While laws for compensation claims vary across the EU, <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/\">a number of mechanisms exist</a> to ensure that EU citizens can claim compensation for injuries suffered in other EU countries. We have extensive experience of helping UK clients claim compensation in the EU in circumstances such as:</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/road-traffic-accidents/\">Road traffic accidents</a><strong>:</strong> where another driver or a badly maintained road surface was to blame</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/hotel-resort/\">Holiday accidents</a>: such as slips, trips and falls in resorts and hotels</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/accident-at-work-compensation/\">Accidents at work</a><strong>:</strong> EU employers are under the same duty of care as those in the UK</p><p><a href=\"https://www.slatergordon.co.uk/personal-injury-claim/holiday-accident-claims/winter-skiing/\">Skiing and other sports injuries</a><strong>:</strong> due to another participant's recklessness, faulty equipment or piste maintenance</p><p><strong>Assaults and terrorism:</strong> if you have been the victim of an assault or caught up in a terrorist incident</p><p>There are at least as many ways for you to be injured by someone else's negligence in the EU as there are in the UK. So talk to us about starting a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee compensation claim</a> for any illness or injury that you or a family member suffered while travelling in Europe.</p>`
                  },
                  {
                    question: "What if I was a victim of crime in the EU?",
                    answer: `<p>At the time of writing, the UK is still a member of the European Union, and subject to the EU law that requires every member country to maintain a compensation fund for victims of crime. While that remains the case, we may be able to help you claim compensation from the offender during any criminal proceedings, or to claim from the relevant compensation authority for the country in which you were injured as a result of criminal activity.</p>`
                  },
                  {
                    question: "How long have I got to make a claim?",
                    answer: `<p>The law in the UK law permits up to three years in which to start a compensation claim for an injury that arose from negligence or a deliberate act. However, while this is also the case in a number of EU countries, time limits can vary. So if you've been injured in an EU country and wish to make a No Win No Fee compensation claim from the UK, we recommend that you begin your claim as soon as possible. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accidents abroad`,
				heading: `EU Travel injury compensation claim lawyers`,
				copy: `There are thousands of amazing places to visit in Europe. But there are just as many ways you can be injured while you're there. Slater and Gordon is a specialist claims firm with extensive experience of helping clients who have been injured in the EU.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/262520595.jpg`,
					medium: `/public/images/bitmap/medium/262520595.jpg`,
					small: `/public/images/bitmap/small/262520595.jpg`,
					default: `/public/images/bitmap/medium/262520595.jpg`,
					altText: `Mother and son spending time together outside`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/europe-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee travel injury claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/europe`,
			sideLinkList: {
				heading: `Questions about accident abroad compensation?`,
				links: [
					{
						url: `/personal-injury-claim/holiday-accident-claims/holiday-claim-guide/`,
						copy: `Holiday claim guide`
					},
					{
						url: `/personal-injury-claim/holiday-accident-claims/travel-conventions-treaties/`,
						copy: `Travel convention treaties`
					},
					{
						url: `/personal-injury-claim/road-traffic-accidents/a-guide-to-road-safety-abroad/`,
						copy: `A guide to road safety abroad`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Compensation for Fall on Holiday Hotel Staircase`,
					link: `/resources/latest-case-studies/2015/10/compensation-for-fall-on-holiday-hotel-staircase/`,
					copy: `A child's fall on a slippery holiday hotel staircase in Tunisia resulted in a settlement of damages awarded to the child's father.`
				},
				{
					heading: `Holiday Accident Compensation for Woman Electrocuted by Plug Socket`,
					link: `/resources/latest-case-studies/2015/02/woman-electrocuted-by-plug-socket-on-holiday/`,
					copy: `Slater and Gordon Personal Injury Lawyer Simon Weilding secured £2,750 compensation for a holidaymaker injured by a dangerous plug socket in her hotel.`
				},
				{
					heading: `Honeymooner's Holiday Ruined by Fall at Resort`,
					link: `/resources/latest-case-studies/2016/07/honeymooners-holiday-ruined-by-fall-at-resort/`,
					copy: `A honeymoon holiday was ruined when unmarked wet painted steps resulted in a fall that caused extensive injuries requiring medical attention.`
				}
			],
		}
	}
}