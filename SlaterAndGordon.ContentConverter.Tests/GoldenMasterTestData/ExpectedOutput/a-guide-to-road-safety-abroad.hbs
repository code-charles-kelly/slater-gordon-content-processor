<div class="inner-block">
  <h2 class="text text--heading text--large">Driving licence requirements</h2>
  <p class="text">The first thing to remember is your driving licence; have this on you at all times and make sure it is not expired and won't expire whilst you are travelling. A full UK licence will allow you to drive in the EU or the European Economic Area countries as well as Switzerland. Currently you do not need an <a href="https://www.gov.uk/driving-abroad" class="link link--inline" target="_blank"><span class="link__text">international driving permit</span></a> to drive in EU countries, however this may change with Brexit.</p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Staying safe and legal when driving in the EU</h2>
  <p class="text">You probably already know that they drive on the left in most EU countries - with the notable exception of the Republic of Ireland, Malta and Cyprus - but that isn't the only big difference you need to know about before you go.</p>
  <p class="text">Driving laws can vary quite significantly between the different member states of the EU, and even the laws that may surprise you are enforced rigorously by each country's respective police forces.</p>
  <p class="text">So while the article below isn't completely exhaustive - as EU driving laws are complex and changing all the time - we've drawn up this short list of guidelines to help you prepare in advance for your trip. </p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">What do I need to buy before driving in the EU?</h2>
  <p class="text">If you're hiring a car at the airport when you arrive, it will normally come complete with most of the legally required equipment you need: though it's worth confirming this with the car hire firm before you fly.</p>
  <p class="text">However, if you're taking your own car, these are some of the things you'll need to add to your boot before you go:</p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      <span class="text--bold">Reflective warning triangles:</span> most EU countries require that you carry one (or in some cases, two) of these, to be placed a little way up the road from you in the event of a breakdown, in addition to having switched on your hazard warning lights
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">High vis/reflective vests: </span>which should be worn by all members of your party whenever you are out of your car after a breakdown or accident
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Personal breathalyser: </span>this is only legally required in France, but you must have one, and it must carry the French 'NF' mark and be within its 'use-by' date
    </li>
  </ul>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Which EU driving laws do I need to be aware of?</h2>
  <p class="text">Many EU driving laws are similar to our own, but bear in mind that they are very rigorously enforced in some countries, with substantial penalties, including hefty roadside fines.</p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      <span class="text--bold">Drink driving: </span>the legal alcohol limit for drivers in the EU is generally lower than here in the UK, meaning that the best policy is to avoid alcohol completely if you are driving
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Headlights: </span>many countries in northern Europe require you to have your dipped headlights on all the time, and you will be fined if you forget to do so
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Mobile phones: </span>as in the UK, it is illegal to use your mobile phone while driving, and you risk a hefty roadside fine if caught doing so
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Children: </span>it is illegal for children under 10 to travel in the front of cars in some EU countries, so do check the individual country's laws before you go if your under-10s are used to travelling in the front
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Seat belts: </span> these are compulsory across the EU for front- and back-seat passengers, and on-the-spot fines will be issued for non-compliance
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Emissions: </span>like London, some large cities in the EU ban high emission vehicles from central areas at certain times, so do look out for warning symbols, and do check online before you go if you know you will be driving into a particular city
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Speed limits in varying weather: </span>in many EU countries, the speed limits are automatically lower when it is raining. For example, in France, the speed limits on motorways fall from 130 km/h to 110 km/h in wet weather. Look out for the signs that tell you the maximum speeds for different weather conditions
    </li>
  </ul>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">What to do in the event of having a road traffic accident in the EU</h2>
  <p class="text">EU laws regarding <a href="/personal-injury-claim/road-traffic-accidents/what-to-do-in-accident/" class="link link--inline"><span class="link__text">what to do in the event of an accident</span></a> while driving are pretty much the same as they are here, but here's a brief refresher just to make sure you stay safe and legal when driving abroad:</p>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      <span class="text--bold">Get everyone to safety first: </span>if people are uninjured and can be safely moved, it's best if they get out of vehicles, on the side away from other traffic, and ideally onto a pavement or behind a barrier
    </li>
  </ul>
  <ul class="bullet-list">
    <li class="bullet-list__item">
      <span class="text--bold">Don't admit liability: </span>even if you think the accident was your fault, you must not apologise or admit liability, as this can adversely affect your legal and insurance position
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Take photographs: </span>these should show the position of all vehicles, including your own
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Get witness details: </span>if anyone saw the accident, try to get a name and a phone number from them before they leave the scene
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Exchange driver details: </span>making sure they include the name, registration number, mobile number, address and insurer of anyone else involved in the accident
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Write down what happened: </span>it's good to record details of what happened while everything is fresh in your memory
    </li>
    <li class="bullet-list__item">
      <span class="text--bold">Report the incident to the car hire company or your insurers:</span> complete with all of the details gathered at the scene
    </li>
  </ul>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Check with the UK Government before you go</h2>
  <p class="text">This article is intended as a simple guide to driving safely in the EU, not legal advice. For that, we always recommend that you check the Government's advice for driving in each individual EU country before you go. You can reach this advice on <a href="https://www.gov.uk/driving-abroad" class="link link--inline" target="_blank"><span class="link__text">the gov website</span></a>. </p>
</div>

<div class="inner-block">
  <h2 class="text text--heading text--large">Ask for legal help when you get home, if you've had an accident</h2>
  <p class="text">Driving in the EU is usually easy and enjoyable, and we hope that your trip is a pleasure. However, in the event that you have had an accident while driving in the EU, you don't have to rely on the car's insurers for advice or compensation, you can call us when you get home for a free consultation on freephone {{{meta.phonenumber.contentLink}}} or <a href="/contact-us/" class="link link--inline"><span class="link__text">contact us online</span></a> and we will call you at your convenience.</p>
</div>
