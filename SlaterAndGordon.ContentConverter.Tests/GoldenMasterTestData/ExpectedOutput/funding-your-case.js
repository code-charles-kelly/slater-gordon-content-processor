module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `funding-your-case`,
		slug: `funding-your-case`,
		parent: `personal-injury-claim`,
		name: `Funding your Personal Injury`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Funding your Personal Injury Claims | Slater + Gordon`,
				description: `Most of the personal injury compensation claims that Slater and Gordon pursue for our clients are on a No Win No fee basis. However, if that isn't a suitable option for you, here we explain some of the other ways of funding your personal injury claim.`,
				robots: ``,
                schema: [
                  {
                    question: "Could I get Legal Aid for my injury claim?",
                    answer: `<p>Unfortunately for the vast majority of cases, Legal Aid is not available for personal injury. There is a small exception. When claiming a personal injury claim for abuse which has been suffered, a small amount of cases may qualify for Legal Aid. Once your lawyer has discussed your case with you, they will advise you on whether or not Legal Aid is available to you.</p><p>If your claim is a medical negligence case rather than a personal injury case, Legal Aid is limited to children who have a neurological injury that has resulted in severe disability, which has been sustained either through pregnancy, during the birth or the first eight weeks of the child's life. For more information about funding a <a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">medical negligence claim, </a><a href=\"https://www.slatergordon.co.uk/clinical-and-medical-negligence-solicitors/\">click here</a>.</p>`
                  },
                  {
                    question: "Would my own insurance pay for an injury claim?",
                    answer: `<p>Many people are surprised to find that policies like home contents, motor and travel insurance policies sometimes include legal expenses cover. It's often an optional extra, but it's always worth checking all of your insurance policies to see if they do offer legal expenses cover. In some cases, it is also included with your bank account or credit card. If so, it's worth checking with your bank, card company or insurers to see if they will pay for a personal injury compensation claim. </p>`
                  },
                  {
                    question: "Can I get Trade Union funding for my injury claim?",
                    answer: `<p>If you're a member of a Trade Union you may be entitled to advice and representation as part on your membership subscription. In the first instance we would advise contacting your Trade Union representative.</p>`
                  },
                  {
                    question: "Can I fund my own personal injury claim?",
                    answer: `<p>Although you're entitled to fund your own personal injury claim, we would usually advise against this. All cases carry an element of risk and there's no 100% guarantee that even if you win your case, you'll be awarded all of your legal costs as well as the full amount of compensation you were seeking. Slater and Gordon would therefore advise that if you're looking to pursue a claim, you do this under a No Win No Fee agreement so there's no financial risk involved in pursuing your claim. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Accident at work`,
				heading: `Funding your personal injury claim`,
				copy: `There are many ways to fund your personal injury claim in order to get the compensation you deserve.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/204325345.jpg`,
					medium: `/public/images/bitmap/medium/204325345.jpg`,
					small: `/public/images/bitmap/small/204325345.jpg`,
					default: `/public/images/bitmap/medium/204325345.jpg`,
					altText: `Young man and little girl outdoors on a sunny day`
				}
			},
			standardContent: `product-template/funding-your-case`,
			sideLinkList: {
				heading: `Questions about personal injury compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faq/`,
						copy: `Personal injury claims FAQ`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can I claim for?`
					},
					{
						url: `/personal-injury-claim/how-to-make-a-claim/`,
						copy: `How to make a personal injury claim`
					},
					{
						url: `/personal-injury-claim/claims-process/`,
						copy: `Personal injury claims process explained`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `How we can help if personal injury puts you in debt`
					},
					{
						url: `/personal-injury-claim/claiming-behalf/`,
						copy: `Claiming on behalf of another`
					},
					{
						url: `/personal-injury-claim/medical-assessment/`,
						copy: `Do I need a medical assessment?`
					},
					{
						url: `/personal-injury-claim/funding-your-case/`,
						copy: `Funding your personal injury claim`
					},
					{
						url: `/personal-injury-claim/is-compensation-taxable/`,
						copy: `Is compensation taxable?`
					},
					{
						url: `/personal-injury-claim/signed-a-waiver/`,
						copy: `Can I claim if I signed a waiver?`
					},
					{
						url: `/personal-injury-claim/support/`,
						copy: `Personal injury support services`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `£80,000 for Post-Traumatic Arthritis after Work Accident`,
					link: `/resources/latest-case-studies/2015/07/gbp80000-for-post-traumatic-arthritis-after-work-accident/`,
					copy: `A man injured after falling down a set of stairs at work received £80,000 in damages after being told he had developed post-traumatic arthritis.`
				},
				{
					heading: `£23,000 for Whiplash and Other Soft Tissue Injuries`,
					link: `/resources/latest-case-studies/2015/07/gbp23000-for-whiplash-and-other-soft-tissue-injuries/`,
					copy: `A woman received over £20,000 compensation after she sustained whiplash and other soft tissue injuries following a road traffic accident.`
				},
				{
					heading: `£10,000 for Teaching Assistant after Classroom Assault`,
					link: `/resources/latest-case-studies/2015/06/gbp10000-for-teaching-assistant-after-classroom-assault/`,
					copy: `Slater and Gordon Solicitor was instructed by a Teaching Assistant who was assaulted while carrying out her duties.`
				}
			],
		}
	}
}