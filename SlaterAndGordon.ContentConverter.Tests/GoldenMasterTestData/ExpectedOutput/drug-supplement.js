module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `drug-supplement`,
		slug: `drug-supplement`,
		parent: `faulty-products`,
		name: `Defective Drug & Supplement`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Defective Drug & Supplement Compensation Claims | Slater + Gordon`,
				description: `When drugs, medicines and supplements are defective, they can cause serious illness, injury, birth defects and even death. Get in touch with Slater and Gordon today about starting a No Win No Fee defective drug compensation claim.`,
				robots: ``,
                schema: [
                  {
                    question: "How can drugs and medicines be defective?",
                    answer: `<p>While all new drugs and medicines are tested extensively, the fact is that some defects don't surface until it is too late. <a href=\"https://www.bbc.co.uk/news/uk-27708295\">Thalidomide is one tragic example of this</a>: the drug was approved for use in the treatment of morning sickness, but the trials were deeply flawed and the drug was later found to cause severe birth defects. Slater and Gordon has acted for many victims of Thalidomide, and still continues to fight for the victims of other defective drugs, medicines and medical treatments. If you've been harmed by a drug, medicine or supplement, we may be able to help you claim compensation.</p>`
                  },
                  {
                    question: "Which drugs and medicines have been found unsafe?",
                    answer: `<p>Drugs, medicines, medical treatments and even health supplements are all subject to clinical trials before they are approved for sale to the public. However, problems can and do arise, in everything from so-called 'fat-burning' diet supplements to the Human Growth Hormone that was found to have been contaminated with <a href=\"https://www.nhs.uk/conditions/creutzfeldt-jakob-disease-cjd/\">Creutzfeldt-Jakob Disease (CJD)</a>. </p><p>So if you have been made unwell or suffered side-effects from any drug, medicine or supplement, talk to our specialist defective drug solicitors today. Whether your condition has been caused by a defective drug, or through contamination during the manufacturing process, we may be able to claim compensation on your behalf. Importantly, most cases like this are conducted on a <a href=\"https://www.slatergordon.co.uk/personal-injury-claim/no-win-no-fee-claims/\">No Win No Fee basis</a>.</p>`
                  }
                ]
			},
			heroHeader: {
				section: `Faulty products`,
				heading: `Defective drug, medicine and supplement compensation claims`,
				copy: `Have you or a loved one been harmed by a defective drug, medicine or health supplement? Slater and Gordon is one of the UK's leading consumer law firms, offering a No Win No Fee service to the majority of our clients suffering as a result of defective drugs and supplements.`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/140393239.jpg`,
					medium: `/public/images/bitmap/medium/140393239.jpg`,
					small: `/public/images/bitmap/small/140393239.jpg`,
					default: `/public/images/bitmap/medium/140393239.jpg`,
					altText: `group of young people having a beverage`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/drug-supplement-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee defective product claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/drug-supplement`,
			sideLinkList: {
				heading: `Questions about faulty product compensation?`,
				links: [
					{
						url: `/personal-injury-claim/faulty-products/claim-guide/`,
						copy: `Faulty product claim guide`
					},
					{
						url: `/personal-injury-claim/what-can-i-claim-for/`,
						copy: `What can i claim for?`
					},
					{
						url: `/personal-injury-claim/personal-injury-debt/`,
						copy: `Personal injury and debt support`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `Five-Figure Settlement for Eye Injury Caused by Faulty Packaging`,
					link: `/resources/latest-case-studies/2017/01/five-figure-settlement-for-eye-injury-caused-by-faulty-packaging/`,
					copy: `A five-figure settlement was awarded to a man in compensation for an eye injury caused by a faulty handle.`
				},
				{
					heading: `Jogger Awarded 5 Figure Settlement for Defective Kerb Fall`,
					link: `/resources/latest-case-studies/2016/01/jogger-awarded-5-figure-settlement-for-defective-kerb-fall/`,
					copy: `A jogger has been awarded a five-figure compensation settlement for extensive injuries to their leg sustained following a fall on defective kerb.`
				},
				{
					heading: `Settlement Awarded for Trip on Defective Manhole`,
					link: `/resources/latest-case-studies/2015/09/settlement-awarded-for-trip-on-defective-manhole/`,
					copy: `Slater and Gordon Lawyers secure settlement for client who tripped on a defective manhole`
				}
			],
		}
	}
}