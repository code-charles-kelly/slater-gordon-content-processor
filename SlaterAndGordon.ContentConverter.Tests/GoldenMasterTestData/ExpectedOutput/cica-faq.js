module.exports = function (contentShared, defaultContent, pageTypes) {
	return {
		id: `cica-faq`,
		slug: `cica-faq`,
		parent: `abuse-criminal-injury`,
		name: `Criminal Injuries Compensation Authority (CICA) FAQs`,
		pageType: pageTypes.productTemplate,
		data: {
			...defaultContent,
			meta: {
				...contentShared.meta,
				title: `Criminal Injuries Compensation Authority (CICA) FAQs | Slater + Gordon`,
				description: `Slater and Gordon have pulled together the most frequently asked questions about criminal injury compensation claims and the CICA process.`,
				robots: ``,
                schema: [
                  {
                    question: "What is the CICA scheme?",
                    answer: `<p>The CICA is a Government funded scheme designed to compensate blameless victims of violent crime in Great Britain. It doesn't cost anything to submit an application but the incident must have happened in England, Wales or Scotland and have been reported to the police.</p>`
                  },
                  {
                    question: "What is the eligibility criteria for CICA claims?",
                    answer: `<p>The CICA have set criteria which enables you to claim under their scheme, which includes:</p><ul><li>You must have suffered the injury as a result of a crime of violence</li><li>The incident must have happened in England, Wales or Scotland </li><li>The incident must have been reported to the police as soon as practically possible</li><li>You must fully cooperate with the police and the CICA</li><li>Your injuries just be serious enough to meet the minimum requirements of the CICA scheme</li></ul>`
                  },
                  {
                    question: "Who can apply for CICA compensation?",
                    answer: `<p>The people who can apply for compensation under the scheme include:</p><ul><li>A person who has suffered an injury </li><li>A close relative of someone who passed away as a result of their injuries</li><li>A witness of a crime or someone who arrived at the scene of the crime immediately afterwards and has suffered a psychological injury</li><li>A person who has paid funeral expenses of someone who passed away as a result of their injuries </li></ul>`
                  },
                  {
                    question: "Are there time limits to applying for CICA compensation?",
                    answer: `<p>If you were an adult at the time of the incident, you should apply within <a href=\"https://www.gov.uk/claim-compensation-criminal-injury/eligibility\">two years of the date of the incident</a>. However, the time limit can be extended if there were exceptional circumstances which prevented you from applying earlier and the evidence to support the application means it can be determined without further extensive enquiries. </p><p>If you were a child under 18 years old at the time of the incident, the two year time limit doesn't apply. If the incident was reported to the police before the child turned 18, then the two year limit would run from the date the child turned 18, i.e. they would have to make the claim before their 20th birthday. </p><p>The CICA will consider extending time limits where you can prove that you were unable to make the claim earlier, for example, if you had a psychological illness as a result of the incident. So it's always best to check with the CICA first to see if it's still possible to bring a claim, even if you're outside of the applied time limit. </p>`
                  },
                  {
                    question: "What information do you need to provide when applying for CICA compensation?",
                    answer: `<p>You will need the : </p><ul><li>Date and location of the crime</li><li>Name of the police station where the crime was reported</li><li>Crime reference number</li><li>GP's name and address</li><li>Dentist's name and address (if you had dental treatment because of your injuries)</li><li>Details of any previous applications you've made to CICA</li><li>Details of any unspent criminal convictions</li><li>Proof of identity for you or anyone you're responsible for, such as a birth or marriage certificate, power of attorney or deed poll</li></ul>`
                  },
                  {
                    question: "What happens once you've completed and submitted your CICA application form?",
                    answer: `<p>Once you've submitted your completed <a href=\"https://www.gov.uk/claim-compensation-criminal-injury\">CICA application</a> form, the CICA will usually request the police file and your consent to access your medical records to assess the injuries you sustained. You may also be asked to attend a medical examination to determine the severity of your injuries and any lasting effects they'll have on your life along with further treatment you may need.</p>`
                  },
                  {
                    question: "What happens once the CICA have all the information they need to process the claim?",
                    answer: `<p>When all the information about your case and injuries has been obtained by the CICA, they'll review it and come to a conclusion on whether your case will be compensated and for how much. This process can take any length of time from a few months to a couple of years depending on the complexity and circumstances of your case. </p>`
                  }
                ]
			},
			heroHeader: {
				section: `Sexual and criminal abuse`,
				heading: `CICA FAQ`,
				copy: `If you have suffered physically or psychologically as a result of a violent or sexual attack or sexual or physical abuse, you may be able to claim compensation from the Criminal Injuries Compensation Authority (CICA).`,
				image: {
					blob: true,
					large: `/public/images/bitmap/large/72149338.jpg`,
					medium: `/public/images/bitmap/medium/72149338.jpg`,
					small: `/public/images/bitmap/small/72149338.jpg`,
					default: `/public/images/bitmap/medium/72149338.jpg`,
					altText: `Woman drinking tea and reading book in cosy chair`
				}
			},
			twoPanelCta: {
				leftHeading: `No Win No Fee Compensation`,
				leftWysiwyg: `product-template/cica-faq-two-panel-cta`,
				rightHeading: `Find out more about No Win No Fee abuse claims`,
				rightCtaCopy: `Contact us`,
				rightCtaLink: `/contact-us/`,
			},
			standardContent: `product-template/cica-faq`,
			sideLinkList: {
				heading: `Questions about abuse compensation claims?`,
				links: [
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/faq/`,
						copy: `Sexual and criminal abuse FAQs`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/abuse-guide/`,
						copy: `A guide to abuse claims`
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/help-support/`,
						copy: `Help and support `
					},
					{
						url: `/personal-injury-claim/sexual-criminal-abuse/confidentiality-anonymity/`,
						copy: `Confidential support claims`
					}
				]
			},
			quotes: [
				{					copy: `Really helpful and always on the other side of the phone if needed. Quick and easy service.`,
					author: `Ashley`
				},
				{
					copy: `Everything is moving along quickly and smoothly. Good professional service I'm glad I chose slater and gordon`,
					author: `Kenny Jackson`
				},
				{
					copy: `Excellent communication, in constant contact with the latest info regarding your case.`,
					author: `David`
				}
			],
			caseStudies: [
				{
					heading: `FamilyLawyer Helps Client's Daughter Build Relationship with Absent Father`,
					link: `/resources/latest-case-studies/2015/05/lawyer-helps-clients-daughter-get-contact-with-dad/`,
					copy: `Slater and Gordon Family Solicitor Claire Reid acted for a mother very concerned about the relationship between her former partner and their daughter.`
				}
			],
		}
	}
}