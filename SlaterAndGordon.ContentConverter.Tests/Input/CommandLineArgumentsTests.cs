﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentAssertions;
using NUnit.Framework;
using SlaterAndGordon.ContentConverter.Input;

namespace SlaterAndGordon.ContentConverter.Tests.Input
{
    [TestFixture]
    class CommandLineArgumentsTests
    {
        [Test]
        public void WhenEmptyArrayOfArgumentsPassed_ArgumentsSetFlagNotSet()
        {
            var args = new string[] { };
            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.ArgumentsSet.Should().BeFalse();
        }

        [Test]
        public void WhenEmptyArrayOfArgumentsPassed_NumberOfInputFoldersIsZero()
        {
            var args = new string[] { };
            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.NumberOfInputFolders.Should().Be(0);
        }

        [Test]
        public void WhenEmptyArrayOfArgumentsPassed_InputFoldersReturnNoPaths()
        {
            var args = new string[] { };
            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.InputFolders.Count().Should().Be(0);
        }

        [Test]
        public void WhenEmptyArrayOfArgumentsPassed_OutputFolderIsEmptyString()
        {
            var args = new string[] { };
            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.OutputFolder.Should().BeEmpty();
        }

        [Test]
        public void WhenNullArrayOfArgumentsPassed_ArgumentsSetFlagNotSet()
        {
            var commandLineArguments =
                new CommandLineArguments(null);

            commandLineArguments.ArgumentsSet.Should().BeFalse();
        }

        [Test]
        public void WhenNullArrayOfArgumentsPassed_NumberOfInputFoldersIsZero()
        {
            var commandLineArguments =
                new CommandLineArguments(null);

            commandLineArguments.NumberOfInputFolders.Should().Be(0);
        }

        [Test]
        public void WhenNullArrayOfArgumentsPassed_InputFoldersReturnNoPaths()
        {
            var commandLineArguments =
                new CommandLineArguments(null);

            commandLineArguments.InputFolders.Count().Should().Be(0);
        }

        [Test]
        public void WhenNullArrayOfArgumentsPassed_OutputFolderIsEmptyString()
        {
            var commandLineArguments =
                new CommandLineArguments(null);

            commandLineArguments.OutputFolder.Should().BeEmpty();
        }

        [Test]
        public void WhenArrayOfOneArgumentPassed_ArgumentsSetFlagNotSet()
        {
            var args = new string[] {"c:\\"};

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.ArgumentsSet.Should().BeFalse();
        }

        [Test]
        public void WhenArrayOfOneArgumentPassed_NumberOfInputFoldersIsZero()
        {
            var args = new string[] {"c:\\"};

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.NumberOfInputFolders.Should().Be(0);
        }

        [Test]
        public void WhenArrayOfOneArgumentPassed_InputFoldersReturnNoPaths()
        {
            var args = new string[] { "c:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.InputFolders.Count().Should().Be(0);
        }

        [Test]
        public void WhenArrayOfOneArgumentPassed_OutputFolderIsEmptyString()
        {
            var args = new string[] { "c:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.OutputFolder.Should().BeEmpty();
        }

        [Test]
        public void WhenArrayOfTwoArgumentsPassed_ArgumentsSetFlagIsSet()
        {
            var args = new string[] {"c:\\", "d:\\"};

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.ArgumentsSet.Should().BeTrue();
        }

        [Test]
        public void WhenArrayOfTwoArgumentsPassed_NumberOfInputFoldersIsOne()
        {
            var args = new string[] {"c:\\", "d:\\"};

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.NumberOfInputFolders.Should().Be(1);
        }

        [Test]
        public void WhenArrayOfTwoArgumentsPassed_InputFoldersReturnOnePath()
        {
            var args = new string[] {"c:\\", "d:\\"};

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.InputFolders.Count().Should().Be(1);
        }

        [Test]
        public void WhenArrayOfTwoArgumentsPassed_InputFolderShouldBeLastArgument()
        {
            var args = new string[] { "c:\\", "d:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.OutputFolder.Should().Be("d:\\");
        }

        [Test]
        public void WhenArrayOfTwoArgumentsPassed_FirstOutputFolderShouldBeFirstArgument()
        {
            var args = new string[] { "c:\\", "d:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.InputFolders.First().Should().Be("c:\\");
        }

        //

        [Test]
        public void WhenArrayOfThreeArgumentsPassed_ArgumentsSetFlagIsSet()
        {
            var args = new string[] { "c:\\", "d:\\", "e:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.ArgumentsSet.Should().BeTrue();
        }

        [Test]
        public void WhenArrayOfThreeArgumentsPassed_NumberOfInputFoldersIsTwo()
        {
            var args = new string[] { "c:\\", "d:\\", "e:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.NumberOfInputFolders.Should().Be(2);
        }

        [Test]
        public void WhenArrayOfThreeArgumentsPassed_InputFolderShouldBeLastArgument()
        {
            var args = new string[] { "c:\\", "d:\\", "e:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.OutputFolder.Should().Be("e:\\");
        }

        [Test]
        public void WhenArrayOfThreeArgumentsPassed_InputFoldersShouldBeAllButLastInputArguments()
        {
            var args = new string[] { "c:\\", "d:\\", "e:\\" };

            var commandLineArguments =
                new CommandLineArguments(args);

            commandLineArguments.InputFolders.Should().Equal("c:\\", "d:\\");
        }
    }
}
