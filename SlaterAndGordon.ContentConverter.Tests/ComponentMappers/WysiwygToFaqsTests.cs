﻿using System.Collections.Generic;
using NUnit.Framework;
using SlaterAndGordon.ContentConverter.ComponentMappers;
using SlaterAndGordon.ContentConverter.Components;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.Tests.ComponentMappers
{
    public class WysiwygToFaqsTests
    {
        [Test]
        public void WysiwygIsMappedToFaqs_WysiwygContainsFaqQuestions_FaqQuestionsMappedToFaqsComponent()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "dummy"},
                    new TextElement {Text = "no you"}
                }
            });
            wysiwyg.TopElements.Add(new H2Element {Text = "Why are clowns racist?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new H2Element
                {Text = "Why does burning ankle feeling mean?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "dummy"},
                    new AElement {Text = "no you", Href = "/candy"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Question, Is.EqualTo("Why are there so many sheep?"));
            Assert.That(faqsComponent.Faqs[1].Question, Is.EqualTo("Why are clowns racist?"));
            Assert.That(faqsComponent.Faqs[2].Question, Is.EqualTo("Why does burning ankle feeling mean?"));
        }

        [Test]
        public void
            WysiwygIsMappedToFaqs_WysiwygContainsParagraphsOfOneTextElementFollowingQuestion_ParagraphsMappedToAnswer()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there are so many cows"}
                }
            });
            wysiwyg.TopElements.Add(new H2Element {Text = "Why are clowns racist?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because they hate themselves"}
                }
            });
            wysiwyg.TopElements.Add(new H2Element
                {Text = "Why does burning ankle feeling mean?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "no one knows"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Question, Is.EqualTo("Why are there so many sheep?"));
            Assert.That(faqsComponent.Faqs[0].Answer, Is.EqualTo("<p>because there are so many cows</p>"));
            Assert.That(faqsComponent.Faqs[1].Question, Is.EqualTo("Why are clowns racist?"));
            Assert.That(faqsComponent.Faqs[1].Answer, Is.EqualTo("<p>because they hate themselves</p>"));
            Assert.That(faqsComponent.Faqs[2].Question, Is.EqualTo("Why does burning ankle feeling mean?"));
            Assert.That(faqsComponent.Faqs[2].Answer, Is.EqualTo("<p>no one knows</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_WysiwygContainsParagraphsNotPartOfAnFaq_ParagraphsNotMappedToAnswers()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there are so many cows"}
                }
            });
            wysiwyg.TopElements.Add(new H2Element {Text = "Why are clowns racist?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new H2Element {Text = "Leave me alone!", IsFaqQuestion = false});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because they hate themselves"}
                }
            });
            wysiwyg.TopElements.Add(new H2Element
                {Text = "Why does burning ankle feeling mean?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new H2Element {Text = "Leave me alone!", IsFaqQuestion = false});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "no one knows"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[1].Answer, Is.Not.EqualTo("<p>because they hate themselves</p>"));
            Assert.That(faqsComponent.Faqs[2].Answer, Is.Not.EqualTo("<p>no one knows</p>"));
        }

        [Test]
        public void
            WysiwygIsMappedToFaqs_WysiwygContainsParagraphsWithMultipleTextElements_TextElementsInParapgrahAreApended()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there "},
                    new TextElement {Text = "are so "},
                    new TextElement {Text = "many cows"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer, Is.EqualTo("<p>because there are so many cows</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_BoldTextIncluded_BoldTextMappedToBeSurroundedByStrongTags()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there "},
                    new TextElement {Text = "are so ", IsBold = true},
                    new TextElement {Text = "many cows"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer,
                Is.EqualTo("<p>because there <strong>are so </strong>many cows</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_LinksIncluded_LinksMappedToBeSurroundedByCorrectHtmlTags()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there "},
                    new TextElement {Text = "are so "},
                    new TextElement {Text = "many cows. "},
                    new AElement
                    {
                        Href =
                            "https://www.google.com/search?q=why+are+there+so+many+sheep&oq=why+are+there+so+many+sheep",
                        Text = "Click here"
                    },
                    new TextElement {Text = " to find out more!"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer,
                Is.EqualTo(
                    "<p>because there are so many cows. <a href=\"https://www.google.com/search?q=why+are+there+so+many+sheep&oq=why+are+there+so+many+sheep\">Click here</a> to find out more!</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_RelativeLinksIncluded_LinksMappedToBeSurroundedByCorrectHtmlTagsConvertedToAbsolute()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element { Text = "What is the address for Slater & Gordon personal injury?", IsFaqQuestion = true });
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new AElement
                    {
                        Href =
                            "/personal-injury-claims/",
                        Text = "Click here",
                        IsRelative = true
                    },
                    new TextElement {Text = " to go to Slater & Gordon personal injury pages!"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer,
                Is.EqualTo(
                    "<p><a href=\"https://www.slatergordon.co.uk/personal-injury-claims/\">Click here</a> to go to Slater & Gordon personal injury pages!</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_AnswerSectionContainsMultipleParagraphs_ParagraphsAllCorrectlyMappedTogether()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "because there "},
                    new TextElement {Text = "are so "},
                    new TextElement {Text = "many cows. "},
                    new AElement
                    {
                        Href =
                            "https://www.google.com/search?q=why+are+there+so+many+sheep&oq=why+are+there+so+many+sheep",
                        Text = "Click here"
                    },
                    new TextElement {Text = " to find out more!"}
                }
            });
            wysiwyg.TopElements.Add(new PElement
            {
                Children = new List<IHtmlElement>
                {
                    new TextElement {Text = "and one more thing!"}
                }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer,
                Is.EqualTo(
                    "<p>because there are so many cows. <a href=\"https://www.google.com/search?q=why+are+there+so+many+sheep&oq=why+are+there+so+many+sheep\">Click here</a> to find out more!</p><p>and one more thing!</p>"));
        }

        [Test]
        public void WysiwygIsMappedToFaqs_AnswerSectionContainsAList_ListMapperWithCorrectHtmlMarkup()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H2Element {Text = "Why are there so many sheep?", IsFaqQuestion = true});
            wysiwyg.TopElements.Add(new ListHtmlElement
                    {
                        ListElements = new List<List<IHtmlElement>>{new List<IHtmlElement>
                        {
                            new PElement
                            {
                                Children = new List<IHtmlElement>
                                {
                                    new TextElement
                                    {
                                        Text = "this is the first list element"
                                    }
                                }
                            }},
                            new List<IHtmlElement>{
                            new PElement
                            {
                                Children = new List<IHtmlElement>
                                {
                                    new TextElement
                                    {
                                        Text = "this is the second list element"
                                    }
                                }
                            }

                        }

                        }
            });

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();
            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(wysiwyg);

            Assert.That(faqsComponent.Faqs[0].Answer,
                Is.EqualTo(
                    "<ul><li>this is the first list element</li><li>this is the second list element</li></ul>"));
        }
    }
}