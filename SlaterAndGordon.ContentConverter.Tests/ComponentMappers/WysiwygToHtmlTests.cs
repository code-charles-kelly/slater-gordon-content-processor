﻿using System;
using System.Collections.Generic;
using System.Text;
using ExCSS;
using FluentAssertions;
using NUnit.Framework;
using SlaterAndGordon.ContentConverter.ComponentMappers;
using SlaterAndGordon.ContentConverter.SectionMappers;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.Tests.ComponentMappers
{
    [TestFixture]
    public class WysiwygToHtmlTests
    {
        [Test]
        public void WysiwygContainsH3_MapH3ToHtml()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new H3Element{Text = "this is the title"});

            WysiwygToHtmlMapper wysiwygToHtmlMapper = new WysiwygToHtmlMapper();

            string html = wysiwygToHtmlMapper.MapToHtml(wysiwyg);

            var expected = "<div class=\"inner-block\">\r\n" +
                           "  <h3 class=\"text text--heading text--medium\">this is the title</h3>\r\n" +
                           "</div>\r\n";

            html.Should().Be(expected);
        }

        [Test]
        public void WysiwygContainsMultiLevelList_MapsListOutputCorrectly()
        {
            var wysiwyg = new Wysiwyg();

            wysiwyg.TopElements.Add(new ListHtmlElement
            {
                ListElements = new List<List<IHtmlElement>>
                {
                    new List<IHtmlElement>
                    {
                        new PElement
                        {
                            Children = new List<IHtmlElement>
                            {
                                new TextElement
                                {
                                    Text = "first level"
                                }
                            }
                        },
                        new ListHtmlElement
                        {
                            ListElements = new List<List<IHtmlElement>>
                            {
                                new List<IHtmlElement>
                                {
                                    new PElement
                                    {
                                        Children = new List<IHtmlElement>
                                        {
                                            new TextElement
                                            {
                                                Text = "this is the second level"
                                            }
                                        }
                                    },
                                    new PElement
                                    {
                                        Children = new List<IHtmlElement>
                                        {
                                            new TextElement
                                            {
                                                Text = "and this is the second level"
                                            }
                                        }
                                    }
                                }
                            }
                        },
                        new PElement
                        {
                            Children = new List<IHtmlElement>
                            {
                                new TextElement
                                {
                                    Text = "still the first level"
                                }
                            }
                        }
                    }
                }
            });

            WysiwygToHtmlMapper wysiwygToHtmlMapper = new WysiwygToHtmlMapper();

            string html = wysiwygToHtmlMapper.MapToHtml(wysiwyg);

            html.Should().Be("<div class=\"inner-block\">\r\n" +
                             "  <ul class=\"bullet-list\">\r\n" +
                             "    <li class=\"bullet-list__item\">\r\n" +
                             "      first level\r\n" +
                             "      <ul class=\"bullet-list\">\r\n" +
                             "        <li class=\"bullet-list__item\">\r\n" +
                             "          this is the second level\r\n" +
                             "          and this is the second level\r\n" +
                             "        </li>\r\n" +
                             "      </ul>\r\n" +
                             "      still the first level\r\n" +
                             "    </li>\r\n" +
                             "  </ul>\r\n" +
                             "</div>\r\n");
        }
    }
}
