using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using NUnit.Framework;

namespace SlaterAndGordon.ContentConverter.Tests
{
    public class GoldenMasterTests
    {
        private const string InputTestDataDirectoryRelativePath = @"\GoldenMasterTestData\Input\";
        private const string Input1TestDataDirectoryRelativePath = @"\GoldenMasterTestData\Input1\";
        private const string Input2TestDataDirectoryRelativePath = @"\GoldenMasterTestData\Input2\";
        private const string Input3TestDataDirectoryRelativePath = @"\GoldenMasterTestData\Input3\";
        private const string Input4TestDataDirectoryRelativePath = @"\GoldenMasterTestData\Input4\";
        private const string ExpectedOutputTestDataDirectoryRelativePath = @"\GoldenMasterTestData\ExpectedOutput\";
        private const string ActualOutputTestDataDirectoryRelativePath = @"\GoldenMasterTestData\ActualOutput\";
        private const string ActualOutputMultipleInputTestDataDirectoryRelativePath = @"\GoldenMasterTestData\ActualOutputMultipleInput\";

        private readonly DirectoryInfo _inputTestDataDirectory ;
        private readonly DirectoryInfo _expectedOutputTestDataDirectory;
        private readonly DirectoryInfo _actualOutputTestDataDirectory;
        private DirectoryInfo _actualOutputMultipleInputTestDataDirectory;
        private DirectoryInfo _input1TestDataDirectory;
        private DirectoryInfo _input2TestDataDirectory;
        private DirectoryInfo _input3TestDataDirectory;
        private DirectoryInfo _input4TestDataDirectory;

        public GoldenMasterTests()
        {
            var projectDirectory = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent;
            _inputTestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + InputTestDataDirectoryRelativePath);
            _input1TestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + Input1TestDataDirectoryRelativePath);
            _input2TestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + Input2TestDataDirectoryRelativePath);
            _input3TestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + Input3TestDataDirectoryRelativePath);
            _input4TestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + Input4TestDataDirectoryRelativePath);
            _expectedOutputTestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + ExpectedOutputTestDataDirectoryRelativePath);
            _actualOutputTestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + ActualOutputTestDataDirectoryRelativePath);
            _actualOutputMultipleInputTestDataDirectory = new DirectoryInfo(projectDirectory?.FullName + ActualOutputMultipleInputTestDataDirectoryRelativePath);
        }

        [OneTimeSetUp]
        public void Setup()
        {
            foreach (var file in _actualOutputTestDataDirectory.GetFiles())
            {
                file.Delete();
            }
            Program.Main(new[] {_inputTestDataDirectory.FullName, _actualOutputTestDataDirectory.FullName});
        }

        [OneTimeSetUp]
        public void SetupMultipleInput()
        {
            foreach (var file in _actualOutputMultipleInputTestDataDirectory.GetFiles())
            {
                file.Delete();
            }
            Program.Main(new[] { _input1TestDataDirectory.FullName, _input2TestDataDirectory.FullName, _input3TestDataDirectory.FullName, _input4TestDataDirectory.FullName, _actualOutputMultipleInputTestDataDirectory.FullName });
        }

        [Test]
        public void EveryExpectedFileExistsInActualOutputFolder()
        {
            foreach (var expectedFile in _expectedOutputTestDataDirectory.GetFiles())
            {
                Assert.IsTrue(File.Exists(_actualOutputTestDataDirectory.FullName + expectedFile.Name));
            }
        }

        [Test]
        public void EveryActualOutputFileExistsInExpectedOutputFolder()
        {
            foreach (var actualOutputFile in _actualOutputTestDataDirectory.GetFiles().Where(x => x.Name != "contentful-import.json"))
            {
                Assert.IsTrue(File.Exists(_expectedOutputTestDataDirectory.FullName + actualOutputFile.Name));
            }
        }

        [Test]
        public void EveryExpectedOutputFileContentsMatchesFileWithSameNameInActualOutputFolder()
        {
            foreach (var expectedFile in _expectedOutputTestDataDirectory.GetFiles())
            {
                var expectedFileContents = File.ReadAllText(expectedFile.FullName);
                var actualFileContents =
                    File.ReadAllText(_actualOutputTestDataDirectory + expectedFile.Name);

                expectedFileContents = expectedFileContents.Replace("\r\n", "\n");
                actualFileContents = actualFileContents.Replace("\r\n", "\n");

                Assert.AreEqual(expectedFileContents, actualFileContents);
            }
        }

        //

        [Test]
        public void EveryExpectedFileExistsInActualOutputMultipleInputFolder()
        {
            foreach (var expectedFile in _expectedOutputTestDataDirectory.GetFiles())
            {
                Assert.IsTrue(File.Exists(_actualOutputMultipleInputTestDataDirectory.FullName + expectedFile.Name));
            }
        }

        [Test]
        public void EveryActualOutputMultipleInputFolderFileExistsInExpectedOutputFolder()
        {
            foreach (var actualOutputFile in _actualOutputMultipleInputTestDataDirectory.GetFiles().Where(x => x.Name != "contentful-import.json"))
            {
                Assert.IsTrue(File.Exists(_expectedOutputTestDataDirectory.FullName + actualOutputFile.Name));
            }
        }

        [Test]
        public void EveryExpectedOutputFileContentsMatchesFileWithSameNameInActualOutputMultipleInputFolder()
        {
            foreach (var expectedFile in _expectedOutputTestDataDirectory.GetFiles())
            {
                var expectedFileContents = File.ReadAllText(expectedFile.FullName);
                var actualFileContents =
                    File.ReadAllText(_actualOutputMultipleInputTestDataDirectory + expectedFile.Name);

                expectedFileContents = expectedFileContents.Replace("\r\n", "\n");
                actualFileContents = actualFileContents.Replace("\r\n", "\n");

                Assert.AreEqual(expectedFileContents, actualFileContents);
            }
        }
    }
}