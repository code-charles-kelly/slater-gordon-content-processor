﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;
using FluentAssertions;
using NUnit.Framework;
using SlaterAndGordon.ContentConverter.Images;

namespace SlaterAndGordon.ContentConverter.Tests.Images
{
    [TestFixture]
    public class ImageMapperCsvTests
    {
        private const string HeadersExample = "New URL,Image 1,Image 2";

        [Test]
        public void WhenEmptyFile_ReturnEmptyList()
        {
            var imageMapperCsv = new ImageMapperCsv(string.Empty);

            imageMapperCsv.Records.Count.Should().Be(0);
        }

        [Test]
        public void WhenJustNewLines_ReturnEmptyList()
        {
            var csv = "\n\n\n\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            imageMapperCsv.Records.Count.Should().Be(0);
        }

        [Test]
        public void WhenJustTheHeader_ReturnEmptyList()
        {
            var csv = $"{HeadersExample}\n\n\n\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            imageMapperCsv.Records.Count.Should().Be(0);
        }

        [Test]
        public void WhenTheHeaderAndRecord_ReturnListWithOneRecord()
        {
            var csv = $"{HeadersExample}\nurl,firstimage,secondimage,thirdimage\n\n\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            imageMapperCsv.Records.Count.Should().Be(1);
        }

        [Test]
        public void WhenRecordsButNoHeader_ReturnListWithNoRecords()
        {
            var csv = "url,firstimage,secondimage,thirdimage\nurl,firstimage,secondimage,thirdimage\nurl,firstimage,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            imageMapperCsv.Records.Count.Should().Be(0);
        }

        [Test]
        public void WhenValidHeaderAndImage1Names_ReturnListWithRecordsContainingImage1Names()
        {
            var csv = $"{HeadersExample}\nurl,image1_0,secondimage,thirdimage\nurl,image1_1,secondimage,thirdimage\nurl,image1_2,secondimage,thirdimage\nurl,image1_3,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image1Name.Should().Be("image1_0");
            records[1].Image1Name.Should().Be("image1_1");
            records[2].Image1Name.Should().Be("image1_2");
            records[3].Image1Name.Should().Be("image1_3");
        }

        [Test]
        public void WhenValidHeaderAndImage1Names_AndImage1LocationIsInARandomPlace_ReturnListWithRecordsContainingImage1Names()
        {
            var csv = $"asdfdf,sdfdsf,sdfsdf,Suggested URL,asdsad,a,a,Img 1, asdasdasd,Img 2\n,,,,,,,image1_0,,,,,,\n,,,,,,,image1_1,,,,,,,\n,,,,,,,image1_2,,,,,,\n,,,,,,,image1_3,,,,,,\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image1Name.Should().Be("image1_0");
            records[1].Image1Name.Should().Be("image1_1");
            records[2].Image1Name.Should().Be("image1_2");
            records[3].Image1Name.Should().Be("image1_3");
        }

        [Test]
        public void WhenValidHeaderAndImage2Names_ReturnListWithRecordsContainingImage2Names()
        {
            var csv = $"{HeadersExample}\nurl,,image2_0,secondimage,thirdimage\nurl,,image2_1,secondimage,thirdimage\nurl,,image2_2,secondimage,thirdimage\nurl,,image2_3,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image2Name.Should().Be("image2_0");
            records[1].Image2Name.Should().Be("image2_1");
            records[2].Image2Name.Should().Be("image2_2");
            records[3].Image2Name.Should().Be("image2_3");
        }

        [Test]
        public void WhenValidHeaderAndImage2Names_AndImage2LocationIsInARandomPlace_ReturnListWithRecordsContainingImage2Names()
        {
            var csv = $"asdfdf,sdfdsf,sdfsdf,Suggested URL,asdsad,a,a,Img 2, asdasdasd,Img 1\n,,,,,,,image2_0,,,,,,\n,,,,,,,image2_1,,,,,,,\n,,,,,,,image2_2,,,,,,\n,,,,,,,image2_3,,,,,,\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image2Name.Should().Be("image2_0");
            records[1].Image2Name.Should().Be("image2_1");
            records[2].Image2Name.Should().Be("image2_2");
            records[3].Image2Name.Should().Be("image2_3");
        }

        [Test]
        public void WhenValidHeaderAndRecordsWithNoImage3Header_ReturnRecordsWithImage3NameSetToEmptyString()
        {
            var csv = $"{HeadersExample}\nurl,,image2_0,secondimage,thirdimage\nurl,,image2_1,secondimage,thirdimage\nurl,,image2_2,secondimage,thirdimage\nurl,,image2_3,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image3Name.Should().BeEmpty();
            records[1].Image3Name.Should().BeEmpty();
            records[2].Image3Name.Should().BeEmpty();
            records[3].Image3Name.Should().BeEmpty();
        }

        [Test]
        public void WhenValidHeaderAndImage3Names_ReturnListWithRecordsContainingImage3Names()
        {
            var csv = $"{HeadersExample},Img 3\nurl,,,image3_0,secondimage,thirdimage\nurl,,,image3_1,secondimage,thirdimage\nurl,,,image3_2,secondimage,thirdimage\nurl,,,image3_3,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image3Name.Should().Be("image3_0");
            records[1].Image3Name.Should().Be("image3_1");
            records[2].Image3Name.Should().Be("image3_2");
            records[3].Image3Name.Should().Be("image3_3");
        }

        [Test]
        public void WhenValidHeaderAndImage3Names_AndImage3LocationIsInARandomPlace_ReturnListWithRecordsContainingImage3Names()
        {
            var csv = $"asdfdf,sdfdsf,sdfsdf,Suggested URL,asdsad,Img 2,a,Img 3, asdasdasd,Img 1\n,,,,,,,image3_0,,,,,,\n,,,,,,,image3_1,,,,,,,\n,,,,,,,image3_2,,,,,,\n,,,,,,,image3_3,,,,,,\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Image3Name.Should().Be("image3_0");
            records[1].Image3Name.Should().Be("image3_1");
            records[2].Image3Name.Should().Be("image3_2");
            records[3].Image3Name.Should().Be("image3_3");
        }

        [Test]
        public void WhenValidHeaderAndUrls_ReturnRecordsWithSlugAndParentSet()
        {
            var csv = $"{HeadersExample}\n/parent0/slug0/,,image2_0,secondimage,thirdimage\n/parent1/slug1/,,image2_1,secondimage,thirdimage\n/parent2/slug2/,,image2_2,secondimage,thirdimage\n/parent3/slug3/,,image2_3,secondimage,thirdimage\n";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug0");
            records[0].Parent.Should().Be("parent0");
            records[1].Slug.Should().Be("slug1");
            records[1].Parent.Should().Be("parent1");
            records[2].Slug.Should().Be("slug2");
            records[2].Parent.Should().Be("parent2");
            records[3].Slug.Should().Be("slug3");
            records[3].Parent.Should().Be("parent3");
        }

        [Test]
        public void WhenValidHeaderAndUrlHasNoForwardSlashes_SlugIsUrlAndParentIsBlank()
        {
            var csv = $"{HeadersExample}\nslug0,,image2_0,secondimage,thirdimage";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug0");
            records[0].Parent.Should().Be(string.Empty);
        }

        [Test]
        public void WhenValidHeaderAndUrlHasAForwardSlashAtTheEnd_SlugIsUrlAndParentIsBlank()
        {
            var csv = $"{HeadersExample}\nslug0/,,image2_0,secondimage,thirdimage";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug0");
            records[0].Parent.Should().Be(string.Empty);
        }

        [Test]
        public void WhenValidHeaderAndUrlHasOneForwardSlashInBetweenStrings_SlugIsAfterSlashAndParentIsBefore()
        {
            var csv = $"{HeadersExample}\nparent/slug,,image2_0,secondimage,thirdimage";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug");
            records[0].Parent.Should().Be("parent");
        }

        [Test]
        public void WhenValidHeaderAndUrlHasOnlySpacesInBetweenSlashes_EntriesWIthOnlySpacesAreIgnored()
        {
            var csv = $"{HeadersExample}\nparent/  /slug/ ,,image2_0,secondimage,thirdimage";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug");
            records[0].Parent.Should().Be("parent");
        }

        [Test]
        public void WhenValidHeaderAndUrlHasOnlySlugAfterSlaterGordonDomain_ParentShouldBeEmpty()
        {
            var csv = $"{HeadersExample}\nhttps://www.slatergordon.co.uk/slug/ ,,image2_0,secondimage,thirdimage";

            var imageMapperCsv = new ImageMapperCsv(csv);

            var records = imageMapperCsv.Records.ToArray();

            records[0].Slug.Should().Be("slug");
            records[0].Parent.Should().Be("");
        }
    }
}
