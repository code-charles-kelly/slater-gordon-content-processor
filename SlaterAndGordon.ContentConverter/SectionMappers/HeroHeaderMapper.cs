﻿using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class HeroHeaderMapper : SectionMapper
    {
        private HeroHeader _heroHeader = new HeroHeader();
        public HeroHeader MapComponent(Section heroHeaderSection)
        {
            _heroHeader = new HeroHeader();

            foreach (var htmlNode in heroHeaderSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters( Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _heroHeader;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters( RemoveMarker(text));

            switch (markerName)
            {
                case "SECTION NAME":
                    _heroHeader.SectionName = contents;
                    break;
                case "HEADING":
                    _heroHeader.Heading = contents;
                    break;
                case "STRAPLINE":
                    _heroHeader.StrapLine = contents;
                    break;
            }
        }
    }
}