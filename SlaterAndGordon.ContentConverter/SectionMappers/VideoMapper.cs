﻿using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    class VideoMapper : SectionMapper
    {
        private Video _heroHeader = new Video();
        public Video MapComponent(Section heroHeaderSection)
        {
            _heroHeader = new Video();

            foreach (var htmlNode in heroHeaderSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _heroHeader;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters(RemoveMarker(text));

            switch (markerName)
            {
                case "HEADING":
                    _heroHeader.Heading = contents;
                    break;
                case "STANDARD CONTENT":
                    _heroHeader.StandardContent = contents;
                    break;
                case "VIDEO":
                    _heroHeader.VideoId = contents.Substring(contents.LastIndexOf("/") + 1);
                    break;
            }
        }
    }
}
