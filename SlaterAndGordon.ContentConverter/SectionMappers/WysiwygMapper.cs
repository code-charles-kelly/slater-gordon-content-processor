﻿using System.Collections.Generic;
using ExCSS;
using HtmlAgilityPack;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class WysiwygMapper : SectionMapper
    {
        private Wysiwyg _wysiwyg = new Wysiwyg();
        private Stylesheet _stylesheet;
        public Wysiwyg MapWysiwyg(Section wysiwygSection, Stylesheet stylesheet)
        {
            _wysiwyg = new Wysiwyg();
            _stylesheet = stylesheet;

            foreach (var topElement in wysiwygSection.Contents)
            {
                var justText = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(topElement));

                if (justText == string.Empty)
                {
                    continue;
                }

                if (!HasMarker(justText))
                {
                    if (topElement.Name == "p")
                    {
                        _wysiwyg.TopElements.Add(GetPElement(topElement));
                    }
                    else if (topElement.Name == "ul")
                    {
                        _wysiwyg.TopElements.Add(GetListElement(topElement));
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (GetMarker(justText).ToUpper() == "H2")
                {
                    _wysiwyg.TopElements.Add(GetH2Element(topElement));
                }
                else if (GetMarker(justText).ToUpper() == "H2FAQ")
                {
                    var h2Element = GetH2Element(topElement);
                    h2Element.IsFaqQuestion = true;
                    _wysiwyg.TopElements.Add(h2Element);
                }
                else if (GetMarker(justText).ToUpper() == "H3")
                {
                    _wysiwyg.TopElements.Add(GetH3Element(topElement));
                }
                else if (GetMarker(justText).ToUpper() == "BUTTON" || GetMarker(justText).ToUpper() == "CTA")
                {
                    _wysiwyg.TopElements.Add(GetButtonElement(topElement));
                }
                else if (GetMarker(justText).ToLower() == "standard content")
                {
                    _wysiwyg.TopElements.Add(GetPElement(topElement));
                }
            }

            return _wysiwyg;
        }

        private IHtmlElement GetH3Element(HtmlNode topElement)
        {
            var justText = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(topElement));

            justText = Utils.TrimUnwantedCharacters(RemoveMarker(justText));

            return new H3Element
            {
                Text = justText
            };
        }

        private ButtonElement GetButtonElement(HtmlNode topElement)
        {
            var link = Utils.FindANode(topElement);

            return new ButtonElement
            {
                Link = Utils.GetAElement(link)
            };

        }

        private ListHtmlElement GetListElement(HtmlNode listNode)
        {
            var listHtmlElement = new ListHtmlElement();

            foreach (var childNode in listNode.ChildNodes)
            {
                if (childNode.Name == "li")
                {
                    var listElementItems = new List<IHtmlElement>();
                    var paragraphNode = HtmlNode.CreateNode(("<p></p>"));
                    foreach (var child in childNode.ChildNodes)
                    {
                        if (child.Name != "ul")
                        {
                            paragraphNode.AppendChild(child);
                        }
                        else
                        {
                            if (paragraphNode.ChildNodes.Count > 0)
                            {
                                listElementItems.Add(GetPElement(paragraphNode));
                            }

                            paragraphNode = HtmlNode.CreateNode(("<p></p>"));
                            listElementItems.Add(GetListElement(child));
                        }
                    }

                    if (paragraphNode.ChildNodes.Count > 0)
                    {
                        listElementItems.Add(GetPElement(paragraphNode));
                    }

                    var childNodeParagraph = GetPElement(childNode);

                    listHtmlElement.ListElements.Add(listElementItems);
                }
            }

            return listHtmlElement;
        }

        private H2Element GetH2Element(HtmlNode h2Node)
        {
            var justText = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(h2Node));

            justText = Utils.TrimUnwantedCharacters( RemoveMarker(justText));

            return new H2Element
            {
                Text = justText
            };
        }

        private PElement GetPElement(HtmlNode pNode)
        {
            var pElement = new PElement {Children = new List<IHtmlElement>()};

            var lastTextElement = ProcessText(pNode, pElement, new TextElement());

            if (lastTextElement.Text != string.Empty) pElement.Children.Add(lastTextElement);
            
            return pElement;
        }

        private TextElement ProcessText(HtmlNode pNode, PElement pElement, TextElement textElement)
        {
            var currentTextElement = textElement;

            foreach (var childNode in pNode.ChildNodes)
            {
                if (childNode.Name == "#text")
                {
                    currentTextElement.Text += Utils.TrimNonSpaceWhiteSpace(childNode.InnerText);
                    currentTextElement.Text = currentTextElement.Text.Replace("[[CallbackNumber]]", "{{{meta.phonenumber.contentLink}}}");
                    currentTextElement.Text = currentTextElement.Text.Replace("[[CALLBACK]]", "{{{meta.phonenumber.contentLink}}}");
                }
                else if (childNode.Name == "a")
                {
                    if (currentTextElement.Text != string.Empty)
                    {
                        pElement.Children.Add(currentTextElement);
                    }

                    pElement.Children.Add(Utils.GetAElement(childNode));

                    var newTextElement = new TextElement
                    {
                        IsBold = currentTextElement.IsBold,
                        IsItalic = currentTextElement.IsItalic
                    };

                    currentTextElement = newTextElement;
                }
                else if (childNode.Name == "span")
                {
                    currentTextElement = ProcessSpan(pElement, currentTextElement, childNode);
                }
            }

            return currentTextElement;
        }

        private TextElement ProcessSpan(PElement pElement, TextElement currentTextElement, HtmlNode childNode)
        {
            if (HaveStylesChanged(currentTextElement, childNode))
            {
                if (currentTextElement.Text != string.Empty)
                {
                    pElement.Children.Add(currentTextElement);
                }

                var newTextElement = new TextElement();

                if (IsFontStyleSet(childNode))
                {
                    newTextElement.IsItalic = IsNodeItalic(childNode);
                }
                else
                {
                    newTextElement.IsItalic = currentTextElement.IsItalic;
                }

                if (IsFontWeightSet(childNode))
                {
                    newTextElement.IsBold = IsNodeBold(childNode);
                }
                else
                {
                    newTextElement.IsBold = currentTextElement.IsBold;
                }

                currentTextElement = newTextElement;
            }

            currentTextElement = ProcessText(childNode, pElement, currentTextElement);
            return currentTextElement;
        }

        private bool HaveStylesChanged(TextElement oldTextElement, HtmlNode newNode)
        {
            if (IsFontStyleSet(newNode) && IsNodeItalic(newNode) != oldTextElement.IsItalic)
            {
                return true;
            }

            if (IsFontWeightSet(newNode) && IsNodeBold(newNode) != oldTextElement.IsBold)
            {
                return true;
            }

            return false;
        }

        

        private bool IsNodeBold(HtmlNode node)
        {
            var nodeStyles = Utils.GetStyleStringForNode(node, _stylesheet);
            return nodeStyles.Style.FontWeight == "bold";
        }

        private bool IsNodeItalic(HtmlNode node)
        {
            var nodeStyles = Utils.GetStyleStringForNode(node, _stylesheet);
            return nodeStyles.Style.FontStyle == "italic";
        }

        private bool IsFontWeightSet(HtmlNode node)
        {
            var nodeStyles = Utils.GetStyleStringForNode(node, _stylesheet);
            return nodeStyles.Style.FontWeight != string.Empty;
        }

        private bool IsFontStyleSet(HtmlNode node)
        {
            var nodeStyles = Utils.GetStyleStringForNode(node, _stylesheet);
            return nodeStyles.Style.FontStyle != string.Empty;
        }
        
    }
}
