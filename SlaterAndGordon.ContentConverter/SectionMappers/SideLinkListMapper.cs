﻿using System.Collections.Generic;
using ExCSS;
using HtmlAgilityPack;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class SideLinkListMapper : SectionMapper
    {
        private SideLinkList _sideLinkList = new SideLinkList();
        public SideLinkList MapComponent(Section heroHeaderSection, Stylesheet stylesheet)
        {
            _sideLinkList = new SideLinkList();
            var linkNodes = new List<HtmlNode>();


            foreach (var htmlNode in heroHeaderSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters( Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
                else if (text != string.Empty)
                {
                    linkNodes.Add(Utils.FindANode(htmlNode));
                }
            }

            foreach (var linkNode in linkNodes)
            {
                if (linkNode != null)
                {
                    _sideLinkList.Links.Add(Utils.GetAElement(linkNode));
                }
            }

            return _sideLinkList;
        }

        

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters( RemoveMarker(text));

            switch (markerName)
            {
                case "HEADING":
                    _sideLinkList.Heading = contents;
                    break;
            }
        }
    }
}