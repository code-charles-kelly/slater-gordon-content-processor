﻿using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class CaseStudiesMapper : SectionMapper
    {
        private CaseStudies _caseStudies = new CaseStudies();
        public CaseStudies MapComponent(Section heroHeaderSection)
        {
            _caseStudies = new CaseStudies();

            foreach (var htmlNode in heroHeaderSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _caseStudies;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters(RemoveMarker(text));

            switch (markerName)
            {
                case "CASE STUDY 1 URL":
                    _caseStudies.Url1 = Utils.ConvertToRelative(contents,"slatergordon.co.uk");
                    break;
                case "CASE STUDY 2 URL":
                    _caseStudies.Url2 = Utils.ConvertToRelative(contents, "slatergordon.co.uk");
                    break;
                case "CASE STUDY 3 URL":
                    _caseStudies.Url3 = Utils.ConvertToRelative(contents, "slatergordon.co.uk");
                    break;
                case "CASE STUDY 1 HEADING":
                    _caseStudies.Heading1 = contents;
                    break;
                case "CASE STUDY 2 HEADING":
                    _caseStudies.Heading2 = contents;
                    break;
                case "CASE STUDY 3 HEADING":
                    _caseStudies.Heading3 = contents;
                    break;
                case "CASE STUDY 1 TEXT":
                    _caseStudies.Copy1 = contents;
                    break;
                case "CASE STUDY 2 TEXT":
                    _caseStudies.Copy2 = contents;
                    break;
                case "CASE STUDY 3 TEXT":
                    _caseStudies.Copy3 = contents;
                    break;
            }
        }
    }
}
