﻿using ExCSS;
using HtmlAgilityPack;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class TwoComponentEnquiryMapper : SectionMapper
    {
        private TwoComponentEnquiry _twoComponentEnquiry = new TwoComponentEnquiry();
        private Section _leftCopySection = new Section { Header = "Left Copy Section" };
        public TwoComponentEnquiry MapComponent(Section heroHeaderSection, Stylesheet stylesheet)
        {
            _twoComponentEnquiry = new TwoComponentEnquiry();


            foreach (var htmlNode in heroHeaderSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters( Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text, htmlNode);
                }
                else if (text != string.Empty)
                {
                    _leftCopySection.Contents.Add(htmlNode);
                }
            }

            var wysiwygMapper = new WysiwygMapper();
            _twoComponentEnquiry.LeftCopy = wysiwygMapper.MapWysiwyg(_leftCopySection, stylesheet);

            return _twoComponentEnquiry;
        }

        private void MapMarker(string text, HtmlNode node)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters( RemoveMarker(text));

            switch (markerName)
            {
                case "HEADING":
                    _twoComponentEnquiry.Heading = contents;
                    break;
                case "RIGHT HAND SIDE HEADING":
                    _twoComponentEnquiry.RightHeading = contents;
                    break;
                case "RIGHT HAND SIDE LINK TEXT":
                    _twoComponentEnquiry.LinkText = contents;
                    break;
                case "RIGHT HAND SIDE LINK URL":
                    _twoComponentEnquiry.LinkUrl = Utils.ConvertToRelative(contents, "slatergordon.co.uk");
                    break;
                case "STANDARD CONTENT":
                    _leftCopySection.Contents.Add(node);
                    break;
            }
        }
    }
}