﻿using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class QuotesMapper : SectionMapper
    {
        private Quotes _quotes = new Quotes();
        private int _quoteCount = 0;
        private int _customerCount = 0;

        public Quotes MapComponent(Section pageMetaSection)
        {
            _quotes = new Quotes();
            _quoteCount = 0;
            _customerCount = 0;

            foreach (var htmlNode in pageMetaSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _quotes;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters(RemoveMarker(text));

            switch (markerName)
            {
                case "QUOTE":
                    if (_quoteCount == 0)
                    {
                        _quotes.Quote1 = contents;
                        _quoteCount++;
                    }
                    else if (_quoteCount == 1)
                    {
                        _quotes.Quote2 = contents;
                        _quoteCount++;
                    }
                    else
                    {
                        _quotes.Quote3 = contents;
                    }
                    break;
                case "CUSTOMER NAME":
                    if (_customerCount == 0)
                    {
                        _quotes.CustomerName1 = contents;
                        _customerCount++;
                    }
                    else if (_customerCount == 1)
                    {
                        _quotes.CustomerName2 = contents;
                        _customerCount++;
                    }
                    else
                    {
                        _quotes.CustomerName3 = contents;
                    }
                    break;
            }
        }
    }
}