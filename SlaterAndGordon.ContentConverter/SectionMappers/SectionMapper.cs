﻿namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public abstract class SectionMapper
    {
        internal string RemoveMarker(string text)
        {
            if (!HasMarker(text)) return text;

            return text.Substring(text.IndexOf("]") + 1);
        }

        internal string GetMarker(string text)
        {
            if (!HasMarker(text)) return "";

            return GetStringBetweenStrings(text, "[", "]");
        }

        private string GetStringBetweenStrings(string original, string start, string end)
        {
            if (!original.Contains(start) || !original.Contains(end) || original.IndexOf(start) >= original.IndexOf(end))
            {
                return "";
            }

            var newString = original.Substring(original.IndexOf(start) + start.Length);
            newString = newString.Substring(0, newString.IndexOf(end));

            return newString;
        }

        internal bool HasMarker(string text)
        {
            return text.StartsWith("[") && text.Contains("]");
        }
    }
}