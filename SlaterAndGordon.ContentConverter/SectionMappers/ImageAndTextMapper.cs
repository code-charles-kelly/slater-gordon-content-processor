﻿using System.Collections.Generic;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{ 
    class ImageAndTextMapper : SectionMapper
    {
        private Dictionary<string, string> _iconsDictionary = new Dictionary<string, string>
        {
            {"HEAD AND BRAIN","head-and-brain"},
            {"NIHL",""},

        };

        private ImageAndText _imageAndText = new ImageAndText();

        private int _numberOfUrls = 0;
        private int _numberOfCtaText = 0;
        private int _numberOfIcons = 0;

        public ImageAndText MapComponent(Section imageAndTextSection)
        {
            _imageAndText = new ImageAndText();

            _numberOfCtaText = 0;
            _numberOfIcons = 0;
            _numberOfUrls = 0;


            foreach (var htmlNode in imageAndTextSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _imageAndText;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters(RemoveMarker(text));

            switch (markerName)
            {
                case "SECTION NAME":
                    _imageAndText.SectionName = contents;
                    break;
                case "HEADING":
                    _imageAndText.Heading = contents;
                    break;
                case "STANDARD CONTENT":
                    _imageAndText.StandardContent = contents;
                    break;
            }

            markerName = markerName.Replace("–", "-");

            if (markerName.EndsWith("- URL"))
            {
                _numberOfUrls++;
                if (_numberOfUrls > _imageAndText.Ctas.Count) _imageAndText.Ctas.Add(new ImageAndTextCta());
                _imageAndText.Ctas[_numberOfUrls - 1].Url = Utils.ConvertToRelative(contents, "slatergordon.co.uk"); ;
            }
            else if (markerName.EndsWith("- TEXT"))
            {
                _numberOfCtaText++;
                if (_numberOfCtaText > _imageAndText.Ctas.Count) _imageAndText.Ctas.Add(new ImageAndTextCta());
                _imageAndText.Ctas[_numberOfCtaText - 1].Text = contents;
            }
            else if (markerName.EndsWith("- ICON"))
            {
                _numberOfIcons++;
                if (_numberOfIcons > _imageAndText.Ctas.Count) _imageAndText.Ctas.Add(new ImageAndTextCta());
                _imageAndText.Ctas[_numberOfIcons - 1].Icon = contents.ToLower().Replace(" ","-");
            }
        }
    }
}
