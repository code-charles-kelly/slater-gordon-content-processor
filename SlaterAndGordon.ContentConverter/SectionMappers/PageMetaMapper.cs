﻿using System;
using System.Linq;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.SectionMappers
{
    public class PageMetaMapper : SectionMapper
    {
        private PageMeta _pageMeta = new PageMeta();
        public PageMeta MapComponent(Section pageMetaSection)
        {
            _pageMeta = new PageMeta();

            foreach (var htmlNode in pageMetaSection.Contents)
            {
                var text = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(htmlNode));
                if (HasMarker(text))
                {
                    MapMarker(text);
                }
            }

            return _pageMeta;
        }

        private void MapMarker(string text)
        {
            var markerName = GetMarker(text).ToUpper();
            var contents = Utils.TrimUnwantedCharacters(RemoveMarker(text));

            switch (markerName)
            {
                case "PAGE TITLE":
                    _pageMeta.PageTitle = contents;
                    _pageMeta.BreadcumbTitle = contents.Replace("| Slater + Gordon", "").Replace("Compensation Claims","").Trim().Replace("| Using a solicitor to write your will", String.Empty).Trim();
                    if (_pageMeta.BreadcumbTitle.EndsWith("Compensation"))
                    {
                        _pageMeta.BreadcumbTitle = _pageMeta.BreadcumbTitle.Remove(_pageMeta.BreadcumbTitle.LastIndexOf("Compensation")).Trim();
                    }
                    if (_pageMeta.BreadcumbTitle.EndsWith("Solicitors"))
                    {
                        _pageMeta.BreadcumbTitle = _pageMeta.BreadcumbTitle.Remove(_pageMeta.BreadcumbTitle.LastIndexOf("Solicitors")).Trim();
                    }
                    if (_pageMeta.BreadcumbTitle.EndsWith("Claims"))
                    {
                        _pageMeta.BreadcumbTitle = _pageMeta.BreadcumbTitle.Remove(_pageMeta.BreadcumbTitle.LastIndexOf("Claims")).Trim();
                    }
                    break;
                case "META DESCRIPTION":
                    _pageMeta.MetaDescription = contents;
                    break;
                case "URL":
                    MapSlugs(contents);
                    break;
            }
        }

        private void MapSlugs(string url)
        {
            if (url == "")
            {
                ErrorLogging.WriteError("Missing page url!");
                _pageMeta.Slug = "missing!";
                _pageMeta.Parent = null;
                return;
            }

            url = url.ToLower();
            if (url.StartsWith("https://"))
            {
                url = url.Replace("https://","");
            }
            else if (url.StartsWith("http://"))
            {
                url = url.Replace("https://", "");
            }

            url = url.Substring(url.IndexOf("/") + 1);

            var slugs = url.Split('/');

            slugs = slugs.Reverse().Where(x => x != string.Empty).ToArray();

            _pageMeta.Slug = slugs[0];
            if (slugs.Length >= 2)
            {
                _pageMeta.Parent = slugs[1];
            }
        }
    }
}