﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class PageMeta
    {
        public string PageTitle { get; set; }
        public string BreadcumbTitle { get; set; }
        public string MetaDescription { get; set; }
        public string Slug { get; set; }
        public string Parent { get; set; }
    }
}