﻿using System.Collections.Generic;

namespace SlaterAndGordon.ContentConverter.Sections
{
    public class SideLinkList
    {
        public string Heading { get; set; }
        public List<AElement> Links { get; set; } = new List<AElement>();
    }
}
