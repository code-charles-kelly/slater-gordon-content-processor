﻿using System.Collections.Generic;

namespace SlaterAndGordon.ContentConverter.Sections
{
    public class Wysiwyg
    {
        public List<IHtmlElement> TopElements { get; set; } = new List<IHtmlElement>();

    }

    public interface IHtmlElement
    {

    }

    public class AElement : IHtmlElement
    {
        public string Href { get; set; }
        public string Text { get; set; }
        public bool IsRelative { get; set; }
    }

    public class PElement : IHtmlElement
    {
        public List<IHtmlElement> Children { get; set; }
    }

    public class TextElement : IHtmlElement
    {
        public bool IsBold { get; set; }
        public bool IsItalic { get; set; }
        public string Text { get; set; }
    }

    public class ListHtmlElement : IHtmlElement
    {
        public List<List<IHtmlElement>> ListElements { get; set; } = new List<List<IHtmlElement>>();
    }

    public class H2Element : IHtmlElement
    {
        public string Text { get; set; }
        public bool IsFaqQuestion { get; set; }
    }

    public class ButtonElement : IHtmlElement
    {
        public AElement Link { get; set; }
    }

    public class H3Element : IHtmlElement
    {
        public string Text { get; set; }
    }
}
