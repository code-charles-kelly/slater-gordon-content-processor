﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class Video
    {
        public string Heading { get; set; }
        public string StandardContent { get; set; }
        public string VideoId { get; set; }
    }
}
