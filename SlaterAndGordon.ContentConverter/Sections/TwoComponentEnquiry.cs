﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class TwoComponentEnquiry
    {
        public string Heading { get; set; }
        public Wysiwyg LeftCopy { get; set; }
        public string RightHeading { get; set; }
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
    }
}
