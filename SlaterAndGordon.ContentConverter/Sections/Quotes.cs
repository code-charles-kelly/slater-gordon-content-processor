﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class Quotes
    {
        public string Quote1 { get; set; }
        public string CustomerName1 { get; set; }
        public string Quote2 { get; set; }
        public string CustomerName2 { get; set; }
        public string Quote3 { get; set; }
        public string CustomerName3 { get; set; }
    }
}