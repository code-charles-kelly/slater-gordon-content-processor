﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class CaseStudies
    {
        public string Url1 { get; set; }
        public string Heading1 { get; set; }
        public string Copy1 { get; set; }
        public string Url2 { get; set; }
        public string Heading2 { get; set; }
        public string Copy2 { get; set; }
        public string Url3 { get; set; }
        public string Heading3 { get; set; }
        public string Copy3 { get; set; }
    }
}
