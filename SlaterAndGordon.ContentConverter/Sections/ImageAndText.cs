﻿using System.Collections.Generic;

namespace SlaterAndGordon.ContentConverter.Sections
{
    public class ImageAndText
    {
        public string SectionName { get; set; }
        public string Heading { get; set; }
        public string StandardContent { get; set; }
        public string ListOfLinks { get; set; }
        public List<ImageAndTextCta> Ctas { get; set; } = new List<ImageAndTextCta>();
    }

    public class ImageAndTextCta
    {
        public string Url { get; set; }
        public string Text { get; set; }
        public string Icon { get; set; }
    }
}
