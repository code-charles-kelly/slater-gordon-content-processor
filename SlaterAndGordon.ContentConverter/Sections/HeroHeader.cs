﻿namespace SlaterAndGordon.ContentConverter.Sections
{
    public class HeroHeader
    {
        public string SectionName { get; set; }
        public string Heading { get; set; }
        public string StrapLine { get; set; }
    }
}