﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace SlaterAndGordon.ContentConverter.Sections
{
    public class Section
    {
        public string Header { get; set; }
        public List<HtmlNode> Contents { get; set; } = new List<HtmlNode>();
    }
}