﻿using System.Collections.Generic;
using System.Linq;

namespace SlaterAndGordon.ContentConverter
{
    public class ImageMapper
    {
        private List<ImageRecord> _imageRecords = new List<ImageRecord>();

        public void MapRecord(string url, string image1, string image2, string image3)
        {
            var newImageRecord = MapSlugs(url);

            newImageRecord.ImageName1 = image1;
            newImageRecord.ImageName2 = image2;
            newImageRecord.ImageName3 = image3;
        }

        public void GetRecord(string slug, string parent)
        {

        }

        private ImageRecord MapSlugs(string url)
        {
            if (url == "")
            {
                return new ImageRecord();
            }

            url = url.ToLower();
            if (url.StartsWith("https://"))
            {
                url = url.Replace("https://", "");
            }
            else if (url.StartsWith("http://"))
            {
                url = url.Replace("https://", "");
            }

            url = url.Substring(url.IndexOf("/") + 1);

            var slugs = url.Split('/');

            slugs = slugs.Reverse().Where(x => x != string.Empty).ToArray();

            var newImageRecord = new ImageRecord {Slug = slugs[0]};

            if (slugs.Length >= 2)
            {
                newImageRecord.Parent = slugs[1];
            }

            return newImageRecord;
        }
    }

    public class ImageRecord
    {
        public string Slug { get; set; }
        public string Parent { get; set; }
        public string ImageName1 { get; set; }
        public string ImageName2 { get; set; }
        public string ImageName3 { get; set; }
    }
}
