﻿using System.Collections.Generic;
using HtmlAgilityPack;

namespace SlaterAndGordon.ContentConverter
{
    public class PreProcessHtml
    {
        public static void ConvertBrToParagraph(HtmlNode htmlNode)
        {
            var nextBr = FindNextBrNode(htmlNode);

            while (nextBr != null)
            {
                var nodeBelowParagraph = FindFirstParentInParagraph(nextBr);

                if (nodeBelowParagraph == null)
                {
                    nextBr.Name = "p";
                    nextBr = FindNextBrNode(htmlNode);
                    continue;
                }
                var parentParagraph = nodeBelowParagraph.ParentNode;

                var listOfBrSiblings = GetListOfNextSiblings(nextBr);
                var parentOfBr = nextBr.ParentNode;

                var newSpan = HtmlNode.CreateNode("<span></span>");
                newSpan.Name = "span";
                foreach (var attribute in parentOfBr.Attributes)
                {
                    newSpan.Attributes.Append((HtmlAttribute) attribute);
                }

                parentOfBr.ParentNode.InsertAfter(newSpan, parentOfBr);
                foreach (var node in listOfBrSiblings)
                {
                    node.Remove();
                    newSpan.ChildNodes.Append(node);
                }

                var list = GetListOfNextSiblings(nodeBelowParagraph);

                var newParagraph = HtmlNode.CreateNode("<p></p>");
                newParagraph.Name = "p";

                parentParagraph.ParentNode.InsertAfter(newParagraph, parentParagraph);

                foreach (var node in list)
                {
                    node.Remove();
                    newParagraph.ChildNodes.Append(node);
                }

                nextBr.Remove();
                nextBr = FindNextBrNode(htmlNode);
            }
        }

        private static HtmlNode FindNextBrNode(HtmlNode htmlNode)
        {
            if (htmlNode.Name == "br")
            {
                return htmlNode;
            }

            foreach (var childNode in htmlNode.ChildNodes)
            {
                var brNode = FindNextBrNode(childNode);

                if (brNode != null)
                {
                    return brNode;
                }
            }
            return null;
        }

        private static HtmlNode FindFirstParentInParagraph(HtmlNode htmlNode)
        {
            if (htmlNode.ParentNode == null) return null;

            if (htmlNode.ParentNode.Name == "p")
            {
                return htmlNode;
            }

            return (FindFirstParentInParagraph(htmlNode.ParentNode));
        }

        private static List<HtmlNode> GetListOfNextSiblings(HtmlNode htmlNode)
        {
            var list = new List<HtmlNode>();
            //list.Add(htmlNode);

            if (htmlNode.NextSibling != null)
            {
                list.Add(htmlNode.NextSibling);
                AddAllSiblingsToList(htmlNode.NextSibling, list);
            }

            return list;
        }

        private static void AddAllSiblingsToList(HtmlNode nextNode, List<HtmlNode> currentList)
        {
            if (nextNode.NextSibling != null)
            {
                currentList.Add(nextNode.NextSibling);
                AddAllSiblingsToList(nextNode.NextSibling, currentList);
            }

            return;
        }
    }
}