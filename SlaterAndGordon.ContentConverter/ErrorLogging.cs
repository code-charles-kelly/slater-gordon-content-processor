﻿namespace SlaterAndGordon.ContentConverter
{
    public static class ErrorLogging
    {
        public static string ErrorFilePath = "";
        public static string CurrentFile = "";

        public static void WriteError(string errorMessage)
        {
            System.IO.File.AppendAllText(ErrorFilePath,"\r\n" +CurrentFile + ": " + errorMessage);
        }
    }
}
