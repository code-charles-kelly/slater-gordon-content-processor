﻿using System.Linq;
using ExCSS;
using HtmlAgilityPack;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter
{
    internal class Utils
    {
        public static string TrimUnwantedCharacters(string original)
        {
            original = original.Replace("‘", "'");
            original = original.Replace("’", "'");
            original = original.Replace("“", "\"");
            original = original.Replace("”", "\"");
            original = original.Replace(" &nbsp;"," ");
            original = original.Replace("&nbsp; ", " ");
            original = original.Replace("&nbsp;", " ");
            original = HtmlEntity.DeEntitize(original);
            original = original.Trim();

            return original;
        }

        public static string GetTextFromNode(HtmlNode node)
        {
            string output = "";

            if (node.ChildNodes.Count == 0)
            {
                return node.InnerText;
            }

            foreach (var childNode in node.ChildNodes)
            {
                output += GetTextFromNode(childNode);
            }

            return output;
        }

        public static string GetStyles(HtmlDocument htmlDocument)
        {
            return htmlDocument.DocumentNode.SelectNodes("//html//head//style").FirstOrDefault()?.InnerText;
        }

        public static bool IsSectionHeader(HtmlNode htmlNode, Stylesheet stylesheet)
        {
            var text = TrimUnwantedCharacters(GetTextFromNode(htmlNode));

            if (text.StartsWith("[")) return false;

            var style = GetStyleStringForNode(htmlNode, stylesheet);

            if (style != null && style.Style.TextDecoration == "underline" && text != "" && htmlNode.ChildNodes.Where(x => x.Name == "a").ToList().Count == 0 || text == "Side Link List")
            {
                return true;
            }

            foreach (var childNode in htmlNode.ChildNodes)
            {
                if (childNode.Name == "span")
                {
                    if (IsSectionHeader(childNode, stylesheet)) return true;
                }
            }

            return false;
        }

        public static StyleRule GetStyleStringForNode(HtmlNode htmlNode, Stylesheet stylesheet)
        {
            foreach (var nodeClass in htmlNode.GetClasses())
            {
                foreach (StyleRule stylesheetStyleRule in stylesheet.StyleRules)
                {
                    if ("." + nodeClass == stylesheetStyleRule.Selector.Text)
                    {
                        return stylesheetStyleRule;
                    }
                }
            }

            return null;
        }

        public static HtmlNode GetBody(HtmlDocument htmlDocument)
        {
            return htmlDocument.DocumentNode.SelectNodes("//html//body").FirstOrDefault();
        }

        public static AElement GetAElement(HtmlNode aNode)
        {
            var aElement = new AElement();

            var link = GetLinkFromAElement(aNode);

            if (IsRelativeLink(link, "slatergordon.co.uk"))
            {
                link = ConvertToRelative(link, "slatergordon.co.uk");
                aElement.IsRelative = true;
            }
            aElement.Href = link;
            aElement.Text = GetTextFromAElement(aNode);

            return aElement;
        }

        public static string GetTextFromAElement(HtmlNode aNode)
        {
            return TrimNonSpaceWhiteSpace(Utils.GetTextFromNode(aNode));
        }

        public static string GetLinkFromAElement(HtmlNode aNode)
        {
            var link = aNode.GetAttributeValue("href", "");
            link = TrimNonSpaceWhiteSpace(link);

            return link;
        }

        public static string ConvertToRelative(string link, string domain)
        {
            if (link.Contains($"https://www.{domain}"))
            {
                link = link.Replace($"https://www.{domain}", "");
            }
            else if (link.Contains($"http://www.{domain}"))
            {
                link = link.Replace($"http://www.{domain}", "");
            }
            else if (link.Contains($"http://stg.{domain}"))
            {
                link = link.Replace($"http://stg.{domain}", "");
            }
            else if (link.Contains($"https://stg.{domain}"))
            {
                link = link.Replace($"https://stg.{domain}", "");
            }
            else if (link.Contains($"http://qa.{domain}"))
            {
                link = link.Replace($"http://qa.{domain}", "");
            }
            else if (link.Contains($"https://qa.{domain}"))
            {
                link = link.Replace($"https://qa.{domain}", "");
            }
            else if (link.Contains($"http://int.{domain}"))
            {
                link = link.Replace($"http://int.{domain}", "");
            }
            else if (link.Contains($"https://int.{domain}"))
            {
                link = link.Replace($"https://int.{domain}", "");
            }

            return link;
        }

        public static bool IsRelativeLink(string link, string domain)
        {
            return link.Contains($"https://www.{domain}") || link.Contains($"http://www.{domain}") || link.Contains($"http://stg.{domain}") || link.Contains($"https://stg.{domain}") || link.Contains($"http://qa.{domain}") || link.Contains($"https://qa.{domain}") || link.Contains($"http://int.{domain}") || link.Contains($"https://int.{domain}");
        }

        public static string TrimNonSpaceWhiteSpace(string text)
        {
            text = HtmlAgilityPack.HtmlEntity.DeEntitize(text);
            text = text.Replace("[Standard Content]", "");
            text = text.Replace("‘", "'");
            text = text.Replace("’", "'");
            text = text.Replace("’", "'");
            text = text.Replace("“", "\"");
            text = text.Replace("”", "\"");
            return text.Trim('\r', '\n', '\t');
            

        }

        public static HtmlNode FindANode(HtmlNode baseNode)
        {
            if (baseNode.Name == "a") return baseNode;

            foreach (var childNode in baseNode.ChildNodes)
            {
                if (childNode.Name == "a") return childNode;

                var foundNode = FindANode(childNode);

                if (foundNode != null) return foundNode;
            }

            return null;
        }
    }
}