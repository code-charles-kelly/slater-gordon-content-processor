﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlaterAndGordon.ContentConverter.Input
{
    public class CommandLineArguments
    {
        private readonly List<string> _inputFolders;

        public CommandLineArguments(string[] args)
        {
            if (ShouldSetArguments(args))
            {
                ArgumentsSet = true;
                _inputFolders = args.ToList().GetRange(0, args.Length - 1);
                OutputFolder = args[^1];
            }
            else
            {
                _inputFolders = new List<string>();
                OutputFolder = String.Empty;
            }
        }

        private static bool ShouldSetArguments(string[] args)
        {
            return args != null && args.Length >= 2;
        }

        public bool ArgumentsSet { get; }
        public int NumberOfInputFolders => _inputFolders.Count;

        public IReadOnlyCollection<string> InputFolders => _inputFolders.ToArray();

        public string OutputFolder { get; }
    }
}
