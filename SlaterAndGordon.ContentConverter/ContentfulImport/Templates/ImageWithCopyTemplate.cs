﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public class ImageWithCopyTemplate : ContentfulImportTemplate
    {
        public string SectionHeading { get; set; }
        public string Heading { get; set; }
        public string Copy { get; set; }
        public string ImageId { get; set; }

        public override string GetTemplate()
        {
            return $@"
    {{
            ""sys"": {{
            ""space"": {{
                ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""Space"",
                    ""id"": ""o8luwa28k6k2""
                }}
            }},
            ""id"": ""{EntryId}"",
            ""type"": ""Entry"",
            ""environment"": {{
                ""sys"": {{
                    ""id"": ""Integration"",
                    ""type"": ""Link"",
                    ""linkType"": ""Environment""
                }}
            }},
            ""createdBy"": {{
                ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""User"",
                    ""id"": ""1zwTxg4NMKfJMdQD0SGIqM""
                }}
            }},
                ""publishedBy"": {{
                  ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""User"",
                    ""id"": ""5I8mIT4uYY9WeP7naBzNtm""
                  }}
                }},
                ""publishedVersion"": 1,
                ""publishedCounter"": 1,
                ""version"": 2,
            ""contentType"": {{
                ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""ContentType"",
                    ""id"": ""imageWithCopy""
                }}
            }}
        }},
        ""fields"": {{
            ""sectionHeading"": {{
                ""en-US"": ""{SectionHeading}""
            }},
            ""heading"": {{
                ""en-US"": ""{Heading}""
            }},
            ""copy"": {{
                ""en-US"": ""{Copy}""
            }},
            ""imageId"": {{
                ""en-US"": ""{ImageId}""
            }}
        }}
    }}";
        }
    }
}
