﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public interface IContentfulImportTemplate
    {
        string EntryId { get; }
        string  GetTemplate();
    }

    public abstract class ContentfulImportTemplate : IContentfulImportTemplate
    {
        public string EntryId { get; protected set;  } = GenerateEntryId();
        public abstract string  GetTemplate();

        private static Random _random = new Random();

        public List<BasePageTemplate> Children = new List<BasePageTemplate>();

        private static string GenerateEntryId()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxz0123456789";
            return new string(Enumerable.Repeat(chars, 22)
                .Select(s => s[_random.Next(s.Length)]).ToArray());
            //return "5I8mIT4uYY9WeP7naBzNtm";
        }
    }
}
