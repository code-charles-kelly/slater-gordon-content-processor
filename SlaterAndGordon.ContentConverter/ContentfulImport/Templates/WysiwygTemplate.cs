﻿using System;
using System.Collections.Generic;
using System.Text;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public class WysiwygTemplate
    {
        public Wysiwyg Wysiwyg { get; set; }

        public string GetTemplate()
        {
            string template = $@"
            {{
                ""data"": {{
                }},
                ""content"": [";

            foreach (var wysiwygTopElement in Wysiwyg.TopElements)
            {
                if (wysiwygTopElement is H2Element h2Element)
                {
                    template += MapH2Template(h2Element);
                }
                else if (wysiwygTopElement is H3Element h3Element)
                {
                    template += MapH3Template(h3Element);
                }
                else if (wysiwygTopElement is ButtonElement buttonElement)
                {
                    template += MapButton(buttonElement);
                }
                else if (wysiwygTopElement is PElement pElement)
                {
                    template += MapParagraph(pElement);
                }
                else if (wysiwygTopElement is ListHtmlElement listHtmlElement)
                {
                    template += MapList(listHtmlElement);
                }
            }

            template += $@"
                ],
                ""nodeType"": ""document""
            }}
";
            return template;
        }

        private string MapList(ListHtmlElement listHtmlElement)
        {
            var listTemplate = "";

            listTemplate += $@"
{{
                                ""data"": {{
                                }},
                                ""content"": [                                  
";


            foreach (var listElement in listHtmlElement.ListElements)
            {
                listTemplate += $@"
                                    {{
                                    ""data"": {{
                                    }},
                                    ""content"": [                                     

";
                foreach (var listElementChild in listElement)
                {
                    if (listElementChild is PElement pElement && pElement.Children.Count > 0)
                    {
                        listTemplate += MapParagraph(pElement);
                    }
                    else if (listElementChild is ListHtmlElement embeddedList)
                    {
                        listTemplate += MapList(embeddedList);
                    }
                }
                if (listElement.Count > 1)
                {
                    var test = "";
                }

                listTemplate += $@"
                                    ],
                                    ""nodeType"": ""list-item""
                                  }},
";
            }

            listTemplate += $@"
                                ],
                                ""nodeType"": ""unordered-list""
                              }},
";

            return listTemplate;
        }

        private string MapParagraph(PElement pElement, bool isListElement = false)
        {
            var paragraphTemplate = "";

            if (!isListElement)
            {
                paragraphTemplate += $@"
{{
                ""data"": {{
                }},
                ""content"": [
";
            }


            foreach (var pElementChild in pElement.Children)
            {
                if (pElementChild is TextElement textElement)
                {
                    paragraphTemplate += MapText(textElement);
                }
                else if (pElementChild is AElement aElement)
                {
                    paragraphTemplate += MapHyperlink(aElement);
                }
            }

            //paragraphTemplate += MapText(new TextElement {IsBold = true, Text = "test"});

            if (!isListElement)
            {
                paragraphTemplate += $@"
                ],
                ""nodeType"": ""paragraph""
              }},
";
            }

            return paragraphTemplate;
        }

        private string MapHyperlink(AElement aElement)
        {
            return $@"
                    {{
                        ""data"": {{
                            ""uri"": ""{aElement.Href}""
                        }},
                        ""content"": [
                            {{
                            ""data"": {{
                            }},
                            ""marks"": [
                            ],
                            ""value"": ""{aElement.Text?.Replace(@"""", @"\""") ?? ""}"",
                            ""nodeType"": ""text""
                            }}
                        ],
                        ""nodeType"": ""hyperlink""
                    }},
";
        }

        private string MapText(TextElement textElement)
        {
            var marks = "";

            if (textElement.IsBold)
            {
                marks += @"{
                ""type"": ""bold""
            }";
            }

            return $@"
            {{
                ""data"": {{
                }},
                ""marks"": [
                    {marks}
                ],
                ""value"": ""{textElement.Text?.Replace(@"""",@"\""").Replace("{{{meta.phonenumber.contentLink}}}","[[callbacknumber]]") ?? ""}"",
                ""nodeType"": ""text""
            }},
";
        }

        private string MapButton(ButtonElement buttonElement)
        {
            return $@"
            {{
                ""data"": {{
                }},
                ""content"": [
                    {{
                        ""data"": {{
                        }},
                        ""marks"": [
                        ],
                        ""value"": ""[button]"",
                        ""nodeType"": ""text""
                    }},
                    {{
                        ""data"": {{
                            ""uri"": ""{buttonElement.Link.Href}""
                        }},
                        ""content"": [
                            {{
                            ""data"": {{
                            }},
                            ""marks"": [
                            ],
                            ""value"": ""{buttonElement.Link.Text}"",
                            ""nodeType"": ""text""
                            }}
                        ],
                        ""nodeType"": ""hyperlink""
                    }}
                ],
                ""nodeType"": ""paragraph""
            }},
";
        }

        private string MapH3Template(H3Element h3Element)
        {
            var h3Template = "";
            h3Template += $@"

            {{
                ""data"": {{
                }},
                ""content"": [
                    {{
                        ""data"": {{
                        }},
                        ""marks"": [
                        ],
                        ""value"": ""{h3Element.Text}"",
                        ""nodeType"": ""text""
                    }}                    
                ],
                ""nodeType"": ""heading-3""
            }},
";
            return h3Template;
        }

        private static string MapH2Template(H2Element h2Element)
        {
            var h2Template = "";
            var text = h2Element.Text;
            if (h2Element.IsFaqQuestion) text = "[h2faq]" + text;
            h2Template += $@"

            {{
                ""data"": {{
                }},
                ""content"": [
                    {{
                        ""data"": {{
                        }},
                        ""marks"": [
                        ],
                        ""value"": ""{text}"",
                        ""nodeType"": ""text""
                    }}                    
                ],
                ""nodeType"": ""heading-2""
            }},
";
            return h2Template;
        }
    }
}