﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public class BasePageTemplate : ContentfulImportTemplate
    {
        public string Title { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Parent { get; set; }
        public string PageType { get; set; }
        public string Description { get; set; }
        public ImageWithCopyTemplate Hero { get; set; }
        public TwoPanelCtaTemplate TwoPanelCta { get; set; }
        public ImageWithCopyTemplate ImageWithCopy1 { get; set; }
        public List<OnwardJourneyTemplate> OnwardJourneys1 { get; set; } = new List<OnwardJourneyTemplate>();
        public ImageWithCopyTemplate ImageWithCopy2 { get; set; }
        public List<OnwardJourneyTemplate> OnwardJourneys2 { get; set; } = new List<OnwardJourneyTemplate>();
        public string Content { get; set; }
        public string SideLinkHeading { get; set; }
        public List<LinkTemplate> SideLinks { get; set; } = new List<LinkTemplate>();
        public VideoTemplate VideoText { get; set; }
        public List<QuoteTemplate> Quotes { get; set; } = new List<QuoteTemplate>();
        public List<CaseStudyTemplate> CaseStudies { get; set; } = new List<CaseStudyTemplate>();

        public override string GetTemplate()
        {
            return $@"
        ""title"": {{
          ""en-US"": ""{Title}""
        }},
        ""name"": {{
          ""en-US"": ""{Name}""
        }},
        ""slug"": {{
          ""en-US"": ""{Slug}""
        }},
        ""description"": {{
          ""en-US"": ""{Description}""
        }},
        {GetLinkField(Hero, "hero")}
        {GetLinkField(TwoPanelCta, "twoPanelCta")}
        {GetLinkField(ImageWithCopy1, "imageWithCopy1")}
        ""onwardJourneys1"": {{
          ""en-US"": [
{GetEntryLinksTemplate(OnwardJourneys1.ToList<IContentfulImportTemplate>())}
          ]
        }},
        {GetLinkField(ImageWithCopy2, "imageWithCopy2")}
        ""onwardJourneys2"": {{
          ""en-US"": [
{GetEntryLinksTemplate(OnwardJourneys2.ToList<IContentfulImportTemplate>())}
]
        }},
        ""content"": {{
          ""en-US"": {Content}
        }},
        ""sideLinkHeading"": {{
          ""en-US"": ""{SideLinkHeading}""
        }},
        ""sideLinks"": {{
          ""en-US"": [
{GetEntryLinksTemplate(SideLinks.ToList<IContentfulImportTemplate>())}
]
        }},
        {GetLinkField(VideoText, "videoText")}
        ""quotes"": {{
          ""en-US"": [
{GetEntryLinksTemplate(Quotes.ToList<IContentfulImportTemplate>())}
]
        }},
        ""caseStudies"": {{
          ""en-US"": [
{GetEntryLinksTemplate(CaseStudies.ToList<IContentfulImportTemplate>())}
]
        }}
    }}

";
        }

        private string GetLinkField(ContentfulImportTemplate template, string fieldName)
        {
            if (template == null)
            {
                return string.Empty;
            }

            return $@"
""{fieldName}"": {{
          ""en-US"": {{
            ""sys"": {{
              ""type"": ""Link"",
              ""linkType"": ""Entry"",
              ""id"": ""{template.EntryId}""
            }}
          }}
        }},
";
        }

        public string GetEntryLinksTemplate(List<IContentfulImportTemplate> templates)
        {
            var links = "";

            for (var i = 0; i < templates.Count; i++)
            {
                var template = templates[i];
                var endComma = i != templates.Count - 1 ? "," : "";
                links += $@"
{{
              ""sys"": {{
                ""type"": ""Link"",
                ""linkType"": ""Entry"",
                ""id"": ""{template.EntryId}""
              }}
            }}{endComma}
";
            }

            return links;
        }
    }
}

//{{
//    ""data"": {{
//    }},
//    ""content"": [{Content}],
//    ""nodeType"": ""document""
//}}