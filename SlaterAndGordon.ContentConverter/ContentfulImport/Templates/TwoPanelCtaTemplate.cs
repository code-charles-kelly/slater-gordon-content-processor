﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public class TwoPanelCtaTemplate: ContentfulImportTemplate
    {
        public string MainHeading { get; set; }
        public string Copy { get; set; }
        public string CtaHeading { get; set; }
        public string CtaLinkText { get; set; }
        public string CtaLinkUrl { get; set; }

        public override string GetTemplate()
        {
            return $@"
    {{
      ""sys"": {{
        ""space"": {{
          ""sys"": {{
                ""type"": ""Link"",
            ""linkType"": ""Space"",
            ""id"": ""o8luwa28k6k2""
          }}
        }},
        ""id"": ""{EntryId}"",
        ""type"": ""Entry"",
        ""environment"": {{
          ""sys"": {{
            ""id"": ""Integration"",
            ""type"": ""Link"",
            ""linkType"": ""Environment""
          }}
        }},
        ""createdBy"": {{
          ""sys"": {{
            ""type"": ""Link"",
            ""linkType"": ""User"",
            ""id"": ""5I8mIT4uYY9WeP7naBzNtm""
          }}
        }},
                ""publishedBy"": {{
                  ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""User"",
                    ""id"": ""5I8mIT4uYY9WeP7naBzNtm""
                  }}
                }},
        ""publishedVersion"": 1,
        ""publishedCounter"": 1,
        ""version"": 2,
        ""contentType"": {{
          ""sys"": {{
            ""type"": ""Link"",
            ""linkType"": ""ContentType"",
            ""id"": ""twoPanelCta""
          }}
        }}
      }},
      ""fields"": {{
        ""mainHeading"": {{
          ""en-US"": ""{MainHeading}""
        }},
        ""copy"": {{
          ""en-US"": {Copy}
        }},
        ""ctaHeading"": {{
          ""en-US"": ""{CtaHeading}""
        }},
        ""ctaLinkText"": {{
          ""en-US"": ""{CtaLinkText}""
        }},
        ""ctaLinkUrl"": {{
          ""en-US"": ""{CtaLinkUrl}""
        }}
      }}
    }}
";
        }
    }
}

/*
 * {{
          ""en-US"": {{
            ""data"": {{
            }},
            ""content"": [
              {{
                ""data"": {{
                }},
                ""content"": [
                  {{
                    ""data"": {{
                    }},
                    ""marks"": [
                    ],
                    ""value"": ""first line\nunder first line"",
                    ""nodeType"": ""text""
                  }}
                ],
                ""nodeType"": ""paragraph""
              }},
              {{
                ""data"": {{
                }},
                ""content"": [
                  {{
                    ""data"": {{
                    }},
                    ""marks"": [
                    ],
                    ""value"": ""second line"",
                    ""nodeType"": ""text""
                  }}
                ],
                ""nodeType"": ""paragraph""
              }}
            ],
            ""nodeType"": ""document""
          }}
        }}
 */
