﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    public class CaseStudyTemplate: ContentfulImportTemplate
    {
        public string Heading { get; set; }
        public string Url { get; set; }
        public string Copy { get; set; }

        public override string GetTemplate()
        {
            return $@"
    {{
                ""sys"": {{
                ""space"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""Space"",
                        ""id"": ""o8luwa28k6k2""
                    }}
                }},
                ""id"": ""{EntryId}"",
                ""type"": ""Entry"",
                ""environment"": {{
                    ""sys"": {{
                        ""id"": ""Integration"",
                        ""type"": ""Link"",
                        ""linkType"": ""Environment""
                    }}
                }},
                ""createdBy"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""User"",
                        ""id"": ""{EntryId}""
                    }}
                }},
                ""publishedVersion"": 1,
                ""publishedCounter"": 1,
                ""version"": 2,
                ""publishedBy"": {{
                  ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""User"",
                    ""id"": ""5I8mIT4uYY9WeP7naBzNtm""
                  }}
                }},
                ""contentType"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""ContentType"",
                        ""id"": ""caseStudy""
                    }}
                }}
            }},
            ""fields"": {{
                ""heading"": {{
                    ""en-US"": ""{Heading}""
                }},
                ""url"": {{
                    ""en-US"": ""{Url}""
                }},
                ""copy"": {{
                    ""en-US"": ""{Copy}""
                }}
            }}
        }}
";
        }
    }
}
