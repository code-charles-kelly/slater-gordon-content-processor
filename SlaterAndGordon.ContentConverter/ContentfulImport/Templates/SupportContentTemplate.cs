﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlaterAndGordon.ContentConverter.ContentfulImport.Templates
{
    class SupportContentTemplate: ContentfulImportTemplate
    {
        public BasePageTemplate BasePageTemplate { get; }

        public SupportContentTemplate(BasePageTemplate basePageTemplate)
        {
            BasePageTemplate = basePageTemplate;
            EntryId = basePageTemplate.EntryId;
        }

        public override string GetTemplate()
        {
            string template = "";

            template += BasePageTemplate.Hero == null ? "" : BasePageTemplate.Hero.GetTemplate() + ",";
            template += BasePageTemplate.TwoPanelCta == null ? "" : BasePageTemplate.TwoPanelCta.GetTemplate() + ",";
            template += BasePageTemplate.ImageWithCopy1 == null ? "" : BasePageTemplate.ImageWithCopy1.GetTemplate() + ",";
            foreach (var onwardJourneyTemplate in BasePageTemplate.OnwardJourneys1)
            {
                template += onwardJourneyTemplate.GetTemplate() + ",";
            }

            template += BasePageTemplate.ImageWithCopy2 == null ? "" : BasePageTemplate.ImageWithCopy2.GetTemplate() + ",";
            foreach (var onwardJourneyTemplate in BasePageTemplate.OnwardJourneys2)
            {
                template += onwardJourneyTemplate.GetTemplate() + ",";
            }

            foreach (var sideLink in BasePageTemplate.SideLinks)
            {
                template += sideLink.GetTemplate() + ",";
            }

            template += BasePageTemplate.VideoText == null ? "" : BasePageTemplate.VideoText.GetTemplate() + ",";
            foreach (var quoteTemplate in BasePageTemplate.Quotes)
            {
                template += quoteTemplate.GetTemplate() + ",";
            }
            foreach (var caseStudyTemplate in BasePageTemplate.CaseStudies)
            {
                template += caseStudyTemplate.GetTemplate() + ",";
            }

            template += $@"
 {{
                ""sys"": {{
                ""space"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""Space"",
                        ""id"": ""o8luwa28k6k2""
                    }}
                }},
                ""id"": ""{EntryId}"",
                ""type"": ""Entry"",
                ""environment"": {{
                    ""sys"": {{
                        ""id"": ""Integration"",
                        ""type"": ""Link"",
                        ""linkType"": ""Environment""
                    }}
                }},
                ""createdBy"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""User"",
                        ""id"": ""1zwTxg4NMKfJMdQD0SGIqM""
                    }}
                }},
                ""publishedBy"": {{
                  ""sys"": {{
                    ""type"": ""Link"",
                    ""linkType"": ""User"",
                    ""id"": ""5I8mIT4uYY9WeP7naBzNtm""
                  }}
                }},
                ""publishedVersion"": 1,
                ""publishedCounter"": 1,
                ""version"": 2,
                ""contentType"": {{
                    ""sys"": {{
                        ""type"": ""Link"",
                        ""linkType"": ""ContentType"",
                        ""id"": ""supportContent""
                    }}
                }}
            }},
      ""fields"": {{
";

            template += BasePageTemplate.GetTemplate();

            template += @"
        }";

            return template;
        }
    }
}
