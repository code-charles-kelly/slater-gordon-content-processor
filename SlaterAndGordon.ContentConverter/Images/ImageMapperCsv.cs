﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CsvHelper;

namespace SlaterAndGordon.ContentConverter.Images
{
    public class ImageMapperCsv
    {
        private readonly List<ImageMapperCsvRecord> _imageMapperCsvRecords;

        public ImageMapperCsv(string csv)
        {
            var csvStringReader = new StringReader(csv);

            using var csvReader = new CsvReader(csvStringReader);

            var csvRecords = new List<ImageMapperCsvRecord>();

            if (!csvReader.Read())
            {
                _imageMapperCsvRecords = csvRecords;
                return;
            }

            csvReader.ReadHeader();

            var urlField = -1;
            var image1Field = -1;
            var image2Field = -1;
            var image3Field = -1;
            var pageTypeField = -1;

            for (int i = 0; i < csvReader.Context.HeaderRecord.Length; i++)
            {
                if (urlField == -1 && csvReader.Context.HeaderRecord[i] == "Suggested URL")
                {
                    urlField = i;
                }
                else if (csvReader.Context.HeaderRecord[i] == "New URL")
                {
                    urlField = i;
                }
                else if (image1Field == -1 && (csvReader.Context.HeaderRecord[i] == "Image 1" ||
                                               csvReader.Context.HeaderRecord[i] == "Img 1" ||
                                               csvReader.Context.HeaderRecord[i] == "Image Reference" ||
                                               csvReader.Context.HeaderRecord[i] == "Image Allocation " ||
                                               csvReader.Context.HeaderRecord[i] == "Image #1"))
                {
                    image1Field = i;
                }
                else if (image2Field == -1 && (csvReader.Context.HeaderRecord[i] == "Image 2" ||
                                               csvReader.Context.HeaderRecord[i] == "Img 2" ||
                                               csvReader.Context.HeaderRecord[i] == "Image #2"))
                {
                    image2Field = i;
                }
                else if (image3Field == -1 && csvReader.Context.HeaderRecord[i] == "Img 3")
                {
                    image3Field = i;
                }
                else if (pageTypeField == -1 && (csvReader.Context.HeaderRecord[i].ToLower() == "page type" ||
                                                 csvReader.Context.HeaderRecord[i].ToLower() == "top level")) //Top Level
                {
                    pageTypeField = i;
                }
            }

            if (urlField == -1 || image1Field == -1 || image2Field == -1)
            {
                _imageMapperCsvRecords = csvRecords;
                return;
            }

            while (csvReader.Read())
            {
                var urlComponents = csvReader.GetField(urlField).Replace("https://www.slatergordon.co.uk", String.Empty)
                    .Split('/').Where(x => x.Trim() != string.Empty).TakeLast(2).ToArray();

                var slug = string.Empty;
                var parent = string.Empty;

                if (urlComponents.Length == 2)
                {
                    slug = urlComponents[1];
                    parent = urlComponents[0];
                }
                else if (urlComponents.Length == 1)
                {
                    slug = urlComponents[0];
                }

                var newRecord = new ImageMapperCsvRecord(
                    csvReader.GetField(image1Field),
                    csvReader.GetField(image2Field),
                    GetImage3Name(image3Field, csvReader),
                    parent,
                    slug,
                    pageTypeField == -1 ? "" : csvReader.GetField(pageTypeField));

                csvRecords.Add(newRecord);
            }

            _imageMapperCsvRecords = csvRecords;
        }

        private static string GetImage3Name(int image3Field, CsvReader csvReader)
        {
            var image3Name = image3Field == -1 ? string.Empty : csvReader.GetField(image3Field);
            return image3Name;
        }

        public IReadOnlyCollection<ImageMapperCsvRecord> Records => _imageMapperCsvRecords.ToArray();
    }

    public class ImageMapperCsvRecord
    {
        public ImageMapperCsvRecord(string image1Name, string image2Name, string image3Name, string parent, string slug,
            string pageType)
        {
            Image1Name = image1Name;
            Image2Name = image2Name;
            Image3Name = image3Name;
            Slug = slug;
            Parent = parent;
            PageType = pageType;
        }

        public string Image1Name { get; }
        public string Image2Name { get; }
        public string Image3Name { get; }

        public string PageType { get; }
        public string Slug { get; }
        public string Parent { get; }
    }
}