﻿using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.ComponentMappers
{
    public class WysiwygToHtmlMapper
    {
        private bool _inBlock = false;

        public string MapToHtml(Wysiwyg wysiwyg)
        {
            _inBlock = false;

            int indentationLevel = 0;

            string html = "";

            foreach (var topElement in wysiwyg.TopElements)
            {
                if (topElement is H2Element h2)
                {
                    if (_inBlock)
                    {
                        html += AddBlockEnd();
                        html += "\r\n";
                        indentationLevel--;
                    }
                    html += AddBlockStart();
                    _inBlock = true;
                    indentationLevel++;
                    html += AddIndentationSpaces(indentationLevel) + AddH2(h2.Text);
                }

                if (!_inBlock)
                {
                    _inBlock = true;
                    html += AddBlockStart();
                    indentationLevel++;
                }

                if (topElement is H3Element h3Element)
                {
                    html += AddIndentationSpaces(indentationLevel) + AddH3Element(h3Element);
                }
                else if (topElement is PElement pElement)
                {
                    html += AddIndentationSpaces(indentationLevel) + AddParagraph(pElement);
                }
                else if (topElement is ListHtmlElement listElement)
                {
                    html +=  AddList(listElement, indentationLevel);
                }
                else if (topElement is ButtonElement buttonElement)
                {
                    html += AddIndentationSpaces(indentationLevel) + AddButton(buttonElement);
                }
            }

            if (_inBlock)
            {
                html += AddBlockEnd();
            }

            return html;
        }

        private string AddH3Element(H3Element h3Element)
        {
            return $"<h3 class=\"text text--heading text--medium\">{h3Element.Text}</h3>\r\n";
        }

        private string AddButton(ButtonElement buttonElement)
        {
            var buttonHtml = "";

            if (buttonElement.Link.IsRelative)
            {
                buttonHtml +=
                    $"<a href=\"{buttonElement.Link.Href}\" class=\"button button--secondary\">{buttonElement.Link.Text}</a>\r\n" +
                    "</div>";
                _inBlock = false;
            }
            else
            {
                buttonHtml +=
                    $"<a href=\"{buttonElement.Link.Href}\" class=\"button button--secondary\" target=\"_blank\">{buttonElement.Link.Text}</a>\r\n" +
                    "</div>";
                _inBlock = false;
            }

            return buttonHtml;
        }

        private string AddList(ListHtmlElement listHtmlElement, int indentationLevel)
        {
            var listHtml = "";
            listHtml += AddIndentationSpaces(indentationLevel) + "<ul class=\"bullet-list\">\r\n";

            indentationLevel++;

            foreach (var elementListElement in listHtmlElement.ListElements)
            {
                listHtml += AddIndentationSpaces(indentationLevel) + "<li class=\"bullet-list__item\">\r\n";
                indentationLevel++;

                var innerListText = string.Empty;

                foreach (var innerListElement in elementListElement)
                {
                    if (innerListElement is ListHtmlElement listElement)
                    {
                        innerListText += AddList(listElement, indentationLevel);
                    }
                    else if (innerListElement is PElement elementPElement && elementPElement.Children.Count > 0)
                    {
                        innerListText += AddIndentationSpaces(indentationLevel) + AddInnerParagraph(innerListElement as PElement) + "\r\n";
                    }
                    
                }
                if (innerListText.Contains("<a href=\"http://www.hse.gov.uk/asbestos/risk-assessments.htm\" class=\"link link--inline\" target=\"_blank\"><span class=\"link__text\"></span></a>where workers weren't trained to recognise and work safely with asbestos"))
                {
                    var test = "sgfdsg";
                }
                listHtml += innerListText;
                indentationLevel--;
                listHtml += AddIndentationSpaces(indentationLevel) + "</li>\r\n";
            }

            indentationLevel--;

            listHtml += AddIndentationSpaces(indentationLevel) + "</ul>\r\n";

            return listHtml;
        }

        private string AddParagraph(PElement paragraph)
        {
            var paragraphHtml = "";

            paragraphHtml += "<p class=\"text\">";
            
            paragraphHtml += AddInnerParagraph(paragraph);

            paragraphHtml += "</p>\r\n";

            return paragraphHtml;
        }

        private string AddInnerParagraph(PElement paragraph)
        {
            var paragraphHtml = "";

            foreach (var paragraphChild in paragraph.Children)
            {
                if (paragraphChild is TextElement textElement)
                {
                    if (textElement.IsBold)
                    {
                        paragraphHtml += $"<span class=\"text--bold\">{textElement.Text}</span>";
                    }
                    else
                    {
                        paragraphHtml += textElement.Text;
                    }

                }
                else if (paragraphChild is AElement aElement)
                {
                    if (aElement.IsRelative)
                    {
                        paragraphHtml +=
                            $"<a href=\"{aElement.Href}\" class=\"link link--inline\"><span class=\"link__text\">{aElement.Text}</span></a>";
                    }
                    else
                    {
                        paragraphHtml +=
                            $"<a href=\"{aElement.Href}\" class=\"link link--inline\" target=\"_blank\"><span class=\"link__text\">{aElement.Text}</span></a>";
                    }
                }
            }

            return paragraphHtml;
        }

        private string AddBlockStart()
        {
            return "<div class=\"inner-block\">\r\n";
        }

        private string AddBlockEnd()
        {
            return "</div>\r\n";
        }

        private string AddH2(string text)
        {
            return $"<h2 class=\"text text--heading text--large\">{text}</h2>\r\n";
        }

        private string AddIndentationSpaces(int indentationLevel)
        {
            string spaces = "";

            for (int i = 0; i < indentationLevel; i++)
            {
                spaces += "  ";
            }

            return spaces;
        }
    }
}
