﻿using SlaterAndGordon.ContentConverter.Components;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.ComponentMappers
{
    public class WysiwygToFaqsMapper
    {
        public FaqsComponent MapToFaqs(Wysiwyg wysiwyg)
        {
            var faqsComponent = new FaqsComponent();
            Faq currentFaq = null;
            var inFaq = false;

            foreach (var topElement in wysiwyg.TopElements)
            {
                currentFaq = ProcessTopElement(topElement, currentFaq, faqsComponent, ref inFaq);
            }

            return faqsComponent;
        }

        private static Faq ProcessTopElement(IHtmlElement topElement, Faq currentFaq, FaqsComponent faqsComponent,
            ref bool inFaq)
        {
            if (topElement is H2Element questionElement && questionElement.IsFaqQuestion)
            {
                currentFaq = new Faq {Question = questionElement.Text, Answer = string.Empty};
                faqsComponent.Faqs.Add(currentFaq);
                inFaq = true;
            }
            else if (topElement is H2Element h2 && !h2.IsFaqQuestion)
            {
                inFaq = false;
            }
            else if (inFaq && topElement is PElement p)
            {
                var paragraphText = GetParagraphInnerHtml(p);

                currentFaq.Answer += $"<p>{paragraphText}</p>";
            }
            else if (inFaq && topElement is ListHtmlElement list)
            {
                var listText = string.Empty;
                foreach (var listElement in list.ListElements)
                {
                    var innerListText = string.Empty;

                    foreach (var innerListElement in listElement)
                    {
                        if (innerListElement is PElement pElement)
                        {
                            innerListText += GetParagraphInnerHtml(pElement);
                        }
                        
                    }
                    listText += $"<li>{innerListText}</li>";
                }

                currentFaq.Answer += $"<ul>{listText}</ul>";
            }

            return currentFaq;
        }

        private static string GetParagraphInnerHtml(PElement p)
        {
            var paragraphText = string.Empty;
            foreach (var child in p.Children)
            {
                if (child is TextElement textElement)
                {
                    if (textElement.IsBold)
                    {
                        paragraphText += AttachBoldHtml(textElement);
                    }
                    else
                    {
                        paragraphText += textElement.Text;
                    }
                }
                else if (child is AElement aElement)
                {
                    paragraphText += AttachLinkHtml(aElement);
                }
            }

            return paragraphText;
        }

        private static string AttachLinkHtml(AElement aElement)
        {
            var href = aElement.Href;
            if (aElement.IsRelative) href = "https://www.slatergordon.co.uk" + href;
            return $"<a href=\"{href}\">{aElement.Text}</a>";
        }

        private static string AttachBoldHtml(TextElement textElement)
        {
            return $"<strong>{textElement.Text}</strong>";
        }
    }
}
