﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ExCSS;
using HtmlAgilityPack;
using Newtonsoft.Json;
using SlaterAndGordon.ContentConverter.ComponentMappers;
using SlaterAndGordon.ContentConverter.ContentfulImport.Templates;
using SlaterAndGordon.ContentConverter.Images;
using SlaterAndGordon.ContentConverter.Input;
using SlaterAndGordon.ContentConverter.Output;
using SlaterAndGordon.ContentConverter.SectionMappers;
using SlaterAndGordon.ContentConverter.Sections;
using Section = SlaterAndGordon.ContentConverter.Sections.Section;

/*
 "C:\Users\charleskelly\Documents\S&G-latest\S_G\\" "C:\Users\charleskelly\Documents\S&G-latest\Wills content\\" "C:\Users\charleskelly\Documents\S&G-latest\Property content\\" "C:\Users\charleskelly\Documents\S&G-latest\Employment Content\\" "C:\Users\charleskelly\Documents\S&G-latest\Family content\\" "C:\Users\charleskelly\source\repos\slatergordon.co.uk\website\data\product-template\\"
 */

namespace SlaterAndGordon.ContentConverter
{
    public class Program
    {
        private static Stylesheet _stylesheet;
        private static HtmlDocument _htmlDocument;
        private static HtmlNode[] _baseChildren;
        private static List<string> _processedFileNames = new List<string>();
        private static string _currentLoadedFilePath = "";
        private static ImageMapperCsv _imageMapperCsv;
        private static List<string> missingAltText = new List<string>();

        private static List<BasePageTemplate> basePageTemplates = new List<BasePageTemplate>();

        private static List<string> invalidIcons = new List<string>();

        private static int AllChildren = 0;

        private static string outputTemplate = "";

        public static void Main(string[] args)
        {
            _processedFileNames = new List<string>();
            basePageTemplates = new List<BasePageTemplate>();
            outputTemplate = string.Empty;
            invalidIcons = new List<string>();

            var baseDirectoryPath = "C:\\Users\\charleskelly\\Documents\\S&G-latest\\S_G\\";
            var outputDirectory =
                "C:\\Users\\charleskelly\\source\\repos\\slatergordon.co.uk\\website\\data\\product-template\\";


            var applicationArguments = new CommandLineArguments(args);

            if (applicationArguments.ArgumentsSet)
            {
                baseDirectoryPath = applicationArguments.InputFolders.First();
                outputDirectory = applicationArguments.OutputFolder;
            }

            //var testDoc = "C:\\Users\\charleskelly\\Documents\\S&G-latest\\S_G\\TEMP_LEGAL_SEO_Defective Products\\TEMP_LEGAL - SEO- Electrical product compensation claim.docx";
            ////var testDoc = "C:\\Users\\charleskelly\\Documents\\Hello.docx";

            //var doc = DocX.Load(testDoc);


            //var textReader = new StreamReader(System.IO.File.OpenRead(imageMappingFile));
            //var csvParser = new CsvParser(textReader);

            //var imageMapper = new ImageMapper();

            //string[] record = csvParser.Read();

            //while (record != null)
            //{
            //    record = csvParser.Read();
            //    //imageMapper.MapRecord(record[0],record[10], record[11], record[12]);
            //}


            if (applicationArguments.ArgumentsSet)
            {
                foreach (var inputFolder in applicationArguments.InputFolders)
                {
                    var imageMappingFile = inputFolder + "ImageMapping.csv";
                    var imageMappingText = System.IO.File.ReadAllText(imageMappingFile);
                    _imageMapperCsv = new ImageMapperCsv(imageMappingText);
                    ProcessInputFolder(inputFolder, outputDirectory);
                }
            }
            else
            {
                var imageMappingFile = baseDirectoryPath + "ImageMapping.csv";
                var imageMappingText = System.IO.File.ReadAllText(imageMappingFile);
                _imageMapperCsv = new ImageMapperCsv(imageMappingText);
                ProcessInputFolder(baseDirectoryPath, outputDirectory);
            }

            var rootPages = new List<BasePageTemplate>();

            AllChildren = 0;

            foreach (var basePageTemplate in basePageTemplates)
            {
                if (string.IsNullOrEmpty(basePageTemplate.Parent))
                {
                    rootPages.Add(basePageTemplate);
                    AllChildren++;
                }
            }

            AddBasePageChildren(rootPages);

            var serviceOverviewPages = new List<ServiceOverviewTemplate>();

            foreach (var rootPage in rootPages)
            {
                var newServiceOverviewPage = new ServiceOverviewTemplate(rootPage);
                serviceOverviewPages.Add(newServiceOverviewPage);

                var qualifyingServiceDetailLevel1Templates = rootPage.Children
                    .Where(x => x.Children.Count > 0 || !x.PageType.ToLower().Contains("support")).ToList();
                var qualifyingSupportContentTemplates = rootPage.Children
                    .Where(x => x.Children.Count == 0 && x.PageType.ToLower().Contains("support")).ToList();

                var serviceDetailLevel1Templates = CreateServiceDetailLevel1(qualifyingServiceDetailLevel1Templates);
                var supportContentTemplates = CreateSupportContent(qualifyingSupportContentTemplates);

                if (qualifyingServiceDetailLevel1Templates.Count + qualifyingSupportContentTemplates.Count !=
                    rootPage.Children.Count)
                {
                    throw new Exception("children mismatch!");
                }


                newServiceOverviewPage.ServiceDetailLevel1Templates.AddRange(serviceDetailLevel1Templates);
                newServiceOverviewPage.SupportContentTemplates.AddRange(supportContentTemplates);

                outputTemplate += newServiceOverviewPage.GetTemplate() + ",";
            }

            outputTemplate = "{entries:[" + outputTemplate.Substring(0, outputTemplate.Length - 1) + "]}";

            var outputTemplateJson = JsonConvert.DeserializeObject(outputTemplate);

            outputTemplate = JsonConvert.SerializeObject(outputTemplateJson, Formatting.Indented);

            System.IO.File.WriteAllText(outputDirectory + "contentful-import.json", outputTemplate);


            //LoadFile("C:\\Users\\charleskelly\\Documents\\S&G-latest\\S_G\\TEMP_LEGAL - ID\\TEMP_LEGAL - SEO - ID.html");

            //MapAndOutputFiles(outputDirectory);
        }

        private static List<ServiceDetailLevel1Template> CreateServiceDetailLevel1(List<BasePageTemplate> children)
        {
            var serviceDetailLevel1Templates = new List<ServiceDetailLevel1Template>();

            foreach (var child in children)
            {
                var newServiceDetailLevel1 = new ServiceDetailLevel1Template(child);
                serviceDetailLevel1Templates.Add(newServiceDetailLevel1);

                var qualifyingServiceDetailLevel2Templates = child.Children
                    .Where(x => x.Children.Count > 0 || !x.PageType.ToLower().Contains("support")).ToList();
                var qualifyingSupportContentTemplates = child.Children
                    .Where(x => x.Children.Count == 0 && x.PageType.ToLower().Contains("support")).ToList();

                var serviceDetailLevel2Templates = CreateServiceDetailLevel2(qualifyingServiceDetailLevel2Templates);
                var supportContentTemplates = CreateSupportContent(qualifyingSupportContentTemplates);

                newServiceDetailLevel1.ServiceDetailLevel2Templates.AddRange(serviceDetailLevel2Templates);
                newServiceDetailLevel1.SupportContentTemplates.AddRange(supportContentTemplates);

                if (qualifyingServiceDetailLevel2Templates.Count + qualifyingSupportContentTemplates.Count !=
                    child.Children.Count)
                {
                    throw new Exception("children mismatch!");
                }

                outputTemplate += newServiceDetailLevel1.GetTemplate() + ",";
            }

            return serviceDetailLevel1Templates;
        }

        private static List<ServiceDetailLevel2Template> CreateServiceDetailLevel2(List<BasePageTemplate> children)
        {
            var serviceDetailLevel2Templates = new List<ServiceDetailLevel2Template>();

            foreach (var child in children)
            {
                var newServiceDetailLevel2 = new ServiceDetailLevel2Template(child);
                serviceDetailLevel2Templates.Add(newServiceDetailLevel2);

                newServiceDetailLevel2.SupportContentTemplates.AddRange(CreateSupportContent(child.Children));

                if (newServiceDetailLevel2.SupportContentTemplates.Count != child.Children.Count)
                {
                    throw new Exception("children mismatch!");
                }

                outputTemplate += newServiceDetailLevel2.GetTemplate() + ",";
            }

            return serviceDetailLevel2Templates;
        }

        private static List<SupportContentTemplate> CreateSupportContent(List<BasePageTemplate> children)
        {
            if (children.Count == 0) return new List<SupportContentTemplate>();
            var supportContent = new List<SupportContentTemplate>();
            foreach (var child in children)
            {
                var newSupportContent = new SupportContentTemplate(child);
                supportContent.Add(newSupportContent);
                outputTemplate += newSupportContent.GetTemplate() + ",";
            }

            return supportContent;
        }

        private static void AddBasePageChildren(List<BasePageTemplate> children)
        {
            if (children.Count == 0) return;

            foreach (var child in children)
            {
                foreach (var basePageTemplate in basePageTemplates)
                {
                    if (child.Slug == basePageTemplate.Parent)
                    {
                        child.Children.Add(basePageTemplate);
                        AllChildren++;
                    }
                }

                AddBasePageChildren(child.Children);
            }
        }

        private static void ProcessInputFolder(string baseDirectoryPath, string outputDirectory)
        {
            var baseDirectory = new DirectoryInfo(baseDirectoryPath);
            ErrorLogging.ErrorFilePath = baseDirectoryPath + "errors.txt";

            var files = baseDirectory.EnumerateFiles("*", SearchOption.AllDirectories);

            //if (test)
            //{
            //    int i = 0;
            //    int folderNum = 1;
            //    foreach (var file in files)
            //    {
            //        if (i == 70)
            //        {
            //            folderNum++;
            //        }

            //        if (i == 140)
            //        {
            //            folderNum++;
            //        }

            //        if (i == 210)
            //        {
            //            folderNum++;
            //        }

            //        var filePath = "C:\\Users\\charleskelly\\Documents\\Inputs\\" + $"Input{folderNum.ToString()}\\" + file.DirectoryName.Substring(file.DirectoryName.IndexOf("S_G") + 3);
            //        Directory.CreateDirectory(filePath);

            //        file.CopyTo(filePath + "\\" + file.Name);

            //        i++;
            //    }
            //}

            foreach (var file in files)
            {
                if (file.Name.EndsWith(".html") && file.Name != "500_404.html" &&
                    file.Name != "PI Practice Case Studies.html" && file.Name != "BEING REUSED_CICA Explained.html" &&
                    file.Name != "64 - LEGAL SEO S + G No Fault Divorce.html")
                {
                    ErrorLogging.CurrentFile = file.Name;
                    LoadFile(file.FullName);
                    MapAndOutputFiles(outputDirectory, missingAltText);
                }
            }

            string missingAltTextOutput = "";

            foreach (var missingAltTextId in missingAltText)
            {
                missingAltTextOutput += missingAltTextId + "\r\n";
            }

            //System.IO.File.WriteAllText(outputDirectory + "missing-alt-text.txt", missingAltTextOutput);
        }

        private static void MapAndOutputFiles(string outputDirectory, List<string> missingAltText)
        {
            var sections = new List<Section>();

            Section section = null;

            var inSection = false;

            foreach (var child in _baseChildren)
            {
                if (Utils.IsSectionHeader(child, _stylesheet))
                {
                    inSection = true;
                    if (section != null)
                    {
                        sections.Add(section);
                    }

                    section = new Section {Header = Utils.TrimUnwantedCharacters(Utils.GetTextFromNode(child))};
                }
                else
                {
                    if (child.Name != "#text" && inSection)
                    {
                        section.Contents.Add(child);
                    }
                }
            }

            if (section != null)
            {
                sections.Add(section);
            }

            var heroHeader = new HeroHeader();
            var pageMeta = new PageMeta();
            var quotes = new Quotes();
            var caseStudies = new CaseStudies();
            var mainContent = new Wysiwyg();
            var twoComponentEnquiry = new TwoComponentEnquiry();
            var sideLinkList = new SideLinkList();
            var video = new Video();
            ImageAndText imageAndText = null;
            ImageAndText imageAndText2 = new ImageAndText();

            foreach (var section1 in sections)
            {
                if (section1.Header == "Hero Header")
                {
                    HeroHeaderMapper mapper = new HeroHeaderMapper();
                    heroHeader = mapper.MapComponent(section1);
                }
                else if (section1.Header == "Page Meta")
                {
                    PageMetaMapper mapper = new PageMetaMapper();
                    pageMeta = mapper.MapComponent(section1);
                }
                else if (section1.Header == "Quotes")
                {
                    QuotesMapper mapper = new QuotesMapper();
                    quotes = mapper.MapComponent(section1);
                }
                else if (section1.Header == "Case Studies")
                {
                    CaseStudiesMapper mapper = new CaseStudiesMapper();
                    caseStudies = mapper.MapComponent(section1);
                }
                else if (section1.Header == "Main Content")
                {
                    WysiwygMapper mapper = new WysiwygMapper();
                    mainContent = mapper.MapWysiwyg(section1, _stylesheet);
                }
                else if (section1.Header == "Two Component Enquiry CTA")
                {
                    TwoComponentEnquiryMapper mapper = new TwoComponentEnquiryMapper();
                    twoComponentEnquiry = mapper.MapComponent(section1, _stylesheet);
                }
                else if (section1.Header == "Side Link List")
                {
                    SideLinkListMapper mapper = new SideLinkListMapper();
                    sideLinkList = mapper.MapComponent(section1, _stylesheet);
                }
                else if (section1.Header == "Video")
                {
                    VideoMapper mapper = new VideoMapper();
                    video = mapper.MapComponent(section1);
                }
                else if (section1.Header == "Image And Text")
                {
                    ImageAndTextMapper mapper = new ImageAndTextMapper();
                    if (imageAndText == null)
                    {
                        imageAndText = mapper.MapComponent(section1);
                    }
                    else
                    {
                        imageAndText2 = mapper.MapComponent(section1);
                    }
                }
            }

            if (imageAndText == null) imageAndText = new ImageAndText();


            ImageMapperCsv imageMapperCsv;
            imageMapperCsv = _imageMapperCsv;

            if (pageMeta.Slug == "personal-injury-claim")
            {
                var test = "sgfdg";
            }

            var records = imageMapperCsv.Records.Where(x =>
                x.Parent == (pageMeta.Parent ?? string.Empty) && x.Slug == (pageMeta.Slug ?? string.Empty));

            ImageMapperCsvRecord record = records != null
                ? records.FirstOrDefault() ?? new ImageMapperCsvRecord("", "", "", "", "", "")
                : new ImageMapperCsvRecord("", "", "", "", "", "");

            var wysiwygToHtmlMapper = new WysiwygToHtmlMapper();

            var mainContentHtml = wysiwygToHtmlMapper.MapToHtml(mainContent);

            var wysiwygToFaqsMapper = new WysiwygToFaqsMapper();

            var faqsComponent = wysiwygToFaqsMapper.MapToFaqs(mainContent);

            var filename = pageMeta.Slug;

            var number = 0;

            while (_processedFileNames.Contains(filename))
            {
                number++;
                filename = pageMeta.Slug + number.ToString();
            }

            _processedFileNames.Add(filename);

            if (!AltText.altText.TryGetValue(record.Image1Name, out var heroAltText))
            {
                heroAltText = "undefined";
                if (!missingAltText.Contains(record.Image1Name)) missingAltText.Add(record.Image1Name);
            }

            if (!AltText.altText.TryGetValue(record.Image2Name, out var imageAndTextAltText))
            {
                imageAndTextAltText = "undefined";
                if (!missingAltText.Contains(record.Image2Name)) missingAltText.Add(record.Image2Name);
            }

            if (!AltText.altText.TryGetValue(record.Image3Name, out var imageAndText2AltText))
            {
                imageAndText2AltText = "undefined";
                if (!missingAltText.Contains(record.Image3Name)) missingAltText.Add(record.Image3Name);
            }


            JsonFileStrings jsonFileStrings = new JsonFileStrings
            {
                Id = filename,
                Name = pageMeta.BreadcumbTitle,
                Slug = pageMeta.Slug,
                _parent = pageMeta.Parent,
                Title = pageMeta.PageTitle,
                Description = pageMeta.MetaDescription,
                FaqsComponent = faqsComponent,
                HeroImage = record.Image1Name,
                HeroImageAltText = heroAltText,
                HeroCopy = heroHeader.StrapLine,
                HeroHeading = heroHeader.Heading,
                HeroSection = heroHeader.SectionName,
                TwoPanelLeftHeading = twoComponentEnquiry.Heading,
                TwoPanelCtaCopy = twoComponentEnquiry.LinkText,
                TwoPanelCtaLink = twoComponentEnquiry.LinkUrl,
                TwoPanelRightHeading = twoComponentEnquiry.RightHeading,
                LeftWysiwygFileName = filename + "-two-panel-cta",
                OnwardJourneyTitle = "",
                _onwardJourneyCtas = imageAndText.Ctas,
                OnwardJourney2Ctas = imageAndText2.Ctas,
                MainContentFileName = filename,
                SideLinkListHeading = sideLinkList.Heading,
                _sideListLinks = sideLinkList.Links,
                QuoteAuthor1 = quotes.CustomerName1,
                QuoteCopy1 = quotes.Quote1,
                QuoteAuthor2 = quotes.CustomerName2,
                QuoteCopy2 = quotes.Quote2,
                QuoteAuthor3 = quotes.CustomerName3,
                QuoteCopy3 = quotes.Quote3,
                ImageTextImageName = record.Image2Name,
                ImageTextAltText = imageAndTextAltText,
                ImageTextCopy = imageAndText.StandardContent,
                ImageTextHeading = imageAndText.Heading,
                ImageTextSection = imageAndText.SectionName,
                ImageText2ImageName = record.Image3Name,
                ImageText2AltText = imageAndText2AltText,
                ImageText2Copy = imageAndText2.StandardContent,
                ImageText2Heading = imageAndText2.Heading,
                ImageText2Section = imageAndText2.SectionName,
                VideoHeading = video.Heading,
                VideoSection = "",
                VideoCopy = video.StandardContent,
                VideoId = video.VideoId,
                CaseStudy1Heading = caseStudies.Heading1,
                CaseStudy1Copy = caseStudies.Copy1,
                CaseStudy1Url = caseStudies.Url1,
                CaseStudy2Heading = caseStudies.Heading2,
                CaseStudy2Copy = caseStudies.Copy2,
                CaseStudy2Url = caseStudies.Url2,
                CaseStudy3Heading = caseStudies.Heading3,
                CaseStudy3Copy = caseStudies.Copy3,
                CaseStudy3Url = caseStudies.Url3,
            };
            var jsonFile = jsonFileStrings.GetJsonFileText();

            var twoPanelWysiwyg = "";

            if (twoComponentEnquiry.LeftCopy != null)
            {
                twoPanelWysiwyg = wysiwygToHtmlMapper.MapToHtml(twoComponentEnquiry.LeftCopy);
            }

            if (pageMeta.Slug == "property-licensing" && pageMeta.Parent == "property-solicitors") return;

            ImageWithCopyTemplate heroTemplate = null;
            if (!string.IsNullOrEmpty(heroHeader.Heading))
            {
                heroTemplate = new ImageWithCopyTemplate
                {
                    SectionHeading = heroHeader.SectionName?.Replace(@"""", @"\""") + "[CMS]",
                    Heading = heroHeader.Heading?.Replace(@"""", @"\""") + "[CMS]",
                    Copy = heroHeader.StrapLine?.Replace(@"""", @"\""") + "[CMS]",
                    ImageId = record.Image1Name?.Replace(@"""", @"\""")
                };
            }

            

            TwoPanelCtaTemplate twoPanelCtaTemplate = null;
            if (!string.IsNullOrEmpty(twoComponentEnquiry.Heading))
            {
                var twoPanelWysiwygTemplate = new WysiwygTemplate { Wysiwyg = twoComponentEnquiry.LeftCopy };
                twoPanelCtaTemplate = new TwoPanelCtaTemplate
                {
                    MainHeading = twoComponentEnquiry.Heading.Replace(@"""", @"\""") + "[CMS]",
                    Copy = twoPanelWysiwygTemplate.GetTemplate(),
                    CtaHeading = twoComponentEnquiry.RightHeading.Replace(@"""", @"\""") + "[CMS]",
                    CtaLinkText = twoComponentEnquiry.LinkText.Replace(@"""", @"\""") + "[CMS]",
                    CtaLinkUrl = twoComponentEnquiry.LinkUrl.Replace(@"""", @"\""")
                };
            }

            ImageWithCopyTemplate imageWithCopy1Template = null;
            if (!string.IsNullOrEmpty(imageAndText.Heading))
            {
                imageWithCopy1Template = new ImageWithCopyTemplate
                {
                    Heading = imageAndText.Heading.Replace(@"""", @"\"""),
                    SectionHeading = imageAndText.SectionName,
                    Copy = imageAndText.StandardContent.Replace(@"""", @"\"""),
                    ImageId = record.Image2Name.Replace(@"""", @"\""")
                };
            }

            var acceptedIconNames = new[]
            {
                "ACCIDENTS IN PUBLIC",
                "ANIMAL",
                "ASBESTOS ROOF",
                "BICYCLE",
                "BREACH OF CONTRACT",
                "BRIEFCASE",
                "BURGER CUP",
                "BURNS,SCARS,LACER",
                "CAR",
                "CARBON MONOX",
                "CHEMICAL",
                "CHILD ABUSE",
                "CONTEST WILL",
                "CONVEYANCING",
                "CRIMINAL INJURY",
                "CRUISE SHIP",
                "DERMATITIS",
                "DISCRIMINATION AGE",
                "DISCRIMINATION DISABILITY",
                "DISCRIMINATION GENDER RECOGNITION",
                "DISCRIMINATION MATERNITY PATERNITY",
                "DISCRIMINATION RACE",
                "DISCRIMINATION RELIGION",
                "DISCRIMINATION SEXUAL ORIENTATION",
                "DIVORCE",
                "ELECTRICAL",
                "ESTATE PLANNING",
                "EXECUTE WILL",
                "FACTORY",
                "FAMILY",
                "FATAL ACCIDENT",
                "FOR SALE",
                "HEAD AND BRAIN",
                "HELMET",
                "HOTEL",
                "HOUSE",
                "INVALID WILLS",
                "JOINT OWNERSHIP",
                "LAND DISPUTE",
                "LANDLORD",
                "LUNG",
                "MAN CROSSING ROAD",
                "MAN CRUTCH",
                "MILITARY",
                "NIHL",
                "ONLINE WILLS",
                "PILLPOT",
                "PLANE",
                "PLUG SOCKET",
                "REDUNDANCY",
                "REPETITIVE STRAIN",
                "SERIOUS INJURY",
                "SHIELD",
                "SOLD",
                "SPANNER SCREWDRIVER",
                "SPINE",
                "SPORT",
                "SUPERMARKET",
                "TRANSFER EQUALITY",
                "TRANSFER EQUITY",
                "TRUSTS",
                "TUPE SOLUTIONS",
                "UNFAIR DISMISSAL",
                "WHIPLASH",
                "WILL STORAGE",
                "WILLS",
                "WORKPLACE HARASSMENT"
            };

            var onwardJourney1Template = new List<OnwardJourneyTemplate>();

            foreach (var imageAndTextCta in imageAndText.Ctas)
            {
                var newIconName = imageAndTextCta.Icon.Replace(@"""", @"\""").Replace("-", " ").ToUpper();

                if (newIconName == "BURNS SCARS LACER")
                {
                    newIconName = "BURNS,SCARS,LACER";
                }
                else if (newIconName == "LUNGS")
                {
                    newIconName = "LUNG";
                }
                else if (newIconName == "PERSON WITH CRUTCH")
                {
                    newIconName = "MAN CRUTCH";
                }
                else if (newIconName == "TRANSFEREQUITY")
                {
                    newIconName = "TRANSFER EQUITY";
                }
                else if (newIconName == "JOINTOWNERSHIP")
                {
                    newIconName = "JOINT OWNERSHIP";
                }
                else if (newIconName == "LANDDISPUTE")
                {
                    newIconName = "LAND DISPUTE";
                }
                else if (newIconName == "EXECUTEWILL")
                {
                    newIconName = "EXECUTE WILL";
                }

                if (!acceptedIconNames.Contains(newIconName) && !invalidIcons.Contains(newIconName))
                {
                    invalidIcons.Add(newIconName);
                    throw new Exception("Invalid icon name: " + newIconName);
                }

                onwardJourney1Template.Add(new OnwardJourneyTemplate
                {
                    Title = imageAndTextCta.Text.Replace(@"""", @"\"""),
                    IconName = newIconName,
                    Url = imageAndTextCta.Url.Replace(@"""", @"\""")
                });
            }

            ImageWithCopyTemplate imageWithCopy2Template = null;
            if (!string.IsNullOrEmpty(imageAndText2.Heading))
            {
                imageWithCopy2Template = new ImageWithCopyTemplate
                {
                    Heading = imageAndText2.Heading.Replace(@"""", @"\"""),
                    SectionHeading = imageAndText2.SectionName,
                    Copy = imageAndText2.StandardContent.Replace(@"""", @"\"""),
                    ImageId = record.Image3Name.Replace(@"""", @"\""")
                };
            }

            var onwardJourney2Template = new List<OnwardJourneyTemplate>();

            foreach (var imageAndTextCta in imageAndText2.Ctas)
            {
                var newIconName = imageAndTextCta.Icon.Replace(@"""", @"\""").Replace("-", " ").ToUpper();

                if (newIconName == "BURNS SCARS LACER")
                {
                    newIconName = "BURNS,SCARS,LACER";
                }

                if (!acceptedIconNames.Contains(newIconName))
                {
                    throw new Exception("Invalid icon name: " + newIconName);
                }

                onwardJourney2Template.Add(new OnwardJourneyTemplate
                {
                    Title = imageAndTextCta.Text.Replace(@"""", @"\"""),
                    IconName = newIconName,
                    Url = imageAndTextCta.Url.Replace(@"""", @"\""")
                });
            }

            var sideLinksTemplate = new List<LinkTemplate>();

            foreach (var sideLink in sideLinkList.Links)
            {
                sideLinksTemplate.Add(new LinkTemplate
                {
                    Url = sideLink.Href.Replace(@"""", @"\"""),
                    Copy = sideLink.Text.Replace(@"""", @"\""")
                });
            }

            VideoTemplate videoTextTemplate = null;
            if (!string.IsNullOrEmpty(video.Heading))
            {
                videoTextTemplate = new VideoTemplate
                {
                    VideoId = video.VideoId.Replace(@"""", @"\"""),
                    Copy = video.StandardContent.Replace(@"""", @"\"""),
                    Heading = video.Heading.Replace(@"""", @"\""")
                };
            }

            var quotesTemplate = new List<QuoteTemplate>();

            if (!string.IsNullOrEmpty(quotes.Quote1))
            {
                quotesTemplate.Add(new QuoteTemplate
                {
                    Copy = quotes.Quote1?.Replace(@"""", @"\"""),
                    Author = quotes.CustomerName1?.Replace(@"""", @"\""")
                });
            }

            if (!string.IsNullOrEmpty(quotes.Quote2))
            {
                quotesTemplate.Add(new QuoteTemplate
                {
                    Copy = quotes.Quote2?.Replace(@"""", @"\"""),
                    Author = quotes.CustomerName2?.Replace(@"""", @"\""")
                });
            }

            if (!string.IsNullOrEmpty(quotes.Quote3))
            {
                quotesTemplate.Add(new QuoteTemplate
                {
                    Copy = quotes.Quote3?.Replace(@"""", @"\"""),
                    Author = quotes.CustomerName3?.Replace(@"""", @"\""")
                });
            }

            var caseStudiesTemplate = new List<CaseStudyTemplate>();

            if (!string.IsNullOrEmpty(caseStudies.Copy1))
            {
                caseStudiesTemplate.Add(new CaseStudyTemplate
                {
                    Copy = caseStudies.Copy1?.Replace(@"""", @"\"""),
                    Url = caseStudies.Url1?.Replace(@"""", @"\"""),
                    Heading = caseStudies.Heading1?.Replace(@"""", @"\""")
                });
            }

            if (!string.IsNullOrEmpty(caseStudies.Copy2))
            {
                caseStudiesTemplate.Add(new CaseStudyTemplate
                {
                    Copy = caseStudies.Copy2?.Replace(@"""", @"\"""),
                    Url = caseStudies.Url2?.Replace(@"""", @"\"""),
                    Heading = caseStudies.Heading2?.Replace(@"""", @"\""")
                });
            }

            if (!string.IsNullOrEmpty(caseStudies.Copy3))
            {
                caseStudiesTemplate.Add(new CaseStudyTemplate
                {
                    Copy = caseStudies.Copy3?.Replace(@"""", @"\"""),
                    Url = caseStudies.Url3?.Replace(@"""", @"\"""),
                    Heading = caseStudies.Heading3?.Replace(@"""", @"\""")
                });
            }

            var wysiwygTemplate = new WysiwygTemplate {Wysiwyg = mainContent};


            var basePageTemplate = new BasePageTemplate
            {
                Title = pageMeta.PageTitle?.Replace(@"""", @"\"""),
                Name = pageMeta.BreadcumbTitle?.Replace(@"""", @"\"""),
                Slug = pageMeta.Slug?.Replace(@"""", @"\"""),
                Parent = pageMeta.Parent?.Replace(@"""", @"\"""),
                PageType = record.PageType,
                Description = pageMeta.MetaDescription?.Replace(@"""", @"\"""),
                Hero = heroTemplate,
                TwoPanelCta = twoPanelCtaTemplate,
                ImageWithCopy1 = imageWithCopy1Template,
                OnwardJourneys1 = onwardJourney1Template,
                ImageWithCopy2 = imageWithCopy2Template,
                OnwardJourneys2 = onwardJourney2Template,
                Content = wysiwygTemplate.GetTemplate(),
                SideLinkHeading = sideLinkList.Heading?.Replace(@"""", @"\"""),
                SideLinks = sideLinksTemplate,
                VideoText = videoTextTemplate,
                Quotes = quotesTemplate,
                CaseStudies = caseStudiesTemplate
            };

            basePageTemplates.Add(basePageTemplate);

            //var serviceOverview = new ServiceOverviewTemplate(basePageTemplate);
            //var serviceOverviewString = serviceOverview.GetTemplate();

            //var serviceDetailLevel1 = new ServiceDetailLevel1Template(basePageTemplate);
            //var serviceDetailLevel1String = serviceDetailLevel1.GetTemplate();

            //var serviceDetailLevel2 = new ServiceDetailLevel2Template(basePageTemplate);
            //var serviceDetailLevel2String = serviceDetailLevel2.GetTemplate();

            //var supportContent = new SupportContentTemplate(basePageTemplate);
            //var supportContentString = supportContent.GetTemplate();

            System.IO.File.WriteAllText(outputDirectory + $"{filename}.hbs", mainContentHtml);
            System.IO.File.WriteAllText(outputDirectory + $"{filename}.js", jsonFile);
            System.IO.File.WriteAllText(outputDirectory + $"{filename}-two-panel-cta.hbs", twoPanelWysiwyg);
        }

        private static void LoadFile(string filePath)
        {
            _currentLoadedFilePath = filePath;

            var fileText = System.IO.File.ReadAllText(
                filePath);
            _htmlDocument = new HtmlDocument();

            _htmlDocument.LoadHtml(fileText);

            var baseNode = Utils.GetBody(_htmlDocument);

            PreProcessHtml.ConvertBrToParagraph(baseNode);

            var stylesNode = Utils.GetStyles(_htmlDocument);

            var stylesheetParser = new StylesheetParser();

            _stylesheet = stylesheetParser.Parse(stylesNode);

            _baseChildren = baseNode.ChildNodes.ToArray();
        }
    }
}