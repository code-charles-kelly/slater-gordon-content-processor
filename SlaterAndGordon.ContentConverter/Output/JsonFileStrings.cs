﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text.Json;
using Newtonsoft.Json;
using SlaterAndGordon.ContentConverter.Components;
using SlaterAndGordon.ContentConverter.Sections;

namespace SlaterAndGordon.ContentConverter.Output
{
    public class JsonFileStrings
    {
        public string CaseStudy1Heading { get; set; }
        public string CaseStudy1Copy { get; set; }
        public string Id { get; set; }
        public string Slug { get; set; }
        public FaqsComponent FaqsComponent { get; set; } = null;
        public string QuoteAuthor3 { get; set; }
        public string QuoteCopy3 { get; set; }
        public string QuoteAuthor2 { get; set; }
        public string QuoteCopy2 { get; set; }
        public string QuoteAuthor1 { get; set; }
        public string QuoteCopy1 { get; set; }
        public string SideLinkListHeading { get; set; }
        public List<AElement> _sideListLinks { get; set; }
        public string MainContentFileName { get; set; }
        public string OnwardJourneyTitle { get; set; }
        public List<ImageAndTextCta> _onwardJourneyCtas { get; set; }
        public string TwoPanelCtaLink { get; set; }
        public string TwoPanelCtaCopy { get; set; }
        public string TwoPanelRightHeading { get; set; }
        public string LeftWysiwygFileName { get; set; }
        public string TwoPanelLeftHeading { get; set; }
        public string HeroCopy { get; set; }
        public string HeroHeading { get; set; }
        public string HeroSection { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string _parent { get; set; }
        public string VideoId { get; set; }
        public string VideoCopy { get; set; }
        public string VideoHeading { get; set; }
        public string VideoSection { get; set; }
        public string ImageTextCopy { get; set; }
        public string ImageTextHeading { get; set; }
        public string ImageTextSection { get; set; }
        public string ImageText2Copy { get; set; }
        public string ImageText2Heading { get; set; }
        public string ImageText2Section { get; set; }
        public object OnwardJourney2Title { get; set; }
        public List<ImageAndTextCta> OnwardJourney2Ctas { get; set; }
        public string CaseStudy1Url { get; set; }
        public string CaseStudy2Heading { get; set; }
        public string CaseStudy2Copy { get; set; }
        public string CaseStudy2Url { get; set; }
        public string CaseStudy3Heading { get; set; }
        public string CaseStudy3Copy { get; set; }
        public string CaseStudy3Url { get; set; }
        public string HeroImage { get; set; }
        public string ImageTextImageName { get; set; }
        public string ImageText2ImageName { get; set; }
        public string HeroImageAltText { get; set; }
        public string ImageText2AltText { get; set; }
        public string ImageTextAltText { get; set; }

        public string GetJsonFileText()
        {
            return
        "module.exports = function (contentShared, defaultContent, pageTypes) {\r\n" +
        "	return {\r\n" +
        $"		id: `{Id}`,\r\n" +
        $"		slug: `{Slug}`,\r\n" +
        $"		parent: {Parent()},\r\n" +
        $"		name: `{Name}`,\r\n" +
        "		pageType: pageTypes.productTemplate,\r\n" +
        "		data: {\r\n" +
        "			...defaultContent,\r\n" +
        GetMetaData() +
        "			heroHeader: {\r\n" +
        $"				section: `{HeroSection}`,\r\n" +
        $"				heading: `{HeroHeading}`,\r\n" +
        $"				copy: `{HeroCopy}`,\r\n" +
        "				image: {\r\n" +
        "					blob: true,\r\n" +
        //"					large: `/public/images/bitmap/test-image-large.png`,\r\n" +
        $"					large: `/public/images/bitmap/large/{HeroImage}.jpg`,\r\n" +
        $"					medium: `/public/images/bitmap/medium/{HeroImage}.jpg`,\r\n" +
        $"					small: `/public/images/bitmap/small/{HeroImage}.jpg`,\r\n" +
        $"					default: `/public/images/bitmap/medium/{HeroImage}.jpg`,\r\n" +
        $"					altText: `{HeroImageAltText}`\r\n" +
        "				}\r\n" +
        "			},\r\n" +
        TwoPanelData() +
        OnwardJourneyData() +
        OnwardJourney2Data() +
        $"			standardContent: `product-template/{MainContentFileName}`,\r\n" +
        "			sideLinkList: {\r\n" +
        $"				heading: `{SideLinkListHeading}`,\r\n" +
        "				links: [\r\n" +
        SideListLinks() +
        "				]\r\n" +
        "			},\r\n" +
        "			quotes: [\r\n" +
        "				{" +
        $"					copy: `{QuoteCopy1}`,\r\n" +
        $"					author: `{QuoteAuthor1}`\r\n" +
        "				},\r\n" +
        "				{\r\n" +
        $"					copy: `{QuoteCopy2}`,\r\n" +
        $"					author: `{QuoteAuthor2}`\r\n" +
        "				},\r\n" +
        "				{\r\n" +
        $"					copy: `{QuoteCopy3}`,\r\n" +
        $"					author: `{QuoteAuthor3}`\r\n" +
        "				}\r\n" +
        "			],\r\n" +
        CaseStudies() +
        ImageAndTextData() +
        ImageAndText2Data() +
        VideoData() +
        "		}\r\n" +
        "	}\r\n" +
        "}";
        }

        private string GetMetaData()
        {
            var metaStart =
                "			meta: {\r\n" +
                "				...contentShared.meta,\r\n" +
                $"				title: `{Title}`,\r\n" +
                $"				description: `{Description}`,\r\n" +
                "				robots: ``";

            var metaMiddle = string.Empty;

            if (FaqsComponent != null && FaqsComponent.Faqs.Count > 0)
            {
                metaMiddle += ",\r\n";
                metaMiddle += "                schema: [\r\n";

                foreach (var faq in FaqsComponent.Faqs)
                {
                    var faqAnswer = JsonConvert.SerializeObject(faq.Answer).Replace("$", "\\$").Replace("`","\\`")
                        .Replace("{{{meta.phonenumber.contentLink}}}", "${contentShared.meta.phonenumber.number}");

                    faqAnswer = "`" + faqAnswer.Substring(1, faqAnswer.Length - 2) + "`";

                    metaMiddle += "                  {\r\n" +
                                  $"                    question: {JsonConvert.SerializeObject(faq.Question)},\r\n" +
                                  $"                    answer: {faqAnswer}\r\n" +
                                  "                  },\r\n";
                }

                metaMiddle = metaMiddle.Remove(metaMiddle.LastIndexOf(","), 1);

                metaMiddle += "                ]\r\n";
            }
            else
            {
                metaMiddle += "\r\n";
            }

            var metaEnd = "			},\r\n";

            return metaStart + metaMiddle + metaEnd;
        }

        private string CaseStudies()
        {
            

            var caseStudiesJson = "			caseStudies: [\r\n";

            var caseStudyList = "";

            if (!string.IsNullOrEmpty(CaseStudy1Heading) && !string.IsNullOrEmpty(CaseStudy1Copy) &&
                !string.IsNullOrEmpty(CaseStudy1Url))
            {
                caseStudyList += "				{\r\n" +
                                   $"					heading: `{CaseStudy1Heading}`,\r\n" +
                                   $"					link: `{CaseStudy1Url}`,\r\n" +
                                   $"					copy: `{CaseStudy1Copy}`\r\n" +
                                   "				},\r\n";
            }

            if (!string.IsNullOrEmpty(CaseStudy2Heading) && !string.IsNullOrEmpty(CaseStudy2Copy) &&
                !string.IsNullOrEmpty(CaseStudy2Url))
            {
                caseStudyList += "				{\r\n" +
                                   $"					heading: `{CaseStudy2Heading}`,\r\n" +
                                   $"					link: `{CaseStudy2Url}`,\r\n" +
                                   $"					copy: `{CaseStudy2Copy}`\r\n" +
                                   "				},\r\n";
            }

            if (!string.IsNullOrEmpty(CaseStudy3Heading) && !string.IsNullOrEmpty(CaseStudy3Copy) &&
                !string.IsNullOrEmpty(CaseStudy3Url))
            {
                caseStudyList += "				{\r\n" +
                                   $"					heading: `{CaseStudy3Heading}`,\r\n" +
                                   $"					link: `{CaseStudy3Url}`,\r\n" +
                                   $"					copy: `{CaseStudy3Copy}`\r\n" +
                                   "				},\r\n";
            }

            if (caseStudyList.Contains(","))
            {
                caseStudyList = caseStudyList.Remove(caseStudyList.LastIndexOf(","), 1);
            }

            if (caseStudyList == "") return "";

            caseStudiesJson += caseStudyList + "			],\r\n";

            return caseStudiesJson;
        }

        private string TwoPanelData()
        {
            if (!string.IsNullOrEmpty(TwoPanelLeftHeading))
            {

                return "			twoPanelCta: {\r\n" +
                       $"				leftHeading: `{TwoPanelLeftHeading}`,\r\n" +
                       $"				leftWysiwyg: `product-template/{LeftWysiwygFileName}`,\r\n" +
                       $"				rightHeading: `{TwoPanelRightHeading}`,\r\n" +
                       $"				rightCtaCopy: `{TwoPanelCtaCopy}`,\r\n" +
                       $"				rightCtaLink: `{TwoPanelCtaLink}`,\r\n" +
                       "			},\r\n";
            }
            else
            {
                return "";
            }
        }

        private string OnwardJourneyData()
        {
            if (!string.IsNullOrEmpty(ImageTextHeading))
            {

                return "			onwardJourneys: {\r\n" +
                       $"				title: `{OnwardJourneyTitle}`,\r\n" +
                       "				ctas: [\r\n" +
                       OnwardJourneyCtas(_onwardJourneyCtas) +
                       "				]\r\n" +
                       "			},\r\n";
            }
            else
            {
                return "";
            }
        }

        private string OnwardJourney2Data()
        {
            if (!string.IsNullOrEmpty(ImageText2Heading))
            {

                return "			onwardJourneysB: {\r\n" +
                       $"				title: `{OnwardJourney2Title}`,\r\n" +
                       "				ctas: [\r\n" +
                       OnwardJourneyCtas(OnwardJourney2Ctas) +
                       "				]\r\n" +
                       "			},\r\n";
            }
            else
            {
                return "";
            }
        }

        

        private string VideoData()
        {
            if (!string.IsNullOrEmpty(VideoId))
            {

                return "			videoTextB: {\r\n" +
                       $"				section: `{VideoSection}`,\r\n" +
                       $"				heading: `{VideoHeading}`,\r\n" +
                       $"				copy: `{VideoCopy}`,\r\n" +
                       "				image: false,\r\n" +
                       $"				video: `{VideoId}`\r\n" +
                       "			}\r\n";
            }
            else
            {
                return "";
            }
        }

        private string ImageAndTextData()
        {
            if (!string.IsNullOrEmpty(ImageTextHeading))
            {

                return "			imageTextA: {\r\n" +
                       $"				section: `{ImageTextSection}`,\r\n" +
                       $"				heading: `{ImageTextHeading}`,\r\n" +
                       $"				copy: `{ImageTextCopy}`,\r\n" +
                       "				image: {\r\n" +
                       "					blob: true,\r\n" +
                       $"					large: `/public/images/bitmap/large/{ImageTextImageName}.jpg`,\r\n" +
                       $"					medium: `/public/images/bitmap/medium/{ImageTextImageName}.jpg`,\r\n" +
                       $"					small: `/public/images/bitmap/small/{ImageTextImageName}.jpg`,\r\n" +
                       $"					default: `/public/images/bitmap/medium/{ImageTextImageName}.jpg`,\r\n" +
                       $"					altText: `{ImageTextAltText}`\r\n" +
                       "				},\r\n" +
                       "				video: false\r\n" +
                       "			},\r\n";
            }
            else
            {
                return "";
            }
        }

        private string ImageAndText2Data()
        {
            if (!string.IsNullOrEmpty(ImageText2Heading))
            {

                return "			imageTextB: {\r\n" +
                       $"				section: `{ImageText2Section}`,\r\n" +
                       $"				heading: `{ImageText2Heading}`,\r\n" +
                       $"				copy: `{ImageText2Copy}`,\r\n" +
                       "				image: {\r\n" +
                       "					blob: true,\r\n" +
                       $"					large: `/public/images/bitmap/large/{ImageText2ImageName}.jpg`,\r\n" +
                       $"					medium: `/public/images/bitmap/medium/{ImageText2ImageName}.jpg`,\r\n" +
                       $"					small: `/public/images/bitmap/small/{ImageText2ImageName}.jpg`,\r\n" +
                       $"					default: `/public/images/bitmap/medium/{ImageText2ImageName}.jpg`,\r\n" +
                       $"					altText: `{ImageText2AltText}`\r\n" +
                       "				},\r\n" +
                       "				video: false\r\n" +
                       "			},\r\n";
            }
            else
            {
                return "";
            }
        }

        private string OnwardJourneyCtas(List<ImageAndTextCta> imageAndTextCtas)
        {
            if (imageAndTextCtas.Count == 0) return "";

            var ctaJson = "";

            foreach (var imageAndTextCta in imageAndTextCtas)
            {
                ctaJson +=
                    "					{\r\n" +
                    $"						title: `{imageAndTextCta.Text}`,\r\n" +
                    $"						url: `{imageAndTextCta.Url}`,\r\n" +
                    $"						icon: `{imageAndTextCta.Icon}`\r\n" +
                    "					},\r\n";
            }

            ctaJson = ctaJson.Remove(ctaJson.LastIndexOf(","), 1);

            return ctaJson;
        }

        private string SideListLinks()
        {
            if (_sideListLinks.Count == 0) return "";
            var linksJson = "";

            foreach (var sideListLink in _sideListLinks)
            {
                linksJson +=
                    "					{\r\n" +
                    $"						url: `{sideListLink.Href}`,\r\n" +
                    $"						copy: `{sideListLink.Text}`\r\n" +
                    "					},\r\n";
            }

            linksJson = linksJson.Remove(linksJson.LastIndexOf(","), 1);

            return linksJson;
        }

        private string Parent()
        {
            if (string.IsNullOrEmpty(_parent))
                return "false";
            else
            {
                return $"`{_parent}`";
            }
        }
    }
}
