﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SlaterAndGordon.ContentConverter.Components
{
    public class FaqsComponent
    {
        public List<Faq> Faqs { get; set; } = new List<Faq>();
    }

    public class Faq
    {
        public string Question { get; set; }
        public string Answer { get; set; }
    }
}
